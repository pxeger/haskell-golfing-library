module Test.Util
  ( shouldAllBe
  , shouldAllSatisfy
  , shouldHaveAtLeast
  , shouldHaveAtMost
  , AtLeast2 (..)
  )
  where


import Test.Hspec
import Test.QuickCheck


import Control.Monad


import P.Foldable.Length


infix 1
    `shouldAllBe`
  , `shouldAllSatisfy`
  , `shouldHaveAtLeast`
  , `shouldHaveAtMost`


newtype AtLeast2 a =
  AtLeast2
    { getAtLeast2 ::
      a
    }
  deriving
    ( Show
    , Read
    , Eq
    , Ord
    )


instance
  (
  )
    => Functor AtLeast2
  where
    fmap f =
      AtLeast2 . f . getAtLeast2


instance
  ( Arbitrary a
  , Num a
  , Ord a
  )
    => Arbitrary (AtLeast2 a)
  where
    arbitrary =
      AtLeast2 <$> (fmap abs arbitrary `suchThat` (> 1))
    shrink (AtLeast2 x) =
      [ AtLeast2 x'
      | x' <- shrink x
      , x' > 1
      ]


expectTrue ::
  ( HasCallStack
  )
    => String -> Bool -> Expectation
expectTrue msg b =
  unless b $ expectationFailure msg


-- | Checks that all elements of a foldable are a certain value.
shouldAllBe ::
  ( HasCallStack
  , Show a
  , Eq a
  , Foldable t
  )
    => t a -> a -> Expectation
shouldAllBe xs x =
  mapM_ (`shouldBe` x) xs


-- | Checs that all elements of a foldable satisfy a predicate
shouldAllSatisfy ::
  ( HasCallStack
  , Foldable t
  , Show a
  )
    => t a -> (a -> Bool) -> Expectation
shouldAllSatisfy xs p =
  mapM_ (`shouldSatisfy` p) xs


-- | Checks that the input is at least a certain size.
shouldHaveAtLeast ::
  ( HasCallStack
  , Show (t a)
  , Foldable t
  )
    => t a -> Int -> Expectation
shouldHaveAtLeast xs size =
  expectTrue
    ("length smaller than " ++ show size ++ " on:" ++ show xs)
    (meL size xs)


-- | Checks that the input is at most a certain size.
shouldHaveAtMost ::
  ( HasCallStack
  , Show (t a)
  , Foldable t
  )
    => t a -> Int -> Expectation
shouldHaveAtMost xs size =
  expectTrue
    ("length larger than " ++ show size ++ " on:" ++ show xs)
    (leL size xs)
