{-# Language ScopedTypeVariables #-}
module P.Algebra.Monoid.Free.SplitSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Algebra.Monoid.Free.Split
import P.Aliases
import P.Arithmetic.Nat
import qualified P.Arithmetic.NatSpec as Nat


spec :: Spec
spec = do
  describe "cxS" $ do
    context "on lists" $ do
      it "is undone by concat" $ property $
        \ (Positive (y :: Int)) (x :: List Int) ->
          concat (cxS y x) `shouldBe` x
      it "doesn't produce parts longer than the chunk size" $ property $
        \ (Positive (y :: Int)) (x :: List Int) ->
          cxS y x `shouldSatisfy` all ((<= y) . length)
    context "on nats" $ do
      it "is undone by sum" $ property $
        \ (Positive (y :: Int)) (x :: Nat) ->
          sum (cxS y x) `shouldBe` x
      it "doesn't produce parts larger than the chunk size" $ property $
        \ (Positive (y :: Int)) (x :: Nat) ->
          cxS y x `shouldSatisfy` all ((<= y) . fromInteger . toInteger)
