{-# Language ScopedTypeVariables #-}
module P.Algebra.PolynomialSpec
  ( spec
  )
  where


import Data.Functor


import Test.Hspec
import Test.QuickCheck


import P.Algebra.Polynomial
import P.Algebra.Monoid.Action
  ( aq
  )


instance
  ( Arbitrary a
  )
    => Arbitrary (Polynomial a)
  where
    arbitrary =
      fmap Y $ choose (0, 5) >>= vector

    shrink (Y x) =
      fmap Y (shrink x)


spec :: Spec
spec = do
  describe "aq" $ do
    it "is linear w.r.t. (+)" $ property $
      \ (x :: Polynomial Integer) y (z :: Integer) ->
        aq (x + y) z `shouldBe` aq x z + aq y z
    it "is linear w.r.t. (*)" $ property $
      \ (x :: Polynomial Integer) y (z :: Integer) ->
        aq (x * y) z `shouldBe` aq x z * aq y z
    it "is constant on degree 0 polynomials" $ property $
      \ (x :: Integer) y ->
        aq (Y [x]) y `shouldBe` x
    it "is the identity on the identiy polynomial" $ property $
      \ (x :: Integer) ->
        aq (Y [0, 1]) x `shouldBe` x
    it "is a functor from (<>) to (<)" $ property $
      \ (f :: Polynomial Integer) g x ->
        aq (f <> g) x `shouldBe` aq f (aq g x)
  describe "(+)" $ do
    it "left distributes w.r.t. (*)" $ property $
      \ (x :: Polynomial Integer) y z ->
        x * (y + z) `shouldBe` (x * y) + (x * z)
    it "right distributes w.r.t. (*)" $ property $
      \ (x :: Polynomial Integer) y z ->
        (y + z) * x `shouldBe` (y * x) + (z * x)
    it "commutes" $ property $
      \ (x :: Polynomial Integer) y ->
        x + y `shouldBe` y + x
  describe "(<>)" $ do
    it "monomials exponentiate constants" $ property $
      \ (y :: Integer) ->
        forAll (arbitrary `suchThat` (>= 0)) $
          \ (x :: Integer) ->
            Y ((0 <$ [1..x]) ++ [1]) <> Y [y] `shouldBe` Y [y ^ x]
    it "monomials exponentiate polynomials" $ property $
      \ (y :: Polynomial Integer) ->
        forAll (arbitrary `suchThat` (>= 0)) $
          \ (x :: Integer) ->
            Y ((0 <$ [1..x]) ++ [1]) <> y `shouldBe` y ^ x
    it "is associative" $ property $
      \ (x :: Polynomial Integer) y z ->
        (x <> y) <> z `shouldBe` x <> (y <> z)
    it "has an identity" $ property $
      \ (x :: Polynomial Integer) ->
        (Y [0, 1]) <> x `shouldBe` x
