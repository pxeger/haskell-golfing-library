{-# Options_GHC -Wno-deprecations #-}
module P.Foldable.LengthSpec
  ( spec
  )
  where


import Prelude
 ( (+)
 , Int
 , error
 )


import Test.Hspec
import Test.QuickCheck


import P.Arithmetic.Nat
import P.Bifunctor.Either
import P.Category
import P.Foldable.Length


spec :: Spec
spec = do
  describe "lnt" $ do
    it "gives zero for the length of the empty list" $
      lnt [] `shouldBe` Zero
    it "adding an element to the front of the list produces a list one longer" $ property $
      \ list ->
        lnt (() : list) `shouldBe` Succ (lnt list)
  describe "lg" $ do
    it "gives zero for the length of the empty list" $
      lg [] `shouldBe` 0
    it "adding an element to the front of the list produces a list one longer" $ property $
      \ list ->
        lg (() : list) `shouldBe` 1 + lg list
  describe "ø" $ do
    it "gives true for the empty list" $
      ([] :: [Int]) `shouldSatisfy` ø
    it "doesn't evaluate excessive terms" $
      () : [error "evaluated excessive terms"] `shouldNotSatisfy` ø
    it "considers left to be null" $
      (Lef () :: Either () ()) `shouldSatisfy` ø
    it "considers right to not be null" $
      (Rit () :: Either () ()) `shouldNotSatisfy` ø
