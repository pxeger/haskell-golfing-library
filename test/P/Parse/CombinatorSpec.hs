{-# Options_GHC -Wno-deprecations #-}
{-# Language ScopedTypeVariables #-}
module P.Parse.CombinatorSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck
import Test.Util


import P.Parse
import P.Parse.Combinator
import P.Swap


spec :: Spec
spec = do
  describe "ʃ" $ do
    it "always parses the input string exactly" $ property $
      \ (s :: String) ->
        s `shouldSatisfy` x1 (ʃ s)
    it "splits its string off the front" $ property $
      \ (s1 :: String) s2 ->
        uP (ʃ s1) (s1 <> s2) `shouldBe` [(s2, s1)]
  describe "lA" $ do
    it "never consumes input" $ property $
      \ (s1 :: String) s2 ->
        gP (Sw $ lA $ ʃ $ s1) s2 `shouldAllBe` s2
    it "succeeds when the parser suceeds" $ property $
      \ (s1 :: String) s2 ->
        let
          parser =
            ʃ $ s1
        in
          gP (lA parser) s2 `shouldBe` gP parser s2
  describe "gre" $ do
    it "never has more than one parse" $ property $
      \ (s1 :: String) ->
        uP (gre h') s1 `shouldHaveAtMost` 1
  describe "laz" $ do
    it "never has more than one parse" $ property $
      \ (s1 :: String) ->
        uP (laz h') s1 `shouldHaveAtMost` 1
  describe "yju" $ do
    it "is idempotent" $ property $
      \ (s1 :: String) s2 ->
        let
          parser =
            ʃ $ s1
        in
          uP (parser @<* parser) s2 `shouldBe` uP parser s2
  describe "ymu" $ do
    it "is idempotent" $ property $
      \ (s1 :: String) s2 ->
        let
          parser =
            ʃ $ s1
        in
          uP (parser @^* parser) s2 `shouldBe` uP parser s2
  describe "hd" $ do
    it "has no parse on empty input" $
      "" `shouldNotSatisfy` pP hd
    it "should parse the first character of non-empty input" $ property $
      forAll (listOf1 arbitrary) $
        \ (s@(x : xs) :: String) ->
          uP hd s `shouldBe` [(xs, x)]
  describe "hdS" $ do
    it "has no parse on empty input" $
      "" `shouldNotSatisfy` pP hdS
    it "should parse the first character of non-empty input" $ property $
      forAll (listOf1 arbitrary) $
        \ (s@(x : xs) :: String) ->
          uP hdS s `shouldBe` [(xs, [x])]
  describe "bhd" $ do
    it "has no parse on empty input" $
      "" `shouldNotSatisfy` pP bhd
    it "should parse the first character of non-empty input" $ property $
      forAll (listOf1 arbitrary) $
        \ (s :: String) ->
          uP bhd s `shouldBe` [(init s, last s)]
  describe "en" $ do
    it "matches empty input" $
      "" `shouldSatisfy` pP en
    it "has no parse on nonempty input" $ property $
      forAll (listOf1 arbitrary) $
        \ (s :: String) ->
          s `shouldNotSatisfy` pP en
