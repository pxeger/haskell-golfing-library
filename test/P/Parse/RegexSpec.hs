{-# Options_GHC -Wno-deprecations #-}
{-# Language FlexibleContexts #-}
module P.Parse.RegexSpec
  ( spec
  )
  where


import Data.Char
import Test.Hspec
import Test.QuickCheck


import P.Parse
import P.Parse.Regex


nonSpecial :: Gen Char
nonSpecial =
  elements $
    concat $
      [ ['A'..'Z']
      , ['a'..'z']
      , ['0'..'9']
      ]


testExpression :: String -> [String] -> [String] -> Spec
testExpression expression matches nonMatches =
  describe expression $ do
    mapM_
      ( \ match ->
        it ("matches '" ++ match ++ "'") $
          match `shouldSatisfy` (pP $ rX expression)
      )
      matches
    mapM_
      ( \ nonMatch ->
        it ("does not match '" ++ nonMatch ++ "'") $
          nonMatch `shouldNotSatisfy` (pP $ rX expression)
      )
      nonMatches


spec :: Spec
spec = do
  -- Test all the examples given
  context "Specific tests" $ do
    testExpression "c[ao]t"
      [ "cat"
      , "cot"
      ]
      [ "cut"
      ]
    testExpression "c[^ao]t"
      [ "cut"
      ]
      [ "cat"
      , "cot"
      ]
    testExpression "c^ut"
      [ "cat"
      , "cot"
      ]
      [ "cut"
      ]
    testExpression "c.t"
      [ "cat"
      , "cot"
      , "cut"
      ]
      [
      ]
    testExpression "[ab]{5}"
      [ "abbba"
      , "bbbaaaaa"
      ]
      [ "bbb"
      ]
    testExpression "[ab]{2-5}"
      [ "abbba"
      , "bbbaaaaa"
      , "bbb"
      , "baa"
      ]
      [ "b"
      ]
    testExpression "[ab]{-5}"
      [ "abbba"
      , "bbbaaaaa"
      , "bbb"
      , "baa"
      ]
      [
      ]
    testExpression "[ab]{4-}"
      [ "abbba"
      , "bbbaaaaa"
      ]
      [ "baa"
      , "bbb"
      ]
    testExpression "[ab]{3,7}"
      [ "babababb"
      , "babb"
      ]
      [
      ]
    testExpression ".+r$"
      [ "sonar"
      ]
      [ "art"
      , "boron"
      ]
    testExpression "[ab][ac]?"
      [ "bc"
      , "aa"
      ]
      [ ""
      ]
    testExpression "([ab][ac])?"
      [ "bc"
      , "aa"
      , ""
      ]
      [
      ]
    testExpression "[bo]+$|[pa]+$"
      [ "bob"
      , "papa"
      ]
      [ "pabo"
      , "boap"
      ]
    testExpression "(bo|pa)+$"
      [ "pabo"
      , "papa"
      ]
      [ "bob"
      , "boap"
      ]
    testExpression "(a{3-})@"
      [ "aaa"
      , "aaaa"
      ]
      [ "aa"
      ]
    testExpression "((.[^a])+$)@(..b)+$"
      [ "anbulb"
      ]
      [ "anbalb"
      , "anclub"
      ]
    testExpression "(cab)![abc]+$"
      [ "baba"
      , "cacb"
      , "acab"
      ]
      [ "cab"
      , "caba"
      ]
    testExpression "(g/0?o)/!"
      [ "go!"
      , "ggoo!"
      , "gggooo!"
      , "ggggoooo!"
      ]
      [ "ggoooo!"
      , "gggoo!"
      , "ggoo"
      , "aabb!"
      , "!"
      ]
    testExpression "(((((((((((g/10?o)))))))))))/!"
      [ "go!"
      , "ggoo!"
      , "gggooo!"
      , "ggggoooo!"
      ]
      [ "ggoooo!"
      , "gggoo!"
      , "ggoo"
      , "aabb!"
      , "!"
      ]
    testExpression "((a/0?b)c)@a+(b/0?c)$"
      [ "aabbcc"
      , "abc"
      ]
      [ "aabbccc"
      , "aabbc"
      , "aabcc"
      , ""
      ]

  describe "rX" $ do
    it "matches a string when no special characters are present" $
      forAll (listOf nonSpecial) $
        \ expression ->
          \ test -> do
            (expression, test) `shouldSatisfy` (== (expression == test)) . uncurry (x1 . rX)
            (expression, expression) `shouldSatisfy` uncurry (x1 . rX)

    it "matches a character with a character class containing it" $
      forAll (listOf1 nonSpecial) $
        \ charClass ->
          forAll (elements charClass) $
            \ test ->
              [test] `shouldSatisfy` (cP $ rX $ '[' : charClass ++ "]")

    it "doesn't match a character with a negative character class containing it" $
      forAll (listOf1 nonSpecial) $
        \ charClass ->
          forAll (elements charClass) $
            \ test ->
              [test] `shouldNotSatisfy` (pP $ rX $ "[^" ++ charClass ++ "]")

    it "doesn't match a character with a negative character class shorthand of it" $
      forAll nonSpecial $
        \ charClass ->
          forAll nonSpecial $
            \ test ->
              [test] `shouldSatisfy` (== (test /= charClass)) . (x1 $ rX $ ['^', charClass])

    it "matches a string with no special characters and a simple multiplexer to that string that many times (n < 10)" $
      forAll (elements $ zip [0..] "0123456789") $
        \ (times, hexadecimal) ->
          forAll (listOf1 nonSpecial) $
            \ baseString -> do
              ([1..times] >> baseString) `shouldSatisfy` (x1 $ rX $ '(' : baseString ++ [')','{',hexadecimal,'}'])

    it "matches a string with no special characters and a simple multiplexer to that string that many times (9 < n < 16)" $
      forAll (elements $ zip [10..] "ABCDEF") $
        \ (times, hexadecimal) ->
          forAll (listOf1 nonSpecial) $
            \ baseString -> do
              ([1..times] >> baseString) `shouldSatisfy` (x1 $ rX $ '(' : baseString ++ [')','{',hexadecimal,'}'])

    it "matches only strings shorter than or equal to n to the regex .{-n}" $
      forAll (elements $ zip [0..] "0123456789ABCDEF") $
        \ (times, hexadecimal) test ->
          gP (rX $ ".{-" ++ hexadecimal : "}") test `shouldSatisfy` (all $ (<= times) .length)

    it "matches only strings longer than or equal to n to the regex .{n-}" $
      forAll (elements $ zip [0..] "0123456789ABCDEF") $
        \ (times, hexadecimal) test ->
          gP (rX $ ".{" ++ hexadecimal : "-}") test `shouldSatisfy` (all $ (>= times) .length)

    describe ".{n-}" $
      it "matches 1 + min n m results on strings of size m" $
        forAll (elements $ zip [0..] "0123456789ABCDEF") $
          \ (times, hexadecimal) test ->
            length (gP (rX $ ".{-" ++ hexadecimal : "}") test) `shouldBe` 1 + min times (length test)

    describe ".{n-}" $
      it "matches 1 + m - n results on strings of size m" $
        forAll (elements $ zip [0..] "0123456789ABCDEF") $
          \ (times, hexadecimal) test ->
            length (gP (rX $ ".{" ++ hexadecimal : "-}") test) `shouldBe` max 0 (1 + length test - times)

    it "'$' on the end of a string with no special characters matches only that string completely" $
      forAll (listOf nonSpecial) $
        \ expression ->
          \ test -> do
            gP (rX $ expression ++ "$") test `shouldBe` [ test | expression == test ]
            gP (rX $ expression ++ "$") expression `shouldBe` [ expression ]

    it "matches a string with no special characters with either side of an option split by '|'" $
      forAll (listOf nonSpecial) $
        \ expression1 ->
          forAll (listOf nonSpecial) $
            \ expression2 -> do
              cP (rX $ expression1 ++ '|' : expression2) expression1 `shouldBe` True
              cP (rX $ expression2 ++ '|' : expression1) expression1 `shouldBe` True

    -- describe "/r" $
    --   it "acts like a '+' when put on the end of a list with a '?'" $
    --     forAll (listOf nonSpecial) $
    --       \ string ->
    --         pP (rX $ string ++ "/r?") `shouldBe`

    context "escapes special characters properly" $ do
      mapM_
        ( \ specialCharacter ->
          it [specialCharacter] $ do
            cP (rX $ ['/', specialCharacter, '+']) (specialCharacter <$ [0..9]) `shouldBe` True
            pP (rX $ ['/', specialCharacter, '+']) ('a' <$ [0..9]) `shouldBe` False
        )
        "/()[]^.+*?{@!$"

    context "escape sequences work properly" $ do
      it "/a matches anything that is a letter" $ property $
        \ char ->
          x1 (rX "/a") [char] `shouldBe` isLetter char
      it "/l matches anything that is lowercase" $ property $
        \ char ->
          x1 (rX "/l") [char] `shouldBe` isLower char
      it "/U matches anything that is uppercase" $ property $
        \ char ->
          x1 (rX "/U") [char] `shouldBe` isUpper char
      it "/w matches anything that is whitespace" $ property $
        \ char ->
          x1 (rX "/w") [char] `shouldBe` isSpace char
