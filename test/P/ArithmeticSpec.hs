{-# Language ScopedTypeVariables #-}
{-# Language LambdaCase #-}
module P.ArithmeticSpec
  ( spec
  )
  where


import Prelude
  ( Integer
  , ($)
  , pure
  , Bool (..)
  , Eq (..)
  )


import Test.Hspec
import Test.QuickCheck


import P.Arithmetic
import P.Foldable.Length


spec :: Spec
spec = do
  describe "xrt" $ do
    it "returns a positive result when the input is a perfect power" $ property $
      \ (b :: Integer) (Positive (p :: Integer)) ->
        xrt p (b^p) `shouldSatisfy` nø
  -- TODO BROKEN?
  --  it "works as a base when it gets a result" $ property $
  --    \ (NonNegative (p :: Integer)) (k :: Integer) ->
  --      xrt p k `shouldSatisfy` (\case [] -> True; [b] -> b^p == k)
  describe "xlg" $ do
    it "returns a positive result when the input is a perfect power of the base" $ property $
      \ (Positive (b :: Integer)) (NonNegative (p :: Integer)) ->
        xlg b (b^p) `shouldSatisfy` nø
    it "works as an exponent when it gets a result" $ property $
      \ (b :: Integer) (k :: Integer) ->
        xlg b k `shouldSatisfy` (\case [] -> True; [p] -> b^p == k)
