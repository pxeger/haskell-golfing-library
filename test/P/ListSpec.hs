{-# Language ScopedTypeVariables #-}
module P.ListSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Aliases
import P.List


spec :: Spec
spec = do
  describe "xts" $ do
    it "preserves the order of the input list" $ property $
      \ (x :: List Int) ->
        fst <$> xts x `shouldBe` x
    it "produces lists which are one shorter than the input list" $ property $
      \ (x :: List Int) ->
        xts x `shouldSatisfy` all (\ (_, y) -> length y == length x - 1)
