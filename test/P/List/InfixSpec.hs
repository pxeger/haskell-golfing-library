{-# Language ScopedTypeVariables #-}
module P.List.InfixSpec
  ( spec
  )
  where

import Test.Hspec
import Test.QuickCheck

import P.List.Infix
import P.Algebra.Monoid.Free.Substring


spec :: Spec
spec = do
  describe "iw" $ do
    it "the empty list is an infix of any list" $ property $
      \ (list :: [Int]) ->
        iw [] list `shouldBe` True
    it "every list is an infix of itself" $ property $
      \ (list :: [Int]) ->
        iw list list `shouldBe` True
    it "a list is an infix of it sandwiched between two other lists" $ property $
      \ (list :: [Int]) ->
        \ (inFront :: [Int]) ->
          \ (behind :: [Int]) ->
            iw list (inFront ++ list ++ behind) `shouldBe` True
    it "if two lists are mutually infixes they are equal" $ property $
      \ (list1 :: [Int]) ->
        \ (list2 :: [Int]) ->
          (iS list1 list2) && (iS list2 list1) `shouldBe` list1 == list2
  describe "ih" $ do
    it "returns an infix of the input" $ property $
      \ (list :: [Int]) ->
        \ (bound :: Int) ->
          ih (< bound) list `shouldSatisfy` fiw list
    it "returns the entire list when everything passes" $ property $
      \ (list :: [Int]) ->
        ih (\ _ -> True) list `shouldSatisfy` (== list)
    it "returns a list with all elements satisfying the predicate" $ property $
      \ (list :: [Int]) ->
        \ (bound :: Int) ->
          ih (< bound) list `shouldSatisfy` all (< bound)
