{-# Language ScopedTypeVariables #-}
module P.List.ComparisonSpec
  ( spec
  )
  where


import Control.Exception
  ( evaluate
  )


import Test.Hspec
import Test.QuickCheck


import P.List.Comparison
import P.Foldable
  ( lS
  )
import P.Scan
  ( sc
  )


listOf2 ::
  (
  )
    => Gen a -> Gen [a]
listOf2 gen =
  sized $
    \ n -> do
      k <- chooseInt (2,2 `max` n)
      vectorOf k gen


spec :: Spec
spec = do
  context "lq" $ do
    describe "lq" $ do
      it "succeeds on an empty list" $
        lq ([] :: [Int]) `shouldBe` True
      it "succeeds on a list of 1 element" $ property $
        \ (element :: Int) ->
          lq [element] `shouldBe` True
      it "behaves as eq when there are two elements" $ property $
        \ (element1 :: Int) element2 ->
          lq [element1, element2] `shouldBe` (element1 == element2)
      it "succeeds on a list of repeated elements" $ property $
        \ (element :: Int) ->
          forAll (listOf2 $ pure element) $
            \ list ->
              lq list `shouldBe` True
      it "behaves as eq when the tail consists of repeated elements" $ property $
        \ (element1 :: Int) element2 ->
          forAll (listOf1 $ pure element2) $
            \ list ->
              lq (element1 : list) `shouldBe` (element1 == element2)
    describe "nlq" $ do
      it "is the opposite of lq" $ property $
        \ (list :: [Int]) ->
          nlq list `shouldNotBe` lq list
    describe "lqb" $ do
      it "produces the same result as lq when the transformation is the identity" $ property $
        \ (list :: [Int]) ->
          lqb id list `shouldBe` lq list
    describe "lqw" $ do
      it "doesn't run the comparison when the list is empty" $
        lqw (\ _ _ -> error "Ran the comparison") [] `shouldBe` True
      it "doesn't run the comparison when the list has 1 element" $ property $
        \ (element :: Int) ->
          lqw (\ _ _ -> error "Ran the comparison") [element] `shouldBe` True
      it "runs the comparison when the list has more than 1 element" $ property $
        forAll (listOf2 $ chooseAny) $
          \ (list :: [Int]) ->
            evaluate (lqw (\ _ _ -> error "Ran the comparison") list) `shouldThrow` anyErrorCall
  context "uq" $ do
    describe "uq" $ do
      it "succeeds on an empty list" $
        uq ([] :: [Int]) `shouldBe` True
      it "succeeds on a list of 1 element" $ property $
        \ (element1 :: Int) ->
          uq [element1] `shouldBe` True
      it "behaves as not eq when there are two elements" $ property $
        \ (element1 :: Int) element2 ->
          uq [element1, element2] `shouldBe` (element1 /= element2)
      it "doesn't assume transitivity" $ property $
        \ (element1 :: Int) element2 ->
          uq [element1, element2, element1] `shouldBe` False
    describe "nuq" $ do
      it "is the opposite of uq" $ property $
        \ (list :: [Int]) ->
          nuq list `shouldNotBe` uq list
    describe "uqb" $ do
      it "produces the same result as uq when the transformation is the identity" $ property $
        \ (list :: [Int]) ->
          uqb id list `shouldBe` uq list
    describe "mno" $ do
      it "succeeds on a list with non-negative deltas" $ property $
        \ (deltas :: [NonNegative Integer])
          (start :: Integer) ->
            sc (+) start (getNonNegative <$> deltas) `shouldSatisfy` mno
      it "succeeds on a list with non-positive deltas" $ property $
        \ (deltas :: [NonPositive Integer])
          (start :: Integer) ->
            sc (+) start (getNonPositive <$> deltas) `shouldSatisfy` mno
      it "fails on a list with mixed positive and negative deltas" $ property $
        \ (NonEmpty (d1 :: [Positive Integer]))
          (d2 :: [Integer])
          (NonEmpty (d3 :: [Negative Integer]))
          (start :: Integer) -> do
            mapM_ (\ list -> lS (+) start list `shouldNotSatisfy` mno)
              [ ((getPositive <$> d1) ++ d2 ++ (getNegative <$> d3))
              , ((getNegative <$> d3) ++ d2 ++ (getPositive <$> d1))
              ]
    describe "mNo" $ do
      it "succeeds on a list with positive deltas" $ property $
        \ (deltas :: [Positive Integer])
          (start :: Integer) ->
            sc (+) start (getPositive <$> deltas) `shouldSatisfy` mNo
      it "succeeds on a list with negative deltas" $ property $
        \ (deltas :: [Negative Integer])
          (start :: Integer) ->
            sc (+) start (getNegative <$> deltas) `shouldSatisfy` mNo
      it "fails on a list with mixed positive and negative deltas" $ property $
        \ (NonEmpty (d1 :: [Positive Integer]))
          (d2 :: [Integer])
          (NonEmpty (d3 :: [Negative Integer]))
          (start :: Integer) -> do
            mapM_ (\ list -> lS (+) start list `shouldNotSatisfy` mNo)
              [ ((getPositive <$> d1) ++ d2 ++ (getNegative <$> d3))
              , ((getNegative <$> d3) ++ d2 ++ (getPositive <$> d1))
              ]
      it "fails on a list with mixed zero and non-zero deltas" $ property $
        \ (d1 :: [Integer])
          (d2 :: [Integer])
          (d3 :: [Integer])
          (NonZero (nonZeroValue :: Integer))
          (start :: Integer) ->
            mapM_ (\ list -> lS (+) start list `shouldNotSatisfy` mNo)
              [ d1 ++ 0 : d2 ++ nonZeroValue : d3
              , d1 ++ nonZeroValue : d2 ++ 0 : d3
              ]

