{-# Language ScopedTypeVariables #-}
module P.List.PairsSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Aliases
import P.Arithmetic.Nat
import P.Foldable.Length
import P.List.Pairs


spec :: Spec
spec = do
  describe "pA" $ do
    it "produces the empty list on empty input" $
      pA ([] :: List Int) `shouldBe` ([] :: List (Int, Int))
    it "produces a list one shorter than the input on non-empty input" $ property $
      forAll (listOf1 arbitrary) $
        \ (x :: List Int) ->
          pA x `shouldSatisfy` (== lnt x) . (+ 1) . lnt
