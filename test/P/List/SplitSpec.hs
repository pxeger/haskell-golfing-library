{-# Language ScopedTypeVariables #-}
module P.List.SplitSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import P.Char
import P.Foldable
import P.List.Infix
import P.List.Split


charPredicate :: Gen (String, Char -> Bool)
charPredicate =
  elements
    [ ("const True", const True)
    , ("const False", const False)
    , ("isSpace", iW)
    ]


spec :: Spec
spec = do
  describe "sY" $ do
    it "creates 1 more pieces than number of characters that match the predicate" $ property $
      \ (string :: String) ->
        forAllShow charPredicate fst $
          \ (_, pred) ->
            length (sY pred string) `shouldBe` cn pred string + 1
    it "doesn't create any chunks with characters which satisfy the predicate" $ property $
      \ (string :: String) ->
        forAllShow charPredicate fst $
          \ (_, pred) ->
            sY pred string `shouldSatisfy` all (not . any pred)
    it "includes every character not passing the predicate in the input in some chunk" $ property $
      \ (string :: String) ->
        forAllShow charPredicate fst $
          \ (_, pred) ->
            sY pred string `shouldSatisfy` (\ x -> all (\ y -> pred y || any (Prelude.elem y) x) string)
    it "only produces chunks which are an infix of the input" $ property $
      \ (string :: String) ->
        forAllShow charPredicate fst $
          \ (_, pred) ->
            sY pred string `shouldSatisfy` all (fiw string)
      

