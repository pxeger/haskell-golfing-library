{-# Language ScopedTypeVariables #-}
module P.List.SubListSpec
  ( spec
  )
  where

import Test.Hspec
import Test.QuickCheck

import P.Algebra.Monoid.Free.Substring

spec :: Spec
spec = do
  describe "iS" $ do
    it "the empty list is a sublist of any list" $ property $
      \ (list :: [Int]) ->
        iS [] list `shouldBe` True
    it "every list is a sublist of itself" $ property $
      \ (list :: [Int]) ->
        iS list list `shouldBe` True
    it "a list is a sublist of it sandwiched between two other lists" $ property $
      \ (list :: [Int]) ->
        \ (inFront :: [Int]) ->
          \ (behind :: [Int]) ->
            iS list (inFront ++ list ++ behind) `shouldBe` True
    it "if two lists are mutually sublists they are equal" $ property $
      \ (list1 :: [Int]) ->
        \ (list2 :: [Int]) ->
          (iS list1 list2) && (iS list2 list1) `shouldBe` list1 == list2
  describe "lsS" $ do
    it "produces a list which is a sublist of both inputs" $
      forAll (resize 13 $ listOf $ chooseAny) $ -- Limit the size until lsS is sped up
        \ (list1 :: [Int]) ->
          forAll (resize 13 $ listOf $ chooseAny) $
            \ (list2 :: [Int]) ->
              do
                let
                  result =
                    lsS list1 list2
                result `shouldSatisfy` fiS list1
                result `shouldSatisfy` fiS list2
  describe "sS" $ do
    it "only produces sublists of the initial list" $
      forAll (resize 18 $ listOf $ chooseAny) $ -- Limit the size since the output is exponential
        \ (list :: [Int]) ->
          sS list `shouldSatisfy` all (fiS list)
    it "always produces the input as a sublist when the input is non-empty" $
      forAll (resize 18 $ listOf1 $ chooseAny) $ -- Limit the size since the output is exponential
        \ (list :: [Int]) ->
          sS list `shouldSatisfy` elem list
    it "produces 2^n-1 sublists where n is the length of the input" $
      forAll (resize 18 $ listOf $ chooseAny) $ -- Limit the size since the output is exponential
        \ (list :: [Int]) ->
          length (sS list) `shouldBe` 2 ^ length list - 1
  describe "ss" $ do
    it "only produces sublists of the initial list" $
      forAll (resize 18 $ listOf $ chooseAny) $ -- Limit the size since the output is exponential
        \ (list :: [Int]) ->
          ss list `shouldSatisfy` all (fiS list)
    it "always produces the input as a sublist" $
      forAll (resize 18 $ listOf $ chooseAny) $ -- Limit the size since the output is exponential
        \ (list :: [Int]) ->
          ss list `shouldSatisfy` elem list
    it "always produces the empty list as a sublist" $ property $
      \ (list :: [Int]) ->
        ss list `shouldSatisfy` elem []
    it "produces 2^n sublists where n is the length of the input" $
      forAll (resize 18 $ listOf $ chooseAny) $ -- Limit the size since the output is exponential
        \ (list :: [Int]) ->
          length (ss list) `shouldBe` 2 ^ length list
  describe "ssn" $ do
    it "only produces sublists of the initial list" $
      forAll (resize 18 $ listOf $ chooseAny) $ -- Limit the size since the output is exponential
        \ (list :: [Int]) ->
          \ (size :: Int) ->
            ssn size list `shouldSatisfy` all (fiS list)
    it "only produces sublists of size equal to the input" $
      forAll (resize 18 $ listOf $ chooseAny) $ -- Limit the size since the output is exponential
        \ (list :: [Int]) ->
          \ (size :: Int) ->
            ssn size list `shouldSatisfy` all ((size ==) . length)
