{-# Language ScopedTypeVariables #-}
module P.Arithmetic.BaseSpec
  ( spec
  )
  where


import Test.Hspec
import Test.QuickCheck


import Test.Util


import P.Arithmetic.Base


spec :: Spec
spec = do
  describe "bs" $ do
    context "positive bases" $ do
      it "produces the empty list when the input is zero" $ property $
        \ (AtLeast2 (base :: Int))
        ->
          bs base 0 `shouldBe` []
      it "doesn't produce negative digits" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
        ->
          bs base x `shouldAllSatisfy` (>= 0)
      it "doesn't produce any digits greater than or equal to the base" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
        ->
          bs base x `shouldAllSatisfy` (< base)
      it "produces different outputs for different numbers and the same base" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
          (NonNegative (y :: Int))
        ->
          bs base x == bs base y `shouldBe` x == y
      it "is undone by ubs" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
        ->
          ubs base (bs base x) `shouldBe` x
    context "negative bases" $ do
      it "produces the empty list when the input is zero" $ property $
        \ (AtLeast2 (base :: Int))
        ->
          bs (negate base) 0 `shouldBe` []
      it "doesn't produce negative digits" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
        ->
          bs (negate base) x `shouldAllSatisfy` (>= 0)
      it "doesn't produce any digits greater than or equal to the absolute value of the base" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
        ->
          bs (negate base) x `shouldAllSatisfy` (< base)
      it "produces different outputs for different numbers and the same base" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
          (y :: Int)
        ->
          bs (negate base) x == bs (negate base) y `shouldBe` x == y
      it "is undone by ubs" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
        ->
          ubs (negate base) (bs (negate base) x) `shouldBe` x
  describe "ubs" $ do
    context "positive bases" $
      it "is consistent with ubS" $ property $
        \ (AtLeast2 (base :: Int))
          (NonNegative (x :: Int))
        ->
          ubs base (bs base x) `shouldBe` ubS base (bS base x)
    context "negative bases" $
      it "is consistent with ubS" $ property $
        \ (AtLeast2 (base :: Int))
          (x :: Int)
        ->
          ubs (negate base) (bs (negate base) x) `shouldBe` ubS (negate base) (bS (negate base) x)
