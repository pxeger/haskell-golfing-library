{-# Options_GHC -Wno-deprecations #-}
{-# Language FlexibleContexts #-}
module P.PermutationSpec
  ( spec
  )
  where

import Prelude hiding
  ( (<>)
  , Monoid
  )

import Test.Hspec
import Test.QuickCheck

import P.Algebra.Group
import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Ord
import P.Permutation
import P.Reverse
import P.Vector

sizedTest ::
  ( Monoid (Permutation n Integer)
  , Show (Vec n Integer)
  )
    => (Gen (Permutation n Integer)) -> Spec
sizedTest genPerm = do
  describe "<>" $
    it "is associative" $
      forAll genPerm $ \ perm1 ->
        forAll genPerm $ \ perm2 ->
          forAll genPerm $ \ perm3 ->
            (perm1 <> (perm2 <> perm3)) `shouldBe` ((perm1 <> perm2) <> perm3)
  describe "mempty" $ do
    it "is a right identity for <>" $
      forAll genPerm $
        \ perm ->
          perm <> i `shouldBe` perm
    it "is a left identity for <>" $
      forAll genPerm $
        \ perm ->
          i <> perm `shouldBe` perm
  describe "_rv" $ do
    it "is its own inverse under function composition" $
      forAll genPerm $
        \ perm ->
          _rv (_rv perm) `shouldBe` perm
    it "_rv i is its own inverse under <>" $
      forAll genPerm $
        \ perm ->
          perm <> (_rv i <> _rv i) `shouldBe` perm
  describe "IV" $ do
    it "creates the right inverse" $
      forAll genPerm $
        \ perm ->
          perm <> IV perm `shouldBe` i
    it "creates the left inverse" $
      forAll genPerm $
        \ perm ->
          IV perm <> perm `shouldBe` i

spec :: Spec
spec = do
  context "Size 0" $
    sizedTest (pure (i :: Permutation Zr Integer))
  context "Size 1" $
    sizedTest (pure (i :: Permutation (Nx Zr) Integer))
  context "Size 2" $
    sizedTest
      ( elements
        [ i :: Permutation (Nx (Nx Zr)) Integer
        , _rv i
        ]
      )
  context "Size 3" $
    sizedTest
      ( elements
        [ UsP $ V3 0 1 2
        , UsP $ V3 2 1 0
        , UsP $ V3 1 2 0
        , UsP $ V3 2 0 1
        , UsP $ V3 0 2 1
        , UsP $ V3 1 0 2
        ]
      )

