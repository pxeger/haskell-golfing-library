{-# Language CPP #-}
module P.Functor
  ( Functor
  , mi
  , mI
  , vd
  , pM
  , (<$)
  , fpM
  , ($>)
  , jpM
  , mw
  , fmw
  , mwn
  , fwn
  , mwc
  , fWC
  -- * Deprecated
  , void
  , fmap
  )
  where


import Prelude
  ( Integral
  )
import qualified Prelude


import Data.Functor
  ( Functor
  )
import qualified Data.Functor


import P.Aliases
import P.Bool
import P.Function.Flip


infixl 4 <$


#if __GLASGOW_HASKELL__ <= 905
instance
  (
  )
    => Functor ((,,,,) a b c d)
  where
    fmap f (x1, x2, x3, x4, x5) =
      (x1, x2, x3, x4, f x5)


instance
  (
  )
    => Functor ((,,,,,) a b c d e)
  where
    fmap f (x1, x2, x3, x4, x5, x6) =
      (x1, x2, x3, x4, x5, f x6)


instance
  (
  )
    => Functor ((,,,,,,) a b c d e f)
  where
    fmap f (x1, x2, x3, x4, x5, x6, x7) =
      (x1, x2, x3, x4, x5, x6, f x7)
#endif


{-# Deprecated fmap "Use m or (<) instead" #-}
-- | Map a function over an arbitrary functor.
-- Long version of 'P.Function.Compose.m' and '(P.Function.Compose.<)'
fmap ::
  ( Functor f
  )
    => (a -> b) -> f a -> f b
fmap =
  Prelude.fmap


-- | Map a function over the non-negative integers.
mi ::
  ( Integral i
  )
    => (i -> a) -> List a
mi =
  f' Prelude.fmap [0 ..]


-- | Map a function over the positive integers.
mI ::
  ( Integral i
  )
    => (i -> a) -> List a
mI =
  f' Prelude.fmap [1 ..]


{-# Deprecated void "Use vd instead" #-}
-- | Long version of 'vd'.
void ::
  ( Functor f
  )
    => f a -> f ()
void =
  vd


-- | Replace all elements of a functor with @()@.
-- Equivalent to 'Data.Functor.void'.
vd ::
  ( Functor f
  )
    => f a -> f ()
vd =
  Data.Functor.void


-- | Prefix version of '(<$)'.
pM ::
  ( Functor f
  )
    => a -> f b -> f a
pM =
  (<$)


-- | Replace all elements of a functor with some fixed element.
--
-- Equivalent to '(Data.Functor.<$)'.
--
-- ==== __Examples__
--
-- >>> "hi" <$ (0 ## 5)
-- ["hi","hi","hi","hi","hi","hi"]
--
(<$) ::
  ( Functor f
  )
    => a -> f b -> f a
(<$) =
  (Data.Functor.<$)


-- | Flip of 'pM'.
fpM ::
  ( Functor f
  )
    => f a -> b -> f b
fpM =
  f' pM


-- | Flip of '(<$)'.
($>) ::
  ( Functor f
  )
    => f a -> b -> f b
($>) =
  (Data.Functor.$>)


-- | Replace every element of a functor with the functor itself.
jpM ::
  ( Functor f
  )
    => f a -> f (f a)
jpM f =
  f <$ f


-- | Restricted map.
-- Applies a function only to values matching a predicate.
mw ::
  ( Functor f
  )
    => Predicate a -> (a -> a) -> f a -> f a
mw pred f =
  fmap
    ( \ x ->
      if
        pred x
      then
        f x
      else
        x
    )


-- | Flip of 'mw'.
fmw ::
  ( Functor f
  )
    => (a -> a) -> Predicate a -> f a -> f a
fmw =
  f' mw


-- | Negated version of 'mw'.
-- Applies a function only to values not matching a predicate.
mwn ::
  ( Functor f
  )
    => Predicate a -> (a -> a) -> f a -> f a
mwn =
  fmap mw (fmap n)


-- | Flip of 'mwn'.
fwn ::
  ( Functor f
  )
    => (a -> a) -> Predicate a -> f a -> f a
fwn =
  f' mwn


-- | Replace all elements matching a predicate.
mwc ::
  ( Functor f
  )
    => Predicate a -> a -> f a -> f a
mwc =
  f' fWC


-- | Flip of 'mwc'.
fWC ::
  ( Functor f
  )
    => a -> Predicate a -> f a -> f a
fWC =
  fmap fmw Prelude.const
