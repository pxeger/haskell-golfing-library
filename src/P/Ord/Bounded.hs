{-# Language TypeSynonymInstances #-}
{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Ord.Bounded
Description :
  Bounded class and functions
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Ord.Bounded
  ( MinBounded (..)
  , MaxBounded (..)
  , pattern NB
  , pattern XB
  , mJ
  , or
  , ay
  , fay
  , all
  , fll
  , nr
  , no
  , fno
  -- * Deprecated
  , minBound
  , maxBound
  , none
  )
  where


import qualified Prelude
import Prelude
  ( (-)
  )


import Data.Word
  ( Word
  , Word8
  , Word16
  , Word32
  , Word64
  )


import P.Aliases
import P.Arithmetic.Nat
import P.Bifunctor.Either
import P.Bifunctor.These
import P.Bool
import P.Category
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Ord


-- | A type with a minimum bound.
-- The minimum bound must be less than or equal to every value in the type.
class
  ( Ord a
  )
    => MinBounded a
  where
    -- | Minimum bound.
    _nb :: a


-- | A type with a maximum bound.
-- The maximum bound must be greater than or equal to every value in the type.
class
  ( Ord a
  )
    => MaxBounded a
  where
    -- | Maximum bound.
    _xb :: a


-- | Minimum bound of a ordered type.
pattern NB ::
  ( MinBounded a
  )
    => a
pattern NB <- (eq _nb -> T) where
  NB =
    _nb


-- | Maximum bound of a ordered type.
pattern XB ::
  ( MaxBounded a
  )
    => a
pattern XB <- (eq _xb -> T) where
  XB =
    _xb


{-# Deprecated minBound "Use NB instead" #-}
-- | Long version of 'NB'.
minBound ::
  ( MinBounded a
  )
    => a
minBound =
  _nb


{-# Deprecated maxBound "Use XB instead" #-}
-- | Long version of 'XB'.
maxBound ::
  ( MaxBounded a
  )
    => a
maxBound =
  _xb


-- | Get the minimum element of a structure.
-- If the structure is empty it returns the maximum bound for the type.
mJ ::
  ( MaxBounded a
  , Foldable f
  )
    => f a -> a
mJ =
  all id


-- | Get the maximum element of a structure.
-- If the structure is empty it returns the minimum bound for the type.
--
-- Generalization of 'Prelude.or'.
or ::
  ( MinBounded a
  , Foldable f
  )
    => f a -> a
or =
  ay id


-- |
--
-- Generalized version of 'Prelude.any'
ay ::
  ( MinBounded b
  , Foldable f
  )
    => (a -> b) -> f a -> b
ay f =
  rF go NB
  where
    go x y =
      ma (f x) y


-- | Flip of 'ay'.
fay ::
  ( MinBounded b
  , Foldable t
  )
    => t a -> (a -> b) -> b
fay =
  f' ay


-- |
--
-- Generalized version of 'Prelude.all', see 'P.Foldable.mF' also.
all ::
  ( MaxBounded b
  , Foldable t
  )
    => (a -> b) -> t a -> b
all f =
  rF (mN < f) XB


-- | Flip of 'all'.
fll ::
  ( MaxBounded b
  , Foldable t
  )
    => t a -> (a -> b) -> b
fll =
  f' all


-- | Determine if all values of a list are false.
--
-- For a version that maps a 'Predicate' over the list see 'no'.
nr ::
  ( Foldable t
  )
    => Predicate (t Bool)
nr =
  n < ay n


-- | Returns true if no element satisfies the predicate.
no ::
  ( Foldable t
  )
    => Predicate a -> t a -> Bool
no =
  n << ay


-- | Flip of 'no'.
fno ::
  ( Foldable t
  )
    => t a -> Predicate a -> Bool
fno =
  f' no


{-# Deprecated none "Use no instead" #-}
-- | Long version of 'no'.
none ::
  ( Foldable t
  )
    => Predicate a -> t a -> Bool
none =
  no


-- MinBounded instances


instance
  ( Ord a
  )
    => MinBounded (List a)
  where
    _nb =
      []


instance
  (
  )
    => MinBounded ()
  where
    _nb =
      ()


instance
  (
  )
    => MinBounded Bool
  where
    _nb =
      B

instance
  (
  )
    => MinBounded Ordering
  where
    _nb =
      LT


instance
  ( MinBounded a
  , Ord b
  )
    => MinBounded (Either a b)
  where
    _nb =
      Lef _nb


instance
  (
  )
    => MinBounded Nat
  where
    _nb =
      0


instance
  ( MinBounded a
  , Ord b
  )
    => MinBounded (These a b)
  where
    _nb =
      Ti _nb


instance
  (
  )
    => MinBounded Word
  where
    _nb =
      0


instance
  (
  )
    => MinBounded Word8
  where
    _nb =
      0


instance
  (
  )
    => MinBounded Word16
  where
    _nb =
      0


instance
  (
  )
    => MinBounded Word32
  where
    _nb =
      0


instance
  (
  )
    => MinBounded Word64
  where
    _nb =
      0


-- MaxBounded instances


instance
  (
  )
    => MaxBounded ()
  where
    _xb =
      ()


instance
  (
  )
    => MaxBounded Bool
  where
    _xb =
      T


instance
  (
  )
    => MaxBounded Ordering
  where
    _xb =
      GT


instance
  ( Ord a
  , MaxBounded b
  )
    => MaxBounded (Either a b)
  where
    _xb =
      Rit _xb


instance
  ( MaxBounded a
  , MaxBounded b
  )
    => MaxBounded (These a b)
  where
    _xb =
      Te _xb _xb


instance
  (
  )
    => MaxBounded Word
  where
    _xb =
      _nb - 1


instance
  (
  )
    => MaxBounded Word8
  where
    _xb =
      _nb - 1


instance
  (
  )
    => MaxBounded Word16
  where
    _xb =
      _nb - 1


instance
  (
  )
    => MaxBounded Word32
  where
    _xb =
      _nb - 1


instance
  (
  )
    => MaxBounded Word64
  where
    _xb =
      _nb - 1


-- Tuple instances


instance
  ( MinBounded a1
  , MinBounded a2
  )
    => MinBounded (a1, a2)
  where
    _nb =
      ( _nb
      , _nb
      )


instance
  ( MinBounded a1
  , MinBounded a2
  , MinBounded a3
  )
    => MinBounded (a1, a2, a3)
  where
    _nb =
      ( _nb
      , _nb
      , _nb
      )


instance
  ( MinBounded a1
  , MinBounded a2
  , MinBounded a3
  , MinBounded a4
  )
    => MinBounded (a1, a2, a3, a4)
  where
    _nb =
      ( _nb
      , _nb
      , _nb
      , _nb
      )


instance
  ( MinBounded a1
  , MinBounded a2
  , MinBounded a3
  , MinBounded a4
  , MinBounded a5
  )
    => MinBounded (a1, a2, a3, a4, a5)
  where
    _nb =
      ( _nb
      , _nb
      , _nb
      , _nb
      , _nb
      )


instance
  ( MaxBounded a1
  , MaxBounded a2
  )
    => MaxBounded (a1, a2)
  where
    _xb =
      ( _xb
      , _xb
      )


instance
  ( MaxBounded a1
  , MaxBounded a2
  , MaxBounded a3
  )
    => MaxBounded (a1, a2, a3)
  where
    _xb =
      ( _xb
      , _xb
      , _xb
      )


instance
  ( MaxBounded a1
  , MaxBounded a2
  , MaxBounded a3
  , MaxBounded a4
  )
    => MaxBounded (a1, a2, a3, a4)
  where
    _xb =
      ( _xb
      , _xb
      , _xb
      , _xb
      )


instance
  ( MaxBounded a1
  , MaxBounded a2
  , MaxBounded a3
  , MaxBounded a4
  , MaxBounded a5
  )
    => MaxBounded (a1, a2, a3, a4, a5)
  where
    _xb =
      ( _xb
      , _xb
      , _xb
      , _xb
      , _xb
      )
