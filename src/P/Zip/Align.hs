{-|
Module :
  P.Zip.Align
Description :
  Alignable functors
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

An @Align@ is a functor which is both 'SemiAlign' and 'UnitalZip'.
Since @UnitalZip@ is not a standard class, @Align@ is often its own class which provides a unit to @SemiAlign@.
However for us no such class is needed.
In this module we provide functions which require both a @SemiAlign@ and @UnitalZip@.
-}
module P.Zip.Align
  ( rZd
  , lZd
  , rzd
  , lzd
  , rzm
  , lzm
  )
  where


import qualified Prelude
import Prelude
  ( Foldable
  )


import P.Algebra.Semigroup
import P.Function.Compose
import P.Function.Flip
import P.Zip.SemiAlign
import P.Zip.UnitalZip


-- | Applies 'zd'' as a right fold.
--
-- ==== __Examples__
--
-- Get the pairwise maximum of a list of lists
--
-- >>> rZd ma [[1,2,3],[3],[0,0]]
-- [3,2,3]
rZd ::
  ( UnitalZip f
  , SemiAlign f
  , Foldable t
  )
    => (a -> a -> a) -> t (f a) -> f a
rZd =
  F Prelude.foldr aId < zd'


-- | Applies 'zd'' as a left fold.
--
-- ==== __Examples__
--
-- Get the pairwise maximum of a list of lists
--
-- >>> lZd ma [[1,2,3],[3],[0,0]]
-- [3,2,3]
lZd ::
  ( UnitalZip f
  , SemiAlign f
  , Foldable t
  )
    => (a -> a -> a) -> t (f a) -> f a
lZd =
  F Prelude.foldl aId < zd'


-- | Applies 'zd' as a right fold.
rzd ::
  ( UnitalZip f
  , SemiAlign f
  , Foldable t
  )
    => (a -> a -> a) -> a -> t (f a) -> f a
rzd =
  F Prelude.foldr aId << zd


-- | Applies 'zd' as a right fold.
lzd ::
  ( UnitalZip f
  , SemiAlign f
  , Foldable t
  )
    => (a -> a -> a) -> a -> t (f a) -> f a
lzd =
  F Prelude.foldl aId << zd


-- | Applies 'zdm' as a right fold.
rzm ::
  ( UnitalZip f
  , SemiAlign f
  , Foldable t
  , Semigroup a
  )
    => t (f a) -> f a
rzm =
  Prelude.foldr zdm aId


-- | Applies 'zdm' as a left fold.
lzm ::
  ( UnitalZip f
  , SemiAlign f
  , Foldable t
  , Semigroup a
  )
    => t (f a) -> f a
lzm =
  Prelude.foldl zdm aId
