{-# Language FlexibleInstances #-}
module P.Zip.WeakAlign
  ( WeakAlign (..)
  , gSo
  , gMy
  , gPy
  , gSO
  , gMY
  , gPY
  , lGo
  , lGy
  -- * Deprecated
  , union
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , otherwise
  )


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Aliases
import P.Applicative
import P.Arrow.Cokleisli
import P.Arrow.Kleisli
import P.Bifunctor.Blackbird
import P.Bifunctor.Either
import P.Category
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Maybe
import P.Swap


-- | Functors supporting an align operation that takes the union of potentially non-uniform shapes.
--
-- === Laws
--
-- ==== Idempotency
--
-- prop> jn cnL = id
-- prop> jn cnR = id
-- prop> jn alL = m Lef
-- prop> jn alR = m Rit
--
-- ==== Associativity
--
-- prop> cnL x (cnL y z) = cnL (cnL x y) z
-- prop> cnR x (cnR y z) = cnR (cnR x y) z
-- prop> alL x (alL y z) = Asc < alL (alL x y) z
-- prop> alR x (alR y z) = Asc < alR (alR x y) z
--
-- ==== Reflection
--
-- prop> cnL = F cnR
-- prop> cnR = F cnL
-- prop> alL = Sw <<< F alR
-- prop> alR = Sw <<< F alL
--
class
  ( Functor f
  )
    => WeakAlign f
  where
    {-# Minimal alL | alR | cnL | cnR | (#|) #-}
    -- | Combines two structures.
    -- Values from the left structure are given as left values and values from the right structure as right.
    -- When values from both structures are present for a particular place, the left value is retained and the right value is discared.
    --
    -- For a version that provides the right value on conflict see 'alR'.
    --
    -- More general version of 'Data.Map.Strict.union'.
    --
    -- ==== __Examples__
    --
    -- On lists:
    --
    -- >>> alL [1,2,3] [4,5,6,7,8]
    -- [1,2,3,7,8]
    alL :: f a -> f b -> f (Either a b)
    alL =
      Sw <<< F alR

    -- | Combines two structures.
    -- Values from the left structure are given as left values and values from the right structure as right.
    -- When values from both structures are present for a particular place, the right value is retained and the left value is discared.
    --
    -- For a version that provides the left value on conflict see 'alL'.
    --
    -- ==== __Examples__
    --
    -- On lists:
    --
    -- >>> alR [1,2,3] [4,5,6,7,8]
    -- [4,5,6,7,8]
    alR :: f a -> f b -> f (Either a b)
    alR x y =
      cnR (Lef < x) (Rit < y)

    -- | Takes two aligns and provides the left value unless not present in which case it provides the right value.
    --
    -- It is like 'alL' but instead of providing an 'Either' the input types are the same so no @Either@ is required.
    --
    -- ==== __Examples__
    --
    -- Here we have a parser which tries to parse @ax@ and if it fails it then tries to parse @a@.
    -- Crucially this never gives the results of both parsers.
    --
    -- >>> f = cnL (ʃ"ax") (ʃ"a")
    -- >>> gP f "axolotl"
    -- ["ax"]
    -- >>> gP f "amalgam"
    -- ["a"]
    --
    -- Compare this with 'P.Alternative.ao' which gives both results in the event they both pass.
    --
    -- >>> f = ao (ʃ"ax") (ʃ"a")
    -- >>> gP f "axolotl"
    -- ["ax","a"]
    -- >>> gP f "amalgam"
    -- ["a"]
    --
    cnL :: f a -> f a -> f a
    cnL =
      fI <<< alL

    -- | Flip of 'cnL'.
    cnR :: f a -> f a -> f a
    cnR =
      F (#|)

    -- | Infix of 'cnL'.
    (#|) :: f b -> f b -> f b
    (#|) =
      cnL




instance
  (
  )
    => WeakAlign []
  where
    alL (x : xs) (_ : ys) =
      Lef x : alL xs ys
    alL [] ys =
      Rit < ys
    alL xs [] =
      Lef < xs


instance
  (
  )
    => WeakAlign ((,) a)
  where
    cnL =
      p


instance
  (
  )
    => WeakAlign ((,,) a b)
  where
    cnL =
      p


instance
  (
  )
    => WeakAlign ((,,,) a b c)
  where
    cnL =
      p


instance
  (
  )
    => WeakAlign ((,,,,) a b c d)
  where
    cnL =
      p


instance
  (
  )
    => WeakAlign ((,,,,,) a b c d e)
  where
    cnL =
      p


instance
  (
  )
    => WeakAlign ((,,,,,,) a b c d e f)
  where
    cnL =
      p


instance
  (
  )
    => WeakAlign Prelude.Maybe
  where
    cnL (Prelude.Just x) _ =
      Prelude.Just x
    cnL _ y =
      y


instance
  (
  )
    => WeakAlign (Either a)
  where
    cnL (Rit x) _ =
      Rit x
    cnL _ (Rit y) =
      Rit y
    cnL (Lef x) _ =
      Lef x


instance
  (
  )
    => WeakAlign ((->) k)
  where
    cnL f _ =
      f


instance
  (
  )
    => WeakAlign (Kleisli Maybe k)
  where
    Kle k1 #| Kle k2 =
      Kle $ \ x ->
        case
          k1 x
        of
          Lef _ ->
            k2 x
          Rit y ->
            Rit y

instance
  (
  )
    => WeakAlign (Cokleisli w k)
  where
    k1 #| _ =
      k1


-- | Treat this type as a multi map.
-- '(#|)' merges two copies preserving the order of the left one.
instance
  ( Eq k
  )
    => WeakAlign (Blackbird [] (,) k)
  where
    MM l1 #| MM l2 =
      MM $ go l1 l2
      where
        go ((key, val) : xs) ys =
          (:) (key, val) $ go xs $ remove key ys
        go [] ys =
          ys

        -- | Removed the first copy of a key
        remove key [] =
          []
        remove key ((k, v) : ys)
          | key == k
          =
            ys
          | otherwise
          =
            (k, v) :  remove key ys


-- | Greedily apply a structure at least once.
--
-- Intended to be used with 'P.Parse.Parser' where it acts as a greedy alternative to 'P.Alternative.so'.
-- Be careful to use this only with parsers that consume some imput otherwise you can easily get infinite loops which are not normally possible with @so@.
gSo ::
  ( Applicative f
  , WeakAlign f
  )
    => f b -> f (List b)
gSo x =
  l2 (:) x (gMy x)


-- | Greedily apply a structure as many times as possible.
--
-- Intended to be used with 'P.Parse.Parser' where it acts as a greedy alternative to 'P.Alternative.my'.
-- Be careful to use this only with parsers that consume some imput otherwise you can easily get infinite loops which are not normally possible with @my@.
gMy ::
  ( Applicative f
  , WeakAlign f
  )
    => f b -> f (List b)
gMy x =
  gSo x #| p []


-- | Greedily apply a structure at most once.
--
-- Intended to be used with 'P.Parse.Parser' where it acts as a greedy alternative to 'P.Alternative.py'.
--
gPy ::
  ( Applicative f
  , WeakAlign f
  )
    => f b -> f (Maybe' b)
gPy x =
  (p < x) #| p []


-- | Greedily apply a structure at least once combining the results with the semigroup action.
--
-- Intended to be used with 'P.Parse.Parser' where it acts as a greedy alternative to 'P.Alternative.sO'.
-- Be careful to use this only with parsers that consume some imput otherwise you can easily get infinite loops which are not normally possible with @sO@.
gSO ::
  ( Applicative f
  , WeakAlign f
  , Semigroup m
  )
    => f m -> f m
gSO x =
  l2 mp x (gSO x) #| x


-- | Greedily apply a structure as many times as possible combining the results with the monoidal action.
--
-- Intended to be used with 'P.Parse.Parser' where it acts as a greedy alternative to 'P.Alternative.mY'.
-- Be careful to use this only with parsers that consume some imput otherwise you can easily get infinite loops which are not normally possible with @mY@.
gMY ::
  ( Applicative f
  , WeakAlign f
  , Monoid m
  )
    => f m -> f m
gMY =
  fo << gMy


-- | Greedily apply a structure at most once combining the results with the monoidal action.
--
-- Intended to be used with 'P.Parse.Parser' where it acts as a greedy alternative to 'P.Alternative.pY'.
--
gPY ::
  ( Applicative f
  , WeakAlign f
  , Monoid m
  )
    => f m -> f m
gPY x =
  x #| p i


-- | Greedily apply a structure at least once counting the results.
--
-- Intended to be used with 'P.Parse.Parser' where it acts as a greedy alternative to 'P.Alternative.lSo'.
-- Be careful to use this only with parsers that consume some imput otherwise you can easily get infinite loops which are not normally possible with @lSo@.
lGo ::
  ( Applicative f
  , WeakAlign f
  , Integral i
  )
    => f b -> f i
lGo =
  l << gSo


-- | Greedily apply a structure as many times as possible counting the results.
--
-- Intended to be used with 'P.Parse.Parser' where it acts as a greedy alternative to 'P.Alternative.lMy'.
-- Be careful to use this only with parsers that consume some imput otherwise you can easily get infinite loops which are not normally possible with @lMy@.
lGy ::
  ( Applicative f
  , WeakAlign f
  , Integral i
  )
    => f b -> f i
lGy =
  l << gMy


{-# Deprecated union "Use cnL instead" #-}
-- | Long version of 'cnL'.
union ::
  ( WeakAlign f
  )
    => f a -> f a -> f a
union =
  cnL
