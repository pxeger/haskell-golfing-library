{-# Language FlexibleInstances #-}
module P.Zip.SemiAlign
  ( SemiAlign (..)
  , fZm
  , zD
  , zd
  , zd'
  , fzd
  , zdm
  , fZd
  -- * Deprecated
  , align
  , alignWith
  , fzdm
  , fzd'
  , unionWith
  )
  where


import qualified Prelude


import P.Algebra.Semigroup
import P.Arrow.Cokleisli
import P.Arrow.Kleisli
import P.Bifunctor.Either
import P.Bifunctor.These
import P.Category
import P.Function.Flip
import P.Function.Compose
import P.Maybe
import P.Zip.WeakAlign


-- | Functors supporting an 'align' operation that takes the union of potentially non-uniform shapes.
--
-- === Laws
--
-- ==== Idempotency
--
-- prop> jn aln = m (jn Te)
--
-- ==== Commutativity
--
-- prop> aln x y = Sw < aln y x
--
-- ==== Associativity
--
-- prop> aln x (aln y z) = Asc < aln (aln x y) z
--
-- ==== With
--
-- prop> alW f = f <<< aln
--
-- ==== Functorality
--
-- prop> aln (f < x) (g < y) = bm f g < (aln x y)
--
class
  ( WeakAlign f
  )
    => SemiAlign f
  where
    {-# Minimal aln | alW #-}
    -- | 'aln' is the 'SemiAlign' equivalent of 'P.Zip.zp'.
    -- Where 'P.Zip.zp' produces tuples and thus requires two elements to make another,
    -- 'aln' produces 'These's and thus can produce elements when 'P.Zip.zp' cannot.
    --
    -- @Ti@ corresponds to elements from the first argument.
    -- @Ta@ corresponds to elements from the second argument.
    -- @Te@ corresponds to elements from both.
    --
    -- Thus where 'P.Zip.zp' takes the intersection,
    -- 'aln' takes the union of the shapes of the two structures.
    aln :: f a -> f b -> f (These a b)
    aln =
      alW id

    -- | 'alW' is the 'SemiAlign' equivalent of 'P.Zip.zW'.
    -- Where @'P.Zip.zW'@'s function takes two arguments and thus requires two elements to make another,
    -- @'alW'@'s function takes a `These` and thus can produce elements when 'P.Zip.zW' cannot.
    --
    -- @Ti@ corresponds to elements from the first argument.
    -- @Ta@ corresponds to elements from the second argument.
    -- @Te@ corresponds to elements from both.
    --
    -- Thus where 'P.Zip.zW' takes the intersection,
    -- 'alW' takes the union of the shapes of the two structures.
    alW :: (These a b -> c) -> f a -> f b -> f c
    alW func =
      func <<< aln


instance
  (
  )
    => SemiAlign []
  where
    aln [] [] =
      []
    aln (x : xs) [] =
      Ti x : Ti < xs
    aln [] (y : ys) =
      Ta y : Ta < ys
    aln (x : xs) (y : ys) =
      Te x y : aln xs ys

    alW func [] [] =
      []
    alW func (x : xs) [] =
      func (Ti x) : (func < Ti) < xs
    alW func [] (y : ys) =
      func (Ta y) : (func < Ta) < ys
    alW func (x : xs) (y : ys) =
      func (Te x y) : alW func xs ys



instance
  (
  )
    => SemiAlign Prelude.Maybe
  where
    aln Prelude.Nothing Prelude.Nothing =
      Prelude.Nothing
    aln (Prelude.Just x) Prelude.Nothing =
      Prelude.Just (Ti x)
    aln Prelude.Nothing (Prelude.Just y) =
      Prelude.Just (Ta y)
    aln (Prelude.Just x) (Prelude.Just y) =
      Prelude.Just (Te x y)


instance
  (
  )
    => SemiAlign ((->) k)
  where
    aln f g x =
      Te (f x) (g x)


instance
  (
  )
    => SemiAlign (Kleisli Maybe k)
  where
    aln (Kle k1) (Kle k2) =
      Kle $ \ x ->
        TfM (k1 x) (k2 x)


instance
  (
  )
    => SemiAlign (Cokleisli w k)
  where
    aln (Cok k1) (Cok k2) =
      Cok $ \ x ->
        Te (k1 x) (k2 x)


instance
  (
  )
    => SemiAlign (Either ())
  where
    alW f (Rit x) (Rit y) =
      Rit $ f $ Te x y
    alW f (Rit x) _ =
      Rit $ f $ Ti x
    alW f _ (Rit y) =
      Rit $ f $ Ta y
    alW f _ _ =
      Lef ()


{-# Deprecated align "Use aln instead" #-}
-- | Long version of 'aln'.
align ::
  ( SemiAlign f
  )
    => f a -> f b -> f (These a b)
align =
  aln


{-# Deprecated alignWith "Use alW instead" #-}
-- | Long version of 'alW'.
alignWith ::
  ( SemiAlign f
  )
    => (These a b -> c) -> f a -> f b -> f c
alignWith =
  alW


-- | 'P.Zip.zW' but with default values.
-- When the structures are of different sizes instead of trimming the long one
-- the short one is extended with the default.
zD ::
  ( SemiAlign f
  )
    => (a -> b -> c) -> a -> b -> f a -> f b -> f c
zD func defL defR =
  alW go
  where
    go (Ti x) =
      func x defR
    go (Ta y) =
      func defL y
    go (Te x y) =
      func x y


{-# Deprecated unionWith "Use zd' instead" #-}
-- | Long version of 'zd''.
unionWith ::
  ( SemiAlign f
  )
   => (a -> a -> a) -> f a -> f a -> f a
unionWith =
  zd'


-- | A zip which takes a binary function and two lists.
-- It defaults to the present value when a value is missing.
zd' ::
  ( SemiAlign f
  )
   => (a -> a -> a) -> f a -> f a -> f a
zd' func =
  alW go
  where
    go (Ti x) =
      x
    go (Ta x) =
      x
    go (Te x y) =
      func x y


-- | 'zD' but it takes one default which it uses for both lists.
zd ::
  ( SemiAlign f
  )
    => (a -> a -> b) -> a -> f a -> f a -> f b
zd func def =
  zD func def def


-- | Flip of 'zd'.
fzd ::
  ( SemiAlign f
  )
    => a -> (a -> a -> b) -> f a -> f a -> f b
fzd =
  F zd


{-# Deprecated fzd' "Use fZd instead" #-}
-- | Long version of 'fZd'
fzd' ::
  ( SemiAlign f
  )
    => f a -> (a -> a -> a) -> f a -> f a
fzd' =
  F zd'


-- | Flip of 'zd''.
fZd ::
  ( SemiAlign f
  )
    => f a -> (a -> a -> a) -> f a -> f a
fZd =
  F zd'


-- | A zip which takes two structures
-- It combines elements with the semigroup operation ('mp') and defaults to the present value when a value is missing.
--
-- If @a@ is a monoid this is the same as padding the lists to the same length with the identity.
--
-- ==== __Examples__
--
-- >>> zdm [[1,2],[3,4],[5,6]] [[7],[8]]
-- [[1,2,7],[3,4,8],[5,6]]
zdm ::
  ( Semigroup a
  , SemiAlign f
  )
    => f a -> f a -> f a
zdm =
  zd' mp


-- | Flip of 'zdm'.
fZm ::
  ( Semigroup a
  , SemiAlign f
  )
    => f a -> f a -> f a
fZm =
  F zdm


{-# Deprecated fzdm "Use fZm instead" #-}
-- | Long version of 'fZm'.
fzdm ::
  ( Semigroup a
  , SemiAlign f
  )
    => f a -> f a -> f a
fzdm =
  F zdm
