{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language FunctionalDependencies #-}
module P.First
  where


import Prelude
  ( Integral
  )
import qualified Prelude


import P.Aliases
import P.Applicative
import P.Arithmetic
import P.Enum
import P.Function.Flip
import P.Ord


{-# Deprecated cons "Use K instead" #-}
-- | Structures that have some sense of a "first" element, which can be removed or added.
class Firstable f g
  | f -> g
  where
    -- | Break a structure into a head and tail if possible.
    --
    -- Mostly internal function used to define 'K'.
    _uC :: f a -> Maybe' (a, g a)

    -- | Drops the first element of a list like structure.
    --
    -- Like 'Data.List.tail' but it doesn't error on the empty list.
    tl :: f a -> g a
    tl xs =
      case
        _uC xs
      of
        [] ->
          Prelude.error "tail of empty structure"
        (_, ys) : _ ->
          ys

    -- | Gives the first element of a list like structure if one can be found.
    -- Errors otherwise.
    he :: f a -> a
    he xs =
      case
        _uC xs
      of
        [] ->
          Prelude.error "head of empty structure"
        (y, _) : _ ->
          y

    -- | Long version of 'K'.
    cons :: a -> g a -> f a

instance Firstable [] [] where
  _uC [] =
    []
  _uC (x : xs) =
    [(x, xs)]

  tl [] =
    []
  tl (_ : xs) =
    xs

  cons =
    (:)

{-# Complete K, [] #-}
-- | List cons.
pattern K ::
  ( Firstable f g
  )
    => a -> g a -> f a
pattern K x xs <- (_uC -> [(x, xs)]) where
  K =
    cons

{-# Complete Fk, [] #-}
-- | Flip of 'K'.
pattern Fk ::
  ( Firstable f g
  )
    => g a -> a -> f a
pattern Fk xs x =
  K x xs

-- | Pattern for things which cannot be unconsed.
pattern UD ::
  ( Firstable f g
  )
    => f a
pattern UD <- (_uC -> [])

{-# Deprecated tail "Use tl instead" #-}
-- | Long version of 'tl'.
-- Removes the first element of a list like structure.
tail ::
  ( Firstable f g
  )
    => f a -> g a
tail =
  tl

{-# Deprecated head "Use he instead" #-}
-- | Long version of 'he'.
-- Gives the first element of a list like structure.
head ::
  ( Firstable f g
  )
    => f a -> a
head =
  he

{-# Deprecated take "Use tk instead" #-}
-- | Long version of 'tk'.
take ::
  ( Integral i
  , Firstable f f
  )
    => i -> f a -> List a
take =
  tk

-- | Given @n@ and @xs@ gives a prefix of length @n@ from @xs@, or all of @xs@ if @n@ is greater than the length of @xs@.
--
-- More general version of 'Data.List.genericTake'.
tk ::
  ( Integral i
  , Firstable f f
  )
    => i -> f a -> List a
tk n (K x xs)
  | n Prelude.> 0
  =
    x : tk (Pv n) xs
tk n _ =
  []

-- | Infix version of 'tk'.
(#<) ::
  ( Integral i
  , Ord i
  , Firstable f f
  )
    => i -> f a -> List a
(#<) =
  tk

-- | Flip of 'tk'.
ft ::
  ( Integral i
  , Ord i
  , Firstable f f
  )
    => f a -> i -> List a
ft =
  F tk

-- | Takes the longest prefix of a list where all elements satisfy a certain predicate.
--
-- Equivalent to 'Data.List.takeWhile'.
tW ::
  ( Firstable f f
  )
    => Predicate a -> f a -> List a
tW pred (K x xs)
  | pred x
  =
    x : tW pred xs
tW _ _ =
  []

-- | Flip of 'tW'.
ftW ::
  ( Firstable f f
  )
    => f a -> Predicate a -> List a
ftW =
  F tW

{-# Deprecated drop, genericDrop "Use dr instead" #-}
-- | Long version of 'dr'.
-- Take @i@ and remove the first @i@ elements from a list.
drop ::
  ( Integral i
  , Firstable f f
  )
    => i -> f a -> f a
drop =
  dr

-- | Long version of 'dr'.
-- Take @i@ and remove the first @i@ elements from a list.
genericDrop ::
  ( Integral i
  , Firstable f f
  )
    => i -> f a -> f a
genericDrop =
  dr

-- | Take @i@ and remove the first @i@ elements from a list.
-- Equivalent to 'Data.List.genericDrop'.
-- More general version of 'Data.List.drop'.
dr ::
  ( Integral i
  , Firstable f f
  )
    => i -> f a -> f a
dr n (K x xs)
  | n Prelude.> 0
  =
    dr (Pv n) xs
dr _ xs =
  xs

-- | Infix version of 'dr'.
(#>) ::
  ( Integral i
  , Firstable f f
  )
    => i -> f a -> f a
(#>) =
  dr

-- | Flip of 'dr'.
fd ::
  ( Integral i
  , Firstable f f
  )
    => f a -> i -> f a
fd =
  F dr

{-# Deprecated dropWhile "Use dW instead" #-}
-- | Long version of 'dW'.
-- Removes the largest prefix satisfying a predicate and returns the remaining.
dropWhile ::
  (
  )
    => Predicate a -> List a -> List a
dropWhile =
  dW

-- | Removes the largest prefix satisfying a predicate and returns the remaining.
--
-- Equivalent to 'Data.List.dropWhile'.
dW ::
  ( Firstable f f
  )
    => Predicate a -> f a -> f a
dW pred (K x xs)
  | pred x
  =
    dW pred xs
dW _ xs =
  xs

-- | Flip of 'dW'.
fdW ::
  ( Firstable f f
  )
    => f a -> Predicate a -> f a
fdW =
  F dW

-- | Lifted cons.
(*:*) ::
  ( Applicative f
  , Firstable g h
  )
    => f a -> f (h a) -> f (g a)
(*:*) =
  l2 K
