{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language TypeOperators #-}
module P.Char
  ( Char
  , iC
  , niC
  , pattern IC
  , iW
  , niW
  , pattern IW
  , iL
  , niL
  , pattern IL
  , iU
  , niU
  , pattern IU
  , α
  , nα
  , pattern Α
  , iA
  , niA
  , pattern IA
  , iP
  , niP
  , pattern IP
  , iD
  , niD
  , pattern IS
  , iSy
  , nSy
  , pattern ID
  , pattern TU
  , mtU
  , pattern TL
  , mtL
  , pattern Or
  , pattern Ch
  , pattern Nl
  , pattern NL
  , pattern SC
  , pattern Sm
  , scI
  -- * Deprecated
  , isControl
  , isSpace
  , isLower
  , isUpper
  , isAlpha
  , isLetter
  , isAlphaNum
  , isPrint
  , isDigit
  , isSymbol
  , toLower
  , toUpper
  )
  where


import qualified Prelude
import Prelude
  ( Char
  , Integral
  , String
  )
import qualified Data.Char


import P.Bool
import P.Category.Iso
import P.Function.Compose
import P.Pattern


-- | Newline character.
pattern Nl ::
  (
  )
    => Char
pattern Nl =
  '\n'


-- | Newline string.
pattern NL ::
  (
  )
    => String
pattern NL =
  "\n"

{-# Deprecated isControl "Use iC instead" #-}
-- | Long version of 'iC'.
isControl ::
  (
  )
    => Char -> Bool
isControl =
  iC


-- | Equivalent to 'Data.Char.isControl'.
iC ::
  (
  )
    => Char -> Bool
iC =
  Data.Char.isControl


-- | Determines whether a character is not a control character.
niC ::
  (
  )
    => Char -> Bool
niC =
  n < iC


-- | A pattern that matches on a control character.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it's a control character and returns a space otherwise.
--
-- @
-- f x@IC =
--   x
-- f y =
--   ' '
-- @
pattern IC :: Char
pattern IC <- (iC -> T)


{-# Deprecated isSpace "Use iW instead" #-}
-- | Long version of 'iW'.
isSpace ::
  (
  )
    => Char -> Bool
isSpace =
  iW


-- | Equivalent to 'Data.Char.isSpace'.
iW ::
  (
  )
    => Char -> Bool
iW =
  Data.Char.isSpace


-- | Determines whether a character is not whitespace.
niW ::
  (
  )
    => Char -> Bool
niW =
  n < iW


-- | A pattern that matches on whitespace.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it's whitespace and returns a space otherwise.
--
-- @
-- f x@IW =
--   x
-- f y =
--   ' '
-- @
pattern IW :: Char
pattern IW <- (iW -> T)


{-# Deprecated isLower "Use iL instead" #-}
-- | Long version of 'iL'.
isLower ::
  (
  )
    => Char -> Bool
isLower =
  iL


-- | Equivalent to 'Data.Char.isLower'.
iL ::
  (
  )
    => Char -> Bool
iL =
  Data.Char.isLower


-- | Determines whether a character is not lowercase.
niL ::
  (
  )
    => Char -> Bool
niL =
  n < iL


-- | A pattern that matches on lowercase characters.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it's lowercase and returns a space otherwise.
--
-- @
-- f x@IL =
--   x
-- f y =
--   ' '
-- @
pattern IL :: Char
pattern IL <- (iL -> T)


{-# Deprecated isUpper "Use iU instead" #-}
-- | Long version of 'iU'.
isUpper ::
  (
  )
    => Char -> Bool
isUpper =
  iU


-- | Equivalent to 'Data.Char.isUpper'.
iU ::
  (
  )
    => Char -> Bool
iU =
  Data.Char.isUpper


-- | Determines whether a character is not uppercase.
niU ::
  (
  )
    => Char -> Bool
niU =
  n < iU


-- | A pattern that matches on uppercase characters.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it's uppercase and returns a space otherwise.
--
-- @
-- f x@IU =
--   x
-- f y =
--   ' '
-- @
pattern IU :: Char
pattern IU <- (iU -> T)


{-# Deprecated isAlpha "Use α instead" #-}
-- | Long version of 'α'.
isAlpha ::
  (
  )
    => Char -> Bool
isAlpha =
  α


{-# Deprecated isLetter "Use α instead" #-}
-- | Long version of 'α'.
isLetter ::
  (
  )
    => Char -> Bool
isLetter =
  α


-- | Determines if a character is a letter.
--
-- Returns true if the unicode character corresponds to any of the following general categories as defined by the unicode standard.
--
-- * @Uppercase_Letter@
-- * @Lowercase_Letter@
-- * @Titlecase_Letter@
-- * @Modifier_Letter@
-- * @Other_Letter@
--
-- Equivalent to 'Data.Char.isAlpha' and 'Data.Char.isLetter'.
--
-- Warning depending on your font this may look like @a@.
-- It is not, it is the greek letter alpha.
α ::
  (
  )
    => Char -> Bool
α =
  Data.Char.isAlpha


-- | Determines whether a character is not alphabetic.
--
-- Warning depending on your font this may look like @na@.
nα ::
  (
  )
    => Char -> Bool
nα =
  n < α


-- | A pattern that matches on alphabetic characters.
--
-- Despite its appearance, the name of this function is not an uppercase latin @A@.
-- It is a uppercase greek alpha!
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it is alphabetic and returns a space otherwise.
--
-- @
-- f x@Α =
--   x
-- f y =
--   ' '
-- @
pattern Α :: Char
pattern Α <- (α -> T)


{-# Deprecated isAlphaNum "Use iA instead" #-}
-- | Long version of 'iA'.
isAlphaNum ::
  (
  )
    => Char -> Bool
isAlphaNum =
  iA


-- | Equivalent to 'Data.Char.isAlphaNum'.
iA ::
  (
  )
    => Char -> Bool
iA =
  Data.Char.isAlphaNum


-- | Determines whether a character is not alphanumeric.
niA ::
  (
  )
    => Char -> Bool
niA =
  n < iA


-- | A pattern that matches on alpha-numeric characters.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it is alpha-numeric and returns a space otherwise.
--
-- @
-- f x@IA =
--   x
-- f y =
--   ' '
-- @
pattern IA :: Char
pattern IA <- (iA -> T)


{-# Deprecated isPrint "Use iP instead" #-}
-- | Long version of 'iP'.
isPrint ::
  (
  )
    => Char -> Bool
isPrint =
  iP


-- | Equivalent to 'Data.Char.isPrint'.
iP ::
  (
  )
    => Char -> Bool
iP =
  Data.Char.isPrint


-- | Determines whether a character is not printable.
niP ::
  (
  )
    => Char -> Bool
niP =
  n < iP


-- | A pattern that matches on printable characters.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it is printable and returns a space otherwise.
--
-- @
-- f x@IA =
--   x
-- f y =
--   ' '
-- @
pattern IP :: Char
pattern IP <- (iP -> T)


{-# Deprecated isDigit "Use iD instead" #-}
-- | Long version of 'iD'.
isDigit ::
  (
  )
    => Char -> Bool
isDigit =
  iP


-- | Equivalent to 'Data.Char.isDigit'.
iD ::
  (
  )
    => Char -> Bool
iD =
  Data.Char.isDigit


-- | Determines whether a character is not a digit.
niD ::
  (
  )
    => Char -> Bool
niD =
  n < iD


-- | A pattern that matches on symbol characters.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returns it if it's symbol and returns a space otherwise.
--
-- @
-- f x@IS =
--   x
-- f y =
--   ' '
-- @
pattern IS :: Char
pattern IS <- (iSy -> T)


{-# Deprecated isSymbol "Use iSy instead" #-}
-- | Long version of 'iSy'.
isSymbol ::
  (
  )
    => Char -> Bool
isSymbol =
  Data.Char.isSymbol


-- | Equivalent to 'Data.Char.isSymbol'.
iSy ::
  (
  )
    => Char -> Bool
iSy =
  Data.Char.isSymbol


-- | Determines whether a character is not a symbol.
nSy ::
  (
  )
    => Char -> Bool
nSy =
  n < iSy


-- |
-- === Function
-- As a function this takes a integer and gives the character representing it's last digit.
--
-- === Pattern
-- A pattern that matches on the characters `0-9`.
--
-- If you want the value of the character use an as pattern (@\@@).
--
-- ==== __Examples__
--
-- Takes a character returning the empty list if it is not a digit and the value otherwise
--
-- @
-- f (ID x) =
--   [ x ]
-- f y =
--   []
-- @
pattern ID ::
  ( Integral i
  )
    => i -> Char
pattern ID x <- (iD -> T) := (Prelude.fromInteger < Prelude.toInteger < Data.Char.digitToInt -> x) where
  ID =
    Prelude.last < Prelude.show < Prelude.toInteger


{-# Deprecated toUpper "Use TU instead" #-}
-- | Long version of 'TU'.
toUpper ::
  (
  )
    => Char -> Char
toUpper =
  TU


-- |
-- === Function
-- As a function this converts a character to its uppercase variant.
--
-- Equivalent to 'Data.Char.toUpper'.
--
-- === Pattern
--
-- As a pattern this matches on any character with it's lower case variant.
--
-- ==== __Examples__
--
-- You can implment a 'Data.Char.toLower' function as such:
--
-- @
-- toLower :: Char -> Char
-- toLower (TU x) =
--   x
-- @
--
pattern TU :: Char -> Char
pattern TU x <- (Data.Char.toLower -> x) where
  TU =
    Data.Char.toUpper


-- | Maps 'TU' across a 'Functor'.
mtU ::
  ( Functor f
  )
    => f Char -> f Char
mtU =
  m TU


{-# Deprecated toLower "Use TL instead" #-}
-- | Long version of 'TL'.
toLower ::
  (
  )
    => Char -> Char
toLower =
  TL


-- |
-- === Function
-- As a function this converts a character to its lowercase variant.
--
-- Equivalent to 'Data.Char.toLower'.
--
-- === Pattern
--
-- As a pattern this matches on any character with it's uppercase variant.
--
-- ==== __Examples__
--
-- You can implment a 'Data.Char.toUpper' function as such:
--
-- @
-- toUpper :: Char -> Char
-- toUpper (TL x) =
--   x
-- @
--
pattern TL :: Char -> Char
pattern TL x <- (Data.Char.toUpper -> x) where
  TL =
    Data.Char.toLower


-- | Maps 'TL' across a 'Functor'.
mtL ::
  ( Functor f
  )
    => f Char -> f Char
mtL =
  m TL


-- |
-- === Function
-- Takes a code point and gives the code-point at that character.
--
-- More general version of 'Data.Char.ord'.
--
-- === Pattern
--
-- Matches the character that gives the matched code-point.
pattern Or ::
  ( Integral i
  )
    => Char -> i
pattern Or c <- (_ch -> c) where
  Or =
    Prelude.fromInteger < Prelude.toInteger < Data.Char.ord


-- | Internal function.
-- Should not be exported equivalent to 'Ch'.
_ch ::
  ( Integral i
  )
    => i -> Char
_ch =
  Data.Char.chr < Prelude.fromInteger < Prelude.toInteger


{-# Complete Ch #-}
-- |
-- === Function
-- Takes a code point and gives the character at that code-point.
--
-- More general version of 'Data.Char.chr'.
--
-- === Pattern
--
-- Matches the code-point that gives the matched character.
pattern Ch ::
  ( Integral i
  )
    => i -> Char
pattern Ch c <- (Or -> c) where
  Ch =
    _ch


-- | Internal version of 'SC'.
_sc x
  | iU x
  =
    TL x
  | iL x
  =
    TU x
  | Prelude.otherwise
  =
    x


{-# Complete SC #-}
-- | Swap cases.
-- Make uppercase characters lowercase and lowercase characters uppercase.
pattern SC ::
  (
  )
    => Char -> Char
pattern SC c <- (_sc -> c) where
  SC =
    _sc


{-# Complete Sm #-}
-- | A mapped version of 'SC'.
pattern Sm ::
  ( Functor f
  )
    => f Char -> f Char
pattern Sm c <- (m SC -> c) where
  Sm =
    m SC


-- | An isomorphism that swaps cases.
scI ::
  (
  )
    => Char <-> Char
scI =
  Iso SC SC
