module P.Foldable
  ( Foldable
  , lF
  , flF
  , lF'
  , fLF
  , fo
  , mF
  , fmF
  , (^<)
  , mf
  , fmf
  , rF
  , frF
  , rHM
  , frm
  , lHM
  , flm
  , lhM
  , fLm
  , rH
  , frH
  , lH
  , flH
  , e
  , (?>)
  , fe
  , ne
  , (?<)
  , fE
  , (!!)
  , kx
  , fkx
  , cn
  , fcn
  , ce
  , fce
  , cne
  , fcN
  , cnT
  , cnn
  , tL
  , rFp
  , fRp
  , lFp
  , fLp
  , sm
  , pd
  -- * Scans
  , lS
  , flS
  , rS
  , frS
  , ls
  , fls
  , rs
  , frs
  , lz
  , flz
  , rz
  , frz
  -- * Deprecated
  , foldr
  , foldl
  , foldl'
  , foldMap
  , foldr1
  , foldl1
  , toList
  , elem
  , notElem
  , count
  , scanl
  , scanr
  )
  where


import qualified Prelude
import Prelude
  ( Foldable
  , Integral
  , Num
  )
import qualified Data.Foldable
import qualified Data.List


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Aliases
import P.Arithmetic
import P.Arithmetic.Pattern
import P.Bool
import P.Category
import P.Eq
import P.Function.Flip
import P.Function.Compose


{-# Deprecated foldl "Use lF instead" #-}
-- | Long version of 'lF'.
foldl ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> b
foldl =
  lF


-- | The left fold.
--
-- Equivalent to 'Data.List.foldl'.
lF ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> b
lF =
  Data.List.foldl


-- | Flip of 'lF'.
flF ::
  ( Foldable t
  )
    => b -> (b -> a -> b) -> t a -> b
flF =
  F lF


{-# Deprecated foldl' "Use lF' instead" #-}
-- | Long version of 'lF''.
foldl' ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> b
foldl' =
  lF


-- | Strict left fold.
-- Equivalent to 'Data.List.foldl''.
lF' ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> b
lF' =
  Data.List.foldl'


-- | Flip of 'lF''.
fLF ::
  ( Foldable t
  )
    => b -> (b -> a -> b) -> t a -> b
fLF =
  F lF'


-- | Given a structure whose elements are part of a monoid,
-- combine them together with the monoid operation '(P.Algebra.Monoid.<>)'.
--
-- Equivalent to 'Data.Foldable.fold'.
-- More general version of 'Data.List.concat'.
-- More general version of 'Prelude.mconcat'.
-- More general version of 'Data.List.and'.
fo ::
  ( Monoid m
  , Foldable t
  )
    => t m -> m
fo =
  rF mp i


{-# Deprecated foldMap "Use mF" #-}
-- | Long version of 'mF'.
foldMap ::
  ( Monoid m
  , Foldable t
  )
    => (a -> m) -> t a -> m
foldMap =
  mF


-- | Perform a map and combine with 'fo'.
--
-- Eqivalent to 'Data.Foldable.foldMap'.
-- More general version of 'Data.List.concatMap'.
-- More general version of 'Data.Foldable.all'.
mF ::
  ( Monoid m
  , Foldable t
  )
    => (a -> m) -> t a -> m
mF f =
  rF (mp < f) i


-- | Flip of 'mF'.
fmF ::
  ( Monoid m
  , Foldable t
  )
    => t a -> (a -> m) -> m
fmF =
  F mF


-- | Infix of 'mF'.
(^<) ::
  ( Monoid m
  , Foldable t
  )
    => (a -> m) -> t a -> m
(^<) =
  mF


-- |
-- Equivalent to 'Data.Foldable.foldMap''.
mf ::
  ( Monoid m
  , Foldable t
  )
    => (a -> m) -> t a -> m
mf f =
  Data.Foldable.foldl' (\ acc a -> acc <> f a) i


-- | Flip of 'mf'.
fmf ::
  ( Monoid m
  , Foldable t
  )
    => t a -> (a -> m) -> m
fmf =
  F mf


{-# Deprecated foldr "Use rF instead" #-}
-- | Long version of 'rF'.
foldr ::
  ( Foldable t
  )
    => (a -> b -> b) -> b -> t a -> b
foldr =
  rF


-- | The right fold.
--
-- Equivalent to 'Data.List.foldr'
rF ::
  ( Foldable t
  )
    => (a -> b -> b) -> b -> t a -> b
rF =
  Data.List.foldr


-- | Flip of 'rF'.
frF ::
  ( Foldable t
  )
    => b -> (a -> b -> b) -> t a -> b
frF =
  F rF


{-# Deprecated scanl "Use lS instead" #-}
-- | Long version of 'lS'.
scanl ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> List b
scanl =
  lS


-- | The left scan.
--
-- See 'P.Scan.sc' for a more general version of this.
--
-- More general version of 'Prelude.scanl'.
lS ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> List b
lS func start =
  Prelude.scanl func start < tL


-- | Flip of 'lS'.
flS ::
  ( Foldable t
  )
    => b -> (b -> a -> b) -> t a -> List b
flS =
  F lS


{-# Deprecated scanr "Use rS instead" #-}
-- | Long version of 'rS'.
scanr ::
  ( Foldable t
  )
    => (a -> b -> b) -> b -> t a -> List b
scanr =
  rS


-- | The right scan.
--
-- See 'P.Scan.sc' for a more general version of this.
--
-- More general version of 'Prelude.scanr'.
rS ::
  ( Foldable t
  )
    => (a -> b -> b) -> b -> t a -> List b
rS func start =
  Prelude.scanr func start < tL


-- | Flip of 'rS'.
frS ::
  ( Foldable t
  )
    => b -> (a -> b -> b) -> t a -> List b
frS =
  F rS


-- | A left scan which does not include the first value in the result.
ls ::
  ( Foldable t
  )
    => (b -> a -> b) -> b -> t a -> List b
ls func start foldable =
  Prelude.drop 1 $ lS func start foldable


-- | Flip of 'ls'.
fls ::
  ( Foldable t
  )
    => b -> (b -> a -> b) -> t a -> List b
fls =
  F ls


-- | A right scan which does not include the initial value in the result.
--
-- ==== __Examples__
--
-- Get all non-empty suffixes of a list
--
-- >>> rs (:) [] [1,2,3,4]
-- [[1,2,3,4],[2,3,4],[3,4],[4]]
--
rs ::
  ( Foldable t
  )
    => (a -> b -> b) -> b -> t a -> List b
rs func start foldable =
  Prelude.init $ rS func start foldable


-- | Flip of 'rs'.
frs ::
  ( Foldable t
  )
    => b -> (a -> b -> b) -> t a -> List b
frs =
  F rs


-- | A left scan using the first element of the input as the start.
--
-- Equivalent to 'Prelude.scanl1'
lz ::
  (
  )
    => (a -> a -> a) -> List a -> List a
lz =
  Prelude.scanl1


-- | Flip of 'lz'.
flz ::
  (
  )
    => List a -> (a -> a -> a) -> List a
flz =
  F lz


-- | A right scan using the first element of the input as the start.
--
-- Equivalent to 'Prelude.scanr1'
rz ::
  (
  )
    => (a -> a -> a) -> List a -> List a
rz =
  Prelude.scanr1


-- | Flip of 'rz'.
frz ::
  (
  )
    => List a -> (a -> a -> a) -> List a
frz =
  F rz


-- | A right fold using the first element of the input as the start.
-- If the input is empty it returns an empty output otherwise it returns the result as a singleton.
rHM ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> Maybe' a
rHM f =
  rF go []
  where
    go y [] =
      [y]
    go y [x] =
      [f x y]


-- | Flip of 'rHM'.
frm ::
  ( Foldable t
  )
    => t a -> (a -> a -> a) -> Maybe' a
frm =
  f' rHM


-- | A left fold using the last element of the input as the start.
-- If the input is empty it returns an empty output otherwise it returns the result as a singleton.
lHM ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> Maybe' a
lHM f =
  lF go []
  where
    go [] y =
      [y]
    go [x] y =
      [f x y]


-- | Flip of 'lHM'.
flm ::
  ( Foldable t
  )
    => t a -> (a -> a -> a) -> Maybe' a
flm =
  f' lHM


-- | Like 'lHM' but with strict application of the operator.
lhM ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> Maybe' a
lhM f =
  lF' go []
  where
    go [] y =
      [y]
    go [x] y =
      [f x y]


-- | Flip of 'lhM'.
fLm ::
  ( Foldable t
  )
    => t a -> (a -> a -> a) -> Maybe' a
fLm =
  f' lhM


{-# Deprecated foldr1 "Use rH instead" #-}
-- | Long version of 'rH'.
foldr1 ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> a
foldr1 =
  rH


-- | A right fold using the first element of the input as the start.
--
-- Errors if the structure is empty.
rH ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> a
rH =
  Prelude.foldr1


-- | Flip of 'rH'.
frH ::
  ( Foldable t
  )
    => t a -> (a -> a -> a) -> a
frH =
  f' rH


{-# Deprecated foldl1 "Use lH instead" #-}
-- | Long version of 'lH'.
foldl1 ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> a
foldl1 =
  lH


-- | A left fold using the last element of the input as the start.
lH ::
  ( Foldable t
  )
    => (a -> a -> a) -> t a -> a
lH =
  Prelude.foldl1


-- | Flip of 'lH'.
flH ::
  ( Foldable t
  )
    => t a -> (a -> a -> a) -> a
flH =
  F lH


{-# Deprecated elem "Use e instead" #-}
-- | Long version of 'e'.
elem ::
  ( Foldable t
  , Eq a
  )
    => a -> t a -> Bool
elem =
  e


-- | Determines if an element is present in a foldable.
--
-- The negation of 'ne'.
-- Equivalent to 'Data.List.elem'.
e ::
  ( Foldable t
  , Eq a
  )
    => a -> t a -> Bool
e =
  Data.List.elem


-- | Infix version of 'e'.
(?>) ::
  ( Foldable t
  , Eq a
  )
    => a -> t a -> Bool
(?>) =
  e


-- | Flip of 'e'.
fe ::
  ( Foldable t
  , Eq a
  )
    => t a -> a -> Bool
fe =
  F e


{-# Deprecated notElem "Use ne instead" #-}
-- | Long version of 'ne'.
notElem ::
  ( Foldable t
  , Eq a
  )
    => a -> t a -> Bool
notElem =
  ne


-- |
-- The negation of 'e'.
-- Equivalent to 'Data.List.notElem'.
ne ::
  ( Foldable t
  , Eq a
  )
    => a -> t a -> Bool
ne =
  mm Prelude.not e


-- | Infix of 'ne'.
(?<) ::
  ( Foldable t
  , Eq a
  )
    => a -> t a-> Bool
(?<) =
  ne


-- | Flip of 'ne'.
fE ::
  ( Foldable t
  , Eq a
  )
    => t a -> a -> Bool
fE =
  F ne


-- | Safe index.
--
-- Indexes a list returning @[]@ if out of bounds.
(!!) ::
  ( Integral i
  , Foldable t
  )
    => t a -> i -> Maybe' a
structure !! index =
  go index $ tL structure
  where
    go _ [] =
      []
    go N _ =
      []
    go 0 (x : _) =
      [x]
    go index' (_ : xs) =
      xs !! (index' - 1)


-- | Safe index.
--
-- Indexes a list returning @[]@ if out of bounds.
--
-- Prefix of '(!!)'
kx ::
  ( Integral i
  , Foldable t
  )
    => t a -> i -> Maybe' a
kx =
  (!!)


-- | Flip of 'kx'.
fkx ::
  ( Integral i
  , Foldable t
  )
    => i -> t a -> Maybe' a
fkx =
  F kx


{-# Deprecated count "Use cn instead" #-}
-- | Long version of 'cn'.
count ::
  ( Foldable t
  , Integral i
  )
    => Predicate a -> t a -> i
count =
  cn


-- | Counts the number of elements of a structure satisfy a predicate.
cn ::
  ( Foldable t
  , Integral i
  )
    => Predicate a -> t a -> i
cn sig =
  rF (\ a count -> if sig a then count + 1 else count) 0


-- | Flip of 'cn'.
fcn ::
  ( Foldable t
  , Integral i
  )
    => t a -> Predicate a -> i
fcn =
  F cn


-- | Counts the number of times an element appears in a structure.
ce ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => a -> t a -> i
ce =
  cn < (==)


-- | Flip of 'ce'.
fce ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => t a -> a -> i
fce =
  F ce


-- | Counts the number of elements in a structure not equal to a certain value.
cne ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => a -> t a -> i
cne =
  cn < nq


-- | Flip of 'cne'.
fcN ::
  ( Foldable t
  , Eq a
  , Integral i
  )
    => t a -> a -> i
fcN =
  F cne


-- | Counts the number of 'True' elements in a structure.
cnT ::
  ( Integral i
  , Foldable t
  )
    => t Bool -> i
cnT =
  cn id


-- | Counts the number of 'False' elements in a structure.
cnn ::
  ( Integral i
  , Foldable t
  )
    => t Bool -> i
cnn =
  cn n


{-# Deprecated toList "Use tL instead" #-}
-- | Long version of 'tL'.
toList ::
  ( Foldable t
  )
    => t a -> List a
toList =
  tL


-- | Creates a list which folds in the same way as the structure.
--
-- Equivalent to 'Data.Foldable.toList'.
tL ::
  ( Foldable t
  )
    => t a -> List a
tL =
  Data.Foldable.toList


-- | Gets the first element of a foldable structure, defaulting to the given value if the structure is empty.
rFp ::
  ( Foldable t
  )
    => a -> t a -> a
rFp =
  rF Prelude.const


-- | Flip of 'rFp'.
fRp ::
  ( Foldable t
  )
    => t a -> a -> a
fRp =
  f' rFp


-- | Gets the last element of a foldable structure, defaulting to the given value if the structure is empty.
lFp ::
  ( Foldable t
  )
    => a -> t a -> a
lFp =
  lF $ f' Prelude.const


-- | Flip of 'lFp'.
fLp ::
  ( Foldable t
  )
    => t a -> a -> a
fLp =
  f' lFp


-- |
-- Equivalent to 'Data.List.sum'.
sm ::
  ( Foldable t
  , Num a
  )
    => t a -> a
sm =
  Data.List.sum


-- |
-- Equivalent to 'Data.List.product'.
pd ::
  ( Foldable t
  , Num a
  )
    => t a -> a
pd =
  Data.List.product
