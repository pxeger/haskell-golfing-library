{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-|
Module :
  P.Constants.Language
Description :
  Constants having to do with natural language
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Constants.Language
  ( pattern W5
  , pattern W5'
  , pattern W6
  , pattern W6'
  , pattern W7
  , pattern W7'
  )
  where


-- | The 5 vowel letters in lower case, @"aeiou"@.
--
-- These are the IPA symbols for the commonly used 5 vowel system.
pattern W5 ::
  (
  )
    => String
pattern W5 <- "aeiou" where
  W5 =
    "aeiou"


-- | The 5 vowel letters in upper case, @"AEIOU"@.
pattern W5' ::
  (
  )
    => String
pattern W5' <- "AEIOU" where
  W5' =
    "AEIOU"


-- | 6 vowel letters in lower case, @"aeiouy"@.
-- Includes @y@.
pattern W6 ::
  (
  )
    => String
pattern W6 <- "aeiouy" where
  W6 =
    "aeiouy"


-- | 6 vowel letters in lower case, @"AEIOUY"@.
-- Includes @Y@.
pattern W6' ::
  (
  )
    => String
pattern W6' <- "AEIOUY" where
  W6' =
    "AEIOUY"


-- | 7 vowel letters in lower case, @"aeiouwy"@.
-- Includes @y@ and @w@. @w@ is used as a vowel in English in obscure Welsh loan words.
pattern W7 ::
  (
  )
    => String
pattern W7 <- "aeiouwy" where
  W7 =
    "aeiouwy"


-- | 7 vowel letters in upper case, @"AEIOUWY"@.
-- Includes @Y@ and @W@. @W@ is used as a vowel in English in obscure Welsh loan words.
pattern W7' ::
  (
  )
    => String
pattern W7' <- "AEIOUWY" where
  W7' =
    "AEIOUWY"
