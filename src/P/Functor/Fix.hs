{-# Language RankNTypes #-}
module P.Functor.Fix
  ( Fix (..)
  , hFx
  , fFx
  , uFx
  , fxF
  -- * Deprecated
  , hoistFix
  , foldFix
  , unfoldFix
  )
  where

import Prelude
  (
  )
import qualified Prelude

import Data.Functor.Classes
  ( Show1 (..)
  )

import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Applicative
import P.Category
import P.Eq
import P.Function.Compose
import P.Ord
import P.Show

-- | The fixed point combinator on types.
newtype Fix f =
  Fix
    { _uF ::
      f (Fix f)
    }

instance
  ( Eq1 f
  )
    => Eq (Fix f)
  where
    Fix f == Fix g =
      q1 eq f g

instance
  ( Ord1 f
  )
    => Ord (Fix f)
  where
    cp (Fix f) (Fix g) =
      lcp cp f g

instance
  ( Show1 f
  )
    => Show (Fix f)
  where
    showsPrec n (Fix f) =
      liftShowsPrec Prelude.showsPrec Prelude.showList n f

instance
  ( Applicative f
  )
    => Semigroup (Fix f)
  where
    Fix xs <> Fix ys =
      Fix $ xs *^* ys

instance
  ( Applicative f
  )
    => Monoid (Fix f)
  where
    mempty =
      fxF p

{-# Deprecated hoistFix "Use hFx instead" #-}
-- | Long version of 'hFx'.
hoistFix ::
  ( Functor g
  )
    => (forall x. f x -> g x) -> Fix f -> Fix g
hoistFix =
  hFx

-- | Lift a natural transformation on functors to a function on @Fix@.
hFx ::
  ( Functor g
  )
    => (forall x. f x -> g x) -> Fix f -> Fix g
hFx func (Fix x) =
  Fix $ hFx func < func x

{-# Deprecated foldFix "Use fFx instead" #-}
-- | Long version of 'fFx'.
foldFix ::
  ( Functor f
  )
    => (f a -> a) -> Fix f -> a
foldFix =
  fFx

-- | Uses a function to fold a fix into a single value.
fFx ::
  ( Functor f
  )
    => (f a -> a) -> Fix f -> a
fFx func =
  func < m (fFx func) < _uF

{-# Deprecated unfoldFix "Use uFx instead" #-}
-- | Long version of 'uFx'.
unfoldFix ::
  ( Functor f
  )
    => (a -> f a) -> a -> Fix f
unfoldFix =
  uFx

-- | Uses a fuction to unfold a value into a fix.
uFx ::
  ( Functor f
  )
    => (a -> f a) -> a -> Fix f
uFx func =
  Fix < m (uFx func) < func

-- | Generates a 'Fix' as the fixed point of a function.
fxF ::
  (
  )
    => (Fix f -> f (Fix f)) -> Fix f
fxF func =
  Fix $ func $ fxF func
