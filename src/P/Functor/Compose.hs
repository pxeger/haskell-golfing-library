{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language PolyKinds #-}
module P.Functor.Compose
  ( Comp (Co)
  , pattern UC
  , pattern LC
  , mR
  , pattern LAC
  , pattern RAC
  )
  where

import qualified Prelude
import Prelude
  (
  )

import qualified Control.Applicative
import qualified Data.Foldable
import qualified Data.Functor.Classes
import qualified Data.Traversable

import P.Applicative
import P.Applicative.Ring
import P.Applicative.Unpure
import P.Bifunctor.Either
import P.Bifunctor.These
import P.Category
import P.Comonad
import P.Distributive
import P.Eq
import P.Foldable
import P.Function
import P.Function.Compose
import P.Functor.Identity.Class
import P.Monad
import P.Ord
import P.Reverse
import P.Traversable
import P.Zip
import P.Zip.SemiAlign
import P.Zip.UnitalZip
import P.Zip.WeakAlign

{-# Deprecated _unCo "Use UC instead" #-}
-- | Functor composition.
-- It takes two functors and composes them to make a functor.
newtype Comp f g a =
  Co
    { _unCo :: f (g a)
    }
  deriving
    ( Prelude.Eq
    , Prelude.Ord
      -- ^ 'P.Ord' should be used in all cases.
      -- We maintain a instance for internal use and this just in case.
    , Prelude.Read
    )

-- | The inverse of 'Co'.
pattern UC ::
  (
  )
    => Comp f g a -> f (g a)
pattern UC x <- (Co -> x) where
  UC =
    _unCo

-- | Lifts a function on nested functors to composed functors.
pattern LC ::
  (
  )
    => (f (g a) -> h (i b)) -> Comp f g a -> Comp h i b
pattern LC x <- (UC << (fm Co) -> x) where
  LC =
    Co << (fm UC)

-- | Performs a map on a composed structure as if that structure were not composed.
--
-- On matrices (which are the composition of vectors) this has the effecto of being a "row map", running a function on each row and making a new matrix out of the results.
mR ::
  ( Functor f
  )
    => (g a -> h b) -> Comp f g a -> Comp f h b
mR =
  LC < m

-- | Left associativity of functor composition.
pattern LAC ::
  ( Functor f
  )
    => Comp (Comp f g) h a -> Comp f (Comp g h) a
pattern LAC x <- (LC $ Co < m UC -> x) where
  LAC =
    LC $ m Co < UC

-- | Right associativity of functor composition.
pattern RAC ::
  ( Functor f
  )
    => Comp f (Comp g h) a -> Comp (Comp f g) h a
pattern RAC x <- (LC $ m Co < UC -> x) where
  RAC =
    LC $ Co < m UC

instance
  ( Functor f
  , Functor g
  )
    => Functor (Comp f g)
  where
    fmap =
      LC < mm

instance
  ( Eq1 f
  , Eq1 g
  )
    => Eq1 (Comp f g)
  where
    q1 =
      fxO UC < q1 < q1

instance
  ( Ord1 f
  , Ord1 g
  )
    => Ord1 (Comp f g)
  where
    lcp =
      fxO UC < lcp < lcp

instance
  ( Ord (f (p a))
  )
    => Ord (Comp f p a)
  where
    cp =
      on cp UC

instance
  ( Foldable f
  , Foldable g
  )
    => Foldable (Comp f g)
  where
    foldMap =
      fm UC < Prelude.foldMap < Prelude.foldMap

instance
  ( Traversable f
  , Traversable g
  )
    => Traversable (Comp f g)
  where
    sequenceA =
      m Co < tv sQ < UC

instance
  ( Applicative f
  , Applicative g
  )
    => Applicative (Comp f g)
  where
    pure =
      Co < p < p
    liftA2 =
      Co <<< fxO UC < l2 < l2

instance
  ( RingApplicative f
  , RingApplicative g
  )
    => RingApplicative (Comp f g)
  where
    ml2 =
      Co <<< fxO UC < ml2 < ml2

instance
  ( ReversableFunctor f
  , Reversable g
  )
    => Reversable (Comp f g)
  where
    _rv =
      LC $ rvW _rv

instance
  ( ReversableFunctor f
  , ReversableFunctor g
  )
    => ReversableFunctor (Comp f g)
  where
    rvW =
      LC < rvW < rvW

instance
  ( Zip f
  , Zip g
  )
    => Zip (Comp f g)
  where
    zW func (Co x) (Co y) =
      Co $ zW (zW func) x y


instance
  ( SemiAlign f
  , WeakAlign g
  )
    => WeakAlign (Comp f g)
  where
    cnL (Co x) (Co y) =
      Co $ alW go x y
      where
        go (Ti x) =
          x
        go (Ta y) =
          y
        go (Te x y) =
          cnL x y

    cnR (Co x) (Co y) =
      Co $ alW go x y
      where
        go (Ti x) =
          x
        go (Ta y) =
          y
        go (Te x y) =
          cnR x y

    alL (Co x) (Co y) =
      Co $ alW go x y
      where
        go (Ti x) =
          Lef < x
        go (Ta y) =
          Rit < y
        go (Te x y) =
          alL x y

    alR (Co x) (Co y) =
      Co $ alW go x y
      where
        go (Ti x) =
          Lef < x
        go (Ta y) =
          Rit < y
        go (Te x y) =
          alR x y

instance
  ( SemiAlign f
  , SemiAlign g
  )
    => SemiAlign (Comp f g)
  where
    aln (Co x) (Co y) =
      Co $ alW go x y
      where
        go (Ti x) =
          Ti < x
        go (Ta y) =
          Ta < y
        go (Te x y) =
          aln x y

    alW func (Co x) (Co y) =
      Co $ alW go x y
      where
        go (Ti x) =
          (func < Ti) < x
        go (Ta x) =
          (func < Ta) < x
        go (Te x y) =
          alW func x y


instance
  ( UnitalZip f
  , SemiAlign f
  , WeakAlign g
  )
    => UnitalZip (Comp f g)
  where
    aId =
      Co aId


instance
  ( Comonad f
  , Distributive g
  , Comonad g
  )
    => Comonad (Comp f g)
  where
    cr =
      cr < cr < UC
    dyp =
      m Co < Co < m dis < dyp < m dyp < UC


instance
  ( Monad f
  , Distributive f
  , Monad g
  )
    => Monad (Comp f g)
  where
    x >>= func =
      Co < m jn < jn < m dis < UC < m (UC < func) $ x

instance
  ( Unpure f
  , Unpure g
  )
    => Unpure (Comp f g)
  where
    pure' =
      Co < Pu < Pu
    unp =
      fb unp < unp < UC

instance
  ( Identity f
  , Distributive f
  , Distributive g
  , Identity g
  )
    => Identity (Comp f g)

