module P.Algebra.Semigroup.Extra
  ( rt2
  , rt3
  , rt4
  , rt5
  , rt6
  , rt7
  , rt8
  , rt9
  , rt0
  , rt1
  )
  where

import Prelude
  (
  )

import P.Algebra.Semigroup

-- | Combine 2 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 2@
rt2 ::
  ( Semigroup a
  )
    => a -> a
rt2 x =
  x <> x

-- | Combine 3 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 3@
rt3 ::
  ( Semigroup a
  )
    => a -> a
rt3 x =
  x <> rt2 x

-- | Combine 4 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 4@
rt4 ::
  ( Semigroup a
  )
    => a -> a
rt4 x =
  x <> rt3 x

-- | Combine 5 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 5@
rt5 ::
  ( Semigroup a
  )
    => a -> a
rt5 x =
  x <> rt4 x

-- | Combine 6 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 6@
rt6 ::
  ( Semigroup a
  )
    => a -> a
rt6 x =
  x <> rt5 x

-- | Combine 7 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 7@
rt7 ::
  ( Semigroup a
  )
    => a -> a
rt7 x =
  x <> rt6 x

-- | Combine 8 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 8@
rt8 ::
  ( Semigroup a
  )
    => a -> a
rt8 x =
  x <> rt7 x

-- | Combine 9 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 9@
rt9 ::
  ( Semigroup a
  )
    => a -> a
rt9 x =
  x <> rt8 x

-- | Combine 10 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 10@
rt0 ::
  ( Semigroup a
  )
    => a -> a
rt0 x =
  x <> rt9 x

-- | Combine 11 copies of an element of a semigroup with '(<>)'.
--
-- Slightly more general shorthand of @'P.Algebra.Monoid.rt' 11@
rt1 ::
  ( Semigroup a
  )
    => a -> a
rt1 x =
  x <> rt0 x
