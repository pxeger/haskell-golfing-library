{-|
Module :
  P.Algebra.Semigroup.Commutative
Description :
  Commutative monoids
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Semigroup.Commutative
  ( Commutative
  )
  where


import P.Algebra.Semigroup


-- | Instances of 'Semigroup' which obey the law of commutativity:
--
-- prop> a <> b = b <> a
class
  ( Semigroup m
  )
    => Commutative m


instance
  (
  )
    => Commutative Integer


instance
  (
  )
    => Commutative Int


instance
  (
  )
    => Commutative Bool


instance
  (
  )
    => Commutative ()


instance
  ( Commutative a
  , Commutative b
  )
    => Commutative (a, b)


instance
  ( Commutative a
  , Commutative b
  , Commutative c
  )
    => Commutative (a, b, c)


instance
  ( Commutative a
  , Commutative b
  , Commutative c
  , Commutative d
  )
    => Commutative (a, b, c, d)


instance
  ( Commutative a
  , Commutative b
  , Commutative c
  , Commutative d
  , Commutative e
  )
    => Commutative (a, b, c, d, e)


instance
  ( Commutative b
  )
    => Commutative (a -> b)
