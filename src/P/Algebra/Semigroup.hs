module P.Algebra.Semigroup
  ( Semigroup (..)
  , mp
  , fmp
  , cy
  -- * Deprecated
  , mappend
  , cycle
  )
  where


import Prelude
  ( Int
  , Integer
  , Semigroup (..)
  )
import qualified Prelude


import P.Bool
import P.Fix
import P.Function
import P.Function.Compose
import P.Function.Flip


{-# Deprecated mappend "Use mp instead" #-}
-- | Long version of 'mp'.
mappend ::
  ( Semigroup a
  )
    => a -> a -> a
mappend =
  mp


-- | Semigroup operation.
--
-- Prefix version of '(<>)'.
-- More general version of 'Prelude.mappend'.
mp ::
  ( Semigroup a
  )
    => a -> a -> a
mp =
  (<>)


-- | Flip of 'mp'.
fmp ::
  ( Semigroup a
  )
    => a -> a -> a
fmp =
  F mp


{-# Deprecated cycle "Use cy instead" #-}
-- | Long version of 'cy'.
cycle ::
  ( Semigroup a
  )
    => a -> a
cycle =
  cy


-- | The fixed point of some element of a 'Semigroup'.
--
-- Takes an element of a semigroup @m@
-- and combines an infinite number of @m@s using '(<>)'.
--
-- It is right associative, so:
--
-- @
-- cy m =
--   m <> (m <> (m <> ...))
-- @
--
-- More general version of 'Data.List.cycle'.
--
-- ==== __Examples__
--
-- On list this behaves like 'Data.List.cycle':
--
-- >>> tk 10 $ cy [1,2,3]
-- [1,2,3,1,2,3,1,2,3,1]
--
cy ::
  ( Semigroup m
  )
    => m -> m
cy =
  yy < mp


instance
  (
  )
    => Semigroup Int
  where
    (<>) =
      (Prelude.+)


instance
  (
  )
    => Semigroup Integer
  where
    (<>) =
      (Prelude.+)


instance
  (
  )
    => Semigroup Bool
  where
    (<>) =
      (Prelude.&&)
