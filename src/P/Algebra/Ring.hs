module P.Algebra.Ring
  ( Ring (..)
  , rim
  )
  where

import qualified Prelude
import Prelude
  ( Int
  , Integer
  )

import P.Algebra.Group

-- | An algebraic structure following the ring axioms.
class
  ( Group a
  )
    => Ring a
  where
    -- | Multiplication action
    (^*) :: a -> a -> a
    -- | Multiplicative identity
    mId :: a

-- | Multiplicative action on a ring.
rim ::
  ( Ring a
  )
    => a -> a -> a
rim =
  (^*)

instance Ring Int where
  (^*) =
    (Prelude.*)
  mId =
    1

instance Ring Integer where
  (^*) =
    (Prelude.*)
  mId =
    1
