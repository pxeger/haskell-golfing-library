{-|
Module :
  P.Algebra.Monoid.Ap
Description :
  Monoids from applicatives
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

We can make a monoid out of applicative instances.
-}
module P.Algebra.Monoid.Ap
  ( Ap (..)
  )
  where


import qualified Prelude


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Applicative
import P.Applicative.Unpure
import P.Comonad
import P.Eq
import P.Function
import P.Function.Compose
import P.Functor
import P.Functor.Identity.Class
import P.Monad
import P.Ord
import P.Show



-- | A wrapper which replaces the semigroup instance of an ordered type with 'P.Applicative.l2' 'P.Algebra.Monoid.mp'.
--
-- Redefined so we can give it a custom 'Show' instance.
newtype Ap f a =
  Ap
    { uAp :: f a
    }


instance
  ( Show (f a)
  )
    => Show (Ap f a)
  where
    show =
      sh < uAp


instance
  ( Functor f
  )
    => Functor (Ap f)
  where
    fmap func =
      Ap < m func < uAp


instance
  ( Applicative f
  )
    => Applicative (Ap f)
  where
    pure =
      Ap < p

    (<*>) (Ap func) =
      Ap < (*^) func < uAp


instance
  ( Monad f
  )
    => Monad (Ap f)
  where
    x >>= y =
      Ap (uAp x >~ (uAp < y))


instance
  ( Comonad f
  )
    => Comonad (Ap f)
  where
    cr =
      cr < uAp

    dyp =
      Ap < m Ap < dyp < uAp


instance
  ( Unpure f
  )
    => Unpure (Ap f)
  where
    unp =
      unp < uAp

    pure' =
      Ap < Pu


instance
  ( Identity f
  )
    => Identity (Ap f)


instance
  ( Eq (f a)
  )
    => Eq (Ap f a)
  where
    (==) =
      qb uAp


instance
  ( Ord (f a)
  )
    => Ord (Ap f a)
  where
    cp =
      on cp uAp


instance
  ( Eq1 f
  )
    => Eq1 (Ap f)
  where
    q1 func (Ap x) =
      q1 func x < uAp


instance
  ( Ord1 f
  )
    => Ord1 (Ap f)
  where
    lcp func (Ap x) =
      lcp func x < uAp


instance
  ( Semigroup a
  , Applicative f
  )
    => Semigroup (Ap f a)
  where
    (<>) =
      l2 (<>)


instance
  ( Monoid a
  , Applicative f
  )
    => Monoid (Ap f a)
  where
    mempty =
      p i
