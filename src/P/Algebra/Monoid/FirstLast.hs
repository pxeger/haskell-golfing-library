module P.Algebra.Monoid.FirstLast
  ( First (..)
  , Last (..)
  )
  where

import Prelude
  ( Traversable
  , Bounded
  , Num
  , Enum
  , Real
  , Integral
  )
import qualified Prelude

import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Applicative
import P.Applicative.Unpure
import P.Comonad
import P.Function.Compose
import P.Function.Flip
import P.Functor.Identity.Class
import P.Monad
import P.Show

-- | A wrapper which replaces the semigroup instance of 'Prelude.const'.
--
-- Redefined so we can give it a custom 'Show' instance.
newtype First a =
  Fst
    { uFt :: a
    }

instance Show a => Show (First a) where
  show =
    sh < UI

instance Functor First where
  fmap func =
    p < func < UI

instance Applicative First where
  pure =
    Fst
  (<*>) func =
    p < UI func < UI

instance Monad First where
  (>>=) =
    F (< UI)

instance Comonad First where
  cr =
    uFt
  dyp =
    p

instance Unpure First where
  unp =
    p < uFt
  pure' =
    Fst

instance Identity First

instance Semigroup (First a) where
  (<>) =
    Prelude.const

-- | A wrapper which replaces the semigroup instance of 'Prelude.flip Prelude.const'.
--
-- Redefined so we can give it a custom 'Show' instance.
newtype Last a =
  Lst
    { uLs :: a
    }

instance Functor Last where
  fmap func =
    p < func < UI

instance Applicative Last where
  pure =
    Lst
  (<*>) func =
    p < UI func < UI

instance Comonad Last where
  cr =
    uLs
  dyp =
    p

instance Unpure Last where
  unp =
    p < uLs
  pure' =
    Lst

instance Identity Last

instance Monad Last where
  (>>=) =
    F (< UI)

instance Semigroup (Last a) where
  (<>) =
    F p
