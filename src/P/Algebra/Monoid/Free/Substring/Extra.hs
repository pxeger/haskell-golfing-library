{-|
Module :
  P.Algebra.Monoid.Free.Substring.Extra
Description :
  Additional functions for the P.Algebra.Monoid.Free.Substring library
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Algebra.Monoid.Free.Substring library.
-}
module P.Algebra.Monoid.Free.Substring.Extra
  where


import P.Aliases
import P.Algebra.Monoid.Free
import P.Algebra.Monoid.Free.Substring


-- | All substrings of an element of a free monoid with size 1.
--
-- Ordered by the index of their last element.
ss1 ::
  ( FreeMonoid m
  )
    => m -> List m
ss1 =
  ssn (1 :: Int)


-- | All substrings of an element of a free monoid with size 2.
--
-- Ordered by the index of their last element.
ss2 ::
  ( FreeMonoid m
  )
    => m -> List m
ss2 =
  ssn (2 :: Int)


-- | All substrings of an element of a free monoid with size 3.
--
-- Ordered by the index of their last element.
ss3 ::
  ( FreeMonoid m
  )
    => m -> List m
ss3 =
  ssn (3 :: Int)


-- | All substrings of an element of a free monoid with size 4.
--
-- Ordered by the index of their last element.
ss4 ::
  ( FreeMonoid m
  )
    => m -> List m
ss4 =
  ssn (4 :: Int)


-- | All substrings of an element of a free monoid with size 5.
--
-- Ordered by the index of their last element.
ss5 ::
  ( FreeMonoid m
  )
    => m -> List m
ss5 =
  ssn (5 :: Int)


-- | All substrings of an element of a free monoid with size 6.
--
-- Ordered by the index of their last element.
ss6 ::
  ( FreeMonoid m
  )
    => m -> List m
ss6 =
  ssn (6 :: Int)


-- | All substrings of an element of a free monoid with size 7.
--
-- Ordered by the index of their last element.
ss7 ::
  ( FreeMonoid m
  )
    => m -> List m
ss7 =
  ssn (7 :: Int)


-- | All substrings of an element of a free monoid with size 8.
--
-- Ordered by the index of their last element.
ss8 ::
  ( FreeMonoid m
  )
    => m -> List m
ss8 =
  ssn (8 :: Int)


-- | All substrings of an element of a free monoid with size 9.
--
-- Ordered by the index of their last element.
ss9 ::
  ( FreeMonoid m
  )
    => m -> List m
ss9 =
  ssn (9 :: Int)


-- | All substrings of an element of a free monoid with size 10.
--
-- Ordered by the index of their last element.
ss0 ::
  ( FreeMonoid m
  )
    => m -> List m
ss0 =
  ssn (10 :: Int)


-- | All substrings of a free monoid element created by removing 1 generator.
--
-- Ordered by the index of their last element.
sc1 ::
  ( FreeMonoid m
  )
    => m -> List m
sc1 =
  scn (1 :: Int)


-- | All substrings of a free monoid element created by removing 2 generators.
--
-- Ordered by the index of their last element.
sc2 ::
  ( FreeMonoid m
  )
    => m -> List m
sc2 =
  scn (2 :: Int)


-- | All substrings of a free monoid element created by removing 3 generators.
--
-- Ordered by the index of their last element.
sc3 ::
  ( FreeMonoid m
  )
    => m -> List m
sc3 =
  scn (3 :: Int)


-- | All substrings of a free monoid element created by removing 4 generators.
--
-- Ordered by the index of their last element.
sc4 ::
  ( FreeMonoid m
  )
    => m -> List m
sc4 =
  scn (4 :: Int)


-- | All substrings of a free monoid element created by removing 5 generators.
--
-- Ordered by the index of their last element.
sc5 ::
  ( FreeMonoid m
  )
    => m -> List m
sc5 =
  scn (5 :: Int)


-- | All substrings of a free monoid element created by removing 6 generators.
--
-- Ordered by the index of their last element.
sc6 ::
  ( FreeMonoid m
  )
    => m -> List m
sc6 =
  scn (6 :: Int)


-- | All substrings of a free monoid element created by removing 7 generators.
--
-- Ordered by the index of their last element.
sc7 ::
  ( FreeMonoid m
  )
    => m -> List m
sc7 =
  scn (7 :: Int)


-- | All substrings of a free monoid element created by removing 8 generators.
--
-- Ordered by the index of their last element.
sc8 ::
  ( FreeMonoid m
  )
    => m -> List m
sc8 =
  scn (8 :: Int)


-- | All substrings of a free monoid element created by removing 9 generators.
--
-- Ordered by the index of their last element.
sc9 ::
  ( FreeMonoid m
  )
    => m -> List m
sc9 =
  scn (9 :: Int)


-- | All substrings of a free monoid element created by removing 10 generators.
--
-- Ordered by the index of their last element.
sc0 ::
  ( FreeMonoid m
  )
    => m -> List m
sc0 =
  scn (10 :: Int)


