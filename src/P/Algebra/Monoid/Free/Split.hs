{-|
Module :
  P.Algebra.Monoid.Free.Split
Description :
  Functions related to splitting free monoid elements.
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Functions related to partitioning (or "splitting") elements of a free monoid.
Part of the P library.
-}
module P.Algebra.Monoid.Free.Split
  ( sPs
  , sPn
  , sPe
  , sPN
  , mps
  , mqn
  , mpe
  , mpN
  , cxS
  , fxS
  -- * Deprecated
  , chunkList
  )
  where


import Prelude
  ( Integral
  )
import qualified Data.List


import P.Aliases
import P.Algebra.Monoid
import P.Algebra.Monoid.Free
import P.Bifunctor
import P.Function.Compose
import P.Function.Curry
import P.Function.Flip
import P.Foldable
import P.Foldable.Unfold


-- | Gives all the ways to split a list into two pieces.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.sps'.
--
-- ==== __Examples__
--
-- >>> sPs "Hello"
-- [("","Hello"),("H","ello"),("He","llo"),("Hel","lo"),("Hell","o"),("Hello","")]
-- >>> sPs 6
-- [(0,6),(1,5),(2,4),(3,3),(4,2),(5,1),(6,0)]
--
sPs ::
  ( FreeMonoid m
  )
    => m -> List (m, m)
sPs xs@(x:*<xs') =
  (Id, xs) : m2t (x <>) (sPs xs')
sPs Id =
  [ (Id, Id) ]


-- | Gives all the ways to split an element with a non-empty prefix.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.spn'.
sPn ::
  ( FreeMonoid m
  )
    => m -> List (m, m)
sPn (x :*< xs) =
  m2t (x<>) (sPs xs)
sPn Id =
  []


-- | Gives all the ways to split an element with a non-empty suffix.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.spe'.
sPe ::
  ( FreeMonoid m
  )
    => m -> List (m, m)
sPe xs@(x:*<xs') =
  (Id, xs) : m2t (x<>) (sPe xs')
sPe Id =
  []


-- | Gives all the ways to split an element into two non-empty sections.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.spN'.
sPN ::
  ( FreeMonoid m
  )
    => m -> List (m, m)
sPN (x:*<xs@(_:*<_)) =
  m2t (x <>) ((Id, xs) : sPN xs)
sPN _ =
  []


-- | Maps a function across all ways to split a element in twain.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.spM'.
mps ::
  ( FreeMonoid m
  )
    => (m -> m -> b) -> m -> List b
mps =
  (<<% sPs)


-- | Maps a function across all ways to split a list with a non-empty prefix.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.snM'.
mqn ::
  ( FreeMonoid m
  )
    => (m -> m -> b) -> m -> List b
mqn =
  (<<% sPn)


-- | Maps a function across all ways to split a list with a non-empty suffix.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.seM'.
mpe ::
  ( FreeMonoid m
  )
    => (m -> m -> b) -> m -> List b
mpe =
  (<<% sPe)


-- | Maps a function across all ways to split a list into two non-empty parts.
--
-- Ordered in ascending size of prefix.
--
-- Alternative to 'P.List.sNM'.
mpN ::
  ( FreeMonoid m
  )
    => (m -> m -> b) -> m -> List b
mpN =
  (<<% sPN)


{-# Deprecated chunkList "Use cxS instead" #-}
-- | Long version of 'cxS'.
chunkList ::
  ( Integral i
  , FreeMonoid m
  )
    => i -> m -> Partition m
chunkList =
  cxS


-- | Takes a non-negative integer and an element and breaks that element into chunks of that size.
-- If the chunk size doesn't divide the size of the list then the last chunk will be shorter.
-- If the chunk size is zero the result is an infinite list of identities.
--
-- ==== __Examples__
--
-- >>> cxS 3 "hello world"
-- ["hel","lo ","wor","ld"]
cxS ::
  ( Integral i
  , FreeMonoid m
  )
    => i -> m -> Partition m
cxS x =
  m fo < uue (Data.List.genericSplitAt x) < gGn


-- | Flip of 'cxS'.
fxS ::
  ( Integral i
  , FreeMonoid m
  )
    => m -> i -> Partition m
fxS =
  f' cxS
