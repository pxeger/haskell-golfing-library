{-|
Module :
  P.Algebra.Monoid.Free.Prefix
Description :
  Functions getting prefixes from elements of free monoids
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A prefix of an element @x@ is an element @a@ such that there is some element @b@ where @a <> b = x@.
For example on lists '(<>)' is concatenation, so a prefix of a list is a list that can be made into the first list by appending some other list.

Non-empty prefixes are prefixes other than the identity ('i') and strict prefixes are prefixes other than the element itself.
-}
module P.Algebra.Monoid.Free.Prefix
  (
  -- * Prefixes
    pxs
  , pXs
  , pxn
  , pXn
  , pxS
  , pXS
  -- * Prefix maps
  , ƥ
  , fƥ
  , mP
  , fmP
  , mpn
  , mPn
  -- * Miscellaneous
  , sw
  , (%<)
  , fsw
  , nsw
  , swW
  , fSW
  , swB
  , fSB
  , lp
  , lPw
  , lPb
  )
  where


import qualified Prelude


import P.Aliases
import P.Algebra.Monoid
import P.Algebra.Monoid.Free
import P.Algebra.Semigroup
import P.Bool
import P.Eq
import P.First
import P.Function.Compose
import P.Function
import P.Function.Flip


-- | Gives all non-empty prefixes in order of increasing size.
pxn ::
  ( FreeMonoid m
  )
    => m -> List m
pxn x =
  case
    px x
  of
    [] ->
      Prelude.error "Inproper prefix function yields no empty prefixes"
    (_ : ps) ->
      ps


-- | Gives all non-empty prefixes in order of decreasing size.
pXn ::
  ( FreeMonoid m
  )
    => m -> List m
pXn x =
  case
    bkO x
  of
    [] ->
      []
    ((g, gs) : _) ->
      (g <>) < pX gs


-- | Gives all strict prefixes in order of increasing size.
--
-- For a version which works on 'P.Scan.Scan' instead of 'FreeMonoid' see 'P.Scan.pxX'.
pxs ::
  ( FreeMonoid m
  )
    => m -> List m
pxs x =
  case
    bKO x
  of
    [] ->
      []
    ((gs, g) : ps) ->
      px gs


-- | Gives all strict prefixes in order of decreasing size.
pXs ::
  ( FreeMonoid m
  )
    => m -> List m
pXs x =
  case
    pX x
  of
    [] ->
      Prelude.error "Inproper prefix function yields no empty prefixes"
    (_ : ps) ->
      ps


-- | Gives all non-empty strict prefixes in order of increasing size.
pxS ::
  ( FreeMonoid m
  )
    => m -> List m
pxS =
  tl < pxs


-- | Gives all non-empty strict prefixes in order of decreasing size.
pXS ::
  ( FreeMonoid m
  )
    => m -> List m
pXS =
  tl < pXn


-- | Maps a function over the prefixes of a list in order of increasing size.
ƥ ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
ƥ func =
  m func < px


-- | Flip of 'ƥ'.
fƥ ::
  ( FreeMonoid m
  )
    => m -> (m -> b) -> List b
fƥ =
  F ƥ


-- | Maps a function over the prefixes of a list in order of decreasing size.
mP ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mP func =
  m func < pX


-- | Flip of 'mP'.
fmP ::
  ( FreeMonoid m
  )
    => m -> (m -> b) -> List b
fmP =
  F mP


-- | Maps a function over the non-empty prefixes in order of increasing size.
--
-- For a version which works on 'P.Scan.Scan' instead of 'FreeMonoid' see 'P.Scan.mpX'.
mpn ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mpn func =
  m func < pxn


-- | Maps a function over the non-empty prefixes in order of decreasing size.
mPn ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mPn func =
  m func < pXn


-- | Maps a function over the strict prefixes in order of increasing size.
--
-- For a version which works on 'P.Scan.Scan' instead of 'FreeMonoid' see 'P.Scan.mPX'.
mps ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> (List b)
mps func =
  m func < pxs


-- | Maps a function over the strict prefixes in order of decreasing size.
mPs ::
  (
  )
    => (List a -> b) -> List a -> List b
mPs func =
  m func < pXs


-- | Maps a function over the non-empty strict prefixes in order of increasing size.
mpS ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mpS func =
  m func < pxS


-- | Maps a function over the non-empty strict prefixes in order of decreasing size.
mPS ::
  ( FreeMonoid m
  )
    => (m -> b) -> m -> List b
mPS func =
  m func < pXS


-- | Takes two lists and determines whether the first list starts with the second.
sw ::
  ( Eq a
  , FreeMonoid a
  )
    => a -> a -> Bool
sw =
  swW (==)


-- | Infix version of 'sw'.
(%<) ::
  ( Eq a
  , FreeMonoid a
  )
    => a -> a -> Bool
(%<) =
  sw


-- | Flip of 'sw'.
fsw ::
  ( Eq a
  , FreeMonoid a
  )
    => a -> a -> Bool
fsw =
  F sw


-- | Negation of 'sw'.
nsw ::
  ( Eq a
  , FreeMonoid a
  )
    => a -> a -> Bool
nsw =
  mm Prelude.not sw


-- | Works like 'sw' but with a user defined equality function.
--
-- In order to be maximally lazy, this assumes that @equality a b@ and @equality x y@ implies that @equality (a <> x) (b <> y)@.
swW ::
  ( FreeMonoid m
  )
    => (m -> m -> Bool) -> m -> m -> Bool
swW equals =
  on go gGn
  where
    go _ [] =
      T
    go [] _ =
      B
    go (x : xs) (y : ys) =
      equals x y <> go xs ys


-- | Flip of 'swW'.
fSW ::
  ( FreeMonoid a
  )
    => a -> (a -> a -> Bool) -> a -> Bool
fSW =
  F swW


-- | Works like 'sw' but with a substitution function mapped across both inputs.
swB ::
  ( Eq b
  , FreeMonoid a
  )
    => (a -> b) -> a -> a -> Bool
swB =
  swW < on eq


-- | Flip of 'swB'.
fSB ::
  ( Eq b
  , FreeMonoid a
  )
    => a -> (a -> b) -> a -> Bool
fSB =
  F swB


-- | Gives the longest common prefix of two lists.
lp ::
  ( Eq a
  , FreeMonoid a
  )
    => a -> a -> a
lp =
  lPw eq


-- | 'lp' but with a user defined comparison.
--
-- In order to be maximally lazy, this assumes that @comparison a b@ and @comparison x y@ implies that @comparison (a <> x) (b <> y)@.
-- Actual comparison is only ever done between generators.
--
-- This assumption can sometimes be abused for different behavior.
-- See examples.
--
-- 'P.List.Prefix.lpw' is a more useful version of this function.
lPw ::
  ( FreeMonoid a
  , FreeMonoid b
  )
    => (a -> b -> Bool) -> a -> b -> a
lPw predicate =
  mX gGn go < gGn
  where
    go [] _ =
      i
    go _ [] =
      i
    go (x1 : xs) (y1 : ys)
      | predicate x1 y1
      =
        x1 <> go xs ys
      | T
      =
        i


-- | Works like 'lp' but with a substitution function applied to the generators.
lPb ::
  ( Eq b
  , FreeMonoid m
  )
    => (m -> b) -> m -> m -> m
lPb =
  lPw < qb
