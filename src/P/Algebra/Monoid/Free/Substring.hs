{-# Language ScopedTypeVariables #-}
{-|
Module :
  P.Algebra.Monoid.Free.Substring
Description :
  Functions related to substrings of free monoid elements.
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Functions related to sublists (also called "subsequences") of a free monoid.
Part of the P library.

A substring of an element of a monoid with a set of generators is another element of the monoid, which can be made from the original element by removing elements.
It need not be contiguous.
-}
module P.Algebra.Monoid.Free.Substring
  ( iS
  , fiS
  , nS
  , fnS
  , iWq
  , nWq
  , iBq
  , nBq
  , iSW
  , nSW
  , iSB
  , nSB
  , lsS
  , lWq
  , lBq
  , lSW
  , lSB
  , ss
  , sS
  , sI
  , sD
  , ssn
  , fsn
  , scn
  , fSn
  , sSt
  , lss
  , sss
  , sSm
  , sSn
  )
  where


import Prelude
  ( Integral
  , Int
  )
import qualified Prelude


import P.Aliases
import P.Applicative
import P.Arithmetic
import P.Algebra.Monoid
import P.Algebra.Monoid.Free
import P.Bool
import P.Category
import P.Comonad
import P.Eq
import P.Foldable
import P.Foldable.Find
import P.Function
import P.Function.Compose
import P.Function.Flip
import P.Monad
import P.Monad.Plus.Filter
import P.Ord
import P.Ord.Bounded
import P.Ord.MinMax
import P.Scan
import P.Tuple
import P.Zip


-- | Takes two elements of a free monoid and determines if the first is a substring of the second.
iS ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> Bool
iS =
  iWq eq


-- | Flip of 'iS'.
fiS ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> Bool
fiS =
  f' iS


-- | Takes two elements and determines if the first is not a substring of the second.
nS ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> Bool
nS =
  n << iS


-- | Flip of 'nS'
fnS ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> Bool
fnS =
  f' nS


-- | 'iS' with a user defined equality operation.
iWq ::
  ( FreeMonoid m1
  , FreeMonoid m2
  )
    => (m1 -> m2 -> Bool) -> m1 -> m2 -> Bool
iWq userEq x y =
  go (gGn x) (gGn y)
  where
    go [] _ =
      T
    go (_ : _) [] =
      B
    go (x : xs) (y : ys)
      | userEq x y
      =
        go xs ys
      | T
      =
        go (x : xs) ys


-- | Negation of 'iWq'.
nWq ::
  ( FreeMonoid m1
  , FreeMonoid m2
  )
    => (m1 -> m2 -> Bool) -> m1 -> m2 -> Bool
nWq =
  n <<< iWq


-- | 'iS' with a user defined equality operation.
iSW ::
  ( FreeMonoid (f m1)
  , FreeMonoid (f m2)
  , Eq1 f
  )
    => (m1 -> m2 -> Bool) -> f m1 -> f m2 -> Bool
iSW =
  iWq < q1


-- | Negation of 'iSW'.
nSW ::
  ( FreeMonoid (f m1)
  , FreeMonoid (f m2)
  , Eq1 f
  )
    => (m1 -> m2 -> Bool) -> f m1 -> f m2 -> Bool
nSW =
  n <<< iSW


-- | 'iS' with a user defined conversion function.
iBq ::
  ( Eq b
  , FreeMonoid m
  )
    => (m -> b) -> m -> m -> Bool
iBq =
  iWq < on eq


-- | Negation of 'iBq'.
nBq ::
  ( Eq b
  , FreeMonoid m
  )
    => (m -> b) -> m -> m -> Bool
nBq =
  n <<< iBq


-- | 'iS' with a user defined conversion.
iSB ::
  ( Eq (f m)
  , FreeMonoid (f a)
  , Functor f
  )
    => (a -> m) -> f a -> f a -> Bool
iSB =
  iBq < m


-- | Negation of 'iSB'.
nSB ::
  ( Eq (f m)
  , FreeMonoid (f a)
  , Functor f
  )
    => (a -> m) -> f a -> f a -> Bool
nSB =
  n <<< iSB


-- | Takes two elements and gives the largest common substring.
lsS ::
  ( Eq m
  , FreeMonoid m
  )
    => m -> m -> m
lsS =
  lWq eq


-- TODO make this fast.
-- | Takes two elements and gives the largest common substring with a user defined equality.
-- Gives the substring of the first input.
lWq ::
  ( FreeMonoid m1
  , FreeMonoid m2
  )
    => (m1 -> m2 -> Bool) -> m1 -> m2 -> m1
lWq userEq x y =
  (fo << go) (gGn x) (gGn y)
  where
    go [] _ =
      []
    go _ [] =
      []
    go (x : xs) (y : ys)
      | userEq x y
      =
        x : go xs ys
      | T
      =
        maL (go (x : xs) ys) (go xs (y : ys))


-- TODO make this fast.
-- | Takes two lists and gives the largest common sublist with a user defined equality.
-- Gives the sublist of the first input.
lSW ::
  ( FreeMonoid (f m1)
  , FreeMonoid (f m2)
  , Eq1 f
  )
    => (m1 -> m2 -> Bool) -> f m1 -> f m2 -> f m1
lSW =
  lWq < q1


-- | Takes a conversion function and finds the longest common substring as compared under the conversion function.
-- Gives the substring of the first input.
lBq ::
  ( Eq b
  , FreeMonoid m
  )
    => (m -> b) -> m -> m -> m
lBq =
  lWq < qb


-- | Takes a conversion function and finds the longest common sublist as compared under the conversion function.
-- Gives the sublist of the first input.
lSB ::
  ( Eq (f b)
  , FreeMonoid (f m)
  , Functor f
  )
    => (m -> b) -> f m -> f m -> f m
lSB =
  lBq < m


-- | Get all substrings of an element of a free monoid.
--
-- Items are ordered by the index of their final element with the identity coming first.
--
-- Despite technically not being elements of a free monoid, this function is suitable for infinite lists.
-- On an infinite list this returns all finite sublists.
--
-- This is the powerset operation on finite lists.
--
-- ==== __Examples__
--
-- >>> sS [1,2,2]
-- [[],[1],[2],[1,2],[2],[2,2],[1,2],[1,2,2]]
-- >>> tk 10 $ sS [4..]
-- [[],[4],[5],[4,5],[6],[5,6],[4,6],[4,5,6],[7],[6,7]]
ss ::
  ( FreeMonoid m
  )
    => m -> List m
ss =
  (i :) < sS


-- | Get all (finite) substrings of an element of a free monoid other than the identity.
--
-- Despite technically not being elements of a free monoid, this function is suitable for infinite lists.
-- On an infinite list this returns all finite non-empty sublists.
--
-- Items are ordered by the index of their final element.
--
sS ::
  ( FreeMonoid m
  )
    => m -> List m
sS =
  l2 bn dph go < gGn
  where
    go (x : _) 0 =
      [x]
    go (x : xs) n =
      let
        rems =
          go xs (n - 1)
      in
        rems <> (x <>) < rems


-- | Get all substrings of a given element ordered by increasing size.
--
-- Use 'ss' for infinite lists.
sI ::
  ( FreeMonoid m
  )
    => m -> List m
sI xs =
  fsn xs ~< (dpH (gGn xs) :: List Int)


-- | Get all substrings of a given element ordered by decreasing size.
sD ::
  ( FreeMonoid m
  )
    => m -> List m
sD =
  Prelude.reverse < sI


-- | Get all substrings of a given size.
--
-- Items are order by the index of their final element.
--
-- ==== __Examples__
--
-- >>> ssn 2 [1,2,2]
-- [[1,2],[2,2],[1,2]]
-- >>> tk 10 $ ssn 3 [4..]
-- [[4,5,6],[5,6,7],[4,6,7],[4,5,7],[6,7,8],[5,7,8],[5,6,8],[4,7,8],[4,6,8],[4,5,8]]
--
ssn ::
  ( Integral i
  , Ord i
  , FreeMonoid m
  )
    => i -> m -> List m
ssn 0 _ =
  [i]
ssn n xs =
  dpH (gGn xs) >~ go n (gGn xs)
  where
    go 0 _ 0 =
      [i]
    go 0 _ _ =
      []
    go n (x : xs) m =
      case
        cp m n
      of
        LT ->
          []
        EQ ->
          (x <>) < go (n-1) xs (m-1)
        GT ->
          go n xs (m-1) <> (x <>) < go (n-1) xs (m-1)
    go _ _ _ =
      []


-- | Flip of 'ssn'.
fsn ::
  ( Integral i
  , Ord i
  , FreeMonoid m
  )
    => m -> i -> List m
fsn =
  f' ssn


-- | Get all substrings removing a given number of generators.
--
-- Items are order by the index of the first removed generator.
--
scn ::
  ( Integral i
  , Ord i
  , FreeMonoid m
  )
    => i -> m -> List m
scn 0 x =
  [x]
scn n Id =
  []
scn n (x :*< xs) =
  scn (n-1) xs <> ((x <>) < scn n xs)


-- | Flip of 'scn'.
fSn ::
  ( Integral i
  , Ord i
  , FreeMonoid m
  )
    => m -> i -> List m
fSn =
  f' scn


-- | Gets all substrings satsifying a predicate.
--
-- Despite technically not being elements of a free monoid, this function is suitable for infinite lists.
-- On an infinite list this returns all finite sublists.
sSt ::
  ( FreeMonoid m
  )
    => Predicate m -> m -> List m
sSt pred =
  fl pred < ss


-- | Gets the longest substring satisfying a predicate.
--
-- Throws an error if there is none.
lss ::
  ( FreeMonoid m
  )
    => Predicate m -> m -> m
lss pred =
  g_N "lss called with no satisfying substrings" (1 :: Int) pred < sD


-- | Gets the shortest substring satisfying a predicate.
--
-- Throws an error if there is none.
sss ::
  ( FreeMonoid m
  )
    => Predicate m -> m -> m
sss pred =
  g_N "sss called with no satisfying substrings" (1 :: Int) pred < sI


-- | Gets all maximal substrings satisfying a predicate.
--
-- A substring is maximal if it is not a substring of another substring also satisfying the predicate.
--
-- ==== __Examples__
--
-- Get all maximal linear sublists:
--
-- >>> sSm (lq<δ) [1,3,5,7,8,9,9]
-- [[9,9],[1,3,5,7,9],[7,8,9],[1,8],[3,8],[5,8]]
-- >>> sSm (lq<δ) [1,2,3,2]
-- [[1,2],[2,2],[3,2],[1,2,3]]
--
sSm ::
  forall m.
  ( Eq m
  , FreeMonoid m
  )
    => Predicate m -> m -> List m
sSm pred xs =
  go [] [eu $ gGn xs]
  where
    go ::
      (
      )
        => List (List (Int, m)) -> List (List (Int, m)) -> List m
    go res (x : xs)
      | pred $ mF cr x
      =
        case
          ay (iBq (m st) x) res
        of
          T ->
            go res xs
          B ->
            go (x : res) xs
      | T
      =
        go res (xs <> scn (1 :: Int) x)
    go res [] =
      mF cr < res


-- | Gets all minimal substrings satisfying a predicate.
--
-- A substring is minimal if no other substring satisfying the predicate is a substring of it.
--
-- ==== __Examples__
--
-- Get all minimal sublists with a sum greater than 7.
--
-- ghci> sSn (gt 7<fo) [1,7,3,5,7,8,9]
-- [[5,7],[3,7],[3,5],[7,7],[7,5],[7,3],[1,7],[1,7],[9],[8]]
sSn ::
  forall m.
  ( Eq m
  , FreeMonoid m
  )
    => Predicate m -> m -> List m
sSn pred xs =
  m conv $ sSm (pred < conv) $ dph $ gGn xs
  where
    conv :: List Int -> m
    conv x =
      mF cr $ fl (fE x < st) $ eu $ gGn xs
