module P.Algebra.Monoid.MinMax
  ( Min (..)
  , Max (..)
  )
  where


import qualified Prelude


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Algebra.Semigroup.Commutative
import P.Applicative
import P.Applicative.Unpure
import P.Comonad
import P.Eq
import P.Function
import P.Function.Compose
import P.Function.Flip
import P.Functor.Identity.Class
import P.Monad
import P.Ord
import P.Ord.Bounded
import P.Ord.MinMax
import P.Show


-- | A wrapper which replaces the semigroup instance of an ordered type with 'Prelude.min'.
--
-- Redefined so we can give it a custom 'Show' instance.
newtype Min a =
  Min
    { uMn :: a
    }


instance Show a => Show (Min a) where
  show =
    sh < UI


instance Functor Min where
  fmap func =
    p < func < UI


instance Applicative Min where
  pure =
    Min
  (<*>) func =
    p < UI func < UI


instance Comonad Min where
  cr =
    uMn
  dyp =
    Min


instance Unpure Min where
  unp =
    p < uMn
  pure' =
    Min


instance Identity Min


instance Monad Min where
  (>>=) =
    F (< UI)


instance Eq a => Eq (Min a) where
  (==) =
    qb UI


instance Ord a => Ord (Min a) where
  cp =
    on cp UI


instance Eq1 Min where
  q1 =
    Li2


instance Ord1 Min where
  lcp =
    Li2


instance Ord a => Semigroup (Min a) where
  (<>) =
    mN


instance
  ( MinBounded a
  , Ord a
  )
    => Monoid (Min a)
  where
    mempty =
      p NB


instance
  ( Ord a
  )
    => Commutative (Min a)


-- | A wrapper which replaces the semigroup instance of an ordered type with 'Prelude.max'.
--
-- Redefined so we can give it a custom 'Show' instance.
newtype Max a =
  Max
    { uMx :: a
    }


instance Show a => Show (Max a) where
  show =
    sh < UI


instance Functor Max where
  fmap func =
    p < func < UI


instance Applicative Max where
  pure =
    Max
  (<*>) func =
    p < UI func < UI


instance Comonad Max where
  cr =
    uMx
  dyp =
    Max


instance Unpure Max where
  unp =
    p < uMx
  pure' =
    Max


instance Identity Max


instance Monad Max where
  (>>=) =
    F (< UI)


instance Eq a => Eq (Max a) where
  (==) =
    qb UI


instance Ord a => Ord (Max a) where
  cp =
    on cp UI


instance Eq1 Max where
  q1 =
    Li2


instance Ord1 Max where
  lcp =
    Li2


instance Ord a => Semigroup (Max a) where
  (<>) =
    ma


instance
  ( MaxBounded a
  , Ord a
  )
    => Monoid (Max a)
  where
    mempty =
      p XB


instance
  ( Ord a
  )
    => Commutative (Max a)
