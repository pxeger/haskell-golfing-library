module P.Algebra.Monoid
  ( Monoid
  , (<>)
  , i
  , rt
  , frt
  -- * Deprecated
  , mempty
  )
  where


import Prelude
  ( Integral
  , (-)
  , (>)
  , Int
  , Integer
  , Monoid
  )
import qualified Prelude


import P.Algebra.Semigroup
import P.Bool
import P.Function.Flip


{-# Deprecated mempty "Use i instead" #-}
-- | Long version of 'i'.
mempty ::
  ( Monoid a
  )
    => a
mempty =
  i


-- | Monoid identity.
--
-- Equivalent to 'Prelude.mempty'.
-- More general version of @[]@.
-- More general version of @0@.
i ::
  ( Monoid a
  )
    => a
i =
  Prelude.mempty


-- | Takes an integer @n@ and an element of a 'Monoid' @m@.
-- Combines @n@ copies of @m@ using '(<>)'.
--
-- Returns 'i' if @n < 1@.
--
-- ====  __Examples__
--
-- It can be used on a list to repeat that list:
--
-- >>> rt 8 "AB"
-- "ABABABABABABABAB"
rt ::
  ( Integral i
  , Monoid m
  )
    => i -> m -> m
rt x _
  | 1 > x
  =
    i
rt x element =
  element <> (rt (x-1) element)


-- | Flip of 'rt'.
frt ::
  ( Integral i
  , Monoid m
  )
    => m -> i -> m
frt =
  F rt


instance
  (
  )
    => Monoid Int
  where
    mempty =
      0


instance
  (
  )
    => Monoid Integer
  where
    mempty =
      0


instance
  (
  )
    => Monoid Bool
  where
    mempty =
      T
