{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Algebra.Group
  ( Group (..)
  , pattern IV
  , (~~)
  )
  where


import Prelude
  ( Int
  , Integer
  , fmap
  )


import P.Algebra.Monoid


class
  ( Monoid m
  )
    => Group m
  where
    _iv :: m -> m


pattern IV ::
  ( Group m
  )
    => m -> m
pattern IV x <- (_iv -> x) where
  IV =
    _iv


-- | Subtraction on groups.
(~~) ::
  ( Group m
  )
    => m -> m -> m
x ~~ y =
  x <> IV y


instance Group Int where
  _iv x =
    -x


instance Group Integer where
  _iv x =
    -x


instance
  ( Group b
  )
    => Group (a -> b)
  where
    _iv =
      fmap _iv


instance
  ( Group a
  , Group b
  )
    => Group (a, b)
  where
    _iv (x, y) =
      ( IV x
      , IV y
      )

instance
  ( Group a
  , Group b
  , Group c
  )
    => Group (a, b, c)
  where
    _iv (x, y, z) =
      ( IV x
      , IV y
      , IV z
      )
