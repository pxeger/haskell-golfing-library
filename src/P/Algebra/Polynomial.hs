{-# Language FlexibleInstances #-}
{-# Language MultiParamTypeClasses #-}
{-|
Module :
  P.Algebra.Polynomial
Description :
  A polynomial type and related functions
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Algebra.Polynomial
  ( Polynomial (..)
  )
  where


import qualified Prelude
import Prelude
  ( Num (..)
  , Eq (..)
  , Int
  , Integral
  , div
  )


import P.Algebra.Monoid
import P.Algebra.Monoid.Action
import P.Algebra.Monoid.Free.Split
import P.Algebra.Semigroup
import P.Aliases
import P.Bool
import P.Category
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Show
import P.Zip
import P.Zip.SemiAlign
import P.Zip.WeakAlign


-- | A type for polynomials.
--
-- Wraps a list of coefficients with the least significant coefficient first in the list.
newtype Polynomial a =
  Y
    { uY ::
      List a
    }


instance
  ( Num a
  , Eq a
  )
    => Eq (Polynomial a)
  where
    Y x == Y y =
      fo $ zd eq 0 x y


-- | TODO improve this
instance
  ( Show a
  )
    => Show (Polynomial a)
  where
    show (Y x) =
      'Y' : sh x


instance
  (
  )
    => Foldable Polynomial
  where
    foldMap f =
      mF f < uY


instance
  ( Num a
  )
    => Num (Polynomial a)
  where
    Y x + Y y =
      Y $ zd' (+) x y

    (*) =
      karatsuba

    negate (Y x) =
      Y $ negate < x

    fromInteger =
      Y < (:[]) < fromInteger

    -- | Implements the l1 norm, returning the value as a constant polynomial.
    abs (Y x) =
      Y [sm $ m abs x]


-- | Semigroup action composes polynomials as functions.
instance
  ( Num a
  )
    => Semigroup (Polynomial a)
  where
    (<>) =
      chunkCompose 4


chunkCompose ::
  ( Num a
  , Integral i
  )
    => i -> Polynomial a -> Polynomial a -> Polynomial a
chunkCompose l f g =
  let
    go1 gl (h1 : h2 : hs) =
      (h1 + h2 * gl) : go1 gl hs
    go1 gl xs =
      xs

    go2 gl [] =
      Y []
    go2 gl [x] =
      x
    go2 gl xs =
      go2 (gl Prelude.^ 2) (go1 gl xs)
  in
    go2 (g Prelude.^ l) $ (`naiveCompose` g) < Y < cxS l (uY f)


naiveCompose ::
  ( Num a
  )
    => Polynomial a -> Polynomial a -> Polynomial a
naiveCompose (Y []) _ =
  Y []
naiveCompose (Y (x : xs)) ys =
  Y [x] + ys * naiveCompose (Y xs) ys


instance
  ( Num a
  )
    => Monoid (Polynomial a)
  where
    mempty =
      Y [0,1]


instance
  ( Num a
  )
    => Action (Polynomial a) a
  where
    aq (Y []) _ =
      0
    aq (Y (c : cs)) x =
      c + x * aq (Y cs) x


instance
  (
  )
    => Functor Polynomial
  where
    fmap f (Y xs) =
      Y $ f < xs


instance
  (
  )
    => WeakAlign Polynomial
  where
    cnL (Y xs) (Y ys) =
      Y $ cnL xs ys


instance
  (
  )
    => SemiAlign Polynomial
  where
    aln (Y xs) (Y ys) =
      Y $ aln xs ys


instance
  (
  )
    => Zip Polynomial
  where
    zW f (Y xs) (Y ys) =
      Y $ zW f xs ys


karatsuba ::
  ( Num a
  )
    => Polynomial a -> Polynomial a -> Polynomial a
karatsuba poly1 poly2 =
  let
    -- Pad the two polynomials out to the same size
    poly1' =
      zd Prelude.const 0 poly1 poly2
    poly2' =
      zd Prelude.const 0 poly2 poly1
    -- Get the size
    size :: Int
    size =
      Prelude.length poly1'
  in
    go size poly1' poly2'
  where
    Y xs << n =
      Y (Prelude.replicate n 0 <> xs)

    go _ (Y []) (Y []) =
      Y []
    go _ (Y [x]) (Y [y]) =
      Y [x * y]
    go n (Y xs) (Y ys) =
      let
        leadSize =
          div n 2
        (x0, x1) =
          Prelude.splitAt leadSize xs
        (y0, y1) =
          Prelude.splitAt leadSize ys
        z0 =
          go leadSize (Y x0) (Y y0)
        z1 =
          z3 - z2 - z0
        z2 =
          go (n - leadSize) (Y x1) (Y y1)
        z3 =
          go (n - leadSize) (Y x1 + Y x0) (Y y1 + Y y0)
      in
        (z2 << (2 * leadSize)) + (z1 << leadSize) + z0
