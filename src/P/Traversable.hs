module P.Traversable
  ( Traversable
  , sQ
  , tv
  , ftv
  , tv2
  , tvA
  , fvA
  , vA2
  -- * Deprecated
  , traverse
  , sequence
  , sequenceA
  )
  where


import qualified Prelude


import qualified Data.Traversable
import Data.Traversable
  ( Traversable
  )


import P.Applicative
import P.Category
import P.Function.Compose
import P.Function.Curry
import P.Function.Flip


{-# Deprecated traverse "Use tv instead" #-}
-- | Long version of 'tv'
traverse ::
  ( Applicative f
  , Traversable t
  )
    => (a -> f b) -> t a -> f (t b)
traverse =
  tv


-- | 'sQ' but it performs a map first.
--
-- Equivalent to 'Data.Traversable.traverse'.
tv ::
  ( Applicative f
  , Traversable t
  )
    => (a -> f b) -> t a -> f (t b)
tv =
  Data.Traversable.traverse


-- | Flip of 'tv'.
ftv ::
  ( Applicative f
  , Traversable t
  )
    => t a -> (a -> f b) -> f (t b)
ftv =
  f' tv


-- | Like 'tv' except it uses a two valued function.
tv2 ::
  ( Applicative f
  , Traversable t
  )
    => (a -> b -> f c) -> t (a, b) -> f (t c)
tv2 =
  tv < U


-- | Like 'sQ' but it applies each member to a value first.
tvA ::
  ( Applicative f
  , Traversable t
  )
    => a -> t (a -> f b) -> f (t b)
tvA =
  tv < fid


-- | Flip of 'tvA'.
fvA ::
  ( Applicative f
  , Traversable t
  )
    => t (a -> f b) -> a -> f (t b)
fvA =
  F tvA


-- | Like 'tvA' but it applies each function to two values.
vA2 ::
  ( Applicative f
  , Traversable t
  )
    => t (a -> b -> f c) -> a -> b -> f (t c)
vA2 fs x y =
  tv (($ y)<($ x)) fs


{-# Deprecated sequence, sequenceA "Use sQ instead" #-}
-- | Long version of 'sQ'.
sequence ::
  ( Applicative f
  , Traversable t
  )
    => t (f a) -> f (t a)
sequence =
  sQ


-- | Long version of 'sQ'.
sequenceA ::
  ( Applicative f
  , Traversable t
  )
    => t (f a) -> f (t a)
sequenceA =
  sQ


-- |
-- Equivalent to 'Data.Traversable.sequenceA'.
-- More general version of 'Data.Traversable.sequence'.
sQ ::
  ( Applicative f
  , Traversable t
  )
    => t (f a) -> f (t a)
sQ =
  Data.Traversable.sequenceA

