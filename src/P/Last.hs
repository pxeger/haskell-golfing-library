{-# Language FunctionalDependencies #-}
{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Last
  ( _rC
  , gj
  , nt
  , pattern (:>)
  -- * Deprecated
  , init
  , last
  )
  where

import qualified Prelude

import P.Aliases
import P.Category
import P.First
import P.Function.Compose
import P.Reverse

infixr 5 :>

-- | Break a structure into an intial section and a final element if possible.
--
-- Mostly internal function used to define '(:>)'.
_rC ::
  ( Reversable f
  , Firstable f g
  , Reversable g
  )
    => f a -> Maybe' (a, g a)
_rC =
  mm Rv < _uC < Rv

{-# Deprecated init "Use nt insted" #-}
-- | Long version of 'nt'.
-- Gives all but the last element of a list.
init ::
  ( Reversable f
  , Firstable f g
  , Reversable g
  )
    => f a -> g a
init =
  nt

-- | Removes the last element of a list like structure.
--
-- May error in some cases.
--
-- More general version of 'Data.List.init'.
nt ::
  ( Reversable f
  , Firstable f g
  , Reversable g
  )
    => f a -> g a
nt =
  Rv < tl < Rv

{-# Deprecated last "Use gj instead" #-}
-- | Long version of 'gj'.
last ::
  ( Reversable f
  , Firstable f g
  )
  => f a -> a
last =
  gj

-- | Gets the last element of a list like structure.
--
-- May error in some cases.
--
-- More general version of 'Data.List.last'
gj ::
  ( Reversable f
  , Firstable f g
  )
  => f a -> a
gj xs =
  case
    _uC (Rv xs)
  of
    (y, _) : _ ->
      y
    [] ->
      Prelude.error "cannot get last element of empty structure"

{-# Complete [], (:>) #-}
-- | Add a single element to the end of a list like structure.
--
-- Also works as a pattern to match.
--
-- ==== __Examples__
--
-- ===== As a pattern
--
-- Determine if the first and last elements are equal:
--
-- @
-- f (x:_ := y:>_) =
--   x == y
-- f [] =
--   F
-- @
--
-- >>> f [1,2,3]
-- False
-- >>> f [1]
-- True
-- >>> f [3,2,3]
-- True
-- >>> f []
-- False
--
pattern (:>) ::
  ( Firstable f g
  , Reversable f
  , Reversable g
  )
    => a -> g a -> f a
pattern x :> xs <- (_rC -> [(x, xs)]) where
  x :> xs =
    Rv $ K x $ Rv xs
