module P.Arrow.Cokleisli
  ( Cokleisli (..)
  )
  where


import qualified Prelude


import P.Applicative
import P.Arrow
import P.Bifunctor.Profunctor
import P.Category
import P.Comonad
import P.Function.Compose
import P.Monad
import P.Tuple


-- | A type corresponding to the cokleisli arrows of a comonad @m@.
newtype Cokleisli w a b =
  Cok
    { rCK ::
      w a -> b
    }


instance
  ( Comonad w
  )
    => Semigroupoid (Cokleisli w)
  where
    Cok k1 <@ Cok k2 =
      Cok $ k2 =>= k1


instance
  ( Comonad w
  )
    => Category (Cokleisli w)
  where
    id =
      Cok cr


instance
  ( Comonad w
  )
    => LooseArrow (Cokleisli w)
  where
    mSt (Cok k) =
      Cok $ (k < m st) -< (cr < cr)

    mNd (Cok k) =
      Cok $ (st < cr) -< (k < m cr)

    Cok k1 =: Cok k2 =
      Cok $ (k1 < m st) -< (k2 < m cr)

    aSw =
      arr aSw


instance
  ( Functor f
  )
    => Profunctor (Cokleisli f)
  where
    Cok k1 <@^ f =
      Cok $ k1 < (f <)


instance
  ( Comonad w
  )
    => Arrow (Cokleisli w)


instance
  (
  )
    => Functor (Cokleisli w a)
  where
    fmap f (Cok k) =
      Cok $ f < k


instance
  (
  )
    => Applicative (Cokleisli w a)
  where
    pure =
      Cok < p

    Cok k1 <*> Cok k2 =
      Cok $ k1 *^ k2


instance
  (
  )
    => Monad (Cokleisli w a)
  where
    Cok k1 >>= f =
      Cok $ k1 >~ ( rCK < f )
