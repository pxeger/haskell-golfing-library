{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Enum
  ( Enum
  , pattern Sc
  , pattern Pv
  , eF
  , ef
  , fef
  , (#>#)
  , tef
  , ftf
  , lef
  , flf
  , xef
  , fxf
  , ref
  , fRf
  , (#<#)
  , xrf
  , fXf
  , eR
  , (##)
  , eR'
  , eRx
  , xR'
  , fln
  , flN
  -- * Constants
  , nn
  , nN
  , β
  , pattern Β
  )
  where


import qualified Prelude
import Prelude
  ( Enum
  , String
  , Integral
  , otherwise
  )


import P.Aliases
import P.Bool
import P.Eq
import P.Foldable.Length
import P.Function.Flip
import P.Function.Compose
import P.Ord


-- | Successor function.
-- Takes an enum and produces the next one if it exists.
--
-- For some numeric types this is the same as 'P.Arithmetic.Pattern.P1'.
--
-- Produces an error when no successor exists.
--
-- Equivalent to 'Prelude.succ'
--
-- ==== __Examples__
--
-- Errors when there is no successor:
--
-- >>> cc True
-- *** Exception: Prelude.Enum.Bool.succ: bad argument
--
pattern Sc ::
  ( Enum a
  )
    => a -> a
pattern Sc x <- (Prelude.pred -> x) where
  Sc =
    Prelude.succ


-- | Predecessor function.
-- Takes an enum and produces the previous one if it exists.
--
-- For numeric types this is the same as 'P.Arithmetic.Pattern.S1'.
--
-- Produces an error when no predecessor exists.
--
-- Equivalent to 'Prelude.succ'
--
-- ==== __Examples__
--
-- Errors when there is no Predecessor:
--
-- >>> pv False
-- *** Exception: Prelude.Enum.Bool.pred: bad argument
--
pattern Pv ::
  ( Enum a
  )
    => a -> a
pattern Pv x <- (Prelude.succ -> x) where
  Pv =
    Prelude.pred


-- |
-- Equivalent to 'Prelude.enumFrom'.
eF ::
  ( Enum a
  )
    => a -> List a
eF =
  Prelude.enumFrom


-- | Outputs the range of enums between two values in ascending order.
--
-- If the first value is larger than the second an empty list is given.
--
-- Equivalent to 'Prelude.enumFromTo'.
--
-- For ranges on tuples use the similar function 'P.Ix.rg'.
--
-- ==== __Examples__
--
-- >>> ef 2 9
-- [2,3,4,5,6,7,8,9]
-- >>> ef 9 2
-- []
--
ef ::
  ( Enum a
  )
    => a -> a -> List a
ef =
  Prelude.enumFromTo


-- | Infix version of 'ef'.
(#>#) ::
  ( Enum a
  )
    => a -> a -> List a
(#>#) =
  ef


-- | Flip of 'ef'.
fef ::
  ( Enum a
  )
    => a -> a -> List a
fef =
  F ef


-- | Outputs the range of enums between two values in ascending order, excluding the first value but including the second.
--
-- If the first value is larger than or equal to the second an empty list is given.
--
-- ==== __Examples__
--
-- >>> tef 2 9
-- [3,4,5,6,7,8,9]
-- >>> tef 9 2
-- []
-- >>> tef 2 2
-- []
--
tef ::
  ( Enum a
  )
    => a -> a -> List a
tef =
  Prelude.drop 1 << ef


-- | Flip of 'tef'.
ftf ::
  ( Enum a
  )
    => a -> a -> List a
ftf =
  f' tef


-- | Outputs the range of enums between two values in ascending order, including the first value but excluding the second.
--
-- If the first value is larger than or equal to the second an empty list is given.
--
-- ==== __Examples__
--
-- >>> lef 2 9
-- [2,3,4,5,6,7,8]
-- >>> lef 9 2
-- []
-- >>> lef 2 2
-- []
--
lef ::
  ( Enum a
  )
    => a -> a -> List a
lef =
  nt << ef
  where
    nt [] =
      []
    nt [x] =
      []
    nt (x : xs) =
      x : nt xs


-- | Flip of 'lef'.
flf ::
  ( Enum a
  )
    => a -> a -> List a
flf =
  f' lef


-- | Outputs the range of enums between two values in ascending order.
-- Excludes the inputs themselves.
--
-- If the first value is larger than or equal to the second an empty list is given.
--
-- ==== __Examples__
--
-- >>> xef 2 9
-- [3,4,5,6,7,8]
-- >>> xef 9 2
-- []
-- >>> xef 2 2
-- []
-- >>> xef 8 9
-- []
--
xef ::
  ( Enum a
  )
    => a -> a -> List a
xef =
  Prelude.drop 1 << lef


-- | Flip of 'xef'.
fxf ::
  ( Enum a
  )
    => a -> a -> List a
fxf =
  f' xef


-- | Outputs the range of enums between two values in descending order.
-- Includes the endpoints.
--
-- If the first value is smaller than the second an empty list is given.
--
-- ==== __Examples__
--
-- >>> ref 9 2
-- [9,8,7,6,5,4,3,2]
-- >>> ref 2 9
-- []
--
ref ::
  ( Enum a
  )
    => a -> a -> List a
ref =
  Prelude.reverse << fef


-- | Flip of 'ref'.
fRf ::
  ( Enum a
  )
    => a -> a -> List a
fRf =
  f' ref


-- | Infix version of 'ref'.
(#<#) ::
  ( Enum a
  )
    => a -> a -> List a
(#<#) =
  ref


-- | Outputs the range of enums between two values in descending order.
-- Includes the endpoints.
--
-- If the first value is smaller than the second an empty list is given.
--
-- ==== __Examples__
--
-- >>> xrf 9 2
-- [8,7,6,5,4,3]
-- >>> xrf 2 9
-- []
--
xrf ::
  ( Enum a
  )
    => a -> a -> List a
xrf =
  Prelude.reverse << fxf


-- | Flip of 'xrf'.
fXf ::
  ( Enum a
  )
    => a -> a -> List a
fXf =
  f' xrf


-- | Output the range of enums between two values.
-- If the first input is smaller than the second they are output in ascending order.
-- If the second is smaller than the first they are output in descending order.
-- Includes its endpoints
--
-- Because 'Enum's are not necessarily ordered this evaluates both branches and takes the longer result.
-- Use 'eR'' if this is a concern.
eR ::
  ( Enum a
  )
    => a -> a -> List a
eR x1 x2 =
  let
    res1 =
      ref x1 x2
    res2 =
      ef x1 x2
  in
    if
      res1 |<| res2
    then
      res2
    else
      res1


-- | Infix of 'eR'.
(##) ::
  ( Enum a
  )
    => a -> a -> List a
(##) =
  eR


-- | Like 'eR' except it uses the 'Ord' instance to speed up the computation.
eR' ::
  ( Enum a
  , Ord a
  )
    => a -> a -> List a
eR' x1 x2
  | x1 > x2
  =
    ref x1 x2
  | otherwise
  =
    ef x1 x2


-- | Output the range of enums between two values.
-- If the first input is smaller than the second they are output in ascending order.
-- If the second is smaller than the first they are output in descending order.
-- Excludes its endpoints
--
-- Because 'Enum's are not necessarily ordered this evaluates both branches and takes the longer result.
-- Use 'xR'' if this is a concern.
eRx ::
  ( Enum a
  )
    => a -> a -> List a
eRx x1 x2 =
  let
    res1 =
      xrf x1 x2
    res2 =
      xef x1 x2
  in
    if
      res1 |<| res2
    then
      res2
    else
      res1


-- | Like 'eRx' except it uses the 'Ord' instance to speed up the computation.
xR' ::
  ( Enum a
  , Ord a
  )
    => a -> a -> List a
xR' x1 x2
  | x1 > x2
  =
    xrf x1 x2
  | otherwise
  =
    xef x1 x2


-- | The uppercase alphabet.
--
-- The function name is an upper case greek letter beta.
-- It may render the same as a @B@, but it is distinct.
pattern Β ::
  (
  )
    => String
pattern Β <- ((== ef 'A' 'Z') -> T) where
  Β =
    ef 'A' 'Z'


-- | The lowercase alphabet.
β ::
  (
  )
    => String
β =
  ef 'a' 'z'


-- | The natural numbers starting from 0.
nn ::
  ( Integral i
  )
    => List i
nn =
  eF 0


-- | The natural numbers starting from 1.
nN ::
  ( Integral i
  )
    => List i
nN =
  eF 1


-- | Get a list of all non-negative integers satisfying a predicate.
fln ::
  ( Integral i
  )
    => Predicate i -> List i
fln =
  f' Prelude.filter nn


-- | Get a list of all positive integers satisfying a predicate.
flN ::
  ( Integral i
  )
    => Predicate i -> List i
flN =
  f' Prelude.filter nN
