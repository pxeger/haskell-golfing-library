module P.Arrow
  ( LooseArrow (..)
  , syr
  , (==:)
  , sy3
  , Arrow (..)
  , ot
  , fot
  , iot
  , oti
  , ot2
  -- * Deprecated
  , fanout
  )
  where


import qualified Prelude


import P.Applicative
import P.Bifunctor.Profunctor
import P.Category
import P.Category.Iso
import P.Function.Compose
import P.Function.Curry
import P.Function.Flip
import P.Swap


infixl 9 -<


-- | A looser version of 'Arrow' which does not require a lift from the @(->)@ category. --
-- ==== Laws
--
-- prop> id =: id = id
-- prop> aSw <@ aSw = id
-- prop> aSw <@ mSt f = mNd f
-- prop> mNd f <@ mSt g = g =: f
-- prop> f ><: g = aSw <@ (f =: g)
--
-- The following laws are not required and may be violated:
--
-- prop> (f <@ g) =: (h <@ j) = (f =: h) <@ (g =: j)
class
  ( Category p
  )
    => LooseArrow p
  where
    {-# Minimal (><:) | ((aSw | tws), (mSt | mNd | (=:))) #-}
    -- | Apply an arrow to the first argument of a tuple and leave the second argument unchanged.
    --
    -- More general version of 'Data.Tuple.Strict.mapFst'.
    -- Equivalent to 'Control.Arrow.first'.
    -- Similar to 'P.Bifunctor.mst'.
    mSt :: p a b -> p (a, c) (b, c)
    mSt f =
      aSw <@ (f ><: id)


    -- | Apply an arrow to the second argument of a tuple and leave the first argument unchanged.
    --
    -- More general version of 'Data.Tuple.Strict.mapSnd'
    mNd :: p a b -> p (c, a) (c, b)
    mNd =
       (<@ aSw) <@ (aSw <@) <@ mSt


    -- | Combine two arrows in parallel.
    --
    -- Note that this does not have to be the morphism product of the category.
    --
    -- Equivalent to '(Control.Arrow.***)' from "Control.Arrow", similar to '(P.Bifunctor.***)' from "P.Bifunctor".
    (=:) :: p a b -> p c d -> p (a, c) (b, d)
    f =: g =
      aSw <@ mNd f <@ aSw <@ mNd g


    -- | A swap morphism.
    aSw :: p (a, b) (b, a)
    aSw =
      id ><: id


    -- | Twists a morphism by composing it with 'aSw'.
    --
    -- Must be equivalent to its definition.
    --
    -- prop> tws f = aSw <@ f
    tws :: p (a, b) (c, d) -> p (a, b) (d, c)
    tws f =
      aSw <@ f


    -- | Crosses two morphisms.
    (><:) :: p a b -> p c d -> p (a, c) (d, b)
    f ><: g =
      tws (f =: g)


instance
  (
  )
    => LooseArrow (->)
  where
    mSt f (x, y) =
      ( f x
      , y
      )

    mNd f (x, y) =
      ( x
      , f y
      )

    (f =: g) (x, y) =
      ( f x
      , g y
      )

    aSw (x, y) =
      ( y
      , x
      )


instance
  ( LooseArrow p
  )
    => LooseArrow (Iso p)
  where
    Iso f1 b1 =: Iso f2 b2 =
      Iso
        (mNd f2 <@ mSt f1)
        (mSt b1 <@ mNd b2)

    aSw =
      Iso aSw aSw


-- | Parallelize an arrow with itself.
--
-- Similar to 'P.Bifunctor.jB'.
-- For a flip see 'P.Bifunctor.fjB'.
--
-- ==== __Examples__
--
-- >>> syr (+3) (1, 2)
-- (4, 5)
--
syr ::
  ( LooseArrow p
  )
    => p a b -> p (a, a) (b, b)
syr f =
  f =: f


-- | A kind of category generalizing the idea of a function.
-- Arrows represent some sort of \"computation\" done from some source type to an output type.
--
-- ==== Laws
--
-- The exact laws are to be determined.
-- They should correspond to the laws in 'Control.Arrow.Arrow'.
--
-- ===== @arr@
--
-- prop> arr id = id
-- prop> arr (f <@ g) = arr f <@ arr g
--
class
  ( LooseArrow p
  , Profunctor p
  )
    => Arrow p
  where
    -- | \"Fanout\".
    -- Takes two arrows with the same source and and produces a new arrow with that source and a tuple of their outputs.
    (-<) :: p a b -> p a c -> p a (b, c)
    f -< g =
      (f =: g) <@^ jbp


instance
  (
  )
    => Arrow (->)
  where
    (f -< g) x =
      ( f x
      , g x
      )


-- | \"Fanout\".
-- Take two arrows and creates a new arrow which applies them both to the same input and gives a tuple.
--
-- Flip of 'fot'.
ot ::
  ( Arrow p
  )
    => p a b -> p a c -> p a (b, c)
ot =
  (-<)


-- | \"Fanout\".
-- Take two arrows and creates a new arrow which applies them both to the same input and gives a tuple.
--
-- Flip of 'ot'.
fot ::
  ( Arrow p
  )
    => p a b -> p a c -> p a (c, b)
fot =
  F (-<)


-- | 'ot' applied to 'id'.
iot ::
  ( Arrow p
  )
    => p a b -> p a (a, b)
iot =
  (id -<)


-- | 'fot' applied to 'id'.
oti ::
  ( Arrow p
  )
    => p a b -> p a (b, a)
oti =
  (-< id)


-- | Lifted fanout.
ot2 ::
  ( Applicative f
  , Arrow p
  )
    => f (p a b) -> f (p a c) -> f (p a (b, c))
ot2 =
  l2 ot


-- | Parallelize across two levels of arrow.
(==:) ::
  ( Functor (p (a, a'))
  , LooseArrow p
  , LooseArrow q
  )
    => p a (q b c) -> p a' (q b' c') -> p (a, a') (q (b, b') (c, c'))
(==:) =
  U (=:) <<< (=:)


-- | Parallelize two leveled arrow with itself.
sy3 ::
  ( Functor (p (a, a))
  , LooseArrow p
  , LooseArrow q
  )
    => p a (q b c) -> p (a, a) (q (b, b) (c, c))
sy3 f =
  f ==: f

{-# Deprecated fanout "Use ot or (-<) instead" #-}
-- | Long version of 'ot' or '(-<)'.
fanout ::
  ( Arrow p
  )
    => p a b -> p a c -> p a (b, c)
fanout =
  ot
