{-# Language ScopedTypeVariables #-}
{-# Language LambdaCase #-}
module P.Arithmetic
  ( Prelude.Int
  , Prelude.Integer
  , Prelude.Num
  , Prelude.Integral
  , (+)
  , pl
  , (-)
  , sb
  , fsb
  , (*)
  , ml
  , db
  , tp
  , (^)
  , pw
  , fpw
  , sq
  , (%)
  , mu
  , fmu
  , (//)
  , dv
  , fdv
  , vD
  , fvD
  , qR
  , fqR
  , av
  , aD
  , (-|)
  , tNu
  , ev
  , od
  , xrt
  , fxr
  , xlg
  , fxl
  , pwt
  , fpt
  , pwo
  , fpo
  -- * Deprecated
  , subtract
  , even
  , divMod
  , quotRem
  , exactLog2
  )
  where


import qualified Prelude
import Prelude
  ( Num
  , Integral
  , String
  , Bool
  )


import qualified Data.List
import Data.Foldable as Foldable


import P.Aliases
import P.Category
import P.Eq
import P.Function.Compose
import P.Function.Flip
import P.Ord


infixl 6  +, -
infixl 7  *


-- | Addition.
--
-- Equivalent to '(Prelude.+)'.
(+) ::
  ( Num a
  )
    => a -> a -> a
(+) =
  (Prelude.+)


-- | Addition.
--
-- Equivalent to '(Prelude.+)'.
-- Prefix version of '(+)'
pl ::
  ( Num a
  )
    => a -> a -> a
pl =
  (+)


-- | Subtraction.
--
-- Equivalent to '(Prelude.-)'.
(-) ::
  ( Num a
  )
    => a -> a -> a
(-) =
  (Prelude.-)


{-# Deprecated subtract "Use sb instead" #-}
-- | Long version of 'sb'.
subtract ::
  ( Num a
  )
    => a -> a -> a
subtract =
  sb


-- | Subtraction.
--
-- Equivalent to 'Prelude.subtract'.
sb ::
  ( Num a
  )
    => a -> a -> a
sb =
  Prelude.subtract


-- | Flip of 'sb'.
fsb ::
  ( Num a
  )
    => a -> a -> a
fsb =
  F sb


-- | Multiplication.
--
-- Equivalent to '(Prelude.*)'.
(*) ::
  ( Num a
  )
    => a -> a -> a
(*) =
  (Prelude.*)


-- | Multiplication.
--
-- Equivalent to '(Prelude.*)'.
ml ::
  ( Num a
  )
    => a -> a -> a
ml =
  (*)


-- | Double or multiply by 2.
db ::
  ( Num a
  )
    => a -> a
db =
  ml 2


-- | Triple or multiply by 3.
tp ::
  ( Num a
  )
    => a -> a
tp =
  ml 3


-- | Integer division.
-- Result is truncated towards negative infinity.
(//) ::
  ( Integral i
  )
    => i -> i -> i
(//) =
  Prelude.div


-- | Integer division.
-- Result is truncated towards negative infinity.
dv ::
  ( Integral i
  )
    => i -> i -> i
dv =
  Prelude.div


-- | Flip of 'dv'.
fdv ::
  ( Integral i
  )
    => i -> i -> i
fdv =
  F Prelude.div


-- | Integer modulus function.
--
-- Equivalent to 'Prelude.mod'.
(%) ::
  ( Integral i
  )
    => i -> i -> i
(%) =
  Prelude.mod


-- | Integer modulus function.
--
-- Equivalent to 'Prelude.mod'.
mu ::
  ( Integral i
  )
    => i -> i -> i
mu =
  (%)


-- | Flip of 'mu'.
fmu ::
  ( Integral i
  )
    => i -> i -> i
fmu =
  F mu


{-# Deprecated divMod "Use vD instead" #-}
-- | Long version of 'vD'.
divMod ::
  ( Integral i
  )
    => i -> i -> (i, i)
divMod =
  vD


-- | Divmod function.
--
-- Produces an error when a zero divisor is given.
--
-- Equivalent to 'Prelude.divMod'.
vD ::
  ( Integral i
  )
    => i -> i -> (i, i)
vD =
  Prelude.divMod


-- | Flip of 'vD'.
fvD ::
  ( Integral i
  )
    => i -> i -> (i, i)
fvD =
  f' vD


{-# Deprecated quotRem "Use qR instead" #-}
-- | Long version of 'qR'.
quotRem ::
  ( Integral i
  )
    => i -> i -> (i, i)
quotRem =
  qR


-- | Quotrem function.
--
-- Produces an error when a zero divisor is given.
--
-- Equivalent to 'Prelude.quotRem'.
qR ::
  ( Integral i
  )
    => i -> i -> (i, i)
qR =
  Prelude.quotRem


-- | Flip of 'qR'.
fqR ::
  ( Integral i
  )
    => i -> i -> (i, i)
fqR =
  f' qR


-- | Squares a number.
sq ::
  ( Num a
  )
    => a -> a
sq x =
  x * x


-- | Raise to the power.
--
-- Equivalent to '(Prelude.^)'.
(^) ::
  ( Integral b
  , Num a
  )
    => a -> b -> a
(^) =
  (Prelude.^)


-- | Raise to the power.
--
-- Prefix version of '(^)'.
pw ::
  ( Integral b
  , Num a
  )
    => a -> b -> a
pw =
  (^)


-- | Flip of 'pw'.
fpw ::
  ( Integral b
  , Num a
  )
    => b -> a -> a
fpw =
  F pw


-- | Absolute value.
av ::
  ( Num a
  )
    => a -> a
av =
  Prelude.abs


-- | Absolute difference.
aD ::
  ( Num a
  )
    => a -> a -> a
aD =
  mm av sb


-- | Infix of 'aD'.
(-|) ::
  ( Num a
  )
    => a -> a -> a
(-|) =
  aD


-- | Convert from any 'Integral' type to any `Num` type.
tNu ::
  ( Integral a
  , Num b
  )
    => a -> b
tNu =
  Prelude.fromInteger < Prelude.toInteger


{-# Deprecated even "use ev isntead" #-}
-- | Long version of 'ev'.
even ::
  ( Integral a
  , Eq a
  )
    => a -> Bool
even =
  ev


-- | Determines if a number is even.
-- Returns @True@ if the input is divisible by 2 and @False@ otherwise.
ev ::
  ( Integral a
  , Eq a
  )
    => a -> Bool
ev =
  (0 ==) < (% 2)


-- | Determines if a number is odd.
-- Returns @False@ if the input is divisible by 2 and @True@ otherwise.
od ::
  ( Integral a
  , Eq a
  )
    => a -> Bool
od =
  (1 ==) < (% 2)


-- | Exact root.
-- Gives the @n@th root of @x@ if @x@ is an @n@th power.
--
-- Does not work for negative exponents.
xrt ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Maybe' a
xrt p y
  | 0 > y
  = do
    _ <- [() | od p ]
    Prelude.negate < xrt p (-y)
xrt 1 y =
  [y]
xrt x y =
  go 0
  where
    go n =
      case
        cp y (n^x)
      of
        EQ ->
          [n]
        GT ->
          go (n+1)
        LT ->
          []


-- | Flip of 'xrt'.
fxr ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Maybe' a
fxr =
  f' xrt


-- | Exact log.
-- Gives the logarithm of @x@ in base @b@ if @x@ is an exact power of @b@.
--
-- ==== Special cases
-- Always returns @Nothing@ when the base is 0.
--
-- In the case of ambiguities the power is always the smallest non-negative solution.
-- So @xlg 1 1@ results in @Just 0@ and @xlg (-1) (-1)@ is @Just 1@.
xlg ::
  ( Integral a
  , Num b
  )
    => a -> a -> Maybe' b
xlg 0 =
  \_ -> []
xlg (-1) =
  \case
    (-1) ->
      [1]
    1 ->
      [0]
    _ ->
      []
xlg 1 =
  \case
    1 ->
     [0]
    _ ->
     []
xlg y =
  go 0
  where
    go i 1 =
      [i]
    go i 0 =
      []
    go i x =
      case
        vD x y
      of
        (m, 0) ->
          go (i+1) m
        (_, _) ->
          []


-- | Flip of 'xlg'.
fxl ::
  ( Integral a
  , Num b
  )
    => a -> a -> Maybe' b
fxl =
  f' xlg


-- | Determines if the input is a perfect @n@th power.
pwt ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Bool
pwt =
  Prelude.not << Foldable.null << xrt


-- | Determines if the input is a perfect power of some base.
pwo ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Bool
pwo =
  Prelude.not << Foldable.null << xlg


-- | Flip of 'pwt'.
fpt ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Bool
fpt =
  f' pwt


-- | Flip of 'pwo'.
fpo ::
  ( Integral a
  , Ord a
  )
    => a -> a -> Bool
fpo =
  f' pwo


{-# Deprecated exactLog2 "Use xl2 instead." #-}
-- | Long verrsion of 'P.Arithmetic.Extra.xl2'.
exactLog2 ::
  ( Integral a
  , Num b
  )
    => a -> Maybe' b
exactLog2 =
  xlg 2
