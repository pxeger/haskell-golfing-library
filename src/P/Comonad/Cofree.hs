{-|
Module :
  P.Comonad.Cofree
Description :
  The Cofree functor combinator
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A cofree functor combinator.
Useful for building trees.
-}
module P.Comonad.Cofree
  ( Cofree (..)
  , hCF
  , hCf
  -- * Deprecated
  , hoistCofree
  )
  where


import Prelude
  ( (==)
  )
import qualified Prelude


import P.Algebra.Semigroup
import P.Alternative
import P.Applicative
import P.Bifunctor.These
import P.Comonad
import P.Eq
import P.Function.Compose
import P.Monad
import P.Ord
import P.Scan
import P.Zip
import P.Zip.SemiAlign
import P.Zip.WeakAlign


-- | Cofree comonads!
--
-- Useful for building tree like structures.
--
-- ==== __Examples__
--
-- An infinite list is the cofree of 'P.Identity.Ident'.
--
-- @
-- type InfiniteList =
--   Cofree Ident
-- @
--
data Cofree f a =
  a :>> f (Cofree f a)

instance
  ( Functor f
  )
    => Functor (Cofree f)
  where
    fmap f (x :>> xs) =
      f x :>> mm f xs


instance
  ( Eq1 f
  )
    => Eq1 (Cofree f)
  where
    q1 userEq (x :>> xs) (y :>> ys) =
      userEq x y <> q1 (q1 userEq) xs ys


instance
  ( Eq1 f
  , Eq a
  )
    => Eq (Cofree f a)
  where
    (x :>> xs) == (y :>> ys) =
      eq x y <> q1 eq xs ys


instance
  ( Ord1 f
  )
    => Ord1 (Cofree f)
  where
    lcp userComp (x :>> xs) (y :>> ys) =
      userComp x y <> lcp (lcp userComp) xs ys


instance
  ( Ord1 f
  , Ord a
  )
    => Ord (Cofree f a)
  where
    cp =
      lcp cp


instance
  ( Alternative f
  )
    => Applicative (Cofree f)
  where
    pure =
      (:>> em)
    xm <*> ym = do
      x <- xm
      y <- ym
      p (x y)


instance
  ( Alternative f
  )
    => Monad (Cofree f)
  where
    (x :>> xs) >>= k =
      case
        k x
      of
        y :>> ys ->
          y :>> (ys ++ m (>~ k) xs)


instance
  ( Functor f
  )
    => Comonad (Cofree f)
  where
    cr (x :>> _) =
      x
    dyp (x :>> xs) =
      (x :>> xs) :>> m dyp xs
    xn f (x :>> xs) =
      f (x :>> xs) :>> m (xn f) xs


instance
  ( Functor f
  )
    => Scan (Cofree f)
  where
    mA func accum (x :>> xs) =
      let
        (accum', x') =
          func accum x
      in
        x' :>> m (mA func accum') xs
    sc func accum (x :>> xs) =
      let
        x' =
          func accum x
      in
        x' :>> m (sc func x') xs


instance
  ( Zip f
  )
    => Zip (Cofree f)
  where
    zW func (x :>> xs) (y :>> ys) =
      func x y :>> zW (zW func) xs ys


instance
  ( SemiAlign f
  )
    => WeakAlign (Cofree f)
  where
    cnL (x :>> xs) (y :>> ys) =
      x :>> alW go xs ys
      where
        go (Ti x) =
          x
        go (Ta y) =
          y
        go (Te x y) =
          cnL x y


instance
  ( SemiAlign f
  )
    => SemiAlign (Cofree f)
  where
    aln (x :>> xs) (y :>> ys) =
      Te x y :>> alW go xs ys
      where
        go (Ti x) =
          Ti < x
        go (Ta y) =
          Ta < y
        go (Te x y) =
          aln x y


{-# Deprecated hoistCofree "Use hCF or hCf instead" #-}
-- | Long version of 'hCF'.
-- See also 'hCf'.
hoistCofree ::
  ( Functor f
  )
    => (f (Cofree g a) -> g (Cofree g a)) -> Cofree f a -> Cofree g a
hoistCofree =
  hCF


-- | A sort of map on the functor part of the cofree.
--
-- It lifts a natural transformation from @f@ to @g@ into a natural transformation from @Cofree f@ to @Cofree g@.
hCF ::
  ( Functor f
  )
    => (f (Cofree g a) -> g (Cofree g a)) -> Cofree f a -> Cofree g a
hCF func (x :>> xs) =
  x :>> func (hCF func < xs)

-- | A sort of map on the functor part of the cofree.
--
-- It lifts a natural transformation from @f@ to @g@ into a natural transformation from @Cofree f@ to @Cofree g@.
hCf ::
  ( Functor g
  )
    => (f (Cofree f a) -> g (Cofree f a)) -> Cofree f a -> Cofree g a
hCf func (x :>> xs) =
  x :>> (hCf func < func xs)
