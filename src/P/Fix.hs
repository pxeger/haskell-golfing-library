{-|
Module :
  P.Fix
Description :
  Functions for fixed points
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A library for functions finding some sort of fixed point.
-}
module P.Fix
  ( yy
  , utl
  , fut
  , whl
  , fwh
  , fyy
  , yyc
  , fyc
  , yyC
  , fyC
  , myy
  , m2y
  )
  where


import qualified Prelude
import Prelude
  ( Functor
  )


import qualified Data.Function


import P.Aliases
import P.Category
import P.Eq
import P.Function.Compose
import P.Function.Flip


-- | The Y combinator.
--
-- Equivalent to 'Data.Function.fix'.
yy ::
  (
  )
    => (a -> a) -> a
yy =
  Data.Function.fix


-- | Flip of 'yy'.
fyy ::
  (
  )
    => a -> ((a -> b) -> (a -> b)) -> b
fyy =
  f' yy


-- | Repeatedly apply a function to a result until a predicate is satisfied.
--
-- Equivalent to 'Prelude.until'.
utl ::
  (
  )
    => Predicate a -> (a -> a) -> a -> a
utl p f =
  go
    where
      go x
        | p x
        =
          x
      go x =
        go (f x)


-- | Flip of 'utl'.
fut ::
  (
  )
    => (a -> a) -> Predicate a -> a -> a
fut =
  f' utl


-- | Repeatedly apply a function to a result while a predicate is satisfied stopping when it fails.
whl ::
  (
  )
    => Predicate a -> (a -> a) -> a -> a
whl =
  utl Prelude.. (Prelude.not Prelude..)


-- | Flip of 'whl'.
fwh ::
  (
  )
    => (a -> a) -> Predicate a -> a -> a
fwh =
  f' whl


-- | Finds a fixed point by iterating a function until it reaches a fixed point.
yyc ::
  ( Eq a
  )
    => (a -> a) -> a -> a
yyc f x =
  go x $ f x
  where
    go x y
      | x == y
      =
        x
      | Prelude.otherwise
      =
        go y $ f y


-- | Flip of 'yyc'.
fyc ::
  ( Eq a
  )
    => a -> (a -> a) -> a
fyc =
  f' yyc


-- | Repeatedly applies a function to its own result until it repeats a value.
-- The first value that was already visited is output.
--
-- This is slower than 'yyc' because it has to check with all previous values, but it sometimes halts where `yyc` would not.
--
-- ===== __Examples__
--
-- This example halts with @yyC@ but not with 'yyc'.
--
-- >>> yyC Rv "Hello,"
-- "Hello,"
yyC ::
  ( Eq a
  )
    => (a -> a) -> a -> a
yyC f =
  go []
  where
    go x y
      | Prelude.any (eq y) x
      =
        y
      | Prelude.otherwise
      =
        go (y : x) $ f y


-- | Flip of 'yyC'.
fyC ::
  ( Eq a
  )
    => a -> (a -> a) -> a
fyC =
  f' yyC


-- | Map 'yy' across a functor.
--
-- ===== __Examples__
--
-- Make infinite lists with @(:)@:
--
-- >>> f = myy (:)
-- >>> tk9 $ f 2
-- [2,2,2,2,2,2,2,2,2]
-- >>> tk9 $ f 'a'
-- "aaaaaaaaa"
--
myy ::
  ( Functor f
  )
    => f (a -> a) -> f a
myy =
  m yy


-- | Map 'yy' two levels into a functor.
m2y ::
  ( Functor f
  , Functor g
  )
    => f (g (a -> a)) -> f (g a)
m2y =
  mm yy
