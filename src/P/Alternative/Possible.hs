{-|
Module :
  P.Alternative.Many
Description :
  Functions related to the "atLeast" function
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Alternative.Possible
  ( py
  , py'
  , pY
  , pY'
  , (?<>)
  , mpY
  , (<>?)
  , pYm
  , (?=>)
  , mPY
  , (<=?)
  , pYM
  , (<*?)
  , apy
  , (?*>)
  , pya
  , lPy
  , lPY
  -- * Deprecated
  , optional
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import P.Algebra.Monoid
import P.Aliases
import P.Alternative
import P.Applicative
import P.Arithmetic.Nat
import P.Foldable.Length
import P.Function.Compose


infixr 7
    ?<>
  , ?=>
  , ?*>
infixl 8
    <>?
  , <=?
  , <*?


{-# Deprecated optional "Use py instead" #-}
-- | One or none.
-- Used to model a computation that is allowed to fail.
-- Long version of 'py'.
optional ::
  ( Alternative t
  )
    => t a -> t (Maybe' a)
optional =
  py


-- | One or none.
-- Used to model a computation that is allowed to fail.
--
-- Returns the empty list on fail and a singleton on success
--
-- Success is given higher priority.
--
-- Equivalent to 'Control.Applicative.optional'
--
-- ==== __Examples__
--
-- You can use it with a parser:
--
-- >>> uP hd "abcd"
-- [("bcd",'a')]
-- >>> uP (py hd) "abcd"
-- [("bcd","a"),("abcd","")]
--
-- You can use it with a list:
--
-- >>> py [1,2]
-- [[1],[2],[]]
--
py ::
  ( Alternative t
  )
    => t a -> t (Maybe' a)
py alt =
  (p < alt) ++ p []


-- | One or none.
-- Used to model a computation that is allowed to fail.
--
-- Returns the identity on fail and the result on success
--
-- Success is given higher priority.
--
-- ==== __Examples__
--
-- You can use it with a parser:
--
-- >>> uP (ʃ "ab") "abcd"
-- [("cd","ab")]
-- >>> uP (pY $ ʃ "ab") "abcd"
-- [("cd","ab"),("abcd","")]
--
-- You can use it with a list to attach the identity onto the back:
--
-- >>> pY [1,2,3]
-- [1,2,3,0]
-- >>> pY ["abc","em"]
-- [abc","em",""]
--
pY ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m
pY alt =
  alt ++ p i


-- | One or none.
-- Used to model a computation that is allowed to fail.
--
-- Returns the empty list on fail and a singleton on success
--
-- Failure is given higher priority.
--
-- ==== __Examples__
--
-- You can use it with a parser:
--
-- >>> uP hd "abcd"
-- [("bcd",'a')]
-- >>> uP (py' hd) "abcd"
-- [("abcd",""),("bcd","a")]
--
-- You can use it with a list:
--
-- >>> py' [1,2]
-- [[],[1],[2]]
--
py' ::
  ( Alternative t
  )
    => t a -> t (Maybe' a)
py' alt =
  p [] ++ (p < alt)


-- | One or none.
-- Used to model a computation that is allowed to fail.
--
-- Returns the identity on fail and the result on success
--
-- Failure is given higher priority.
--
-- ==== __Examples__
--
-- You can use it with a parser:
--
-- >>> uP (ʃ "ab") "abcd"
-- [("cd","ab")]
-- >>> uP (pY' $ ʃ "ab") "abcd"
-- [("abcd",""),("cd","ab")]
--
-- You can use it with a list to cons the identity onto the front:
--
-- >>> pY' [1,2,3]
-- [0,1,2,3]
-- >>> pY' ["abc","em"]
-- ["","abc","em"]
--
pY' ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m
pY' alt =
  p i ++ alt


-- | A possibility followed by a certainty.
--
-- Like '(<>)' but with 'pY' applied to the first argument.
--
-- ==== __Examples__
--
-- >>> [1,2] ?<> [9,13]
-- [10,14,11,15,9,13]
--
(?<>) ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
(?<>) =
  l2p < pY


-- | Prefix of '(?<>)'.
mpY ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
mpY =
  (?<>)


-- | A certainty followed by a possibility.
--
-- Like '(<>)' but with 'pY' applied to the second argument.
--
-- ==== __Examples__
--
-- >>> [1,2] <>? [9,13]
-- [1,10,14,2,11,15]
(<>?) ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
(<>?) =
  l2p ^. pY


-- | Prefix version of '(<>?)'.
pYm ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
pYm =
  (<>?)


-- | A certainty followed by a possibility.
--
-- Like '(<>)' but with 'pY'' applied to the second argument.
--
-- ==== __Examples__
--
-- >>> [10]?<>[3,4]
-- [13,14,3,4]
(?=>) ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
(?=>) =
  l2p < pY'


-- | Prefix version of '(?=>)'.
mPY ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
mPY =
  (?=>)


-- | A certainty followed by a possibility.
--
-- Like '(<>)' but with 'pY'' applied to the first argument.
--
(<=?) ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
(<=?) =
  l2p ^. pY'


-- | Prefix version of '(<=?)'
pYM ::
  ( Alternative t
  , Monoid m
  )
    => t m -> t m -> t m
pYM =
  (<=?)


-- | A certainty followed by a possibility.
-- Returns the value of the certainty.
--
-- @
-- x <*? y ≡ x <* py y
-- @
(<*?) ::
  ( Alternative t
  , Applicative t
  )
    => t m -> t m -> t m
x <*? y =
  x <* py y


-- | Prefix of '(<*?)'.
apy ::
  ( Alternative t
  , Applicative t
  )
    => t m -> t m -> t m
apy =
  (<*?)


-- | A possibility followed by a certainty.
-- Returns the value of the certainty.
--
-- @
-- x ?*> y ≡ py x *> y
-- @
(?*>) ::
  ( Alternative t
  , Applicative t
  )
    => t m -> t m -> t m
x ?*> y =
  py x *> y


-- | Prefix of '(?*>)'.
pya ::
  ( Alternative t
  , Applicative t
  )
    => t m -> t m -> t m
pya =
  (?*>)


-- | Works like 'py', but counts the number of results instead.
lPy ::
  ( Alternative t
  )
    => t a -> t Nat
lPy =
  lPY


-- | Like 'lPy' but on a more general type.
-- The output contains a type which cannot be determined from the input alone.
-- As a result, this can cause type inferencce problems where 'lPy' does not.
lPY ::
  ( Alternative t
  , Integral n
  )
    => t a -> t n
lPY =
  m l < py
