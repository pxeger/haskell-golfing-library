{-|
Module :
  P.Alternative.Between
Description :
  Functions related to the "between" function
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Alternative.Between
  ( bw
  , bw'
  , bW
  , bW'
  , lBw
  , lBW
  -- * Deprecated
  , between
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , (-)
  )


import P.Algebra.Monoid
import P.Aliases
import P.Alternative
import P.Alternative.AtMost
import P.Applicative
import P.Arithmetic.Nat
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Ord


{-# Deprecated between "Use bw instead" #-}
-- | Long version of 'bw'.
between ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> n -> t a -> t (List a)
between =
  bw


-- | Between @n@ and @m@.
--
-- ==== __Examples__
--
-- Get every binary string of length 2 or 3:
--
-- bw 2 3 "01"
-- ["000","001","00","010","011","01","100","101","10","110","111","11"]
bw ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> n -> t a -> t (List a)
bw n m x
  | n > m
  =
    p []
  | m >= n
  =
    xly n x *^* am (m - n) x


-- | Between @n@ and @m@
--
-- Similar to 'am' but in the reverse order.
-- Smaller results come earlier.
--
bw' ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> n -> t a -> t (List a)
bw' n m x
  | n > m
  =
    p []
  | m >= n
  =
    xly n x *^* am' (m - n) x


-- | Works like 'bw', but the result is combined into one element using the monoidal product.
bW ::
  ( Integral n
  , Ord n
  , Monoid a
  , Alternative t
  )
    => n -> n -> t a -> t a
bW =
  m fo <<< bw


-- | A combination of 'bW' and 'bw''.
--
-- Parses between @n@ and @m@ of a monoid with a preference for fewer.
bW' ::
  ( Integral n
  , Ord n
  , Monoid a
  , Alternative t
  )
    => n -> n -> t a -> t a
bW' =
  m fo <<< bw'


-- | Works like 'bw', but counts the number of results instead.
lBw ::
  ( Integral n
  , Ord n
  , Alternative t
  )
    => n -> n -> t a -> t Nat
lBw =
  lBw


-- | Like 'lBw' but on a more general type.
-- The output contains a type which cannot be determined from the input alone.
-- As a result, this can cause type inferencce problems where 'lBw' does not.
lBW ::
  ( Integral n
  , Ord n
  , Alternative t
  , Integral m
  )
    => n -> n -> t a -> t m
lBW =
  m l <<< bw
