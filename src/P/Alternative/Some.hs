{-|
Module :
  P.Alternative.Some
Description :
  Functions related to the "some" function
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Alternative.Some
  ( so
  , so'
  , sO
  , sO'
  , (+<>)
  , msO
  , (<>+)
  , sOm
  , (+=>)
  , mSO
  , (<=+)
  , sOM
  , (+<*)
  , soa
  , (<*+)
  , aso
  , (+*>)
  , soA
  , (*>+)
  , aSo
  , lSo
  , lSO
  , sot
  , sOt
  , sow
  , soW
  -- * Deprecated
  , some
  , someTill
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import qualified Control.Applicative


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Aliases
import P.Alternative
import P.Alternative.Many
import P.Applicative
import P.Arithmetic.Nat
import P.Category
import P.Foldable
import P.Foldable.Length
import P.Function.Compose


infixr 7
    +<>
  , +=>
  , +<*
  , +*>
infixl 8
    <>+
  , <=+
  , <*+
  , *>+


{-# Deprecated some "Use so instead" #-}
-- | Long version of 'so'.
some ::
  ( Alternative t
  )
    => t a -> t (List a)
some =
  so


-- | One or more.
--
-- Equivalent to 'Control.Applicative.some'.
so ::
  ( Alternative t
  )
    => t a -> t (List a)
so =
  Control.Applicative.some


-- | One or more.
--
-- Similar to 'so' but in the reverse order.
-- Smaller results come earlier.
so' ::
  ( Alternative t
  )
    => t a -> t (List a)
so' x =
  go
  where
    go =
      p < x ++ l2 (:) x go


-- | A combination of 'sO' and 'so''.
--
-- Parses one or more of a semigroup with a preference for fewer.
sO' ::
  ( Semigroup a
  , Alternative t
  )
    => t a -> t a
sO' x =
  go
  where
    go =
      x ++ x *^* go


-- | Some of something followed by something else.
--
-- Like '(<>)' but with 'sO' applied to the first argument.
(+<>) ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a -> t a
(+<>) =
  l2p < sO


-- | Prefix of '(+<>)'.
msO ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a -> t a
msO =
  (+<>)


-- | Something followed by some of something else.
--
-- Like '(<>)' but with 'sO' applied to the second argument.
(<>+) ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a -> t a
(<>+) =
  l2p ^. sO


-- | Prefix of '(<>+)'.
sOm ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a -> t a
sOm =
  (<>+)


-- | Some of something followed by something else.
--
-- Like '(<>)' but with 'sO'' applied to the second argument.
(+=>) ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a -> t a
(+=>) =
  l2p < sO'


-- | Prefix of '(+=>)'.
mSO ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a -> t a
mSO =
  (+=>)


-- | Something followed by some of something else.
--
-- Like '(<>)' but with 'sO'' applied to the second argument.
(<=+) ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a -> t a
(<=+) =
  l2p ^. sO'


-- | Prefix of '(<=+)'.
sOM ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a -> t a
sOM =
  (<=+)


-- | Some of something followed by something else.
-- Returns the value of the first input.
--
-- @
-- x +<* y ≡ so x <* y
-- @
(+<*) ::
  ( Alternative t
  )
    => t a -> t b -> t (List a)
(+<*) =
  (<*) < so


-- | Prefix of '(+<*)'.
soa ::
  ( Alternative t
  )
    => t a -> t b -> t (List a)
soa =
  (+<*)


-- | Something followed by some of something else.
-- Returns the value of the first input.
--
-- @
-- x <*+ y ≡ x <* so y
-- @
(<*+) ::
  ( Alternative t
  )
    => t a -> t b -> t a
(<*+) =
  (<*) ^. so


-- | Prefix of '(<*+)'.
aso ::
  ( Alternative t
  )
    => t a -> t b -> t a
aso =
  (<*+)


-- | Some of something followed by something else.
-- Returns the value of the second input.
--
-- @
-- x +*> y ≡ so x *> y
-- @
(+*>) ::
  ( Alternative t
  )
    => t a -> t b -> t b
(+*>) =
  (*>) < so


-- | Prefix of '(+*>)'.
soA ::
  ( Alternative t
  )
    => t a -> t b -> t b
soA =
  (+*>)


-- | Something followed by some of something else.
-- Returns the value of the second input.
--
-- @
-- x *>+ y ≡ x *> so y
-- @
(*>+) ::
  ( Alternative t
  )
    => t a -> t b -> t (List b)
(*>+) =
  (*>) ^. so


-- | Prefix of 'aSo'.
aSo ::
  ( Alternative t
  )
    => t a -> t b -> t (List b)
aSo =
  (*>+)


-- | Works like 'so', but the result is combined into one element using the monoidal product.
sO ::
  ( Monoid a
  , Alternative t
  )
    => t a -> t a
sO =
  m fo < so


-- | Works like 'so', but counts the numer of results instead.
lSo ::
  ( Alternative t
  )
    => t a -> t Nat
lSo =
  lSO


-- | Like 'lSo' but on a more general type.
-- The output contains a type which cannot be determined from the input alone.
-- As a result, this can cause type inferencce problems where 'lSo' does not.
lSO ::
  ( Alternative t
  , Integral n
  )
    => t a -> t n
lSO =
  m l < so


-- | Works like 'so' but combines results using a user defined function.
sow ::
  ( Alternative t
  )
    => (b -> a -> a) -> a -> t b -> t a
sow f s a =
  l2 f a $ sow f s a ++ p s


-- | Works like 'so'' but combines results using a user defined function.
soW ::
  ( Alternative t
  )
    => (b -> a -> a) -> a -> t b -> t a
soW f s a =
  l2 f a $ p s ++ soW f s a


{-# Deprecated someTill "Use sot instead" #-}
-- | Long version of 'sot'.
someTill ::
  ( Alternative t
  )
    => t a -> t end -> t (List a)
someTill =
  sot


-- | Like 'myt' this takes two parsers and applies the first repeatedly until the second succeeds.
-- However 'sot' requires the first parser be applied at least once.
--
-- Results from the first parser are collected in a list.
sot ::
  ( Alternative t
  )
    => t a -> t end -> t (List a)
sot parser end =
  l2 (:) parser (myt parser end)


-- | Like 'mYt' this takes two parsers and applies the first repeatedly until the second succeeds.
-- However 'sOt' requires the first parser be applied at least once.
--
-- Results from the first parser are combined with the semigroup action.
sOt ::
  ( Semigroup m
  , Alternative t
  )
    => t m -> t end -> t m
sOt parser end =
  go
  where
    go =
      (parser <* end) ++ parser *^* go

