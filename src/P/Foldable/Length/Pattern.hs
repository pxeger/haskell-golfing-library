{-# Language ViewPatterns #-}
{-# Language PatternSynonyms #-}
{-|
Module :
  P.Folable.Length.Pattern
Description :
  Patterns having to do with the size of containers
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Patterns for getting the size of things.
-}
module P.Foldable.Length.Pattern
  ( pattern Ø
  , pattern NO
  )
  where


import qualified Prelude


import P.Bool
import P.Foldable
import P.Foldable.Length


-- Imports for for documentation and pragmata
import P.Aliases
import P.Bifunctor.Either


-- TODO: There are a lot more pragmata that could be added
{-# Complete Ø, NO :: List #-}
{-# Complete Ø, NO :: Either #-}
{-# Complete Ø, Rit :: Either #-}
-- | A pattern which matches on empty structures.
--
-- ==== __Examples__
--
-- For 'Either', @Ø@ is a short way to match left values ignoring their internals.
-- The alternative would be @Le _@, which is two bytes longer.
--
-- >>> isLeft Ø=T;isLeft _=B
-- >>> isLeft (Rit 0)
-- False
-- >>> isLeft (Lef 0)
-- True
--
pattern Ø ::
  ( Foldable t
  )
    => t a
pattern Ø <- (ø -> T)


{-# Complete [], NO :: List #-}
{-# Complete Lef, NO :: Either #-}
-- | A pattern that matches on non-empty structures.
--
-- Shorter than @(_:_)@ on 'List's.
--
-- ==== __Examples__
--
-- For 'Either', @NO@ is a short way to match right values ignoring their internals.
-- The alternative would be @Pu _@, which is two bytes longer.
--
-- >>> isLeft NO=B;isLeft _=T
-- >>> isLeft (Rit 0)
-- False
-- >>> isLeft (Lef 0)
-- True
--
pattern NO ::
  ( Foldable t
  )
    => t a
pattern NO <- (nø -> T)
