{-|
Module :
  P.Foldable.Extra
Description :
  Additional functions for the P.Foldable library
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Foldable library.
-}
module P.Foldable.Extra
  where


import P.Foldable

-- | Determines if a structure contains 0.
h0 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h0 =
  e 0


-- | Negation of 'h0'. Returns the opposite result.
ne0 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne0 =
  ne 0


-- | Counts the number of times a structure contains 0.
ce0 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce0 =
  ce 0


-- | Counts the number of elements in a structure not equal to 0.
cE0 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE0 =
  cne 0


-- | Determines if a structure contains 1.
h1 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h1 =
  e 1


-- | Negation of 'h1'. Returns the opposite result.
ne1 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne1 =
  ne 1


-- | Counts the number of times a structure contains 1.
ce1 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce1 =
  ce 1


-- | Counts the number of elements in a structure not equal to 1.
cE1 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE1 =
  cne 1


-- | Determines if a structure contains 2.
h2 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h2 =
  e 2


-- | Negation of 'h2'. Returns the opposite result.
ne2 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne2 =
  ne 2


-- | Counts the number of times a structure contains 2.
ce2 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce2 =
  ce 2


-- | Counts the number of elements in a structure not equal to 2.
cE2 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE2 =
  cne 2


-- | Determines if a structure contains 3.
h3 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h3 =
  e 3


-- | Negation of 'h3'. Returns the opposite result.
ne3 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne3 =
  ne 3


-- | Counts the number of times a structure contains 3.
ce3 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce3 =
  ce 3


-- | Counts the number of elements in a structure not equal to 3.
cE3 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE3 =
  cne 3


-- | Determines if a structure contains 4.
h4 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h4 =
  e 4


-- | Negation of 'h4'. Returns the opposite result.
ne4 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne4 =
  ne 4


-- | Counts the number of times a structure contains 4.
ce4 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce4 =
  ce 4


-- | Counts the number of elements in a structure not equal to 4.
cE4 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE4 =
  cne 4


-- | Determines if a structure contains 5.
h5 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h5 =
  e 5


-- | Negation of 'h5'. Returns the opposite result.
ne5 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne5 =
  ne 5


-- | Counts the number of times a structure contains 5.
ce5 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce5 =
  ce 5


-- | Counts the number of elements in a structure not equal to 5.
cE5 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE5 =
  cne 5


-- | Determines if a structure contains 6.
h6 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h6 =
  e 6


-- | Negation of 'h6'. Returns the opposite result.
ne6 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne6 =
  ne 6


-- | Counts the number of times a structure contains 6.
ce6 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce6 =
  ce 6


-- | Counts the number of elements in a structure not equal to 6.
cE6 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE6 =
  cne 6


-- | Determines if a structure contains 7.
h7 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h7 =
  e 7


-- | Negation of 'h7'. Returns the opposite result.
ne7 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne7 =
  ne 7


-- | Counts the number of times a structure contains 7.
ce7 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce7 =
  ce 7


-- | Counts the number of elements in a structure not equal to 7.
cE7 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE7 =
  cne 7


-- | Determines if a structure contains 8.
h8 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h8 =
  e 8


-- | Negation of 'h8'. Returns the opposite result.
ne8 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne8 =
  ne 8


-- | Counts the number of times a structure contains 8.
ce8 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce8 =
  ce 8


-- | Counts the number of elements in a structure not equal to 8.
cE8 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE8 =
  cne 8


-- | Determines if a structure contains 9.
h9 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
h9 =
  e 9


-- | Negation of 'h9'. Returns the opposite result.
ne9 ::
  ( Num i
  , Foldable t
  , Eq i
  )
    => t i -> Bool
ne9 =
  ne 9


-- | Counts the number of times a structure contains 9.
ce9 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
ce9 =
  ce 9


-- | Counts the number of elements in a structure not equal to 9.
cE9 ::
  ( Num i
  , Foldable t
  , Eq i
  , Integral j
  )
    => t i -> j
cE9 =
  cne 9
