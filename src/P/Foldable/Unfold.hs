{-# Language FlexibleContexts #-}
module P.Foldable.Unfold
  ( ufo
  , fuf
  , uf1
  , ff1
  , ufl
  , ffl
  , ul1
  , fl1
  , ufu
  , fFu
  , ufU
  , fFU
  , uuf
  , uUf
  , uue
  , fue
  , uUe
  , fUe
  , uef
  , uEf
  , uuz
  , fuz
  , uUz
  , fUz
  , uzf
  , uZf
  -- * Iterate
  , ix
  , fX
  , ixu
  , ixe
  , fxe
  , ixz
  , fxz
  -- * Deprecated
  , unfoldr
  , unfoldl
  , iterate
  )
  where


import qualified Prelude
import Prelude
  ( Integer
  , Num
  )


import qualified Data.List


import P.Aliases
import P.Arrow
import P.Eq
import P.Eq.Extra
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.Map.Class
import P.Reverse


{-# Deprecated unfoldr "Use ufo instead" #-}
-- | Long version of 'ufo'.
unfoldr ::
  ( Indexable f Integer
  )
    => (b -> f (a, b)) -> b -> List a
unfoldr =
  ufo


-- | Right "unfold" a list from a generating function.
--
-- @f@ should ideally be a 'P.Maybe.Maybe', although 'List' can be used as a substitute.
--
-- More general version of 'Data.List.unfoldr'.
--
ufo ::
  ( Indexable f Integer
  )
    => (a -> f (b, a)) -> a -> List b
ufo f x =
  case
    f x !? 0
  of
    [] ->
      []
    (y, x') : _ ->
      y : ufo f x'


-- | Flip of 'ufo'.
fuf ::
  ( Indexable f Integer
  )
    => a -> (a -> f (b, a)) -> List b
fuf =
  F ufo


-- | A right "unfold" uses the same value for both the accumulator and the result.
--
-- @f@ should ideally be a 'P.Maybe.Maybe', although 'List' can be used as a substitute.
uf1 ::
  ( Indexable f Integer
  )
    => (a -> f a) -> a -> List a
uf1 f x =
  case
    f x !? 0
  of
    [] ->
      [x]
    x' : _ ->
      x : uf1 f x'


-- | Flip of 'uf1'.
ff1 ::
  ( Indexable f Integer
  )
    => a -> (a -> f a) -> List a
ff1 =
  F uf1


{-# Deprecated unfoldl "Use ufl instead" #-}
-- | Long version of 'ufl'.
unfoldl ::
  ( Indexable f Integer
  )
    => (b -> f (a, b)) -> b -> List a
unfoldl =
  ufl


-- | Left unfold into a list.
-- Unlike the right unfold ('ufo') this is not lazy and must fully evaluate to return.
--
-- @f@ should ideally be a 'P.Maybe.Maybe', although 'List' can be used as a substitute.
ufl ::
  ( Indexable f Integer
  )
    => (a -> f (b, a)) -> a -> List b
ufl =
  Rv << ufo


-- | Flip of 'ufl'.
ffl ::
  ( Indexable f Integer
  )
    => a -> (a -> f (b, a)) -> List b
ffl =
  F ufl


-- | A left "unfold" uses the same value for both the accumulator and the result.
-- Unlike the right unfold ('uf1') this is not lazy and must fully evaluate to return.
--
-- @f@ should ideally be a 'P.Maybe.Maybe', although 'List' can be used as a substitute.
ul1 ::
  ( Indexable f Integer
  )
    => (a -> f a) -> a -> List a
ul1 =
  Rv << uf1


-- | Flip of 'ul1'.
fl1 ::
  ( Indexable f Integer
  )
    => a -> (a -> f a) -> List a
fl1 =
  F ul1


{-# Deprecated iterate "Use ix instead" #-}
-- | Long version of 'ix'.
iterate ::
  (
  )
    => (a -> a) -> a -> List a
iterate =
  ix


-- | Take a function and a starting value and repeatedly apply the function to the result.
--
-- Equivalent to 'Data.List.iterate'.
ix ::
  (
  )
    => (a -> a) -> a -> List a
ix =
  Data.List.iterate


-- | Flip of 'ix'.
fX ::
  (
  )
    => a -> (a -> a) -> List a
fX =
  F ix


-- | Unfold to the right until the accumulator meets a particular condition.
--
-- ===== __Examples__
--
-- >>> ufu lt1 (Sw < fvD 2) 9
-- [1,0,0,1]
-- >>> ufu lt1 (Sw < fvD 2) 90
-- [0,1,0,1,1,0,1]
--
ufu ::
  (
  )
    => Predicate a -> (a -> (b, a)) -> a -> List b
ufu pred f =
  go
  where
    go x
      | pred x
      =
        []
      | Prelude.otherwise
      =
        let
          (y, x') =
            f x
        in
          y : go x'


-- | Flip of 'ufu'.
fFu ::
  (
  )
    => (a -> (b, a)) -> Predicate a -> a -> List b
fFu =
  f' ufu


-- | Unfold to the left until the accumulator meets a particular condition.
ufU ::
  (
  )
    => Predicate a -> (a -> (b, a)) -> a -> List b
ufU =
  Rv <<< ufu


-- | Flip of 'ufU'.
fFU ::
  (
  )
    => (a -> (b, a)) -> Predicate a -> a -> List b
fFU =
  f' ufU


-- | 'ufu' with a precomposed fanout.
uuf ::
  (
  )
    => Predicate a -> (a -> b) -> (a -> a) -> a -> List b
uuf x =
  ufu x << (-<)


-- | 'ufU' with a precomposed fanout.
uUf ::
  (
  )
    => Predicate a -> (a -> b) -> (a -> a) -> a -> List b
uUf x =
  ufU x << (-<)


-- | Unfold to the right until the accumulator is empty.
uue ::
  ( Foldable f
  )
    => (f a -> (b, f a)) -> f a -> List b
uue =
  ufu ø


-- | Flip of 'uue'.
fue ::
  ( Foldable f
  )
    => f a -> (f a -> (b, f a)) -> List b
fue =
  f' uue


-- | Unfold to the left until the accumulator is empty.
uUe ::
  ( Foldable f
  )
    => (f a -> (b, f a)) -> f a -> List b
uUe =
  ufU ø


-- | Flip of 'uUe'.
fUe ::
  ( Foldable f
  )
    => f a -> (f a -> (b, f a)) -> List b
fUe =
  f' uUe


-- | 'uue' with precomposed fanout.
uef ::
  ( Foldable f
  )
    => (f a -> b) -> (f a -> f a) -> f a -> List b
uef =
  uuf ø


-- | 'uUe' with precomposed fanout.
uEf ::
  ( Foldable f
  )
    => (f a -> b) -> (f a -> f a) -> f a -> List b
uEf =
  uUf ø


-- | Unfold to the right until the accumulator is zero.
uuz ::
  ( Eq a
  , Num a
  )
    => (a -> (b, a)) -> a -> List b
uuz =
  ufu eq0


-- | Flip of 'uuz'.
fuz ::
  ( Eq a
  , Num a
  )
    => a -> (a -> (b, a)) -> List b
fuz =
  f' uuz


-- | Unfold to the left until the accumulator is zero.
uUz ::
  ( Eq a
  , Num a
  )
    => (a -> (b, a)) -> a -> List b
uUz =
  ufU eq0


-- | Flip of 'uUz'.
fUz ::
  ( Eq a
  , Num a
  )
    => a -> (a -> (b, a)) -> List b
fUz =
  f' uUz


-- | 'uuz' with precomposed fanout.
uzf ::
  ( Eq a
  , Num a
  )
    => (a -> b) -> (a -> a) -> a -> List b
uzf =
  uuf eq0


-- | 'uUz' with precomposed fanout.
uZf ::
  ( Eq a
  , Num a
  )
    => (a -> b) -> (a -> a) -> a -> List b
uZf =
  uUf eq0


-- | Iterate a function until the value satisfies a predicate.
-- Collect all the values that satisfy the predicate into a list.
--
-- ===== __Examples__
--
-- >>> ixu lt3 (//2) 96
-- [96,48,24,12,6,3]
--
ixu ::
  (
  )
    => Predicate a -> (a -> a) -> a -> List a
ixu pred =
  Data.List.takeWhile (Prelude.not < pred) << Data.List.iterate


-- | Flip of 'ixu'.
fXu ::
  (
  )
    => (a -> a) -> Predicate a -> a -> List a
fXu =
  f' ixu


-- | Iterate until the value is empty.
-- Collect all the non-empty values into a list.
ixe ::
  ( Foldable f
  )
    => (f a -> f a) -> f a -> List (f a)
ixe =
  ixu ø


-- | Flip of 'ixe'.
fxe ::
  ( Foldable f
  )
    => f a -> (f a -> f a) -> List (f a)
fxe =
  f' ixe


-- | Iterate until the value is zero.
-- Collect all the non-zero values into a list.
ixz ::
  ( Eq a
  , Num a
  )
    => (a -> a) -> a -> List a
ixz =
  ixu eq0


-- | Flip of 'ixz'.
fxz ::
  ( Eq a
  , Num a
  )
    => a -> (a -> a) -> List a
fxz =
  f' ixz
