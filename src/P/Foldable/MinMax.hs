module P.Foldable.MinMax
  ( mx
  , mn
  , x_W
  , xW
  , fxW
  , m_W
  , mW
  , fmW
  , xB
  , fxB
  , mB
  , fmB
  , xM
  , fxM
  , nM
  , fnM
  , xb
  , fxb
  , xbW
  , xbB
  , xbM
  , mb
  , fmb
  , mbW
  , mbB
  , mbM
  , xd
  , fxd
  , xdW
  , xdB
  , xdM
  , md
  , fmd
  , ndW
  , mdB
  , mdM
  , xBl
  , xBL
  , mBl
  , xMl
  , mMl
  -- * Extract extrema
  , xxo
  , xoW
  , xoB
  , xol
  , mmo
  , moW
  , moB
  , mol
  -- * Deprecated
  , maximum
  , minimum
  , maximumBy
  , minimumBy
  , maximumWith
  , minimumWith
  , maximumBound
  , minimumBound
  , maximumDef
  , minimumDef
  , xoW_
  )
  where


import Prelude
  ( Foldable
  , Integral
  , String
  )
import qualified Prelude
import qualified Data.List


import P.Aliases
import P.Applicative
import P.Bool
import P.Category
import P.Comonad
import P.Foldable
import P.Foldable.Length
import P.Function
import P.Function.Compose
import P.Function.Flip
import P.List
import P.Ord
import P.Ord.MinMax
import P.Tuple
import P.Zip.SemiAlign


{-# Deprecated maximum "Use mx instead" #-}
-- | Long version of 'mx'.
-- Gets the maximum of a structure.
maximum ::
  ( Ord a
  , Foldable t
  )
    => t a -> a
maximum =
  x_W "maximum called on an empty structure" cp


-- | Takes a foldable and returns the maximum element.
--
-- Equivalent to 'Data.List.maximum'.
--
-- This errors on an empty structure.
mx ::
  ( Ord a
  , Foldable t
  )
    => t a -> a
mx =
  x_W "mx called on an empty structure" cp


{-# Deprecated minimum "Use mn instead" #-}
-- | Long version of 'mn'.
-- Gets the maximum of a structure.
minimum ::
  ( Ord a
  , Foldable t
  )
    => t a -> a
minimum =
  m_W "minimum called on an empty structure" cp


-- | Takes a foldable and returns the minimum element.
--
-- Equivalent to 'Data.List.minimum'.
--
-- This errors on an empty structure.
mn ::
  ( Ord a
  , Foldable t
  )
    => t a -> a
mn =
  m_W "mn called on an empty structure" cp


{-# Deprecated maximumBy "Use xW instead" #-}
-- | Long version of 'xW'.
maximumBy ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> t a -> a
maximumBy =
  x_W "maximumBy called on an empty structure"


-- | Like 'xW' but with a custom error message for empty structures.
--
-- For internal use.
x_W ::
  ( Foldable t
  )
    => String -> (a -> a -> Ordering) -> t a -> a
x_W errorString ordering =
  rFp (Prelude.errorWithoutStackTrace errorString) <
    lHM (maW ordering)


-- | Gives the largest element of a structure with respect to a provided comparison.
--
-- Defaults in favor of earlier elements.
--
-- Similar to 'Data.List.maximumBy' except this defaults to earlier elements to be consistent with 'mW' while 'Data.List.maximumBy' defaults to later elements.
--
-- This errors on an empty structure.
xW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> t a -> a
xW =
  x_W "xW called on an empty structure"


-- | Flip of 'xW'.
fxW ::
  ( Foldable t
  )
    => t a -> (a -> a -> Ordering) -> a
fxW =
  f' $ x_W "fxW called on an empty structure"


{-# Deprecated minimumBy "Use mW instead" #-}
-- | Long version of 'mW'.
minimumBy ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> t a -> a
minimumBy =
  m_W "minimumBy called on an empty structure"


-- | Like 'mW' but with a custom error message for empty structures.
--
-- For internal use.
m_W ::
  ( Foldable t
  )
    => String -> (a -> a -> Ordering) -> t a -> a
m_W errorString ordering =
  rFp (Prelude.errorWithoutStackTrace errorString) <
    lHM (maW $ RC ordering)


-- | Gives the smallest element of a structure with respect to a provided comparison.
--
-- Defaults in favor of earlier elements.
--
-- Equivalent to 'Data.List.minimumBy'.
--
-- This errors on an empty structure.
mW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> t a -> a
mW =
  m_W "mW called on an empty structure"


-- | Flip of 'mW'.
fmW ::
  ( Foldable t
  )
    => t a -> (a -> a -> Ordering) -> a
fmW =
  f' $ m_W "fmW called on an empty structure"


{-# Deprecated maximumWith "Use xB instead" #-}
-- | Long version of 'xB'.
maximumWith ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> t a -> a
maximumWith =
  x_W "maximumWith called on an empty structure" < cb


-- | Takes a conversion function and a list.
-- Gives the maximum element of the list as if the elements were the result of the conversion function.
--
-- Defaults in favor of earlier elements.
--
-- This errors on an empty structure.
--
-- ==== __Examples__
--
-- We can use @xB sh@ to get the lexographical maximum:
--
-- >>> xB sh (0 ## 25)
-- 9
--
-- We can use @xB l@ to get the longest string:
--
-- >>> xB l ["aardvark", "apple", "beard", "beaver", "cat", "chord"]
-- "aardvark"
xB ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> t a -> a
xB =
  x_W "xB called on an empty structure" < cb


-- | Flip of 'xB'.
fxB ::
  ( Ord b
  , Foldable t
  )
    => t a -> (a -> b) -> a
fxB =
  f' $ x_W "fxB called on an empty structure" < cb


{-# Deprecated minimumWith "Use mB instead" #-}
-- | Long version of 'mB'.
minimumWith ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> t a -> a
minimumWith =
  m_W "minimumWith called on an empty structure" < cb


-- | Takes a conversion function and a list.
-- Gives the minimum element of the list as if the elements were the result of the conversion function.
--
-- Defaults in favor of earlier elements.
--
-- This errors on an empty structure.
--
-- ==== __Examples__
--
-- We can use @mB sh@ to get the lexographical minimum:
--
-- >>> mB sh (5 ## 25)
-- 10
--
-- We can use @mB l@ to get the shortest string:
--
-- >>> mB l ["aardvark", "apple", "beard", "beaver", "cat", "chord"]
-- "cat"
mB ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> t a -> a
mB =
  m_W "mB called on an empty structure" < cb


-- | Flip of 'mB'.
fmB ::
  ( Ord b
  , Foldable t
  )
    => t a -> (a -> b) -> a
fmB =
  f' $ m_W "fmB called on an empty structure" < cb


-- | Takes a user conversion function and gets the maximum result using that function.
-- As opposed to 'xB' which gets the original value that produces the largest output this gives the largest output.
--
-- On a functor this is the same as mapping and taking the maximum, but it works more generally.
--
-- This errors on an empty structure.
--
-- ===== __Examples__
--
-- Get the length of the longest list
--
-- >>> xM l ["test","testing","","test"]
-- 7
xM ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> t a -> b
xM func =
  xdM func (Prelude.errorWithoutStackTrace "xM called on an empty structure")


-- | Flip of 'xM'.
fxM ::
  ( Ord b
  , Foldable t
  )
    => t a -> (a -> b) -> b
fxM =
  F xM


-- | Takes a user conversion function and gets the minimum result using that function.
-- As opposed to 'mB' which gets the original value that produces the smallest output this gives the largest output.
--
-- On a functor this is the same as mapping and taking the minimum, but it works more generally.
--
-- This errors on an empty structure.
--
-- ===== __Examples__
--
-- Get the length of the shortest list
--
-- >>> nM l ["test","testing","","test"]
-- 0
nM ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> t a -> b
nM func =
  mdM func (Prelude.errorWithoutStackTrace "nM called on an empty structure")


-- | Flip of 'nM'.
fnM ::
  ( Ord b
  , Foldable t
  )
    => t a -> (a -> b) -> b
fnM =
  F nM


{-# Deprecated maximumBound "Use xb instead" #-}
-- | Long version of 'xb'.
maximumBound ::
  ( Ord a
  , Foldable t
  )
    => a -> t a -> a
maximumBound =
  xb


-- | Gets the largest element smaller than the given bound.
-- If no element is smaller than the given bound the bound is given back.
xb ::
  ( Ord a
  , Foldable t
  )
    => a -> t a -> a
xb =
  xbW cp


-- | Flip of 'xb'.
fxb ::
  ( Ord a
  , Foldable t
  )
    => t a -> a -> a
fxb =
  F xb


-- | Like 'xb' except with a user defined comparison.
--
-- In the case that multiple values are the largest it returns the earliest one.
xbW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> a -> t a -> a
xbW =
  Prelude.foldr < maW


-- | Like 'xb' except with a user defined conversion function.
--
-- In the case that multiple values are the largest it returns the earliest one.
xbB ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> a -> t a -> a
xbB =
  xbW < on cp


-- | Like 'xbB' except it gives the result of the conversion function rather than the input.
--
-- In the case that multiple values are the largest it returns the earliest one.
xbM ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> b -> t a -> b
xbM =
  Prelude.foldr < m ma


{-# Deprecated minimumBound "Use mb instead" #-}
-- | Long version of 'mb'.
minimumBound ::
  ( Ord a
  , Foldable t
  )
    => a -> t a -> a
minimumBound =
  mb


-- | Gets the smallest element larger than the given bound.
-- If no element is larger than the given bound the bound is given back.
mb ::
  ( Ord a
  , Foldable t
  )
    => a -> t a -> a
mb =
  mbW cp


-- | Flip of 'mb'.
fmb ::
  ( Ord a
  , Foldable t
  )
    => t a -> a -> a
fmb =
  F mb


-- | Like 'xb' except with a user defined comparison.
--
-- In the case that multiple values are the largest it returns the earliest one.
mbW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> a -> t a -> a
mbW =
  Prelude.foldr < mNW


-- | Like 'xb' except with a user defined conversion function.
--
-- In the case that multiple values are the largest it returns the earliest one.
mbB ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> a -> t a -> a
mbB =
  mbW < on cp


-- | Like 'mbB' except it gives the result of the conversion function rather than the input.
--
-- In the case that multiple values are the largest it returns the earliest one.
mbM ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> b -> t a -> b
mbM =
  Prelude.foldr < m mN


{-# Deprecated maximumDef "Use xd instead" #-}
-- | Long version of 'xd'.
maximumDef ::
  ( Ord a
  , Foldable t
  )
    => a -> t a -> a
maximumDef =
  xd


-- | Gets the maximum element with a default if the structure is empty.
xd ::
  ( Ord a
  , Foldable t
  )
    => a -> t a -> a
xd =
  xdW cp


-- | Flip of 'fxd'.
fxd ::
  ( Ord a
  , Foldable t
  )
    => t a -> a -> a
fxd =
  F xd


-- | Like 'xd' but with a user defined function.
xdW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> a -> t a -> a
xdW comparison =
  Prelude.snd << Prelude.foldr (maW (lcp comparison) < Bp T) < Bp B


-- | Like 'xd' but with a user defined conversion function.
xdB ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> a -> t a -> a
xdB =
  xdW < on cp


{-# Deprecated minimumDef "Use md instead" #-}
-- | Long version of 'md'.
minimumDef ::
  ( Ord a
  , Foldable t
  )
    => a -> t a -> a
minimumDef =
  md


-- | Gets the minimum element with a default if the structure is empty.
md ::
  ( Ord a
  , Foldable t
  )
    => a -> t a -> a
md =
  ndW cp


-- | Flip of 'fmd'.
fmd ::
  ( Ord a
  , Foldable t
  )
    => t a -> a -> a
fmd =
  F md


-- | Like 'md' but with a user defined function.
ndW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> a -> t a -> a
ndW comparison =
  Prelude.snd << Prelude.foldr (mNW (lcp comparison) < Bp B) < Bp T


-- | Like 'md' but with a user defined conversion function.
mdB ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> a -> t a -> a
mdB =
  ndW < on cp


-- | 'xM' with a default value.
xdM ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> b -> t a -> b
xdM func def xs =
  case
    Prelude.foldr go [] xs
  of
    [] ->
      def
    x : _ ->
      x
  where
    go x [] =
      [func x]
    go x (y : _) =
      [ma (func x) y]


-- | 'nM' with a default value.
mdM ::
  ( Ord b
  , Foldable t
  )
    => (a -> b) -> b -> t a -> b
mdM func def xs =
  case
    Prelude.foldr go [] xs
  of
    [] ->
      def
    x : _ ->
      x
  where
    go x [] =
      [func x]
    go x (y : _) =
      [mN (func x) y]


-- | Gets the element with the largest size.
-- If the input is empty it throws an error.
--
-- If two elements are the same size it defaults to the earlier one.
xBl ::
  ( Foldable f
  , Foldable g
  )
    => f (g a) -> g a
xBl =
  xB Prelude.length


-- | Gets the element with the smallest size.
-- If the input is empty it throws an error.
--
-- If two elements are the same size it defaults to the earlier one.
mBl ::
  ( Foldable f
  , Foldable g
  )
    => f (g a) -> g a
mBl =
  mB Prelude.length


-- | Gets the list with the largest size.
-- If the input is empty it returns an empty list.
--
-- If two elements are the same size it defaults to the earlier one.
xBL ::
  ( Foldable t
  )
    => t (List a) -> List a
xBL =
  xbB Prelude.length []


-- | Gets the size of the largest element.
-- Gives to 0 if no elements are present.
xMl ::
  ( Foldable f
  , Foldable g
  , Integral i
  , Ord i
  )
    => f (g a) -> i
xMl =
  xbM l 0


-- | Gets the size of the smallest element.
-- If the input is empty it throws an error.
mMl ::
  ( Foldable f
  , Foldable g
  , Integral i
  , Ord i
  )
    => f (g a) -> i
mMl =
  nM l


-- | Extract the maximum element from a list, returning the element and the list with it removed.
-- This prioritizes elements earlier in the list when there are multiple maxima.
xxo ::
  ( Foldable t
  , Ord a
  )
    => t a -> (a, List a)
xxo =
  xoW_ "xxo called on an empty structure" cp


-- | Like 'xxo' but with a user defined comparison operator.
xoW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> t a -> (a, List a)
xoW =
  xoW_ "xoW called on an empty structure"


-- | Like 'xxo' but using a user defined conversion function.
xoB ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> t a -> (a, List a)
xoB =
  xoW_ "xoB called on an empty structure" < cb


-- | Like 'xxo' but compares elements using their lengths.
xol ::
  ( Foldable t
  , Foldable f
  )
    => t (f a) -> (f a, List (f a))
xol =
  xoW_ "xol called on an empty structure" lCp


{-# Deprecated xoW_ "Internal function" #-}
-- | Like 'xoW' but with a custom error message on an empty list.
xoW_ ::
  ( Foldable t
  )
    => String -> (a -> a -> Ordering) -> t a -> (a, List a)
xoW_ errorString ordering =
  x_W errorString (on ordering st) < xts < tL


-- | Extract the minimum element from a list returning the element and the list with it removed.
-- This prioritiezes elements earlier in the list when there are multiple maxima.
mmo ::
  ( Foldable t
  , Ord a
  )
    => t a -> (a, List a)
mmo =
  xoW_ "mmo called on an empty structure" $ RC cp


-- | Like 'mmo' but with a user defined comparison operator.
moW ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> t a -> (a, List a)
moW =
  xoW_ "moW called on an empty structure" < RC


-- | Like 'mmo' but using a user defined conversion function.
moB ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> t a -> (a, List a)
moB =
  xoW_ "moB called on an empty structure" < RC < cb


-- | Like 'mmo' but compares elements using their lengths.
mol ::
  ( Foldable t
  , Foldable f
  )
    => t (f a) -> (f a, List (f a))
mol =
  xoW_ "mol called on an empty structure" $ RC lCp
