{-|
Module :
  P.Foldable.Length
Description :
  Functions for getting the size of something.
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Functions for getting the size of something.
Part of the P library.
-}
module P.Foldable.Length
  ( l
  , lnt
  , lg
  , ø
  , nø
  , eL
  , lL
  , leL
  , mL
  , meL
  , lEq
  , (|=|)
  , nlE
  , (|~|)
  , lLX
  , lGX
  , (|<|)
  , lLE
  , lGE
  , (|>|)
  , lCp
  , (|?|)
  -- * Deprecated
  , length
  , null
  , notNull
  , equalLength
  , ltLength
  , leLength
  , compareLength
  )
  where


import qualified Prelude
import Prelude
  ( Foldable
  , Integral
  , Int
  )


import qualified Data.Foldable as Foldable
import qualified Data.List


import P.Aliases
import P.Arithmetic
import P.Arithmetic.Nat
import P.Bool
import P.Eq
import P.Function.Compose
import P.Function.Flip
import P.Ord


infixr 4 |=|, |~|, |<|, |>|


{-# Deprecated length "Use l or lg instead" #-}
-- | Gets the length of something.
-- Use 'l', 'lnt' or 'lg' instead.
length ::
  (
  )
    => List a -> Int
length =
  l


-- | Gets the length of something as a natural number.
--
-- Unlike other length functions this is lazy so the input can be partially evaluated.
-- Useful in situations where performance is inportant or when dealing with potentially infinite lists.
--
-- ==== __Examples__
--
-- Since @lnt@ is lazy it can be used for comparing the sizes of infinite lists in finite time, or just really long lists quickly.
--
-- >>> cp (lnt [1,2,3]) (lnt [1..])
-- LT
-- >>> cp (lnt [1..]) (lnt [1..45])
-- GT
-- >>> eq (lnt [1..]) (lnt [1..45])
-- False
--
-- If you use 'lg' in any of the above cases evaluation will never halt.
lnt ::
  ( Foldable t
  )
    => t a -> Nat
lnt =
  Prelude.foldr (\ _ -> Succ) 0


-- | Gets the length of something.
-- Returns an 'Int'.
-- for a polymorphic version see 'l'.
lg ::
  ( Foldable t
  )
    => t a -> Int
lg =
  l


-- | Gets the length of something.
--
-- More general than 'Data.List.length', 'lg' or 'Data.List.genericLength'.
l ::
  ( Integral i
  , Foldable t
  )
    => t a -> i
l =
  Prelude.foldr (\ _ -> (Prelude.+ 1)) 0


{-# Deprecated null "Use ø instead" #-}
-- | Long version of 'ø'.
null ::
  ( Foldable t
  )
    => t a -> Bool
null =
  ø


-- | Test whether a structure is empty.
--
-- Equivalent to 'Data.List.null'.
--
-- ==== __Examples__
--
-- Can be used on 'P.Bifunctor.Either.Either's to determine if they are left or right:
--
-- >>> ø (Ri 3)
-- False
-- >>> ø (Le 3)
-- True
--
ø ::
  ( Foldable t
  )
    => t a -> Bool
ø =
  Data.List.null


{-# Deprecated notNull "Use nø instead" #-}
-- | Long version of 'nø'.
notNull ::
  ( Foldable t
  )
    => t a -> Bool
notNull =
  nø


-- | Test whether a structure is non-empty.
--
-- Gives the opposite result as 'ø'.
--
-- Equivalent to 'Data.List.notNull'.
--
-- ==== __Examples__
--
-- Can be used on 'P.Either.Either's to determine if they are left or right:
--
-- >>> nø (Ri 3)
-- True
-- >>> nø (Le 3)
-- False
--
nø ::
  ( Foldable t
  )
    => t a -> Bool
nø =
  n < ø


-- | Checks if the length of a struncture is equal to a certain value.
eL ::
  ( Foldable t
  , Integral i
  , Ord i
  )
    => i -> t a -> Bool
eL bound =
  go bound < Foldable.toList
  where
    go 0 [] =
      T
    go n _
      | 0 > n
      =
        B
    go n (_ : xs) =
      go (n - 1) xs
    go _ _ =
      B


-- | Checks if the length of a structure is less than a certain value.
--
-- It is lazy on the structure only evaluating up to the point where the count exceeds the bound.
lL ::
  ( Foldable t
  , Integral i
  , Ord i
  )
    => i -> t a -> Bool
lL bound =
  go bound < Foldable.toList
  where
    go n _
      | 1 > n
      =
        B
    go n (_ : xs) =
      go (n - 1) xs
    go n [] =
      T


-- | Checks if the length of a structure is less than or equal to a certain value.
--
-- It is lazy on the structure only evaluating up to the point where the count exceeds the bound.
leL ::
  ( Foldable t
  , Integral i
  , Ord i
  )
    => i -> t a -> Bool
leL =
  lL < pl 1


-- | Checks if the length of a structure is more than a certain value.
--
-- It is lazy on the structure only evaluating up to the point where the count exceeds the bound.
mL ::
  ( Foldable t
  , Integral i
  , Ord i
  )
    => i -> t a -> Bool
mL =
  n << leL


-- | Checks if the length of a structure is more than or equal to a certain value.
--
-- It is lazy on the structure only evaluating up to the point where the count exceeds the bound.
meL ::
  ( Foldable t
  , Integral i
  , Ord i
  )
    => i -> t a -> Bool
meL =
  n << lL


{-# Deprecated equalLength "Use lEq or (|=|) instead." #-}
-- | Long version of 'lEq'.
equalLength ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
equalLength =
  lEq


-- | Checks if the lengths of two structures are equal.
--
-- It is lazy on structures only evaluating up to the length of the shorter one.
lEq ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
lEq x1 x2 =
  lnt x1 == lnt x2


-- | Infix of 'lEq'.
(|=|) ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
(|=|) =
  lEq


-- | Checks if the lengths of two structures are not equal.
--
-- It is lazy on structures only evaluating up to the length of the shorter one.
nlE ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
nlE =
  n << lEq


-- | Infix of 'nlE'.
(|~|) ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
(|~|) =
  nlE


{-# Deprecated ltLength "Use lLX or (|<|) instead." #-}
-- | Long version of 'lLX'.
ltLength ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
ltLength =
  lLX


-- | Checks if the length of one structure is less than the length of another.
--
-- It is lazy on structures only evaluating up to the length of the shorter one.
--
-- Flip of 'lGX'.
lLX ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
lLX x1 x2 =
  lnt x2 > lnt x1


-- | Checks if the length of one structure is greater than the length of another.
--
-- It is lazy on structures only evaluating up to the length of the shorter one.
--
-- Flip of 'lLX'.
lGX ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
lGX =
  F lLX


-- | Infix of 'lLX' and 'lGX'.
(|<|) ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
(|<|) =
  lLX


{-# Deprecated leLength "Use lLE instead." #-}
-- | Long version of 'lLE'.
leLength ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
leLength =
  lLE


-- | Checks if the length of one structure is less than or equal to the length of another.
--
-- It is lazy on structures only evaluating up to the length of the shorter one.
--
-- Flip of 'lGE'.
lLE ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
lLE x1 x2 =
  lnt x2 >= lnt x1


-- | Checks if the length of one structure is greater than or equal to the length of another.
--
-- It is lazy on structures only evaluating up to the length of the shorter one.
--
-- Flip of 'lLE'.
lGE ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
lGE =
  F lLE

-- | Infix of 'lLE' and 'lGE'.
--
-- *Not* a flip of '(|<|)'.
(|>|) ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Bool
(|>|) =
  lGE


{-# Deprecated compareLength "Use lCp or (|?|) instead." #-}
-- | Long version of 'lCp'.
compareLength ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Ordering
compareLength =
  lCp


-- | Compare the lengths of two structures.
--
-- It is lazy on structures only evaluating up to the length of the shorter one.
lCp ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Ordering
lCp x1 x2 =
  cp (lnt x1) (lnt x2)


-- | Infix of 'lCp'.
(|?|) ::
  ( Foldable t1
  , Foldable t2
  )
    => t1 a -> t2 b -> Ordering
(|?|) =
  lCp
