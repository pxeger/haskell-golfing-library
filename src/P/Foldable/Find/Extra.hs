module P.Foldable.Find.Extra
  where

import P.Aliases
import P.Foldable.Find
import P.Function.Flip


-- | Gets the second element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if there is.
g2s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g2s =
  gNs (2 :: Int)


-- | Flip of 'g2s'.
fG2 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG2 =
  f' g2s


-- | Gets the third element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if there is.
g3s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g3s =
  gNs (3 :: Int)


-- | Flip of 'g3s'.
fG3 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG3 =
  f' g3s


-- | Gets the 4th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if there is.
g4s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g4s =
  gNs (4 :: Int)


-- | Flip of 'g4s'.
fG4 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG4 =
  f' g4s


-- | Gets the 5th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if there is.
g5s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g5s =
  gNs (5 :: Int)


-- | Flip of 'g5s'.
fG5 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG5 =
  f' g5s


-- | Gets the 6th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if there is.
g6s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g6s =
  gNs (6 :: Int)


-- | Flip of 'g6s'.
fG6 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG6 =
  f' g6s


-- | Gets the 7th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if there is.
g7s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g7s =
  gNs (7 :: Int)


-- | Flip of 'g7s'.
fG7 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG7 =
  f' g7s


-- | Gets the 8th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if there is.
g8s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g8s =
  gNs (8 :: Int)


-- | Flip of 'g8s'.
fG8 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG8 =
  f' g8s


-- | Gets the 9th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if there is.
g9s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g9s =
  gNs (9 :: Int)


-- | Flip of 'g9s'.
fG9 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG9 =
  f' g9s


-- | Gets the 10th element satisfying a predicate.
-- Returns the empty list if there isn't one and a singleton list if there is.
g0s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g0s =
  gNs (10 :: Int)


-- | Flip of 'g0s'.
fG0 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG0 =
  f' g0s


-- | Gets the 2nd element satisfying a predicate.
-- Errors when there are fewer than 2 elements satsifying the predicate.
g2 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g2 =
  gN (2 :: Int)


-- | Flip of 'g2'.
fg2 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg2 =
  f' g2


-- | Gets the 3rd element satisfying a predicate.
-- Errors when there are fewer than 3 elements satsifying the predicate.
g3 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g3 =
  gN (3 :: Int)


-- | Flip of 'g3'.
fg3 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg3 =
  f' g3


-- | Gets the 4th element satisfying a predicate.
-- Errors when there are fewer than 4 elements satsifying the predicate.
g4 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g4 =
  gN (4 :: Int)


-- | Flip of 'g4'.
fg4 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg4 =
  f' g4


-- | Gets the 5th element satisfying a predicate.
-- Errors when there are fewer than 5 elements satsifying the predicate.
g5 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g5 =
  gN (5 :: Int)


-- | Flip of 'g5'.
fg5 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg5 =
  f' g5


-- | Gets the 6th element satisfying a predicate.
-- Errors when there are fewer than 6 elements satsifying the predicate.
g6 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g6 =
  gN (6 :: Int)


-- | Flip of 'g6'.
fg6 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg6 =
  f' g6


-- | Gets the 7th element satisfying a predicate.
-- Errors when there are fewer than 7 elements satsifying the predicate.
g7 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g7 =
  gN (7 :: Int)


-- | Flip of 'g7'.
fg7 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg7 =
  f' g7


-- | Gets the 8th element satisfying a predicate.
-- Errors when there are fewer than 8 elements satsifying the predicate.
g8 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g8 =
  gN (8 :: Int)


-- | Flip of 'g8'.
fg8 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg8 =
  f' g8


-- | Gets the 9th element satisfying a predicate.
-- Errors when there are fewer than 9 elements satsifying the predicate.
g9 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g9 =
  gN (9 :: Int)


-- | Flip of 'g9'.
fg9 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg9 =
  f' g9


-- | Gets the 10th element satisfying a predicate.
-- Errors when there are fewer than 10 elements satsifying the predicate.
g0 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g0 =
  gN (10 :: Int)


-- | Flip of 'g0'.
fg0 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg0 =
  f' g0




