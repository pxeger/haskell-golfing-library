module P.Foldable.Uniques
  ( uqs
  , uQW
  , uQB
  , dpc
  , dPW
  , dpB
  , nb
  , nW
  , fnW
  , nB
  , fnB
  , lnb
  , lnB
  , lnW
  , xuq
  , xqW
  , xqB
  , nbT
  , nTf
  , nBT
  , nbr
  , nRw
  , nRb
  , unb
  , uNw
  , uNb
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )
import Data.List


import P.Aliases
import P.Bool
import P.Eq
import P.Foldable
import P.Foldable.Bag
import P.Foldable.Length
import P.Foldable.Length.Extra
import P.Function
import P.Function.Compose
import P.Function.Flip
import P.Monad.Plus.Filter
import P.Ord
import P.Ord.Bounded


-- | Get all elements that appear exactly once.
--
-- Elements are ordered by their first appearence in the input.
uqs ::
  ( Eq a
  , Foldable t
  )
    => t a -> List a
uqs =
  uQW eq


-- | Get all elements that appear exactly once using a user defined equality.
--
-- Elements are ordered by their first appearence in the input.
uQW ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List a
uQW =
  Prelude.head <<< fl lL2 << bGW


-- | Get all elements that appear exactly once using a user defined conversion function.
--
-- Elements are ordered by their first appearence in the input.
--
-- ==== __Examples__
--
-- Get elements with a unique length:
--
-- >>> uQB l ["Hello", "world", "nice", "to", "meet", "you"]
-- ["to","you"]
--
uQB ::
  ( Foldable t
  , Eq b
  )
    => (a -> b) -> t a -> List a
uQB =
  uQW < qb


-- | Get all elements that appear more than once.
--
-- Elements are ordered by their first appearence in the input.
dpc ::
  ( Eq a
  , Foldable t
  )
    => t a -> List a
dpc =
  Prelude.head << fl mL1 < bG


-- | Get all elements that appear more than once using a user defined equality.
--
-- Elements are ordered by their first appearence in the input.
dPW ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List a
dPW =
  Prelude.head <<< fl mL1 << bGW


-- | Get all elements that appear more than once using a user defined conversion function.
--
-- Elements are ordered by their first appearence in the input.
dpB ::
  ( Eq b
  , Foldable t
  )
    => (a -> b) -> t a -> List a
dpB =
  dPW < qb


-- | Removes duplicate occurences from a list after the first.
--
-- More general version of 'Data.List.nub'.
nb ::
  ( Eq a
  , Foldable t
  )
    => t a -> List a
nb =
  Prelude.head << bG


-- | Takes a equality function and a list and removes elements that are equal under the function.
-- It expects the provided function to be reflexive and transitive.
--
-- Equivalent to 'Data.List.nubBy'.
nW ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List a
nW =
  Prelude.head <<< bGW


-- | Flip of 'nW'.
fnW ::
  ( Foldable t
  )
    => t a -> (a -> a -> Bool) -> List a
fnW =
  F nW


-- | Takes a conversion function and a list.
-- Nubs the list as if the values were the result of the conversion function.
--
-- ==== __Examples__
--
-- We can use @nB@ with 'P.First.Extra.tk1' on a word list to get the first word of each letter.
--
-- >>> nB tk1 ["aardvark", "apple", "beard", "beaver", "cat", "chord"]
-- ["aardvark","beard","cat"]
nB ::
  ( Eq b
  , Foldable t
  )
    => (a -> b) -> t a -> List a
nB =
  nW < qb


-- | Flip of 'nB'.
fnB ::
  ( Eq b
  , Foldable t
  )
    => t a -> (a -> b) -> List a
fnB =
  F nB


-- | Counts the number of distinct elements in a given structure.
lnb ::
  ( Integral b
  , Foldable t
  , Eq a
  )
    => t a -> b
lnb =
  l < bg


-- | Counts the number of distinct elements in a given structure using a user defined equality function.
lnW ::
  ( Integral b
  , Foldable t
  )
    => (a -> a -> Bool) -> t a -> b
lnW =
  l << bGW


-- | Counts the number of distinct elements in a given structure using a user defined conversion function.
lnB ::
  ( Integral c
  , Foldable t
  , Eq b
  )
    => (a -> b) -> t a -> c
lnB =
  l << bgB


-- | Remove all unique elements from a structure.
xuq ::
  ( Eq a
  , Foldable t
  )
    => t a -> List a
xuq =
  xqW eq


-- | Remove all unique elements from a structure using a user defined equality function.
xqW ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List a
xqW userEq xs =
  fl (fay (dPW userEq xs) < userEq) (tL xs)


-- | Remove all unique elements from a structure using a user defined conversion function.
xqB ::
  ( Eq b
  , Foldable t
  )
    => (a -> b) -> t a -> List a
xqB =
  xqW < qb


-- | Takes a transitive (but not necessarily symmetric) relation @-<-@ and gives all elements @x@ such that there is not another element @y@ where @x -<- y@.
--
-- ==== __Examples__
--
-- Get the maxima of a list with '(>)':
--
-- >>> nbT (>) [1,9,2,3,9,7,9]
-- [9,9,9]
--
-- Get all maximal linear sublists:
--
-- >>> nbT fiS $ sSt (lq<δ) [1,3,5,7,8,9,9]
-- [[9,9],[1,3,5,7,9],[7,8,9],[1,8],[3,8],[5,8]]
--
nbT ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List a
nbT uGt xs =
  go [] (tL xs)
  where
    go ys [] =
      ys
    go ys (x : xs)
      | ay (`uGt` x) ys
      =
        go ys xs
      | Prelude.otherwise
      =
        go (x : fl (n < (x `uGt`)) ys) xs


-- | Like 'nbT' but it flips the relation.
nTf ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List a
nTf =
  nbT < f'


-- | Like 'nbT' but it takes a comparison operator instead of a boolean relation.
nBT ::
  ( Foldable t
  )
    => (a -> a -> Ordering) -> t a -> List a
nBT comp =
  nbT ((== GT) << comp)


-- | Performs a nub on a list returning the nubbed list and the duplicate elements removed.
--
-- ==== __Examples__
--
-- >>> nbr [1,3,1,2,3,2,2,1,2,2]
-- ([1,3,2],[1,1,3,2,2,2,2])
nbr ::
  ( Eq a
  , Foldable t
  )
    => t a -> (List a, List a)
nbr =
  nRw eq


-- | Like 'nbr' but with a user defined equality function.
nRw ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> (List a, List a)
nRw userEq x =
  let
    bagged =
      bGW userEq x
  in
    ( Prelude.head < bagged
    , Prelude.drop 1 Prelude.=<< bagged
    )

-- | Like 'nbr' but a user defined conversion function is used to determine equality.
nRb ::
  ( Foldable t
  , Eq b
  )
    => (a -> b) -> t a -> (List a, List a)
nRb =
  nRw < on eq


-- | Repeatedly applies 'nbr' to split a list into a series of shells.
--
-- This is the transpose of 'bG'.
--
-- ==== __Examples__
--
-- >>> unb [1,3,1,2,3,2,2,1,2,2]
-- [[1,3,2],[1,3,2],[1,2],[2],[2]]
unb ::
  ( Foldable t
  , Eq a
  )
    => t a -> List (List a)
unb =
  uNw eq


-- | Like 'unb' but with a user defined equality function.
uNw ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> List (List a)
uNw =
  Data.List.transpose << bGW


-- | Like 'unb' but a user defined conversion function is used to determine equality.
uNb ::
  ( Foldable t
  , Eq b
  )
    => (a -> b) -> t a -> List (List a)
uNb =
  uNw < on eq
