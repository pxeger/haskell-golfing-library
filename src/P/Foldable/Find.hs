module P.Foldable.Find
  ( gN
  , fgN
  , gNs
  , fGN
  , g1
  , fg1
  , g_N
  , g1s
  , fG1
  -- * Deprecated
  , find
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  , String
  , Int
  )


import qualified Data.List
import qualified Data.Maybe


import P.Aliases
import P.Arithmetic.Extra
import P.Foldable
import P.Function.Compose
import P.Function.Flip


-- | Gets the nth elemnt satisfying a predicate.
--
-- Errors with a custom message if there are less than n elements satsifying the predicate.
--
-- For internal use.
g_N ::
  ( Foldable t
  , Integral i
  )
    => String -> i -> Predicate a -> t a -> a
g_N errorM count pred xs =
  case
    gNs count pred xs
  of
    [] ->
      Prelude.error errorM
    x : _ ->
      x


-- | Gets the nth element satisfying a predicate.
--
-- Errors if there are less than n elements satisfying the predicate.
-- For a safe version returning a 'Maybe'' see 'gNs'.
gN ::
  ( Foldable t
  , Integral i
  )
    => i -> Predicate a -> t a -> a
gN =
  g_N "gN n pred called on a list with fewer than n elements satsifying pred"


-- | Flip of 'gN'.
fgN ::
  ( Foldable t
  , Integral i
  )
    => Predicate a -> i -> t a -> a
fgN =
  f' (g_N "fgN pred n called on a list with fewer than n elements satisfying pred")


-- | Gets the nth element satisfying a predicate.
--
-- When there are less than n elements satisfying the predicate this returns the empty list.
-- For an unsafe version see 'gN'.
gNs ::
  ( Foldable t
  , Integral i
  )
    => i -> Predicate a -> t a -> Maybe' a
gNs count pred =
  (!! S1 count) < rF go []
  where
    go x y
      | pred x
      =
        x : y
    go _ y =
      y

-- | Flip of 'gNs'.
fGN ::
  ( Foldable t
  , Integral i
  )
    => Predicate a -> i -> t a -> Maybe' a
fGN =
  f' gNs


{-# Deprecated find "Use g1 instead" #-}
-- | Long version of 'g1'.
-- Gets the first element satisfying a predicate.
find ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
find =
  g1s


-- | Gets the first element satisfying a predicate.
-- Returns the empty list if there is none and a singleton list if there is.
--
-- Equivalent to 'Data.List.find'.
g1s ::
  ( Foldable t
  )
    => Predicate a -> t a -> Maybe' a
g1s =
  Data.Maybe.maybeToList << Data.List.find


-- | Flip of 'g1s'.
fG1 ::
  ( Foldable t
  )
    => t a -> Predicate a -> Maybe' a
fG1 =
  F g1s


-- | Gets the first element satisfying a predicate.
-- Errors when there are fewer than 1 elements satsifying the predicate.
g1 ::
  ( Foldable t
  )
    => Predicate a -> t a -> a
g1 =
  gN (1 :: Int)


-- | Flip of 'g1'.
fg1 ::
  ( Foldable t
  )
    => t a -> Predicate a -> a
fg1 =
  f' g1
