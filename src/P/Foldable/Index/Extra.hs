{-|
Module :
  P.Foldable.Index.Extra
Description :
  Additional indexing functions for the P.Foldable library
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.Foldable library.
-}
module P.Foldable.Index.Extra
  where


import P.Aliases
import P.Foldable


-- | Index a foldable structure by 0.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx0 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx0 =
  fkx (0 :: Int)


-- | Index a foldable structure by 1.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx1 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx1 =
  fkx (1 :: Int)


-- | Index a foldable structure by 2.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx2 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx2 =
  fkx (2 :: Int)


-- | Index a foldable structure by 3.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx3 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx3 =
  fkx (3 :: Int)


-- | Index a foldable structure by 4.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx4 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx4 =
  fkx (4 :: Int)


-- | Index a foldable structure by 5.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx5 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx5 =
  fkx (5 :: Int)


-- | Index a foldable structure by 6.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx6 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx6 =
  fkx (6 :: Int)


-- | Index a foldable structure by 7.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx7 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx7 =
  fkx (7 :: Int)


-- | Index a foldable structure by 8.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx8 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx8 =
  fkx (8 :: Int)


-- | Index a foldable structure by 9.
-- If there is a value at that index it returns it wrapped in a list, otherwise it returns an empty list.
kx9 ::
  ( Foldable t
  )
    => t a -> Maybe' a
kx9 =
  fkx (9 :: Int)
