{-# Language StandaloneDeriving #-}
{-# Language InstanceSigs #-}
{-# Language RankNTypes #-}
module P.Monad.Free
  ( Free (..)
  , lFe
  , dFP
  , dPF
  , (*~|)
  , dPr
  , dFe
  , rtc
  , itr
  , foF
  , hoF
  , shp
  , fSp
  , shq
  , fSq
  , maF
  , feu
  , fdm
  , ffm
  , dsy
  , lyr
  -- * Deprecated
  , liftF
  , retract
  , iter
  , foldFree
  , hoistFree
  )
  where


import Prelude
  ( showList
  , showsPrec
  , Num
  , (+)
  )
import qualified Prelude


import Data.Functor.Classes
  ( Show1 (..)
  , showsPrec1
  )
import qualified Data.Functor.Classes


import P.Aliases
import P.Alternative
import P.Applicative
import P.Bool
import P.Category
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.Monad
import P.Ord
import P.Show


infixr 8 *~|


-- | Monads for free!
data Free f a
  = Pur a
  | Fe (f (Free f a))


instance
  ( Show1 f
  )
    => Show1 (Free f)
  where
    liftShowsPrec userShowsPrec _ n (Pur x) prog
      | n >= 10
      =
        "(Pur " ++ userShowsPrec 10 x (')' : prog)
      | T
      =
        "Pur " ++ userShowsPrec 10 x prog
    liftShowsPrec userShowsPrec showList n (Fe x) prog
      | n >= 10
      =
        "(Fe " ++
          liftShowsPrec
            (liftShowsPrec userShowsPrec showList)
            (liftShowList userShowsPrec showList)
            10
            x
            (')' : prog)
      | T
      =
        "Fe " ++
          liftShowsPrec
            (liftShowsPrec userShowsPrec showList)
            (liftShowList userShowsPrec showList)
            10
            x
            prog


instance
  ( Show a
  , Show1 f
  )
    => Show (Free f a)
  where
    showsPrec n x prog =
      liftShowsPrec showsPrec showList n x prog


instance
  ( Eq1 f
  )
    => Eq1 (Free f)
  where
    q1 userEq (Pur x) (Pur y) =
      userEq x y
    q1 userEq (Fe x) (Fe y) =
      q1 (q1 userEq) x y
    q1 _ _ _ =
      B


instance
  ( Eq a
  , Eq1 f
  )
    => Eq (Free f a)
  where
    (==) =
      q1 eq


instance
  ( Ord1 f
  )
    => Ord1 (Free f)
  where
    lcp userCp (Pur x) (Pur y) =
      userCp x y
    lcp _ (Pur _) (Fe _) =
      LT
    lcp userCP (Fe xs) (Fe ys) =
      lcp (lcp userCP) xs ys


instance
  ( Ord a
  , Ord1 f
  )
    => Ord (Free f a)
  where
    cp =
      lcp cp


instance
  ( Functor f
  )
    => Functor (Free f)
  where
    fmap func (Pur x) =
      Pur (func x)
    fmap func (Fe xs) =
      Fe (m func < xs)


instance
  ( Functor f
  )
    => Applicative (Free f)
  where
    pure =
      Pur
    Pur x <*> Pur y =
      Pur (x y)
    Pur x <*> Fe y =
      Fe (x << y)
    Fe x <*> y =
      Fe $ (*^ y) < x


instance
  ( Functor f
  )
    => Monad (Free f)
  where
    Pur x >>= func =
      func x
    Fe x >>= func =
      Fe (fb func < x)


{-# Deprecated liftF "Use lFe instead" #-}
-- | Long version of 'lFe'.
liftF ::
  ( Functor f
  )
    => f a -> Free f a
liftF =
  lFe


-- | Lift a functor to its free monad.
lFe ::
  ( Functor f
  )
    => f a -> Free f a
lFe =
  Fe < m p


-- | Takes two functions and runs them across the left and right patterns of the free monad to produce a single result.
--
-- Flip of 'dPF'.
dFP ::
  (
  )
    => (f (Free f a) -> b) -> (a -> b) -> Free f a -> b
dFP func _ (Fe x) =
  func x
dFP _ func (Pur x) =
  func x


-- | Takes two functions and runs them across the left and right patterns of the free monad to produce a single result.
--
-- Infix of 'dFP' and 'dPF'.
(*~|) ::
  (
  )
    => (f (Free f a) -> b) -> (a -> b) -> Free f a -> b
(*~|) =
  dFP


-- | Takes two functions and runs them across the left and right patterns of the free monad to produce a single result.
--
-- Flip of 'dFP'.
dPF ::
  (
  )
    => (a -> b) -> (f (Free f a) -> b) -> Free f a -> b
dPF =
  F dFP


-- | '(*~|)' with the identity applied to the right patterns.
dPr ::
  (
  )
    => (a -> f (Free f a)) -> Free f a -> f (Free f a)
dPr =
  dFP id


-- | '(*~|)' with the identity applied to the left patterns.
--
-- ==== __Examples__
--
-- Get the length, otherwise return the value
--
-- >>> dFe l (Fe [p 1, p 2, Fe [p 2], Fe [], p 9, p 2])
-- 6
-- >>> dFe l (p 2)
-- 2
-- >>> dFe l (p 3)
-- 3
-- >>> dFe l (Fe [])
-- 0
dFe ::
  (
  )
    => (f (Free f a) -> a) -> Free f a -> a
dFe =
  dPF id


{-# Deprecated retract "Use rtc instead" #-}
-- | Long version of 'rtc'.
retract ::
  ( Monad m
  )
    => Free m a -> m a
retract =
  rtc


-- | Flatten a free monad
rtc ::
  ( Monad m
  )
    => Free m a -> m a
rtc =
  fb (dPF p (fb rtc)) < p


{-# Deprecated iter "Use itr instead" #-}
-- | Long version of 'itr'.
-- Tear down a free monad using iteration.
iter ::
  ( Functor f
  )
    => (f a -> a) -> Free f a -> a
iter =
  itr


-- | Tear down a free monad using iteration.
--
-- ==== __Examples__
--
-- You can use @itr@ get the depth of a @Free List@
--
-- @
-- itr(P1<mx)<pM 0
-- @
itr ::
  ( Functor f
  )
    => (f a -> a) -> Free f a -> a
itr _ (Pur x) =
  x
itr func (Fe xs) =
  func $ itr func < xs


-- | Recursion over a free monad treating all terminal elements as the same.
--
-- Like `shq` except we don't care about the initial value of terminal elements, so we just replace them with a base case value.
shp ::
  ( Functor f
  )
    => a -> (f a -> a) -> Free f b -> a
shp =
  shq < p


-- | Flip of 'shp'.
fSp ::
  ( Functor f
  )
    => (f a -> a) -> a -> Free f b -> a
fSp =
  F shp


-- | Recurse over a free monad.
--
-- Takes a base case @:: a -> b@ and an inductive step @:: f b -> b@.
shq ::
  ( Functor f
  )
    => (a -> b) -> (f b -> b) -> Free f a -> b
shq x y =
  itr y < m x


-- | Flip of 'shq'.
fSq ::
  ( Functor f
  )
    => (f b -> b) -> (a -> b) -> Free f a -> b
fSq =
  F shq


{-# Deprecated foldFree "Use foF instead" #-}
-- | Long version of 'foF'.
foldFree ::
  ( Monad m
  )
    => (forall x. f x -> m x) -> Free f a -> m a
foldFree =
  foF


-- | Given a natural transformation produce a monad homomorphism.
foF ::
  ( Monad m
  )
    => (forall x. f x -> m x) -> Free f a -> m a
foF _ (Pur x) =
  p x
foF func (Fe xs) =
  func xs >~ foF func


{-# Deprecated hoistFree "Use hoF instead" #-}
-- | Long version of 'hoF'.
hoistFree ::
  ( Functor g
  )
    => (f (Free f a) -> g (Free f a)) -> Free f a -> Free g a
hoistFree =
  hoF


-- | A sort of map on the functor part of the free.
--
-- It lifts a natural transformation from @f@ to @g@ into a natural transformation from @Free f@ to @Free g@.
--
-- When the function supplied is not a natural transformation this acts as a top down map.
-- For a bottom up map see 'maF'.
-- On natural transformations the two act identically.
--
-- ==== __Examples__
--
-- To reverse a structure globally you can use:
--
-- @
-- hoF Rv
-- @
hoF ::
  ( Functor g
  )
    => (f (Free f a) -> g (Free f a)) -> Free f a -> Free g a
hoF _ (Pur x) =
  p x
hoF func (Fe xs) =
  Fe (hoF func < func xs)


-- | A sort of map on the functor part of the free.
--
-- It lifts a natural transformation from @f@ to @g@ into a natural transformation from @Free f@ to @Free g@.
--
-- When the function supplied is not a natural transformation this acts as a bottom up map.
-- For a top down map see 'hoF'.
-- On natural transformations the two act identically.
--
-- ==== __Examples__
--
-- To reverse a structure globally you can use:
--
-- @
-- maF Rv
-- @
--
-- To sort a list at every level you can use:
--
-- @
-- maF sr
-- @
--
-- Since 'P.List.Sort.sr' is not a natural transformation, @hoF sr@ and @maF sr@ behave differently.
maF ::
  ( Functor f
  )
    => (f (Free g a) -> g (Free g a)) -> Free f a -> Free g a
maF func (Pur x) =
  Pur x
maF func (Fe xs) =
  Fe (func $ maF func < xs)


-- | Pair each element in a free with its depth.
--
-- ==== __Examples__
--
-- >>> feu (Fe [p 1, p 2, Fe [], Fe [p 2, p 7, Fe [p 9], p 4]])
-- Fe [Pur (1,1),Pur (1,2),Fe [],Fe [Pur (2,2),Pur (2,7),Fe [Pur (3,9)],Pur (2,4)]]
feu ::
  ( Num i
  , Functor f
  )
    => Free f a -> Free f (i, a)
feu =
  fdm (,)


-- | Combine each element of a free monad with its depth using a user defined function.
fdm ::
  ( Num i
  , Functor f
  )
    => (i -> a -> b) -> Free f a -> Free f b
fdm f =
  go 0
  where
    go n (Pur x) =
      Pur $ f n x
    go n (Fe xs) =
      Fe $ go (n+1) < xs


-- | Flip of 'fdm'.
ffm ::
  ( Num i
  , Functor f
  )
    => Free f a -> (i -> a -> b) -> Free f b
ffm =
  f' fdm


-- | Breaks a ragged list into layers, grouping elements by depth but preserving divisions between lists.
--
-- ==== __Examples__
--
-- >>> dsy (Fe [Pur 2, Pur 3, Fe [Pur 4, Pur 3], Pur 9, Fe [Fe [Pur 2]], Fe [Pur 5]])
-- [[[]],[[2,3,9]],[[4,3],[],[5]],[[2]]]
dsy ::
  ( Foldable f
  , Monad f
  , Alternative f
  )
    => Free f a -> List (f (f a))
dsy =
  unfoldr < p < p
  where
    unfoldr xs
      | ø xs
      =
        []
      | Prelude.otherwise
      =
        go1 xs : unfoldr (go2 xs)
    go1 =
      m $ cM $ p em *~| p
    go2 =
      fb $ cM $ p *~| p em


-- | Breaks a ragged list into layers, grouping elements by depth.
--
-- ==== __Examples__
--
-- >>> lyr (Fe [Pur 2, Pur 3, Fe [Pur 4], Pur 9, Fe [Pur 5]])
-- [[],[2,3,9],[4,5]]
lyr ::
  (
  )
    => Free List a -> List (List a)
lyr =
  unfoldr < p
  where
    unfoldr xs
      | ø xs
      =
        []
      | Prelude.otherwise
      =
        (go1 ~< xs) : unfoldr (go2 ~< xs)
    go1 =
      jn < p em *~| p
    go2 =
      jn < (p *~| p em)
