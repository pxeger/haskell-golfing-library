{-|
Module :
  P.Monad.Plus.Filter
Description :
  Functions for filtering
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Monad.Plus.Filter
  ( wh
  , fl
  , (@~)
  , wn
  , fn
  , (@!)
  , er
  , fer
  , (=-)
  , cX
  , fcX
  , qX
  , fqX
  , fLs
  , fFL
  -- * Deprecated
  , filter
  , partition
  )
  where


import qualified Prelude


import P.Aliases
import P.Alternative
import P.Applicative
import P.Arrow
import P.Bool
import P.Eq
import P.Foldable
import P.Function.Flip
import P.Function.Compose
import P.Monad


-- | A positive filter.
--
-- Flip of 'fl'.
wh ::
  ( Alternative t
  , Monad t
  )
    => t a -> Predicate a -> t a
wh monad1 predicate =
  do
    result <- monad1
    gu (predicate result)
    p result


{-# Deprecated filter "Use fl instead" #-}
-- | Long version of 'fl'.
filter ::
  ( Alternative t
  , Monad t
  )
    => Predicate a -> t a -> t a
filter =
  fl


-- | A positive filter.
-- Removes all values that don't pass a predicate.
--
-- Equivalent to 'Control.Monad.mfilter'.
-- More general version of 'Prelude.filter'.
fl ::
  ( Alternative t
  , Monad t
  )
    => Predicate a -> t a -> t a
fl =
  F wh


-- | Infix of 'wh' and 'fl'.
(@~) ::
  ( Alternative t
  , Monad t
  )
    => t a -> Predicate a -> t a
(@~) =
  wh


-- | A negative filter.
-- Removes all values that pass a predicate.
--
-- Flip of 'fn'.
wn ::
  ( Alternative t
  , Monad t
  )
    => t a -> Predicate a -> t a
wn =
  F fn


-- | A negative filter.
-- Removes all values that pass a predicate.
--
-- Flip of 'wn'.
fn ::
  ( Alternative t
  , Monad t
  )
    => Predicate a -> t a -> t a
fn =
  --fl .!< n
  fl < (n <)


-- | Infix of 'wn' and 'fn'.
(@!) ::
  ( Alternative t
  , Monad t
  )
    => t a -> Predicate a -> t a
(@!) =
  wn


-- | Removes all of a particular element.
er ::
  ( Alternative t
  , Monad t
  , Eq a
  )
    => a -> t a -> t a
er =
  fn < (==)


-- | Flip of 'er'.
fer ::
  ( Alternative t
  , Monad t
  , Eq a
  )
    => t a -> a -> t a
fer =
  F er


-- | Infix of 'er' and 'fer'.
(=-) ::
  ( Alternative t
  , Monad t
  , Eq a
  )
    => a -> t a -> t a
(=-) =
  er


-- | Removes all elements in the first input present in the second.
cX ::
  ( Alternative t1
  , Monad t1
  , Foldable t2
  , Eq a
  )
    => t1 a -> t2 a -> t1 a
cX =
  F fcX


-- | Flip of 'cX'.
fcX ::
  ( Alternative t1
  , Monad t1
  , Foldable t2
  , Eq a
  )
    => t2 a -> t1 a -> t1 a
fcX =
  fn < fe


-- | Keeps all elements of the first input present in the second input.
--
-- It doesn't matter how many times the element is present in the second input as long as it's greater than 1.
-- For a version which keeps elements up until the number of times it appears in the second input, see 'P.Intersection.nx'.
--
-- ==== __Examples__
--
-- Compared with @nx@, @qX@ may keep more copies of an element than are present in the second input.
--
-- >>> nx "topology" "foot"
-- "too"
-- >>> qX "topology" "foot"
-- "tooo"
--
qX ::
  ( Alternative t1
  , Monad t1
  , Foldable t2
  , Eq a
  )
    => t1 a -> t2 a -> t1 a
qX =
  F fqX


-- | Flip of 'qX'.
--
-- Keeps all elements of the second input present in the first input.
fqX ::
  ( Alternative t1
  , Monad t1
  , Foldable t2
  , Eq a
  )
    => t2 a -> t1 a -> t1 a
fqX =
  fl < fe


{-# Deprecated partition "Use fLs instead" #-}
-- | Long version of 'fLs'.
partition ::
  ( Alternative t
  , Monad t
  )
    => Predicate a -> t a -> (t a, t a)
partition =
  fLs


-- | Divide a structure into elements that satisfy a predicate and those that do not.
--
-- More general version of 'Data.List.partition'.
fLs ::
  ( Alternative t
  , Monad t
  )
    => Predicate a -> t a -> (t a, t a)
fLs =
  l2 ot fl fn


-- | Flip of 'fLs'.
fFL ::
  ( Alternative t
  , Monad t
  )
    => t a -> Predicate a -> (t a, t a)
fFL =
  f' fLs
