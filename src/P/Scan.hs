{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
module P.Scan
  ( Scan (..)
  , fmA
  , fsc
  , ixm
  , ixM
  , ixo
  , fio
  , (!//)
  , scP
  , dph
  , dpH
  , eu
  , cz
  , zcz
  , czm
  , scp
  , scs
  , fSs
  -- * Paths & Prefixes
  , pxx
  , pxX
  , mpX
  , mPX
  )
  where


import Prelude
  ( Num
  )
import qualified Prelude


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Aliases
import P.Applicative.Unpure
import P.Arithmetic
import P.Arrow
import P.Bifunctor.Either
import P.Bifunctor.Profunctor
import P.Category
import P.Comonad
import P.First
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Functor.Compose
import P.Functor.Identity
import P.Monad.Free
import P.Reverse
import P.Tuple


-- | Functors which have a way of doing an accumulating map.
class Functor t => Scan t where
  {-# Minimal mA | sc#-}
  -- | A map scan.
  --
  -- Traverses the structure with an accumulator.
  --
  -- ==== __Examples__
  --
  -- Use with @sA@ to get consecutive chunks of a certain size.
  --
  -- For example this gets the prefixes of the integers in increasing size:
  --
  -- >>> mA sA [1..] [1..]
  -- [[1],[2,3],[4,5,6],[7,8,9,10],[11,12,13,14,15]...
  --
  -- From here you could get the triangular numbers with:
  --
  -- >>> m gj $ mA sA [1..] [1..]
  -- [1,3,6,10,15,21,28...
  --
  -- If the sizes are stored in a vector @mA@ will preserve the structure of that vector.
  --
  -- >>> mA sA [1..] (V3 4 5 2)
  -- V3 [1,2,3,4] [5,6,7,8,9] [10,11]
  --
  mA :: (b -> a -> (b, c)) -> b -> t a -> t c
  mA func x =
    m cr < sc (func < st) (x, Prelude.undefined)

  -- | A scan which preserves structure.
  --
  -- ==== __Examples__
  --
  -- You can scan over a vector to produce another vector of the same size.
  --
  -- >>> sc (+) 0 (V3 1 2 3)
  -- V3 1 3 6
  --
  sc :: (b -> a -> b) -> b -> t a -> t b
  sc =
    mA < mm jbp


instance Scan List where
  mA _ _ [] =
    []
  mA func accum (x : xs) =
    let
      (accum', x') =
        func accum x
    in
      x' : mA func accum' xs
  sc _ _ [] =
    []
  sc func accum (x : xs) =
    let
      x' =
        func accum x
    in
      x' : sc func x' xs


instance Scan Ident where
  mA func accum (Pu x) =
    let
      (_, x') =
        func accum x
    in
      Pu x'
  sc func accum (Pu x) =
    Pu (func accum x)


instance Scan (Either a) where
  sc _ _ (Lef x) =
    Lef x
  sc func accum (Rit x) =
    Rit (func accum x)

  mA _ _ (Lef x) =
    Lef x
  mA func accum (Rit x) =
    let
      (_, x') =
        func accum x
    in
      Rit x'


instance Scan ((,) a) where
  sc func accum (x, y) =
    (x, func accum y)


instance Scan ((,,) a b) where
  sc func accum (x, y, z) =
    (x, y, func accum z)


instance Scan ((,,,) a b c) where
  sc func accum (w, x, y, z) =
    (w, x, y, func accum z)


instance
  ( Functor f
  , Scan g
  )
    => Scan (Comp f g)
  where
    sc func accum (Co x) =
      Co $ m (sc func accum) x


instance
  ( Scan f
  )
    => Scan (Free f)
  where
    mA func accum (Pur x) =
      let
        (_, x') =
          func accum x
      in
        Pur x'
    mA func accum (Fe x) =
      Fe $ mA go accum x
      where
        go accum (Pur x) =
          let
            (accum', x') =
              func accum x
          in
            (accum', Pur x')
        go accum (Fe x) =
          (accum, Fe $ mA go accum x)


-- | Flip of 'mA'.
fmA ::
  ( Scan f
  )
    => a -> (a -> b -> (a, c)) -> f b -> f c
fmA =
  F mA


-- | Flip of 'sc'.
fsc ::
  ( Scan f
  )
    => a -> (a -> b -> a) -> f b -> f a
fsc =
  F sc


-- | Replaces every element with its depth from the root.
-- The root itself is zero.
dph ::
  ( Scan f
  , Num b
  )
    => f a -> f b
dph =
  ixm (\x _ -> x)


-- | Replaces every element with one greater than the depth from the root.
-- The root itself is one.
--
-- Results are 1 greater than 'dph'.
dpH ::
  ( Scan f
  , Num b
  )
    => f a -> f b
dpH =
  ixm (\x _ -> x + 1)


-- | Groups every element with its depth from the root.
--
-- For a list the pairs every element with its index.
eu ::
  ( Scan f
  , Num b
  )
    => f a -> f (b, a)
eu =
  ixm (,)


-- | Index map.
-- Takes a binary function that takes an index and a value and combines every value with its index.
ixm ::
  ( Scan t
  , Num a
  )
    => (a -> b -> c) -> t b -> t c
ixm =
  ixo 0


-- | Flip of 'ixm'.
ixM ::
  ( Scan t
  , Num a
  )
    => t b -> (a -> b -> c) -> t c
ixM =
  F ixm


-- | An index map starting at a user defined value.
-- Takes a binary function that takes an index and a value and combines every value with its index.
--
-- Flip of 'fio'.
ixo ::
  ( Scan t
  , Num a
  )
    => a -> (a -> b -> c) -> t b -> t c
ixo =
  F fio


-- | An index map starting at a user defined value.
-- Takes a binary function that takes an index and a value and combines every value with its index.
--
-- Flip of 'ixo'.
--
-- ===== __Examples__
--
-- Use @fio@ with 'ixm' to get a the distance from a corner in a list of lists:
--
-- >>> ixm (fio (,)) [[1,2],[1,2],[1,2],[1,2]]
-- [[(0,1),(1,2)],[(1,1),(2,2)],[(2,1),(3,2)],[(3,1),(4,2)]]
fio ::
  ( Scan t
  , Num a
  )
    => (a -> b -> c) -> a -> t b -> t c
fio func =
  mA (\y x -> (y + 1, func y x))


-- | Infix of 'ixo' and 'fio'.
(!//) ::
  ( Scan t
  , Num a
  )
    => a -> (a -> b -> c) -> t b -> t c
(!//) =
  ixo


-- | Performs a scan pairing the results with the original values.
scP ::
  ( Scan t
  )
    => (a -> b -> a) -> a -> t b -> t (a, b)
scP func =
  mA go
  where
    go accum x =
      let
        result =
          func accum x
      in
        ( result
        , ( result
          , x
          )
        )

-- | Reshapes one object to another.
--
-- ==== __Examples__
--
-- Chop off the front of a list to the size of a vector
--
-- >>> cz [1..5] $ V3 9 4 2
-- V3 1 2 3
--
cz ::
  ( Scan t
  , Firstable f f
  )
    => f c -> t a -> t c
cz =
  czm (\x _ -> x)

-- | Reshapes one object to another and zips them together.
zcz ::
  ( Scan t
  , Firstable f f
  )
    => f a -> t b -> t (a, b)
zcz =
  czm (,)

-- | Reshapes one object to another and uses a function to combine them.
czm ::
  ( Scan t
  , Firstable f f
  )
    => (a -> b -> c) -> f a -> t b -> t c
czm func xs =
  mA go xs
  where
    go (K y ys) x =
      (ys, func y x)
    go _ x =
      go xs x


-- | Replace every element with the path from the root to it.
-- Paths include the element itself.
--
-- On 'List's this gives all non-empty prefixes of the list in order of increasing size.
--
-- For a version which work on 'P.Algebra.Monoid.Free.FreeMonoid' instead of 'Scan' see 'P.Algebra.Monoid.Free.px'.
pxx ::
  ( Scan f
  )
    => f a -> f (List a)
pxx =
  m Rv < sc Fk i


-- | Map a function over every path starting from the root.
-- Paths include the element itself.
--
-- On 'List's this maps a function over the non-empty prefixes in order of increasing size.
--
-- For a version which works on 'P.Algebra.Monoid.Free.FreeMonoid' instead of 'Scan' see 'P.Algebra.Monoid.Free.Prefix.mpn'.
mpX ::
  ( Scan f
  )
    => (List a -> b) -> f a -> f b
mpX func =
  m func < pxx


-- | Replace every element with the path from the root to it.
-- Paths do not include the element itself.
--
-- On 'List's this gives all strict prefixes of the list in order of increasing size.
--
-- For a version which works on 'P.Algebra.Monoid.Free.FreeMonoid' instead of 'Scan' see 'P.Algebra.Monoid.Free.Prefix.pxs'.
pxX ::
  ( Scan f
  )
    => f a -> f (List a)
pxX =
  m Rv < mA (\x y -> (x, y : x)) []


-- | Map a function over every path starting from the root.
-- Paths do not include the element itself.
--
-- On 'List's this maps a function over the strict prefixes in order of increasing size.
--
-- For a version which works on 'P.Algebra.Monoid.Free.FreeMonoid' instead of 'Scan' see 'P.Algebra.Monoid.Free.Prefix.mps'.
mPX ::
  ( Scan f
  )
    => (List a -> b) -> f a -> f b
mPX func =
  m func < pxX


-- | Scan over a structure containing semigroup elements using the semigroup action.
scs ::
  ( Scan f
  , Semigroup m
  )
    => m -> f m -> f m
scs =
  sc mp


-- | Flip of 'scs'.
fSs ::
  ( Scan f
  , Semigroup m
  )
    => f m -> m -> f m
fSs =
  F scs


-- | Scan over a structure containing monoid elements using the monoidal action.
--
-- For integers this gives the cummulative sums.
--
-- ==== __Examples__
--
-- >>> scp [1,5,9,3,2,6]
-- [1,6,15,18,20,26]
-- >>> scp ["a","b","c","d","e","f"]
-- ["a","ab","abc","abcd","abcde","abcdef"]
--
scp ::
  ( Scan f
  , Monoid m
  )
    => f m -> f m
scp =
  sc mp i
