{-# Language ConstraintKinds #-}
{-|
Module :
  P.Aliases
Description :
  A module for aliases used with the P library
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A module for aliases used with the P library.
-}
module P.Aliases
  ( List
  , Predicate
  , Maybe'
  -- , Ring
  )
  where


-- Needed for documentation
import qualified Prelude


-- | Alias of the list type used for nicer type signatures.
type List =
  []


-- | Alias used for nicer type signatures.
type Predicate a =
  a -> Prelude.Bool


-- | Since traditional Maybes are just lists with a limit on the number of elements they can have they are not of a lot of use in a golfing library.
-- Instead everywhere we would use Maybe we just use a list instead.
-- However we will use this type alias to make it clear that the result of certain functions is functioning as a Maybe and can't ever have more than one value.
type Maybe' a =
  List a


-- | @Num@ roughly equivalent to a ring type class.
-- This alias makes it explicit we are operating on a ring.
type Ring =
  Prelude.Num
