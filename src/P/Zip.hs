{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
module P.Zip
  ( Zip (..)
  , (=#=)
  , zwp
  , uz
  , z3
  , fz3
  , ź
  , fZ3
  , fzW
  , fzp
  , jzW
  , (=#?)
  -- * Deprecated
  , zipWith
  )
  where


import Prelude
  (
  )
import qualified Prelude


import P.Algebra.Semigroup
import P.Function.Compose
import P.Function.Flip
import P.Function.Curry
import P.Monad
import P.Zip.SemiAlign


-- | Functors with a zip operation that thakes the intersection of potentially non-uniform shapes.
--
class SemiAlign f => Zip f where
  {-# Minimal zW | zp #-}
  -- | When given two 'Zip's this combines them pairwise using a given binary function.
  -- The longer list is trimmed to the length of the shorter list.
  --
  -- In general this an alternative 'P.Applicative.l2', obeying all of the 'P.Applicative.Applicative' laws.
  -- However we privledge the normal implementation because it has a 'Prelude.Monad' instance while this does not.
  --
  -- More general version of 'Data.List.zipWith'.
  zW :: (a -> b -> c) -> f a -> f b -> f c
  zW func =
    U func <<< zp

  -- | Takes two lists and combines them pairwise into a list of tuples.
  --
  -- More general version of 'Data.List.zip'.
  zp :: f a -> f b -> f (a, b)
  zp =
    zW (,)


instance Zip [] where
  zW =
    Prelude.zipWith
  zp =
    Prelude.zip


infixr 9 =#=
-- | Infix of 'zW'.
(=#=) ::
  ( Zip f
  )
    => (a -> b -> c) -> f a -> f b -> f c
(=#=) =
  zW


{-# Deprecated zipWith "Use zW instead" #-}
-- | Long version of 'zW'.
zipWith ::
  ( Zip f
  )
    => (a -> b -> c) -> f a -> f b -> f c
zipWith =
  zW


-- | Flip of 'zW'.
fzW ::
  ( Zip f
  )
    => f a -> (a -> b -> c) -> f b -> f c
fzW =
  F zW


-- | Flip of 'zp'.
fzp ::
  ( Zip f
  )
    => f a -> f b -> f (b, a)
fzp =
  F zp


-- | Zips with semigroup action.
--
-- If you want to pad out to the length of the longer list use 'zdm'.
zwp ::
  ( Zip f
  , Semigroup a
  )
    => f a -> f a -> f a
zwp =
  zW mp


{-# Deprecated unzip "Use uz instead" #-}
-- | Long version of 'uz'.
unzip ::
  ( Functor f
  )
    => f (a, b) -> (f a, f b)
unzip =
  uz


-- | Takes a functor of pairs and gives a pair of functors.
-- The first one "contains" all the first elements.
-- And the second one "contains" all of the second elements.
--
-- More general version of 'Data.List.unzip'.
uz ::
  ( Functor f
  )
    => f (a, b) -> (f a, f b)
uz start =
  ( m Prelude.fst start
  , m Prelude.snd start
  )


-- | Zip with and concat.
jzW ::
  ( Monad m
  , Zip m
  )
    => (a -> b -> m c) -> m a -> m b -> m c
jzW =
  jn <<< zW


infixr 9 =#?
-- | Infix of 'jzW'.
(=#?) ::
  ( Monad m
  , Zip m
  )
    => (a -> b -> m c) -> m a -> m b -> m c
(=#?) =
  jzW


-- | A three argument version of 'zW'.
z3 ::
  ( Zip f
  )
    => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
z3 func =
  zW (Prelude.uncurry func) << zp


-- | The flip of 'z3'.
fz3 ::
  ( Zip f
  )
    => f a -> (a -> b -> c -> d) -> f b -> f c -> f d
fz3 =
  F z3


-- | A version of 'zp' which takes three arguments and makes a 3-tuple.
--
-- Equivalent to 'Data.List.zip3'.
ź ::
  ( Zip f
  )
    => f a -> f b -> f c -> f (a, b, c)
ź =
  z3 (,,)


-- | Flip of 'ź'.
fZ3 ::
  ( Zip f
  )
    => f b -> f a -> f c -> f (a, b, c)
fZ3 =
  F ź
