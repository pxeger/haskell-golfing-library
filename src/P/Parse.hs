{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
module P.Parse
  ( Parser (..)
  -- * Perform a parse
  , pP
  , (-@)
  , cP
  , (-#)
  , x1
  , (-$)
  , gP
  , (-%)
  , gc
  , (-^)
  , gk
  , (-!)
  , glk
  , (-!$)
  , gpW
  , gpB
  , gpg
  , (->#)
  , gpl
  , (-<#)
  , ggL
  , (->|)
  , glL
  , (-<|)
  , gcy
  , (-^*)
  , gcY
  , gky
  , (-!*)
  , gkY
  , cPy
  , x1y
  , sre
  , sk
  , (-!%)
  -- * Misc
  , lp2
  -- * Deprecated
  , liftParser2
  )
  where


import qualified Prelude
import Prelude
  (
  )


import qualified Control.Applicative


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Aliases
import P.Alternative
import P.Alternative.Many
import P.Applicative
import P.Category
import P.Bool
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Foldable.MinMax
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Monad
import P.Monad.Plus.Filter
import P.Ord
import P.Reverse
import P.Swap
import P.Zip.UnitalZip
import P.Zip.WeakAlign


-- | A parser type.
newtype Parser j k a =
  P
    { uP ::
      (j -> List (k, a))
    }


instance Functor (Parser j k) where
  fmap func =
    P < m3 func < uP


{-# Deprecated liftParser2 "Use lp2 instead" #-}
-- | A more general version of 'Control.Applicative.liftA2' for the 'Parser' type.
--
-- Long version of 'P.Parse.Combinator.lp2'.
liftParser2 ::
  (
  )
    => (a -> b -> c) -> Parser d e a -> Parser e f b -> Parser d f c
liftParser2 func (P parser1) (P parser2) =
  P
    ( \ start ->
      do
        (rem1, res1) <- parser1 start
        (rem2, res2) <- parser2 rem1
        p (rem2, func res1 res2)
    )


lp2 ::
  (
  )
    => (a -> b -> c) -> Parser d e a -> Parser e f b -> Parser d f c
lp2 =
  liftParser2


instance Applicative (Parser k k) where
  pure =
    P < p << F (,)

  liftA2 =
    liftParser2


instance Alternative (Parser k k) where
  empty =
    P $ p []

  P parser1 <|> P parser2 =
    P $ l2 (++) parser1 parser2


instance Monad (Parser k k) where
  P parser1 >>= func =
    P
      ( \ start ->
        do
          (rem1, res1) <- parser1 start
          uP (func res1) rem1
      )


instance Semigroup m => Semigroup (Parser k k m) where
  (<>) =
    l2 (<>)


instance Monoid m => Monoid (Parser k k m) where
  mempty =
    p i


instance WeakAlign (Parser j k) where
  cnL parser1 parser2 =
    P
      { uP =
        ( \ start ->
            let
              parse1 =
                uP parser1 start
            in
              if
                nø parse1
              then
                parse1
              else
                uP parser2 start
        )
    }


instance UnitalZip (Parser k k) where
  aId =
    em


instance Swap (Parser k) where
  swap parser =
    P
      { uP =
        Sw << uP parser
      }


instance
  (
  )
    => Reversable (Parser k r)
  where
    _rv =
      rvW id


instance
  (
  )
    => ReversableFunctor (Parser k r)
  where
    rvW f parser =
      P
        { uP =
          rvW (m f) < uP parser
        }


-- | Takes a parser and an input and returns @True@ if there is any parse of the input even an incomplete one.
pP ::
  (
  )
    => Parser k j a -> k -> Bool
pP =
  Prelude.not << Prelude.null << uP


-- | Infix of 'pP'.
(-@) ::
  (
  )
    => Parser k j a -> k -> Bool
(-@) =
  pP


-- | Takes a parser and an input and returns @True@ if there is at least 1 complete parse.
--
-- A complete parse being a parse where the remainder is the identiy of the canonical monoid.
-- For most cases lists are being parsed so this is the empty list.
cP ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> Bool
cP =
  Prelude.not << Prelude.null << gc


-- | Infix of 'cP'.
(-#) ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> Bool
(-#) =
  cP


-- | Takes a parser and an input and returns @True@ if there is exactly 1 complete parse.
x1 ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> Bool
x1 =
  eq 1 << Prelude.length << gc


-- | Infix of 'x1'.
(-$) ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> Bool
(-$) =
  x1


-- | Gets the results of all parses.
gP ::
  (
  )
    => Parser k j a -> k -> List a
gP =
  m Prelude.snd << uP


-- | Infix of 'gP'.
(-%) ::
  (
  )
    => Parser k j a -> k -> List a
(-%) =
  gP


-- | Gets all complete parses.
gc ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> List a
gc =
  Prelude.snd <<< fl (eq i < Prelude.fst) << uP


-- | Infix of 'gc'.
(-^) ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> List a
(-^) =
  gc


-- | Gets the first complete parse, throws an error if there are no parses.
gk ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> a
gk parser x =
  case
    gc parser x
  of
    x : _ ->
      x
    [] ->
      Prelude.error "gk with no parse"


-- | Infix of 'gk'.
(-!) ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> a
(-!) =
  gk


-- | Gets the last complete parse, throws an error if there are no parses.
glk ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> a
glk parser x =
  case
    Prelude.reverse $ gc parser x
  of
    x : _ ->
      x
    [] ->
      Prelude.error "glk with no parse"


-- | Infix of 'glk'.
(-!$) ::
  ( Monoid j
  , Eq j
  )
    => Parser k j a -> k -> a
(-!$) =
  glk


-- | Gets the highest priority parse using a user defined comparison to determine priority.
-- This throws an error if there is no parse.
gpW ::
  (
  )
    => (a -> a -> Ordering) -> Parser k j a -> k -> a
gpW comp =
  x_W "gpW with no parse" comp << gP


-- | Gets the highest priority parse using a user defined conversion to determine priority.
-- This throws an error if there is no parse.
gpB ::
  ( Ord b
  )
    => (a -> b) -> Parser k j a -> k -> a
gpB conv =
  x_W "gpB with no parse" (cb conv) << gP


-- | Gets the greatest parse.
gpg ::
  ( Ord a
  )
    => Parser k j a -> k -> a
gpg =
  x_W "gpg with no parse" cp << gP


-- | Infix of 'gpg'.
(->#) ::
  ( Ord a
  )
    => Parser k j a -> k -> a
(->#) =
  x_W "(->#) with no parse" cp << gP


-- | Gets the least parse.
gpl ::
  ( Ord a
  )
    => Parser k j a -> k -> a
gpl =
  x_W "gpl with no parse" fcp << gP


-- | Infix of 'gpl'.
(-<#) ::
  ( Ord a
  )
    => Parser k j a -> k -> a
(-<#) =
  x_W "(~<#) with no parse" fcp << gP


-- | Gets the longest parse.
ggL ::
  ( Foldable f
  )
    => Parser k j (f a) -> k -> f a
ggL =
  x_W "ggL with no parse" lCp << gP


-- | Infix of 'ggL'.
(->|) ::
   ( Foldable f
   )
     => Parser k j (f a) -> k -> f a
(->|) =
  x_W "(->|) with no parse" lCp << gP


-- | Gets the shortest parse.
glL ::
  ( Foldable f
  )
    => Parser k j (f a) -> k -> f a
glL =
  x_W "glL with no parse" (f' lCp) << gP


-- | Infix of 'glL'.
(-<|) ::
  ( Foldable f
  )
    => Parser k j (f a) -> k -> f a
(-<|) =
  x_W "(~<|) with no parse" (f' lCp) << gP


-- | Apply a parser as many times as is required to consume the input and return complete parses.
gcy ::
  ( Monoid j
  , Eq j
  , Alternative (Parser k j)
  )
    => Parser k j a -> k -> List (List a)
gcy =
  gc < my


-- | Infix of 'gcy'.
(-^*) ::
  ( Monoid j
  , Eq j
  , Alternative (Parser k j)
  )
    => Parser k j a -> k -> List (List a)
(-^*) =
  gcy


-- | 'gcy' and concat the results with the monoid action.
gcY ::
  ( Monoid j
  , Eq j
  , Monoid a
  , Alternative (Parser k j)
  )
    => Parser k j a -> k -> List a
gcY =
  gc < mY


-- | Apply a parser as many times as is required to consume the input and return the first result.
--
-- Throws an error if there is no complete parse.
gky ::
  ( Monoid j
  , Eq j
  , Alternative (Parser k j)
  )
    => Parser k j a -> k -> List a
gky =
  gk < my


-- | Infix of 'gky'.
(-!*) ::
  ( Monoid j
  , Eq j
  , Alternative (Parser k j)
  )
    => Parser k j a -> k -> List a
(-!*) =
  gky


-- | 'gky' and concat the results with the monoid action.
--
-- Throws an error if there is no complete parse.
gkY ::
  ( Monoid j
  , Eq j
  , Monoid a
  , Alternative (Parser k j)
  )
    => Parser k j a -> k -> a
gkY =
  gk < mY


-- | Determines if a parser can be applied multiple times to fully consume the input.
cPy ::
  ( Monoid j
  , Eq j
  , Alternative (Parser k j)
  )
    => Parser k j a -> k -> Bool
cPy =
  cP < my


-- | Determines if there is exactly one complete parse by applying a parser multiple times.
x1y ::
  ( Monoid j
  , Eq j
  , Alternative (Parser k j)
  )
    => Parser k j a -> k -> Bool
x1y =
  x1 < my


-- | Find and replace using parser.
--
-- Finds every way to apply the parser in the input and applies it replacing the parsed input with the result of the parse.
--
-- Output will always include the option of the input with no substitutions and thus you can assume the output is non-empty.
--
-- ==== __Examples__
--
-- All the ways to replace @a@ with @i@:
--
-- >>> sre (xxh"ai") "banana"
-- ["binini","binina","binani","binana","banini","banina","banani","banana"]
--
-- All the ways to replace @aa@ with @oo@:
--
-- >>> sre (hh"aa""oo") "aaaaa"
-- ["ooooa","ooaoo","ooaaa","aoooo","aooaa","aaooa","aaaoo","aaaaa"]
sre ::
  (
  )
    => Parser (List a) (List a) (List a) -> List a -> List (List a)
sre parser =
  -- gk $ mY (my' hd <> rX rgx) <> gre h'
  gP $ mY (my' (P run) <> parser) <> (P < Prelude.take 1 << uP) (my (P run))
  where
    run [] =
      em
    run (x : xs) =
      [(xs, x)]


-- | Find and replace greedily using a parser.
--
-- Scans through a list applying a parser when it can and replacing the consumed input with the result of the parse.
--
-- If the parser cannot be applied anywhere in the input the input will be returned unchanged.
--
-- ==== __Examples__
--
-- Replace @a@ with @i@:
--
-- >>> sk (xxh"ai") "banana"
-- "binini"
--
-- Greedily replae @aa@ with @oo@:
--
-- >>> sk (hh"aa""oo") "aaaaa"
-- "ooooa"
sk ::
  (
  )
    => Parser (List a) (List a) (List a) -> List a -> List a
sk parser input =
  case
    sre parser input
  of
    [] ->
      -- Should never happen! Return the input anyway.
      input
    (x : _) ->
      x


-- | Infix version of 'sk'.
(-!%) ::
  (
  )
    => Parser (List a) (List a) (List a) -> List a -> List a
(-!%) =
  sk
