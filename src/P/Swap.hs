{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language TypeOperators #-}
module P.Swap
  ( Swap (..)
  , pattern Sw
  )
  where


import P.Bifunctor.Flip
import P.Category.Iso


-- |
--
-- prop> swap < swap = id
class Swap p where
  -- | Swap function used to define instances of the 'Swap' class.
  -- Use 'Sw' instead.
  swap :: p a b -> p b a
  swap =
    fwd swI

  -- | Swap as an isomorphism.
  swI :: p a b <-> p b a
  swI =
    Iso swap swap

{-# Deprecated swap "Use Sw instead" #-}



{-# Complete Sw, (,) #-}
-- | Swaps the arguments of a swappable.
pattern Sw ::
  ( Swap p
  )
    => p a b -> p b a
pattern Sw x <- (swap -> x) where
  Sw =
    swap


instance
  (
  )
    => Swap (,)
  where
    swap (x,y) =
      (y,x)


instance
  (
  )
    => Swap ((,,) a)
  where
    swap (x,y,z) =
      (x,z,y)


instance
  (
  )
    => Swap ((,,,) a b)
  where
    swap (w,x,y,z) =
      (w,x,z,y)


instance
  (
  )
    => Swap ((,,,,) a b c)
  where
    swap (v,w,x,y,z) =
      (v,w,x,z,y)


instance
  ( Swap f
  )
    => Swap (Flip f) where
  swap (Flp x) =
    Flp (Sw x)


instance
  (
  )
    => Swap (Iso p)
  where
    swap (Iso f b) =
      Iso b f
