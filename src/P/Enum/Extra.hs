module P.Enum.Extra
  ( eF2
  , eF3
  , eF4
  , eF5
  , eF6
  , eF7
  , eF8
  , eF9
  , eF0
  , eF1
  , e0
  , e1
  , e2
  , e3
  , e4
  , e5
  , e6
  , e7
  , e8
  , e9
  , et0
  , et1
  , et2
  , et3
  , et4
  , et5
  , et6
  , et7
  , et8
  , et9
  )
  where

import Prelude
  ( Integral
  )

import P.Aliases
import P.Enum

-- | A list of all integers starting from 2.
eF2 ::
  ( Integral i
  )
    => List i
eF2 =
  eF 2

-- | A list of all integers starting from 3.
eF3 ::
  ( Integral i
  )
    => List i
eF3 =
  eF 3

-- | A list of all integers starting from 4.
eF4 ::
  ( Integral i
  )
    => List i
eF4 =
  eF 4

-- | A list of all integers starting from 5.
eF5 ::
  ( Integral i
  )
    => List i
eF5 =
  eF 5

-- | A list of all integers starting from 6.
eF6 ::
  ( Integral i
  )
    => List i
eF6 =
  eF 6

-- | A list of all integers starting from 7.
eF7 ::
  ( Integral i
  )
    => List i
eF7 =
  eF 7

-- | A list of all integers starting from 8.
eF8 ::
  ( Integral i
  )
    => List i
eF8 =
  eF 8

-- | A list of all integers starting from 9.
eF9 ::
  ( Integral i
  )
    => List i
eF9 =
  eF 9

-- | A list of all integers starting from 10.
-- For all integers starting from 0 see 'nn'.
eF0 ::
  ( Integral i
  )
    => List i
eF0 =
  eF 10

-- | A list of all integers starting from 11.
-- For all integers starting from 1 see 'nN'.
eF1 ::
  ( Integral i
  )
    => List i
eF1 =
  eF 11

-- | Shorthand for @'ef' 0@.
e0 ::
  ( Integral i
  )
    => i -> List i
e0 =
  ef 0

-- | Shorthand for @'ef' 1@.
e1 ::
  ( Integral i
  )
    => i -> List i
e1 =
  ef 1

-- | Shorthand for @'ef' 2@.
e2 ::
  ( Integral i
  )
    => i -> List i
e2 =
  ef 2

-- | Shorthand for @'ef' 3@.
e3 ::
  ( Integral i
  )
    => i -> List i
e3 =
  ef 3

-- | Shorthand for @'ef' 4@.
e4 ::
  ( Integral i
  )
    => i -> List i
e4 =
  ef 4

-- | Shorthand for @'ef' 5@.
e5 ::
  ( Integral i
  )
    => i -> List i
e5 =
  ef 5

-- | Shorthand for @'ef' 6@.
e6 ::
  ( Integral i
  )
    => i -> List i
e6 =
  ef 6

-- | Shorthand for @'ef' 7@.
e7 ::
  ( Integral i
  )
    => i -> List i
e7 =
  ef 7

-- | Shorthand for @'ef' 8@.
e8 ::
  ( Integral i
  )
    => i -> List i
e8 =
  ef 8

-- | Shorthand for @'ef' 9@.
e9 ::
  ( Integral i
  )
    => i -> List i
e9 =
  ef 9

-- | Shorthand for @'fef' 0@.
et0 ::
  ( Integral i
  )
    => i -> List i
et0 =
  fef 0

-- | Shorthand for @'fef' 1@.
et1 ::
  ( Integral i
  )
    => i -> List i
et1 =
  fef 1

-- | Shorthand for @'fef' 2@.
et2 ::
  ( Integral i
  )
    => i -> List i
et2 =
  fef 2

-- | Shorthand for @'fef' 3@.
et3 ::
  ( Integral i
  )
    => i -> List i
et3 =
  fef 3

-- | Shorthand for @'fef' 4@.
et4 ::
  ( Integral i
  )
    => i -> List i
et4 =
  fef 4

-- | Shorthand for @'fef' 5@.
et5 ::
  ( Integral i
  )
    => i -> List i
et5 =
  fef 5

-- | Shorthand for @'fef' 6@.
et6 ::
  ( Integral i
  )
    => i -> List i
et6 =
  fef 6

-- | Shorthand for @'fef' 7@.
et7 ::
  ( Integral i
  )
    => i -> List i
et7 =
  fef 7

-- | Shorthand for @'fef' 8@.
et8 ::
  ( Integral i
  )
    => i -> List i
et8 =
  fef 8

-- | Shorthand for @'fef' 9@.
et9 ::
  ( Integral i
  )
    => i -> List i
et9 =
  fef 9
