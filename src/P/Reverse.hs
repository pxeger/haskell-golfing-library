{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Reverse
  ( rv
  , Reversable (..)
  , ReversableFunctor (..)
  , pattern Rv
  , pattern Rrv
  -- * Deprecated
  , reverse
  )
  where


import Prelude
  (
  )


import P.Aliases
import P.First
import P.Foldable
import P.Function.Compose


{-# Deprecated reverse "Use rv or Rv instead" #-}
-- | Long version of 'rv'.
-- For a pattern version see also 'Rv'.
-- Reverses a list.
reverse ::
  ( Foldable t
  )
    => t a -> List a
reverse =
  rv


-- | Reverses a foldable structure to a list.
--
-- More general version of 'Data.List.reverse'.
rv ::
  ( Foldable t
  )
    => t a -> List a
rv =
  lF' Fk []


-- | Structures that can be "reversed".
--
-- Should obey the law:
--
-- prop> _rv < _rv = id
class
  (
  )
    => Reversable f
  where
    {-# Minimal _rv #-}
    -- | Reverses a structure.
    --
    -- More general version of 'Data.List.reverse'
    _rv :: f a -> f a


-- | Something which is both a functor and a reversable.
--
-- Allows for an efficient reverse map to be defined.
--
-- Instances should follow the laws:
--
-- prop> rvW f < rvW g = map (f < g)
--
-- prop> rvW id = Rv
--
-- An example of a structure which is 'Reversable' but not a 'Functor' is 'P.Permutations.Permutation'.
class
  ( Reversable f
  , Functor f
  )
    => ReversableFunctor f
  where
    -- | Reverse with.
    -- Reverses and applies a function.
    rvW :: (a -> b) -> f a -> f b
    rvW =
      _rv << m


-- | Fails the law because reversing an infinite list does not halt.
instance Reversable [] where
  _rv =
    rv


instance ReversableFunctor [] where
  rvW func =
    lF' (Fk ^. func) []


{-# Complete Rv :: List #-}
-- | A pattern reversal.
--
-- More general version of 'Data.List.reverse'.
pattern Rv ::
  ( Reversable f
  )
    => f a -> f a
pattern Rv x <- (_rv -> x) where
  Rv =
    _rv


{-# Complete Rrv :: List #-}
-- | Reverse two layers deep.
pattern Rrv ::
  ( ReversableFunctor f
  , Reversable g
  )
    => f (g a) -> f (g a)
pattern Rrv x <- (rvW Rv -> x) where
  Rrv =
    rvW Rv
