{-|
Module :
  P.List.Group
Description :
  Functions for grouping lists.
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Functions for grouping lists.
Part of the P library.
-}
module P.List.Group
  ( gr
  , gW
  , fgW
  , gB
  , fgB
  , gF
  , fgF
  , gFn
  , gfn
  , gfq
  , fgq
  , gnq
  , gNq
  -- * Deprecated
  , group
  , groupBy
  , groupWith
  )
  where


import Prelude
  (
  )


import qualified Data.List


import P.Aliases
import P.Bool
import P.Function.Compose
import P.Function.Flip
import P.Eq


{-# Deprecated group "User gr instead" #-}
-- | Long version of 'gr'.
group ::
  ( Eq a
  )
    => List a -> List (List a)
group =
  gr


-- | Splits a list into sublists with all equal elements.
--
-- Equivalent to 'Data.List.group'.
gr ::
  ( Eq a
  )
    => List a -> List (List a)
gr =
  Data.List.group


{-# Deprecated groupBy "User gW instead" #-}
-- | Long version of 'gW'.
groupBy ::
  (
  )
    => (a -> a -> Bool) -> List a -> List (List a)
groupBy =
  gW


-- |
-- Equivalent to 'Data.List.groupBy'.
gW ::
  (
  )
    => (a -> a -> Bool) -> List a -> List (List a)
gW =
  Data.List.groupBy


-- | Flip of 'gW'.
fgW ::
  (
  )
    => List a -> (a -> a -> Bool) -> List (List a)
fgW =
  f' gW


{-# Deprecated groupWith "Use gB instead" #-}
-- | Long version of 'gB'.
groupWith ::
  ( Eq b
  )
    => (a -> b) -> List a -> List (List a)
groupWith =
  gB


-- | Takes a conversion function and a list.
-- Groups the list as if the values were the result of the conversion function.
--
-- ==== __Examples__
--
-- We can use @gB@ to group strings together based on length:
--
-- >>> gB l ["Hello", "world", "it's", "me"]
-- [["Hello","world"],["it's"],["me"]]
--
-- We could use a predicate to group a list into members that do and don't satisfy it.
-- Here we group the some integers based on whether they are @7@:
--
-- >>> gB (==7) [1 .. 9]
-- [[1,2,3,4,5,6],[7],[8,9]]
gB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List (List a)
gB =
  gW < qb


-- | Flip of 'gB'.
fgB ::
  ( Eq b
  )
    => List a -> (a -> b) -> List (List a)
fgB =
  f' gB


-- | Takes a predicate and a list and gives back groups of the list satisfying the predicate.
--
-- ==== __Examples__
--
-- Find the largest contiguous sublist where all elements are greater than @2@
--
-- >>>xB l<gF gt2$[1,2,3,4,2,4]
-- [3,4]
-- >>>xB l<gF gt2$[1,9,2,2,2,8,6,2]
-- [8,6]
-- >>>xB l<gF gt2$[2,1,2,2,1,0]
-- *** Exception: maximumBy: empty structure
gF ::
  (
  )
    => (a -> Bool) -> List a -> List (List a)
gF func =
  go
  where
    go [] =
      []
    go x =
      case
        Data.List.span func x
      of
        ([], rest) ->
          go (Data.List.dropWhile (n < func) rest)
        (group1, rest) ->
          group1 : go (Data.List.dropWhile (n < func) rest)


-- | Flip of 'gF'.
fgF ::
  (
  )
    => List a -> (a -> Bool) -> List (List a)
fgF =
  f' gF


-- | Like 'gF' except it gives sublists failing the predicate.
gFn ::
  (
  )
    => (a -> Bool) -> List a -> List (List a)
gFn =
  gF < m n


-- | Flip of 'gFn'.
gfn ::
  (
  )
    => List a -> (a -> Bool) -> List (List a)
gfn =
  f' gFn


-- | Takes a predicate and a list and gives back groups of the list equal to a certain value.
gfq ::
  ( Eq a
  )
    => a -> List a -> List (List a)
gfq =
  gF < eq


-- | Flip of 'gfq'.
fgq ::
  ( Eq a
  )
    => List a -> a -> List (List a)
fgq =
  f' gfq


-- | Takes a predicate and a list and gives back groups of the list not equal to a certain value.
gnq ::
  ( Eq a
  )
    => a -> List a -> List (List a)
gnq =
  gF < nq


-- | Flip of 'gnq'.
gNq ::
  ( Eq a
  )
    => List a -> a -> List (List a)
gNq =
  f' gnq
