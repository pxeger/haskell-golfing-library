{-|
Module :
  P.List.Pairs
Description :
  Functions operating on pairs of elements drawn from a list
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A library for functions operating on pairs of elements drawn from a list.
Most of these operate on all the pairs of consecutive elements.
-}
module P.List.Pairs
  ( pa
  , (%%)
  , fpa
  , paf
  , paF
  , pA
  , pac
  , pax
  , fpx
  , δ
  , δ'
  , paq
  , pnq
  , pam
  , pmx
  , pmn
  , xQ
  , xX
  -- * Deprecated
  , deltas
  )
  where


import qualified Prelude
import Prelude
  ( Num
  )


import P.Algebra.Semigroup
import P.Aliases
import P.Bool
import P.Eq
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.List
import P.Ord


-- | Applies a function to consecutive elements of a list.
--
-- ==== __Examples__
--
-- @pa (-)@ calculates the descending differences or the difference between each element and the previous.
--
-- >>> pa (-) [1,2,4,2,1]
-- [-1,-2,2,1]
--
-- @pa (,)@ gives all pairs of consecutive elements.
-- This is the same as 'pA'.
--
-- >>> pa (,) [1,2,4,2,1]
-- [(1,2),(2,4),(4,2),(2,1)]
--
pa ::
  ( Foldable f
  )
    => (a -> a -> b) -> f a -> List b
pa func =
  go < tL
  where
    go (x1 : x2 : xs) =
      func x1 x2 : go (x2 : xs)
    go _ =
      []


-- | Infix of 'pa'.
(%%) ::
  ( Foldable f
  )
    => (a -> a -> b) -> f a -> List b
(%%) =
  pa


-- | Flip of 'pa'.
fpa ::
  ( Foldable f
  )
    => f a -> (a -> a -> b) -> List b
fpa =
  f' pa


-- | 'pa' but using a flipped version of the given function.
--
-- ==== __Examples__
--
-- When calculating the consecutive difference between elements 'pa' will give the descending difference:
--
-- >>> pa (-) [1,3..9]
-- [-2,-2,-2,-2]
--
-- If we want the ascending difference then we need to flip the @(-)@.
--
-- >> paf (-) [1,3..9]
-- [2,2,2,2]
--
paf ::
  ( Foldable f
  )
    => (a -> a -> b) -> f a -> List b
paf =
  pa < f'


-- | Flip of 'paf'.
paF ::
  ( Foldable f
  )
    => f a -> (a -> a -> b) -> List b
paF =
  f' paf


-- | Gives all pairs of consecutive elements in a list.
--
-- ==== __Examples__
--
-- >>> pA [1,2,4,2,1]
-- [(1,2),(2,4),(4,2),(2,1)]
--
pA ::
  (
  )
    => List a -> List (a, a)
pA =
  pa (,)


-- | Gives the contour of a list.
--
-- Compares consecutive elements of a list with 'cp'.
pac ::
  ( Ord a
  , Foldable f
  )
    => f a -> List Ordering
pac =
  pa cp


-- | Map over pairs of consecutive elements of a list and concat.
pax ::
  ( Foldable f
  )
    => (a -> a -> List a) -> f a -> List a
pax =
  fo << pa


-- | Flip of 'pax'.
fpx ::
  ( Foldable f
  )
    => f a -> (a -> a -> List a) -> List a
fpx =
  f' pax


{-# Deprecated deltas "Use δ instead" #-}
-- | Gives the deltas of a list.
-- Long version of δ.
deltas ::
  ( Num i
  )
    => List i -> List i
deltas =
  δ


-- | Gives the deltas of a list.
δ ::
  ( Num i
  , Foldable f
  )
    => f i -> List i
δ =
  paf (Prelude.-)


-- | Gives the absolute difference between consecutive elements of a list.
δ' ::
  ( Num i
  , Foldable f
  )
    => f i -> List i
δ' =
  paf (mm Prelude.abs (Prelude.-))


-- | Compares consecutive elements of a list with 'eq'.
paq ::
  ( Eq a
  , Foldable f
  )
    => f a -> List Bool
paq =
  pa eq


-- | Compares consecutive elements of a list with 'nq'.
pnq ::
  ( Eq a
  , Foldable f
  )
    => f a -> List Bool
pnq =
  pa nq


-- | Combines consecutive elements with the 'Semigroup' action.
pam ::
  ( Semigroup a
  , Foldable f
  )
    => f a -> List a
pam =
  pa mp


-- | Gets the maximum of each pair of consecutive elements.
--
-- For booleans this gets the logical or of consecutive elements.
pmx ::
  ( Ord a
  , Foldable f
  )
    => f a -> List a
pmx =
  pa ma


-- | Gets the minimum of each pair of consecutive elements.
--
-- For booleans this gets the logical and of consecutive elements.
pmn ::
  ( Ord a
  , Foldable f
  )
    => f a -> List a
pmn =
  pa mN


-- | Takes a function and applies to all pairs in which the first element occurs before the second.
--
-- For a commutative function this prevents duplication.
--
-- Outputs in diagonalized order so that it works with infinite lists.
--
-- For a version that also includes applying the function to every element and itself, see 'xX'.
--
-- For a version that just applies to all pairs see 'P.Applicative.l2'.
--
-- ==== __Examples__
--
-- Use it to get all numbers with exactly two 1s in their binary representation.
--
-- >>> xQ pl $ m (2^) [0..]
-- [3,6,5,12,9,10,17,24,33,18,65,20,129,34,257,48,513,66,1025,36...
--
xQ ::
  (
  )
    => (a -> a -> b) -> List a -> List b
xQ func =
  go
  where
    go [] =
      []
    go (x : xs) =
      m (func x) xs :% go xs


-- | Takes a function and applies to all pairs in which the first element occurs at or before the second.
--
-- Outputs in diagonalized order so that it works with infinite lists.
--
-- For a commutative function this prevents duplication.
--
-- For a version which does not apply a function to each element and itself see 'xQ'.
xX ::
  (
  )
    => (a -> a -> b) -> List a -> List b
xX func =
  go
  where
    go [] =
      []
    go (x : xs) =
      m (func x) (x : xs) :% go xs
