{-|
Module :
  P.List.Extra
Description :
  Additional functions for the P.List library
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions that have been moved to this file to reduce clutter in the main P.List library.
-}
module P.List.Extra
  ( ak3
  , ak4
  , ak5
  , ak6
  , ak7
  )
  where


import P.Aliases
import P.List


-- | Takes 3 lists and creates a new list alternating elements between the three.
ak3 ::
  (
  )
    => List a -> List a -> List a -> List a
ak3 [] ys zs =
  ak ys zs
ak3 (x : xs) ys zs =
  x : ak3 ys zs xs


-- | Takes 4 lists and creates a new list alternating elements between the four.
ak4 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a
ak4 [] ys zs ws =
  ak3 ys zs ws
ak4 (x : xs) ys zs ws =
  x : ak4 ys zs ws xs


-- | Takes 5 lists and creates a new list alternating elements between the five.
ak5 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a -> List a
ak5 [] ys zs ws vs =
  ak4 ys zs ws vs
ak5 (x : xs) ys zs ws vs =
  x : ak5 ys zs ws vs xs


-- | Takes 6 lists and creates a new list alternating elements between the six.
ak6 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a -> List a -> List a
ak6 [] ys zs ws vs us =
  ak5 ys zs ws vs us
ak6 (x : xs) ys zs ws vs us =
  x : ak6 ys zs ws vs us xs


-- | Takes 7 lists and creates a new list alternating elements between the six.
ak7 ::
  (
  )
    => List a -> List a -> List a -> List a -> List a -> List a -> List a -> List a
ak7 [] ys zs ws vs us ts =
  ak6 ys zs ws vs us ts
ak7 (x : xs) ys zs ws vs us ts =
  x : ak7 ys zs ws vs us xs ts
