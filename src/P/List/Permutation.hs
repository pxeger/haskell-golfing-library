{-|
Module :
  P.List.Permutation
Description :
  Permutations on lists
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Provides functions for permutations on lists.
-}
module P.List.Permutation
  ( pm
  , pmN
  , ƪ
  , ƪN
  -- * Deprecated
  , permutations
  )
  where


import Prelude
  ( Integral
  )
import qualified Data.List


import P.Aliases
import P.Arithmetic.Extra
import P.Enum.Extra
import P.Function.Compose


{-# Deprecated permutations "Use pm instead" #-}
-- | Long version of 'pm'.
-- Gives all permutations of a given list.
permutations ::
  (
  )
    => List a -> List (List a)
permutations =
  pm


-- | Gives all permutations of a given list.
--
-- Equivalent to 'Data.List.permutations'.
pm ::
  (
  )
    => List a -> List (List a)
pm =
  Data.List.permutations


-- | Gives all permutations on @n@ elements.
-- i.e. all the ways to arrange the numbers @0@ through @n-1@.
pmN ::
  ( Integral i
  )
    => i -> List (List i)
pmN =
  pm < e0 < S1


-- | Maps a function over all permutations of a given list.
ƪ ::
  (
  )
    => (List a -> b) -> List a -> List b
ƪ =
  fM pm


-- | Maps a function over all permutations on @n@ elements.
ƪN ::
  ( Integral i
  )
    => (List i -> b) -> i -> List b
ƪN =
  fM pmN
