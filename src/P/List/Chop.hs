{-# Language PatternSynonyms #-}
{-|
Module:
  P.List.Chop
Description:
  Functions for breaking lists into sized segments.
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.List.Chop
  ( wx
  , fwx
  , wX
  , fwX
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import P.Aliases
import P.Category
import P.First
import P.Foldable.Length
import P.Function.Curry
import P.Function.Compose
import P.Function.Flip
import P.List.Split
import P.Ord


-- | Chop a list into segments of the given size, anything left over will be placed in its own segment
--
-- ==== __Examples__
--
-- >>> wx 3 "Hello, world!"
-- ["Hel","lo,"," wo","rld","!"]
wx ::
  ( Integral i
  )
    => i -> List a -> List (List a)
wx n [] =
  []
wx n x =
  U ((:) ^. wx n) $ sA n x


-- | Flip of 'wx'.
fwx ::
  ( Integral i
  )
    => List a -> i -> List (List a)
fwx =
  F wx


-- | Chop a list into segments of the given size, discarding any leftovers.
--
-- ==== __Examples__
--
-- >>> wX 3 "Hello, world!"
-- ["Hel","lo,"," wo","rld"]
wX ::
  ( Integral i
  , Ord i
  )
    => i -> List a -> List (List a)
wX n x
  | lL n x =
    []
  | Prelude.otherwise =
    U ((:) ^. wX n) $ sA n x


-- | Flip of 'wX'.
fwX ::
  ( Integral i
  , Ord i
  )
    => List a -> i -> List (List a)
fwX =
  F wX

