{-# Language NoMonomorphismRestriction #-}
module P.List.Suffix
  where


import qualified Prelude
import Prelude
  ( Bool
  , Integral
  , otherwise
  )
import qualified Data.Function


import P.Aliases
import P.Algebra.Monoid.Free
import P.Algebra.Monoid.Free.Prefix
import P.Algebra.Monoid.Free.Suffix
import P.Category
import P.Eq
import P.First
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Last
import P.Ord.Bounded
import P.Reverse
import P.Tuple


-- | Works like 'ew' but with a user defined equality function.
ewW ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> Bool
ewW equals =
  F (ay < q1 equals) < sx


-- | Flip of 'ewW'.
fEW ::
  (
  )
    => List a -> (a -> a -> Bool) -> List a -> Bool
fEW =
  F ewW


-- | Works like 'ew' but with a substitution function mapped across both inputs.
ewB ::
  ( Eq b
  )
    => (a -> b) -> List a -> List a -> Bool
ewB =
  Data.Function.on ew < m


-- | Flip of 'ewB'.
fEB ::
  ( Eq b
  )
    => List a -> (a -> b) -> List a -> Bool
fEB =
  F ewB


-- | Gives the longest common suffix of two lists.
lx ::
  ( Eq a
  )
    => List a -> List a -> List a
lx =
  lSw eq


-- | 'lx' but with a user defined comparison.
lSw ::
  ( FreeMonoid a
  , FreeMonoid b
  )
    => (a -> b -> Bool) -> a -> b -> a
lSw predicate list1 list2 =
  rvg $ lPw predicate (rvg list1) (rvg list2)


-- | Works like 'lx' but with a substitution function mapped across both inputs.
lSb ::
  ( Eq b
  , FreeMonoid a
  )
    => (a -> b) -> a -> a -> a
lSb =
  lSw < qb


-- | Given @n@ and @xs@ gives a suffix of length @n@ from @xs@, or all of @xs@ if @n@ is greater than the length of @xs@.
yv ::
  ( Integral i
  , Reversable f
  , Foldable f
  , Firstable f f
  )
    => i -> f a -> List a
yv n list
  -- For the case of infinite lists since they cannot be reversed.
  | n Prelude.< 1
  =
    []
  | otherwise
  =
    rv $ tk n $ rv list


-- | Flip of 'yv'.
fY ::
  ( Integral i
  , Reversable f
  , Foldable f
  , Firstable f f
  )
    => f a -> i -> List a
fY =
  f' yv


-- | Gives the longest suffix where all elements satisfy a predicate.
xh ::
  (
  )
    => Predicate a -> List a -> List a
xh =
  tW <.^ Rv


-- | Flip of 'xh'.
fxh ::
  (
  )
    => List a -> Predicate a -> List a
fxh =
  F xh
