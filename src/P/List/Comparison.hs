module P.List.Comparison
  ( lq
  , nlq
  , lqw
  , lqW
  , lqb
  , lqB
  , cδ
  , cΔ
  , uq
  , nuq
  , uqb
  , uqB
  , uqw
  , uqW
  , sMI
  , miw
  , miW
  , mnI
  , mIw
  , mIW
  , sMD
  , mdw
  , mdW
  , mnD
  , mDw
  , mDW
  , mno
  , mNo
  )
  where


import Prelude
  ( Num
  )


import P.Algebra.Semigroup
import P.Aliases
import P.Applicative
import P.Bool
import P.Category
import P.Comonad
import P.Eq
import P.Function
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Foldable
import P.List
import P.List.Pairs
import P.Monad
import P.Ord
import P.Ord.Bounded


-- | Determines if all elements of a list are equal.
lq ::
  ( Foldable t
  , Eq a
  )
    => t a -> Bool
lq =
  lqw eq


-- | Negation of 'lq'.
-- Determines if there are two unequal elements in a list.
nlq ::
  ( Foldable t
  , Eq a
  )
    => t a -> Bool
nlq =
  n < lq


-- | Takes a comparison function and checks that consecutive elements pass.
--
-- This is 'lq' with a user defined equality function.
-- It works so long as that function is transitive.
lqw ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> Bool
lqw comparison =
  fo < pa comparison < tL


-- | Flip of 'lqw'.
lqW ::
  ( Foldable t
  )
    => t a -> (a -> a -> Bool) -> Bool
lqW =
  F lqw


-- | Takes a user defined conversion function and checks that all elements are equal under conversion.
--
-- ==== __Examples__
--
-- Check if all elements of a list are the same length.
--
-- >>> lqb l ["abc","def","ghi"]
-- True
-- >>> lqb l ["abc","def","gh"]
-- False
lqb ::
  ( Foldable t
  , Eq b
  )
    => (a -> b) -> t a -> Bool
lqb =
  lqw < on eq


-- | Flip of 'lqb'.
lqB ::
  ( Foldable t
  , Eq b
  )
    => t a -> (a -> b) -> Bool
lqB =
  F lqb


-- | Determines if that deltas of a list are constant, that is it is an arithmetic sequence.
cδ ::
  ( Num i
  , Eq i
  , Foldable f
  )
    => f i -> Bool
cδ =
  lq < δ


-- | Negation of 'cδ'.
cΔ ::
  ( Num i
  , Eq i
  , Foldable f
  )
    => f i -> Bool
cΔ =
  n < cδ


-- | Determines if all elements of a list are unique.
uq ::
  ( Foldable t
  , Eq a
  )
    => t a -> Bool
uq =
  uqw eq


-- | Negation of 'uq', takes a list and determines if there are any duplicates.
nuq ::
  ( Foldable t
  , Eq a
  )
    => t a -> Bool
nuq =
  n < uq


-- | Takes a user defined equality function and checks that all pairs of elements fail the predicate.
--
-- This is 'uq' with a user defined equality function.
-- It works so long as that function is symmetric.
uqw ::
  ( Foldable t
  )
    => (a -> a -> Bool) -> t a -> Bool
uqw comparison =
  go < tL
  where
    go (x : xs) =
      no (comparison x) xs <> go xs
    go [] =
      T


-- | Flip of 'uqw'.
uqW ::
  ( Foldable t
  )
    => t a -> (a -> a -> Bool) -> Bool
uqW =
  f' uqw


-- | Takes a user defined conversion function and checks that all elements are unique under the conversion.
--
-- ==== __Examples__
--
-- Check if all elements of a list are different lengths.
--
-- >>> uqb l ["a","bc","def","ghij"]
-- True
-- >>> uqb l ["a","bc","de","fghij"]
-- False
--
uqb ::
  ( Foldable t
  , Eq b
  )
    => (a -> b) -> t a -> Bool
uqb =
  uqw < on eq


-- | Flip of 'uqb'.
uqB ::
  ( Foldable t
  , Eq b
  )
    => t a -> (a -> b) -> Bool
uqB =
  f' uqb


-- | Determines if a list is strictly monotonically increasing.
sMI ::
  ( Foldable t
  , Ord a
  )
    => t a -> Bool
sMI =
  lqw gt


-- | Determines if a list is strictly monotonically increasing under a user defined conversion function.
miw ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> t a -> Bool
miw =
  lqw < on gt


-- | Flip of 'miw'.
miW ::
  ( Foldable t
  , Ord b
  )
    => t a -> (a -> b) -> Bool
miW =
  f' miw


-- | Determines if a list is weakly monotonically increasing.
mnI ::
  ( Foldable t
  , Ord a
  )
    => t a -> Bool
mnI =
  lqw ge


-- | Determines if a list is weakly monotonically increasing under a user defined conversion function.
mIw ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> t a -> Bool
mIw =
  lqw < on ge


-- | Flip of 'mIw'.
mIW ::
  ( Foldable t
  , Ord b
  )
    => t a -> (a -> b) -> Bool
mIW =
  f' mIw


-- | Determines if a list is strictly monotonically decreasing.
sMD ::
  ( Foldable t
  , Ord a
  )
    => t a -> Bool
sMD =
  lqw lt


-- | Determines if a list is strictly monotonically decreasing under a user defined conversion function.
mdw ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> t a -> Bool
mdw =
  lqw < on lt


-- | Flip of 'mdw'.
mdW ::
  ( Foldable t
  , Ord b
  )
    => t a -> (a -> b) -> Bool
mdW =
  f' mdw


-- | Determines if a list is weakly monotonically decreasing.
mnD ::
  ( Foldable t
  , Ord a
  )
    => t a -> Bool
mnD =
  lqw le


-- | Determines if a list is weakly monotonically decreasing under a user defined conversion function.
mDw ::
  ( Foldable t
  , Ord b
  )
    => (a -> b) -> t a -> Bool
mDw =
  lqw < on le


-- | Flip of 'mDw'.
mDW ::
  ( Foldable t
  , Ord b
  )
    => t a -> (a -> b) -> Bool
mDW =
  f' mDw


-- | Determines if a list is weakly monotonic, either increasing or decreasing.
mno ::
  ( Foldable t
  , Ord a
  )
    => t a -> Bool
mno =
  go < pac
  where
    go [] =
      T
    go (LT : xs) =
      ne GT xs
    go (GT : xs) =
      ne LT xs
    go (EQ : xs) =
      go xs


-- | Determines if a list is strictly monotonic, either increasing, decreasing or constant.
mNo ::
  ( Foldable t
  , Ord a
  )
    => t a -> Bool
mNo =
  lq < pac
