{-# Language ScopedTypeVariables #-}
module P.List.Find
  ( FindAutomaton
  -- * Making automata
  , mAt
  , mAW
  -- * Using automata
  -- | These functions consume an automaton as an input.
  , afw
  , alf
  , arw
  , avw
  , aRw
  , asw
  , azw
  -- * Precomposed
  -- | These functions precompose the building of the automaton with its use.
  -- This saves you from having to build the automaton, but doesn't allow you to reuse it for other functions.
  , fa
  , lfa
  , ra
  , rA
  , va
  , sa
  , za
  , faw
  , raw
  , rAw
  , vaw
  , saw
  , zaw
  )
  where


import Prelude
  ( Int
  , Num
  , Integer
  )


import qualified Data.IntMap as IntMap
import Data.IntMap
  ( IntMap
  , (!)
  )


import P.Algebra.Semigroup
import P.Aliases
import P.Applicative
import P.Arithmetic
import P.Bool
import P.Category
import P.Eq
import P.Foldable.Length
import P.Function.Compose
import P.List.Split
import P.Ord
import P.Reverse
import P.Tuple
import P.Zip


-- | A precomputed automaton to assist in finding a specific substring.
data FindAutomaton a =
  SFA
    { test ::
      Predicate a
    -- ^ A test to be performed on the read head.
    -- Usually a simple equality, but abstracted out here to allow for user defined equality.
    , failState ::
      FindAutomaton a
    -- ^ State to transition when the read head fails the test.
    -- Do not progress the read head.
    -- The new value will be tested as well.
    , successState ::
      FindAutomaton a
    -- ^ State to transition when the read head passes the test.
    -- Progress the read head when transitioning.
    , accept ::
      Bool
    -- ^ When this is true it indicates that the state is accepting.
    -- When an accepting state has been reached it means that a match has been found.
    -- To find more matches advance the read head and move to the success state
    , progress ::
      Int
    -- ^ Numer of steps from the initial match of the string.
    }


-- | Takes a list of tests and a transition map to produce a full featured automaton.
-- Internal function for 'mAW'.
--
-- ==== __Order notation__
-- To create an automaton \(O(\lambda n.n\log n)\) time.
--
-- Once created following a transition of the automaton is \(O(\lambda n. 1)\) time.
-- Altough due to laziness some of the creation time may occur at the first time a particular transition is taken.
automatonFromTransitions ::
  forall a.
  (
  )
    => (IntMap.Key, IntMap (Predicate a, IntMap.Key)) -> FindAutomaton a
automatonFromTransitions (finalSuccess, transitions) =
  l ! 0
  where
    go :: IntMap.Key -> (Predicate a, IntMap.Key) -> FindAutomaton a
    go index (localTest, failIndex) =
      SFA
        { test =
          localTest
        , failState =
          l ! failIndex
        , successState =
          if
            isFinal
          then
            l ! finalSuccess
          else
            l ! (index + 1)
        , accept =
          isFinal
        , progress =
          index
        }
      where
        isFinal =
          index == st (IntMap.findMax transitions)
    l :: IntMap (FindAutomaton a)
    l =
      IntMap.mapWithKey go transitions


-- | Produces the intermediate transition map from a string and a user defined equality.
-- Internal function for 'mAW'.
-- Exposed for testing.
--
-- ==== __Order notation__
-- To build the transitions is \(O(\lambda n.n\log n)\)
buildTransitions ::
  forall a.
  (
  )
    => (a -> a -> Bool) -> IntMap a -> (IntMap.Key, IntMap (Predicate a, IntMap.Key))
buildTransitions userEq charMap =
  ( finalSuccess
  , transMap
  )
  where
    ((_, finalSuccess), transMap) =
      IntMap.mapAccum go (B, 0) charMap
    go :: (Bool, IntMap.Key) -> a -> ((Bool, IntMap.Key), (Predicate a, IntMap.Key))
    go (limit, fallback) x =
      let
        newFallback =
          if
            userEq (charMap ! fallback) x <> limit
          then
            fallback + 1
          else
            0
      in
        ( ( T, newFallback )
        , ( userEq x, fallback )
        )


-- | Builds an automaton from a string.
--
-- ==== __Order notation__
-- \(O(\lambda n.n\log n)\)
mAt ::
  ( Eq a
  )
    => List a -> FindAutomaton a
mAt =
  mAW eq


-- | Builds an automaton from a string and a user defined equality function.
--
-- ==== __Order notation__
-- \(O(\lambda n.n\log n)\)
mAW ::
  (
  )
    => (a -> a -> Bool) -> List a -> FindAutomaton a
mAW userEq =
  automatonFromTransitions <
    buildTransitions userEq <
      IntMap.fromDistinctAscList <
        zp [0..]


-- | Uses an automaton to find all occurrences of a particular substring.
-- Occurrences can overlap.
--
-- ==== __Order notation__
-- \(O(\lambda n.n)\)
afw ::
  ( Num b
  )
    => FindAutomaton a -> List a -> List b
afw =
  go 0
  where
    go _ _ [] =
      []
    go depth automaton (x : xs)
      | accept automaton
      , test automaton x
      =
        tNu (depth - progress automaton) : go (depth + 1) (successState automaton) xs
      | test automaton x
      =
        go (depth + 1) (successState automaton) xs
      | T
      =
        if
          1 > progress automaton
        then
          go (depth + 1) automaton xs
        else
          go depth (failState automaton) (x : xs)


-- | Use an automaton to count the occurrences of a particuar substring.
-- Occurrences can overlap.
--
-- ==== __Order notation__
-- \(O(\lambda n.n)\)
alf ::
  (
  )
    => FindAutomaton a -> List a -> Integer
alf =
  l << afw


-- | Uses an automaton to find and replace the first occurrence of a particular substring with another string.
--
-- Returns the input unchanged if there is no occurrence.
--
-- ==== __Order notation__
-- \(O(\lambda n.n)\)
arw :: forall a.
  (
  )
    => FindAutomaton a -> List a -> List a -> List a
arw auto repl =
  go [] auto
  where
    go :: List a -> FindAutomaton a -> List a -> List a
    go work _ [] =
      Rv work
    go work automaton (x : xs)
      | accept automaton
      , test automaton x
      =
        repl <> xs
      | test automaton x
      =
        go (x : work) (successState automaton) xs
      | T
      =
        if
          1 > progress automaton
        then
          Rv (x : work) <> go [] automaton xs
        else
          let
            (Rv processedWork, unprocessedWork) =
              sA (progress automaton) work
          in
            processedWork <> go unprocessedWork (failState automaton) (x : xs)


-- | Uses an automaton to remove the first occurrence of a particular substring with another string.
--
-- Returns the input unchanged if there is no occurrence.
--
-- ==== __Order notation__
-- \(O(\lambda n.n)\)
avw ::
  (
  )
    => FindAutomaton a -> List a -> List a
avw auto =
  arw auto []


-- | Uses an automaton to replace all occurrences of a particular substring.
--
-- Where two occurrences overlap the earlier one is replaced.
--
-- ==== __Order notation__
-- \(O(\lambda n.n)\)
aRw :: forall a.
  (
  )
    => FindAutomaton a -> List a -> List a -> List a
aRw auto repl =
  go [] auto
  where
    go :: List a -> FindAutomaton a -> List a -> List a
    go work _ [] =
      Rv work
    go work automaton (x : xs)
      | accept automaton
      , test automaton x
      =
        repl <> go [] auto xs
      | test automaton x
      =
        go (x : work) (successState automaton) xs
      | T
      =
        if
          1 > progress automaton
        then
          Rv (x : work) <> go [] automaton xs
        else
          let
            (Rv processedWork, unprocessedWork) =
              sA (progress automaton) work
          in
            processedWork <> go unprocessedWork (failState automaton) (x : xs)


-- | Split between strings matching an automaton and the rest of the input.
--
-- In the case of overlapping matches the earlier match is the one considered.
--
-- ==== __Order notation__
-- \(O(\lambda n.n)\)
asw :: forall a .
  (
  )
    => FindAutomaton a -> List a -> List (List a)
asw auto =
  go [] auto
  where
    go :: List a -> FindAutomaton a -> List a -> List (List a)
    go [] _ [] =
      []
    go work _ [] =
      [Rv work]
    go work automaton (x : xs)
      | accept automaton
      , test automaton x
      , (Rv p, Rv q) <- sA (progress automaton + 1) $ x : work
      =
        case
          q
        of
          [] ->
            p : go [] auto xs
          _ ->
            q : p : go [] auto xs
      | test automaton x
      =
        go (x : work) (successState automaton) xs
      | T
      =
        go (x : work) (failState automaton) xs


-- | Split a string along segments matching an automaton.
-- The substrings matching the automaton are removed.
--
-- In the case of overlapping matches the earlier match is the one considered.
--
-- ==== __Order notation__
-- \(O(\lambda n.n)\)
azw :: forall a .
  (
  )
    => FindAutomaton a -> List a -> List (List a)
azw auto =
  go [] auto
  where
    go :: List a -> FindAutomaton a -> List a -> List (List a)
    go [] _ [] =
      []
    go work _ [] =
      [Rv work]
    go work automaton (x : xs)
      | accept automaton
      , test automaton x
      , (Rv p, Rv q) <- sA (progress automaton + 1) $ x : work
      =
        case
          q
        of
          [] ->
            go [] auto xs
          _ ->
            q : go [] auto xs
      | test automaton x
      =
        go (x : work) (successState automaton) xs
      | T
      =
        go (x : work) (failState automaton) xs


-- | Find all occurrences of a particular substring.
-- Occurrences can overlap.
--
-- If you wish to perform multiple searches using the same string you can get better performance by using @'mAW' eq substring@ to perform precomputation.
-- Then you can use the equivalent version 'afw' to do search using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
fa ::
  ( Eq a
  , Num b
  )
    => List a -> List a -> List b
fa =
  faw eq


-- | Count the number of occurrences of a particular substring.
-- Occurrences can overlap.
--
--
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
lfa ::
  ( Eq a
  )
    => List a -> List a -> Integer
lfa =
  l << fa


-- | Replace the first occurrence of a particular substring.
--
-- If you wish to perform multiple replacements using the same string you can get better performance by using @'mAW' eq substring@ to perform precomputation.
-- Then you can use the equivalent version 'arw' to do replacement using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
ra ::
  ( Eq a
  )
    => List a -> List a -> List a -> List a
ra =
  raw eq


-- | Remove the first occurrence of a particular substring.
--
-- If you wish to perform multiple removals using the same string you can get better performance by using @'mAW' eq substring@ to perform precomputation.
-- Then you can use the equivalent version 'avw' to do replacement using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
va ::
  ( Eq a
  )
    => List a -> List a -> List a
va =
  vaw eq


-- | Replace all occurrences of a particular substring.
--
-- Where two occurrences overlap the earlier one is replaced.
--
-- If you wish to perform multiple replacements using the same string you can get better performance by using @'mAW' eq substring@ to perform precomputation.
-- Then you can use the equivalent version 'aRw' to do replacement using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\).
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
rA ::
  ( Eq a
  )
    => List a -> List a -> List a -> List a
rA =
  rAw eq


-- | Split between strings matching a particular string and the rest of the input.
--
-- In the case of overlapping matches the earlier match is the one considered.
--
-- If you wish to perform multiple operations using the same string you can get better performance by using @'mAW' 'eq' substring@ to perform precomputation.
-- Then you can use the equivalent version 'asw' to do replacement using this precomputed automaton.
--
-- ==== __Examples__
--
-- >>> sa "to" "tomato"
-- ["to","ma","to"]
-- >>> sa "aaa" "aaaabaaaaaaa"
-- ["aaa","ab","aaa","aaa","a"]
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\).
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
sa ::
  ( Eq a
  )
    => List a -> List a -> List (List a)
sa =
  saw eq


-- | Split a string along segments matching a substring.
-- The matching substrings are removed.
--
-- In the case of overlapping matches the earlier match is the one considered.
--
-- If you wish to perform multiple operations using the same string you can get better performance by using @'mAW' 'eq' substring@ to perform precomputation.
-- Then you can use the equivalent version 'azw' to do replacement using this precomputed automaton.
--
-- ==== __Examples__
--
-- >>> za "ma" "tomato"
-- ["to","to"]
-- >>> za "to" "tomato"
-- ["ma"]
-- >>> sa "aaa" "aaaabaaaaaaa"
-- ["ab","a"]
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\).
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
za ::
  ( Eq a
  )
    => List a -> List a -> List (List a)
za =
  zaw eq


-- | Split between strings matching a particular string and the rest of the input using a user defined equality function.
--
-- In the case of overlapping matches the earlier match is the one considered.
--
-- If you wish to perform multiple operations using the same string you can get better performance by using @'mAW' 'eq' substring@ to perform precomputation.
-- Then you can use the equivalent version 'asw' to do search using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\).
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
saw ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> List (List a)
saw =
  asw << mAW


-- | Find all occurrences of a particular substring using a user defined equality function.
-- Occurrences can overlap.
--
-- If you wish to perform multiple searches using the same string you can get better performance by using @'mAW' equals substring@ to perform precomputation.
-- Then you can use the equivalent version 'afw' to do search using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
faw ::
  ( Num b
  )
    => (a -> a -> Bool) -> List a -> List a -> List b
faw =
  afw << mAW


-- | Replace the first occurrence of a particular substring using a user defined equality function.
--
-- If you wish to perform multiple replacements using the same string you can get better performance by using @'mAW' equals substring@ to perform precomputation.
-- Then you can use the equivalent version 'arw' to do replacement using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
raw ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> List a -> List a
raw =
  arw << mAW


-- | Replace all occurrences of a particular substring using a user defined equality function.
--
-- Where two occurrences overlap the earlier one is replaced.
--
-- If you wish to perform multiple replacements using the same string you can get better performance by using @'mAW' equals substring@ to perform precomputation.
-- Then you can use the equivalent version 'arw' to do replacement using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
rAw ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> List a -> List a
rAw =
  aRw << mAW


-- | Remove the first occurrence of a particular substring using a user defined equality function.
--
-- If you wish to perform multiple removals using the same string you can get better performance by using @'mAW' equals substring@ to perform precomputation.
-- Then you can use the equivalent version 'avw' to do replacement using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\) time.
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
vaw ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> List a
vaw =
  avw << mAW


-- | Split a string along segments matching a substring using a user defined equality function.
-- The matching substrings are removed.
--
-- In the case of overlapping matches the earlier match is the one considered.
--
-- If you wish to perform multiple operations using the same string you can get better performance by using @'mAW' equals substring@ to perform precomputation.
-- Then you can use the equivalent version 'azw' to do replacement using this precomputed automaton.
--
-- ==== __Order notation__
-- Over the two parameters separately it is \(O(\lambda m n.m\log m+n)\).
--
-- In terms of raw input size it is \(O(\lambda n.n\log n)\) time.
zaw ::
  (
  )
    => (a -> a -> Bool) -> List a -> List a -> List (List a)
zaw =
  azw << mAW
