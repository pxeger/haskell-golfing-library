{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Tuple
  ( st
  , pattern Bp
  , pattern (:@)
  , pattern Pb
  , pattern Jbp
  )
  where


import qualified Prelude
import Prelude
  ( Num
  , (+)
  , (*)
  )


import P.Bool
import P.Category
import P.Comonad
import P.Eq
import P.Function.Compose
import P.Function.Flip
import P.Pattern
import P.Swap


infixr 1 :@


instance (Num a, Num b) => Num (a, b) where
  (x1, x2) + (y1, y2) =
    ( x1 + y1
    , x2 + y2
    )

  (x1, x2) * (y1, y2) =
    ( x1 * y1
    , x2 * y2
    )

  fromInteger i =
    ( Prelude.fromInteger i
    , Prelude.fromInteger i
    )

  negate (x1, x2) =
    ( Prelude.negate x1
    , Prelude.negate x2
    )

  abs (x1, x2) =
    (Prelude.abs x1, Prelude.abs x2)

  signum (x1, x2) =
    (Prelude.signum x1, Prelude.signum x2)


-- | Get the second to last element of a tuple.
--
-- More general version of 'Prelude.fst'.
--
-- For the last element see 'P.Comonad.cr'.
--
-- ==== __Examples__
--
-- >>> st (1,2)
-- 1
-- >>> st (1,3)
-- 1
-- >>> st (1,3,4)
-- 3
-- >>> st (1,3,4,5)
-- 4
-- >>> st (1,3,4,5,7)
-- 5
st ::
  ( Swap p
  , Comonad (p b)
  )
    => p a b -> a
st =
  cr < Sw


{-# Complete Bp #-}
-- | Makes a pair.
pattern Bp ::
  (
  )
    => a -> b -> (a, b)
pattern Bp x1 x2 =
  (x1, x2)


{-# Complete Pb #-}
-- | Flip of 'Bp'.
pattern Pb ::
  (
  )
    => b -> a -> (a, b)
pattern Pb x1 x2 =
  (x2, x1)


-- | A pattern version of 'P.Arrow.jbp'.
-- Since it must go both ways it requires @Eq@.
pattern Jbp ::
  ( Eq a
  )
    => a -> (a, a)
pattern Jbp a <- (a,_) := (Prelude.uncurry eq -> T) where
  Jbp x =
    ( x
    , x
    )


-- | A replacement infix pattern for 2-tuples.
--
-- As a prefix 'Bp' is shorter, as an infix @(,)@ may be shorter as well depending on precedence.
--
-- ==== __Examples__
--
-- In a list of tuples parentheses are not required because the list has such low precedence:
--
-- >>> [(1,2),(3,4)]
-- [(1,2),(3,4)]
-- >>> [1:@2,3:@4]
-- [(1,2),(3,4)]
--
-- However if one of the expressions needs to use '($)' it loses the saved byte.
--
-- When binding to a pattern with @<-@ @(:@)@ is usually shorter:
--
-- >>> f x|(x,y)<-vD x 2=x+y
-- >>> f x|x:@y<-vD x 2=x+y
--
pattern (:@) ::
  (
  )
    => a -> b -> (a, b)
pattern a :@ b =
  (a, b)
