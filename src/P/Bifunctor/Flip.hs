{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language PolyKinds #-}
{-# Language FlexibleInstances #-}
module P.Bifunctor.Flip
  ( Flip (..)
  , pattern UFl
  , pattern LF2
  )
  where

import Prelude
  ( ($)
  , fmap
  )

import P.Functor hiding
  ( fmap
  )

-- | A type level flip.
--
-- A flip function has the type:
--
-- @
-- flip :: (a -> b -> c) -> (b -> a -> c)
-- @
--
-- This flip type has kind:
--
-- @
-- Flip :: (α -> β -> *) -> (β -> α -> *)
-- @
newtype Flip f (a :: β) (b :: α) =
  -- | Flips a type not to be confused with 'P.Function.Flip.F' which flips a function.
  Flp
    { uFl ::
      f b a
    }

-- | Unflip function and pattern.
-- The opposite of 'Flp'.
pattern UFl ::
  (
  )
    => Flip f a b -> f b a
pattern UFl x <- (Flp -> x) where
  UFl =
    uFl

pattern LF2 ::
  (
  )
    => (Flip f a b -> Flip g c d -> e) -> f b a -> g d c -> e
pattern LF2 x <- ((\ func x y -> func (uFl x) (uFl y)) -> x) where
  LF2 func x y =
    func (Flp x) (Flp y)

instance
  ( Functor (p a)
  )
    => Functor (Flip (Flip p) a)
  where
    fmap func (Flp (Flp x)) =
      Flp $ Flp $ fmap func x

instance Functor (Flip (,) a) where
  fmap func (Flp (x, y)) =
    Flp (func x, y)

instance Functor (Flip ((,,) a) b) where
  fmap func (Flp (x, y, z)) =
    Flp (x, func y, z)

instance Functor (Flip ((,,,) a b) c) where
  fmap func (Flp (w, x, y, z)) =
    Flp (w, x, func y, z)

instance Functor (Flip ((,,,,) a b c) d) where
  fmap func (Flp (v, w, x, y, z)) =
    Flp (v, w, x, func y, z)

instance Functor (Flip ((,,,,,) a b c d) e) where
  fmap func (Flp (u, v, w, x, y, z)) =
    Flp (u, v, w, x, func y, z)

instance Functor (Flip ((,,,,,,) a b c d e) f) where
  fmap func (Flp (t, u, v, w, x, y, z)) =
    Flp (t, u, v, w, x, func y, z)

instance Functor (Flip ((,,,,,,,) a b c d e f) g) where
  fmap func (Flp (s, t, u, v, w, x, y, z)) =
    Flp (s, t, u, v, w, x, func y, z)
