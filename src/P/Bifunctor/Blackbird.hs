{-# Language PolyKinds #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
module P.Bifunctor.Blackbird
  ( Blackbird (..)
  )
  where


import qualified Prelude


import qualified Control.Applicative


import P.Applicative
import P.Arrow
import P.Bifunctor
import P.Bifunctor.Flip
import P.Bifunctor.Profunctor
import P.Category
import P.Eq
import P.Function
import P.Function.Compose
import P.Ord


-- | The blackbird combinator on types.
--
-- Takes a functor and a bifunctor and makes a bifunctor by wrapping the functor around the bifunctor.
data Blackbird f p a b =
  -- | Constructor for the 'Blackbird' type.
  -- Compare name with 'P.Function.Composition.mm'
  MM
    { uMM ::
      f (p a b)
    }
  deriving
    ( Prelude.Eq
    , Prelude.Ord
      -- ^ 'P.Ord' should be used in all cases.
      -- We maintain a instance for internal use and this just in case.
    , Prelude.Show
    , Prelude.Read
    )


instance
  ( Functor f
  , Functor (p a)
  )
    => Functor (Blackbird f p a)
  where
    fmap func (MM x) =
      MM (mm func x)


instance
  ( Functor f
  , Functor (Flip p a)
  )
    => Functor (Flip (Blackbird f p) a)
  where
    fmap func (Flp (MM x)) =
      Flp $ MM $ m (mst func) x


instance
  ( Applicative f
  , Applicative (p a)
  )
    => Applicative (Blackbird f p a)
  where
    pure =
      MM < p < p

    liftA2 biFunc (MM x) (MM y) =
      MM (l2 (l2 biFunc) x y)


instance
  ( Eq1 f
  , Eq1 (p a)
  )
    => Eq1 (Blackbird f p a)
  where
    q1 =
      fxO uMM < q1 < q1


instance
  ( Eq1 f
  , Eq2 p
  )
    => Eq2 (Blackbird f p)
  where
    q2 =
      (m $ fm uMM) << fm uMM << q1 << q2


instance
  ( Eq1 f
  , Eq1 (Flip p a)
  )
    => Eq1 (Flip (Blackbird f p) a)
  where
    q1 userEq (Flp (MM x)) (Flp (MM y)) =
      q1 (\ x y -> q1 userEq (Flp x) (Flp y)) x y


instance
  ( Ord1 f
  , Ord1 (p a)
  )
    => Ord1 (Blackbird f p a)
  where
    lcp =
      fxO uMM < lcp < lcp


instance
  ( Ord1 f
  , Ord1 (Flip p a)
  )
    => Ord1 (Flip (Blackbird f p) a)
  where
    lcp userCompare (Flp (MM x)) (Flp (MM y)) =
      lcp (\ x y -> lcp userCompare (Flp x) (Flp y)) x y


instance
  ( Ord (f (p a b))
  )
    => Ord (Blackbird f p a b)
  where
    cp =
      on cp uMM


instance
  ( Semigroupoid p
  , Applicative f
  )
    => Semigroupoid (Blackbird f p)
  where
    MM f <@ MM g =
      MM $ l2 (<@) f g


instance
  ( Category p
  , Applicative f
  )
    => Category (Blackbird f p)
  where
    id =
      MM $ p id


instance
  ( Applicative f
  , LooseArrow p
  )
    => LooseArrow (Blackbird f p)
  where
    MM f =: MM g =
      MM $ l2 (=:) f g

    aSw =
      MM $ p aSw


instance
  ( Functor f
  , Profunctor p
  )
    => Profunctor (Blackbird f p)
  where
    MM f <@^ g =
      MM $ (<@^ g) < f


instance
  ( Applicative f
  , Arrow p
  )
    => Arrow (Blackbird f p)
