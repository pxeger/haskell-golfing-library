{-# Language FlexibleInstances #-}
module P.Bifunctor.Either
  ( Either (..)
  -- * Extract
  , fI
  , fR
  , ffR
  , fL
  , ffL
  , ei
  -- * Deprecated
  , either
  )
  where


import qualified Prelude


import qualified Control.Applicative
import qualified Data.Bifunctor


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Applicative
import P.Applicative.Unpure
import P.Assoc
import P.Bifunctor
import P.Bifunctor.Flip
import P.Bool
import P.Category
import P.Eq
import P.Foldable
import P.Function.Flip
import P.Function.Compose
import P.Functor
import P.Monad
import P.Ord
import P.Swap
import P.Traversable


-- | A sum type.
--
-- Can be the left value or the right value but not both or neither.
data Either a b
  = Lef a -- ^ 'Le' is shorter.
  | Rit b -- ^ For functions 'p' is shorter. For pattern matches 'Pu' is shorter.
  deriving
    ( Prelude.Show
    , Prelude.Read
    , Prelude.Eq
    , Prelude.Ord
    )


instance Functor (Either a) where
  fmap func (Rit x) =
    Rit $ func x
  fmap _ (Lef x) =
    Lef x


instance Functor (Flip Either a) where
  fmap func (Flp (Lef x)) =
    Flp $ Lef $ func x
  fmap _ (Flp (Rit x)) =
    Flp $ Rit x


instance
  ( Semigroup a
  )
    => Applicative (Either a)
  where
    pure =
      Rit
    liftA2 func (Lef x) (Lef y) =
      Lef (x <> y)
    liftA2 func (Lef x) (Rit _) =
      Lef x
    liftA2 func (Rit _) (Lef x) =
      Lef x
    liftA2 func (Rit x) (Rit y) =
      Rit (func x y)


instance
  ( Semigroup a
  )
    => Applicative (Flip Either a)
  where
    pure =
      Flp < Lef
    liftA2 func (Flp (Rit x)) (Flp (Rit y)) =
      Flp $ Rit $ x <> y
    liftA2 func (Flp (Rit x)) (Flp (Lef _)) =
      Flp $ Rit x
    liftA2 func (Flp (Lef _)) (Flp (Rit x)) =
      Flp $ Rit x
    liftA2 func (Flp (Lef x)) (Flp (Lef y)) =
      Flp $ Lef $ func x y


instance Unpure (Either a) where
  unp (Rit x) =
    [ x ]
  unp _ =
    []
  pure' =
    Rit


instance Unpure (Flip Either a) where
  unp (Flp (Lef x)) =
    [ x ]
  unp _ =
    []
  pure' =
    Flp < Lef


instance
  ( Semigroup a
  )
    => Monad (Either a)
  where
    Lef x >>= _ =
      Lef x
    Rit x >>= func =
      func x


instance
  ( Semigroup a
  )
    => Monad (Flip Either a)
  where
    (Flp (Rit x)) >>= _ =
      Flp $ Rit x
    (Flp (Lef x)) >>= func =
      func x


instance Foldable (Either a) where
  foldMap func (Lef _) =
    Prelude.mempty
  foldMap func (Rit x) =
    func x


instance Foldable (Flip Either a) where
  foldMap func (Flp (Rit _)) =
    Prelude.mempty
  foldMap func (Flp (Lef x)) =
    func x


instance Swap Either where
  swap (Rit x) =
    Lef x
  swap (Lef x) =
    Rit x


instance Assoc Either where
  assoc (Lef x) =
    Lef (Lef x)
  assoc (Rit (Lef x)) =
    Lef (Rit x)
  assoc (Rit (Rit x)) =
    Rit x


  unassoc (Lef (Lef x)) =
    Lef x
  unassoc (Lef (Rit x)) =
    Rit (Lef x)
  unassoc (Rit x) =
    Rit (Rit x)


instance
  ( Eq a
  )
    => Eq1 (Either a)
  where
    q1 =
      q1'


instance
  (
  )
    => Eq2 Either
  where
    q2 userEq1 _ (Lef x) (Lef y) =
      userEq1 x y
    q2 _ userEq2 (Rit x) (Rit y) =
      userEq2 x y
    q2 _ _ _ _ =
      B


instance
  ( Ord a
  , Ord b
  )
    => Ord (Either a b)
  where
    cp =
      lcp cp


instance
  ( Ord a
  )
    => Ord1 (Either a)
  where
    lcp _ (Lef _) (Rit _) =
      LT
    lcp _ (Rit _) (Lef _) =
      GT
    lcp _ (Lef x) (Lef y) =
      cp x y
    lcp func (Rit x) (Rit y) =
      func x y


instance Traversable (Either a) where
  traverse _ (Lef x) =
    p (Lef x)
  traverse func (Rit x) =
    Rit < func x


-- | Extract the value from an 'Either'.
fI ::
  (
  )
    => Either a a -> a
fI (Lef x) =
  x
fI (Rit x) =
  x


-- | Extract a value from an 'Either'.
--
-- When it is right return the value.
-- When it is left uses a conversion function to get the value.
fR ::
  (
  )
    => (a -> b) -> Either a b -> b
fR func (Lef x) =
  func x
fR _ (Rit x) =
  x


-- | Flip of 'fR'.
ffR ::
  (
  )
    => Either a b -> (a -> b) -> b
ffR =
  F fR


-- | Extract a value from an 'Either'.
--
-- When it is left return the value.
-- When it is right uses a conversion function to get the value.
fL ::
  (
  )
    => (b -> a) -> Either a b -> a
fL _ (Lef x) =
  x
fL func (Rit x) =
  func x


-- | Flip of 'fL'.
ffL ::
  (
  )
    => Either a b -> (b -> a) -> a
ffL =
  F fL


-- | Extract a value from an 'Either'.
--
-- Use two conversion functions for each side of the either.
ei ::
  (
  )
    => (a -> c) -> (b -> c) -> Either a b -> c
ei func1 _ (Lef x) =
  func1 x
ei _ func2 (Rit x) =
  func2 x


{-# Deprecated either "Use ei instead" #-}
-- | Long version of 'ei'.
either ::
  (
  )
    => (a -> c) -> (b -> c) -> Either a b -> c
either =
  ei
