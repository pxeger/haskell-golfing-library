-- |
-- Module :
--   P.Parse.Regex
-- Description :
--   Regex parsing
-- Copyright :
--   (c) E. Olive, 2022
-- License :
--   GPL-3
-- Maintainer :
--   ejolive97@gmail.com
-- Stability :
--   Experimental
--
-- This library provides a small regex parser compatible with the main 'P.Parse' library.
--
-- == Regex Specification
--
-- We use a custom regex flavor suited for the needs of the library.
-- It is a somewhat minimalistic with a focus on terseness.
--
-- This is not a thorough specification some edge cases will not be addressed.
--
-- === __Character Classes__
--
-- A character class is enclosed in square brackets (@[...]@).
-- It will match one single character as long as it is between the brackets.
--
-- >>> f=rX"c[ao]t"
-- >>> cP f "cat"
-- True
-- >>> cP f "cot"
-- True
-- >>> cP f "cut"
-- False
--
-- Negative character classes are enclosed in square brackets with a carrot (@[^...]@).
-- They will match one single character as long as it is *not* in between the brackets.
--
-- >>> f=rX"c[^ao]t"
-- >>> cP f "cat"
-- False
-- >>> cP f "cot"
-- False
-- >>> cP f "cut"
-- True
--
-- As a short hand, if you prepend a character with @^@ without brackets it will act as a 1 character negative character class.
-- That is @^x@ is equivalent to @[^x]@.
--
-- >>> f=rX"c^ut"
-- >>> cP f "cat"
-- True
-- >>> cP f "cot"
-- True
-- >>> cP f "cut"
-- False
--
-- You can also use @.@ as a short hand for an empty negative character class.
-- That is @.@ is equivalent to @[^]@.
--
-- >>> f=rX"c.t"
-- >>> cP f "cot"
-- True
-- >>> cP f "cat"
-- True
-- >>> cP f "cut"
-- True
--
-- Character classes, positive or negative, do not nest.
--
-- === __Multiplexers__
--
-- If you append a certain expression with a number @n@ in curly braces (@{}@), it will repeat that expression @n@ times.
-- So for example @.{9}@ is the same as the expression:
--
-- @
-- .........
-- @
--
-- Numbers are formatted as hexadecimal values, so @{10}@ is sixteen and @{A}@ (or @{a}@) is ten.
--
-- >>> f=rX"[ab]{5}"
-- >>> gP f "abbba"
-- ["abbba"]
-- >>> gP f "bbb"
-- []
-- >>> gP f "bbbaaaaa"
-- ["bbbaa"]
--
-- You can also specify a range of values with a @-@ between the minimum and maximum of the range (inclusive).
--
-- >>> f=rX"[ab]{2-5}"
-- >>> gP f "abbba"
-- ["abbba","abbb","abb","ab"]
-- >>> gP f "bbb"
-- ["bbb","bb"]
-- >>> gP f "bbbaaaaa"
-- ["bbbaa","bbba","bbb","bb"]
-- >>> gP f "b"
-- []
--
-- If you omit one of the numbers in the range it will be assumed to extend indefinitely in that direction.
-- So for example @{@@-4}@ means "up to four" and @{6-@@}@ means "at least six".
--
-- >>> f=rX"[ab]{-5}"
-- >>> gP f "bbbaaaaa"
-- ["bbbaa","bbba","bbb","bb","b",""]
-- >>> gP f "baa"
-- ["baa","ba","b",""]
-- >>> f=rX"[ab]{4-}"
-- >>> gP f "bbbaaaaa"
-- ["bbbaaaaa","bbbaaaa","bbbaaa","bbbaa","bbba"]
-- >>> gP f "baa"
-- []
--
--
-- You can combine multiple ranges with @,@.
-- So for example @{3,7}@ means "three or seven".
--
-- >>> f=rX"[ab]{3,7}"
-- >>> gP f "babababb"
-- ["bab","bababab"]
-- >>> gP f "babb"
-- ["bab"]
--
-- There are also the familar shorthands available
--
--  - @?@: Matches 1 or zero copies and is the same as @{@@-1}@.
--  - @*@: Matces any number of copies and is the same as @{0-@@}@.
--  - @+@: Matches at least one copy and is the same as @{1-@@}@.
--
-- Currently mulitplexers cannot stack, if you wish to use multiple multiplexers you must use groups.
--
-- === __End__
--
-- @$@ matches the end of the input.
--
--
-- >>> f=rX".+r$"
-- >>> gP f "sonar"
-- ["sonar"]
-- >>> gP f "art"
-- []
-- >>> gP f "boron"
-- []
--
-- @$@ does not consume any input so @$$@ behaves the same way as @$@.
--
-- Because of the way our parser works there is no way to match the beginning of a string.
--
--
-- === __Groups__
--
-- Groups are a way to make multiple units act as a single unit.
-- To make a group enclose the intended group in parentheses (@()@).
-- For example if we have: @[ab][ac]?@ the @?@ only effects the @[ac]@ not the whole regex.
--
-- >>> f=rX"[ab][ac]?"
-- >>> gP f "bc"
-- ["bc","b"]
-- >>> gP f ""
-- []
-- >>> gP f "aa"
-- ["aa","a"]
--
-- If we use a group the two character classes can act as one group and the @?@ effects them together.
--
-- >>> f=rX"([ab][ac])?"
-- >>> gP f "bc"
-- ["bc",""]
-- >>> gP f ""
-- [""]
-- >>> gP f "aa"
-- ["aa",""]
--
-- === __Options__
--
-- If we have two expressions and we want to make an expression which matches either of them we can use @|@.
-- When placed between two expressions @|@ will try both of them and match if either of them match.
--
-- >>> f=rX"[bo]+$|[pa]+$"
-- >>> gP f "bob"
-- ["bob"]
-- >>> gP f "papa"
-- ["papa"]
-- >>> gP f "pabo"
-- []
-- >>> gP f "boap"
-- []
--
-- @|@ can be enclosed in a group and operated on as a unit.
--
-- >>> f=rX"(bo|pa)+$"
-- >>> gP f "bob"
-- []
-- >>> gP f "papa"
-- ["papa"]
-- >>> gP f "pabo"
-- ["pabo"]
-- >>> gP f "boap"
-- []
--
-- === __Look aheads__
--
-- A look ahead will check an expression without consuming it.
--
-- Appending a @\@@ to a unit makes it a positive look ahead.
-- A positive lookahead will check that the expression matches the input but doesn't consume it.
--
-- So below we have a look ahead that checks for 3 or more @a@s, but when the match is always empty.
--
-- >>> f=rX"(a{3-})@"
-- >>> gP f "aa"
-- []
-- >>> gP f "aaa"
-- [""]
-- >>> gP f "aaaa"
-- ["",""]
--
-- We can combine lookaheads with other expressions to check the same input twice:
--
-- >>> f=rX"((.[^a])+$)@(..b)+$"
-- >>> gP f "anbulb"
-- ["anbulb"]
-- >>> gP f "anbalb"
-- []
-- >>> gP f "anclub"
-- []
--
-- Appending @!@ to a unit makes it a negative look ahead.
-- A negative look ahead also doesn't consume the input however it only matches when the expression doesn't match on the input.
--
-- >>> f=rX"(cab)![abc]+$"
-- >>> gP f "baba"
-- ["baba"]
-- >>> gP f "cacb"
-- ["cacb"]
-- >>> gP f "acab"
-- ["acab"]
-- >>> gP f "cab"
-- []
-- >>> gP f "caba"
-- []
--
--
-- === __Silencing__
--
-- A silenced group will consume input but wont contribute to the result.
--
-- Appending a @_@ to a group will cause it to to become silenced.
--
-- For example the following regex matches any length of @moo@ but only gives the first two @o@s in the result.
--
-- >>> f=rX"moo(o*)_$"
-- >>> gP f "moooooo"
-- ["moo"]
-- >>> gP f "moo"
-- ["moo"]
-- >>> gP f "moooooooo"
-- ["moo"]
--
-- @_@ can also be applied without parenthesis like other post-fixes:
--
-- >>> f=rX"boa_t"
-- >>> gP f "boat"
-- ["bot"]
--
-- You can also replace the output of a group with an entirely different value using @{@ and @_@.
-- This is a post-fix like @_@ but it replaces the result with whatever is in between the @{@ and the @_@.
--
-- >>> f=rX"ba{i_t"
-- >>> gP f "bat"
-- ["bit"]
--
-- Positive look aheads are silenced by default. If you add an @_@ post-fix to a positive look ahead it will unsilence it.
-- This means that whatever is parsed inside the lookahead will contribute to the result even though it won't be consumed.
--
-- >>> f=rX"(..)@.."
-- >>> gP f "12"
-- ["12"]
-- >>> f=rX"(..)@_.."
-- >>> gP f "12"
-- ["1212"]
--
-- Positive look aheads still work as normal with the @{.._@ silencer:
--
-- >>> f=rX"(..)@{ab_.."
-- >>> gP f "12"
-- ["ab12"]
--
-- Since there's no meaningful way to unsilence a negative lookahead, @!_@ will not do anything special.
--
-- === __Escaping__
--
-- @/@ escapes the character before it.
-- If that character is a syntactic character (@(@, @)@, @^@, @/@ etc.) then it acts as a non-sytactic version.
--
-- For example:
--
-- >>> f=rX"/$+"
-- >>> gP f "$$$$"
-- ["$$$$","$$$","$$","$"]
--
-- Other characters become syntactic when escaped.
--
-- * @/a@ matches a letter. See 'P.Char.α'.
-- * @/d@ matches a single digit
-- * @/l@ matches a an lowercase character. See 'P.Char.iL'.
-- * @/p@ performs a user specified parser. See 'rXw'.
-- * @/r@ performs the entire regex as a parse. See Recursion for info.
-- * @/U@ matches a an uppercase character. See 'P.Char.iU'.
-- * @/w@ matches a whitespace character. See 'P.Char.iW'.
--
-- === __Recursion__
--
-- The @/r@ escape sequece can be used to create a recursive expression.
-- @/r@ acts as a stand-in for the entire parser including the @\/r@ itself.
--
-- For example the following regex matches any string of @a@s followed by @b@s where the number of @a@s and @b@s are equal.
--
-- >>> f=rX"(a/rb)?"
-- >>> gP f ""
-- [""]
-- >>> gP f "ab"
-- ["ab",""]
-- >>> gP f "aabb"
-- ["aabb",""]
-- >>> gP f "aaabbb"
-- ["aaabbb",""]
-- >>> gP f "aaabb"
-- [""]
--
-- @/0@ is similar to @\/r@ except instead of standing in for the entire parser it stands in for the enclosing capture group.
--
-- For example we can enclose a recursive regex in a group and require something come after it:
--
-- >>> f=rX"(g/0?o)/!"
-- >>> gP f "go!"
-- ["go!"]
-- >>> gP f "ggoo!"
-- ["ggoo!"]
-- >>> gP f "gggooo!"
-- ["gggooo!"]
-- >>> gP f "ggoooo!"
-- []
--
-- As another example heres a regex to match a string of @a@s followed by @b@s followed by @c@s where the number of @a@s, @b@s and @c@ are equal:
--
-- >>> f=rX"((a/0?b)c)@a+(b/0?c)$"
-- >>> uP f"aabbcc"
-- [("","aabbcc")]
-- >>> uP f"aabbbccc"
-- []
-- >>> uP f"abc"
-- [("","abc")]
-- >>> uP f"aabbc"
-- []
--
-- @/0@ accesses the enclosing group, to access the scope enclosing that you can use @/1@.
-- And in general to access n scopes up use @/@n with n in decimal.
--
-- The top level acts as a enclosing group so that can be accessed with @/r@ or with the appropriate number.
--
-- >>> f=rX"(a/1b)?"
-- >>> gP f ""
-- [""]
-- >>> gP f "ab"
-- ["ab",""]
-- >>> gP f "aabb"
-- ["aabb",""]
-- >>> gP f "aaabbb"
-- ["aaabbb",""]
-- >>> gP f "aaabb"
-- [""]
--
-- === __User defined command__
--
-- The @/p@ escape sequence is reserved so the user can redefine it.
-- When using 'rXw' the user passes a custom parser, @/p@ then uses this parser when it is called in the regex.
-- When using 'rX' or an other functon that doesn't take a custom parser @/p@ has no special behavior and does not act as an escape sequence.
--
module P.Parse.Regex
  ( RegexString
  , rX
  , rXw
  , rxw
  , rXW
  , rxW
  , rXx
  , uPx
  , uPX
  , pPx
  , pPX
  , x1x
  , x1X
  , gPx
  , gPX
  , gcx
  , gcX
  , gkx
  , gkX
  , srX
  , skX
  -- * Internal use
  , tokenizeRegex
  , stringToRegex
  )
  where


import qualified Prelude
import Prelude
  ( Int
  )


import P.Algebra.Semigroup
import P.Aliases
import P.Alternative
import P.Alternative.AtLeast
import P.Alternative.Between
import P.Alternative.Many
import P.Alternative.Possible
import P.Alternative.Some
import P.Applicative
import P.Arithmetic
import P.Bool
import P.Category
import P.Char
import P.Comonad
import P.First
import P.Fix
import P.Foldable
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.List
import P.Monad
import P.Parse
import P.Parse.Combinator
import P.Parse.Extra
import P.Scan.Foldable
import P.String
import P.Zip.WeakAlign
import P.Zip.UnitalZip


-- | Regex is an alias for String.
-- Used to make types clearer.
type RegexString =
  String


-- | Convex range on positive integers.
data ConvexRange
  = RangeFrom Int
  | RangeFromTo Int Int
  deriving
    ( Prelude.Show
    )


-- | A structured regex object for internal use.
data Regex
  = Char Char
  | NegCharClass String
  | Alt (List Regex)
  | Sequence (List Regex)
  | Count (List ConvexRange) Regex
  | LookAhead Regex
  | NegLookAhead Regex
  | Silencer String Regex
  | End
  | Letter
  | Space
  | Lower
  | Upper
  | UserParser
  | CompleteRecurse
  | RecurseFrom Int
  deriving
    ( Prelude.Show
    )


-- | Breaks a regex up into tokens
tokenizeRegex ::
  (
  )
    => Parser RegexString String (List String)
tokenizeRegex =
  gMy $
    (ʃ "/" *^* gSo (xay "0123456789"))
    #| (ʃ "/" *^* xys "_/()[]|^.+*?{@!$adlprUw")
    #| hdS


-- | Converts a list of tokens into a structured regex object.
parseRegex ::
  (
  )
    => Parser (List String) (List String) Regex
parseRegex =
  Alt < isP option (χ "|") <* en
  where
    group =
      m Alt $ χ "(" *> isP option (χ "|") <* χ ")"

    option =
      Sequence < my step

    step =
      l2 (F ($))
        smallStep
        postPosition
      where
        smallStep =
          m NegCharClass (ʃ ["[","^"] *> charGroup <* χ "]")
          #| m (NegCharClass < p < detoken) (χ "^" *> nxy ["(",")"])
          #| m (Alt < m Char) (χ "[" *> charGroup <* χ "]")
          #| group
          #| recurseFrom
          #| escapeSequences
          #| m Char (detoken < nxy ["(","|",")"])

        postPosition =
          (((< LookAhead) < Silencer) < (ʃ ["@", "{"] *> mY (nχ "_") <* χ "_"))
          #| (LookAhead <$ ʃ ["@", "_"])
          #| (Silencer "" < LookAhead <$ χ "@")
          #| (NegLookAhead <$ χ "!")
          #| (Count < (χ "{" *> isP range (χ ",") <* χ "}"))
          #| (Silencer < (χ "{" *> mY (nχ "_") <* χ "_"))
          #| (Silencer "" <$ χ "_")
          #| (Count [RangeFromTo 0 1] <$ χ "?")
          #| (Count [RangeFrom 0] <$ χ "*")
          #| (Count < p < RangeFrom < lSO (χ "+"))
          #| p id

        recurseFrom :: Parser (List String) (List String) Regex
        recurseFrom =
          RecurseFrom < (hd @<* (ʃ "/" *> gre ν))

        escapeSequences =
          cFM
            ( \ (key, expression) ->
              expression <$ χ key
            )
            [ ( "/a"
              , Letter
              )
            , ( "/d"
              , Alt $ Char < ['0'..'9']
              )
            , ( "/l"
              , Lower
              )
            , ( "/p"
              , UserParser
              )
            , ( "/r"
              , CompleteRecurse
              )
            , ( "/U"
              , Upper
              )
            , ( "/w"
              , Space
              )
            , ( "$"
              , End
              )
            , ( "."
              , NegCharClass ""
              )
            ]

    range =
      ( do
        n <- localParseHex
        χ "-"
        m <- localParseHex
        p $ RangeFromTo n m
      )
      #|
        ( do
          χ "-"
          n <- localParseHex
          p $ RangeFromTo 0 n
        )
      #|
        ( do
          n <- localParseHex
          χ "-"
          p $ RangeFrom n
        )
      #|
        ( do
          n <- localParseHex
          p $ RangeFromTo n n
        )

    charGroup =
      gMy $ detoken < nχ "]"

    detoken [x] = x
    detoken ('/':[x]) = x

    localParseHex :: Parser (List String) (List String) Int
    localParseHex =
      digit >~ go
      where
        go n =
          ( (16*n+) < digit >~ go )
          #| p n
        xayp = xay < m p
        digit =
          (Prelude.read < xayp "0123456789")
          #| (xayp "Aa" *> p 10)
          #| (xayp "Bb" *> p 11)
          #| (xayp "Cc" *> p 12)
          #| (xayp "Dd" *> p 13)
          #| (xayp "Ee" *> p 14)
          #| (xayp "Ff" *> p 15)


-- | Converts a structured regex object into a string parser.
regexToParser ::
  (
  )
    => List (Parser String String String) -> Regex -> Parser String String String
regexToParser =
  cr << go []
  where
    go
      :: List (Parser String String String)
      -> List (Parser String String String)
      -> Regex
      -> ( List (Parser String String String)
         , Parser String String String
         )
    go _ userParsers (Char char) =
      ( userParsers
      , ʃ [char]
      )
    go _ userParsers (NegCharClass "") =
      ( userParsers
      , hdS
      )
    go _ userParsers (NegCharClass chars) =
      ( userParsers
      , p < nxy chars
      )
    go _ userParsers End =
      ( userParsers
      , ens
      )
    go stack userParsers (Alt regexes) =
      yy (\ ~(_, frame) -> cx < mAa (go (frame : stack)) userParsers regexes)
    go stack userParsers (Sequence regexes) =
      Prelude.foldl l2p (p "") < mAa (go stack) userParsers regexes
    go _ userParsers (Count [] regex) =
      ( userParsers
      , em
      )
    go stack userParsers (Count (range : ranges) regex) =
      let
        mod :: Parser String String String -> Parser String String String
        mod =
          case
            range
          of
            RangeFrom 0 ->
              mY
            RangeFrom 1 ->
              sO
            RangeFrom n ->
              aL n
            RangeFromTo 0 1 ->
              pY
            RangeFromTo n m ->
              bW n m
      in
        ((++ cr (go stack userParsers (Count ranges regex))) < mod) < go stack userParsers regex
    go stack userParsers (LookAhead regex) =
      lA < go stack userParsers regex
    go stack userParsers (NegLookAhead regex) =
      m (p "") < nA < go stack userParsers regex
    go stack userParsers (Silencer string regex) =
      m (p string) < go stack userParsers regex
    go _ userParsers Letter =
      ( userParsers
      , p < kB α
      )
    go _ userParsers Space =
      ( userParsers
      , p < kB iW
      )
    go _ userParsers Lower =
      ( userParsers
      , p < kB iL
      )
    go _ userParsers Upper =
      ( userParsers
      , p < kB iU
      )
    go stack userParsers (RecurseFrom rIndex) =
      ( userParsers
      , case
          stack !! rIndex
        of
          [x] ->
            x
          _ ->
            Prelude.error $ "Recursion index " ++ Prelude.show rIndex ++ " exceeds the bounds of the stack frame."
      )
    go stack userParsers CompleteRecurse =
      ( userParsers
      , Prelude.last stack
      )
    go _ [] UserParser =
      ( []
      , ʃ "/p"
      )
    go _ (p1 : userParsers) UserParser =
      ( userParsers
      , p1
      )


-- | Takes a string representing a regex and outputs a regex object.
stringToRegex ::
  (
  )
    => RegexString -> Regex
stringToRegex regexString =
  case
    gc tokenizeRegex regexString
  of
    [] ->
      Prelude.error "Failure to tokenize regex"
    (tkns : _) ->
      case
        gc parseRegex tkns
      of
        [] ->
          Prelude.error "Failure to parse regex"
        (regex : _) ->
          regex


-- | Takes a regex as a string and creates an equivalent parser.
rX ::
  (
  )
    => RegexString -> Parser String String String
rX =
  rXW []


-- | Takes a regex as a string and creates an equivalent parser.
-- Takes a custom parser from the user, each time @/p@ is called in the regex it uses the custom parser.
--
-- ==== __Examples__
--
-- Use with this to asign a long string to @/p@ to avoid reapeats:
--
-- >>> f = rXw (ʃ"xyzabcuu") "/p|x/px|yy/pyy"
-- >>> cP f "xyzabcuu"
-- True
-- >>> cP f "xxyzabcuux"
-- True
-- >>> cP f "xxxyzabcuuxx"
-- False
-- >>> cP f "yyxyzabcuuyy"
-- True
--
-- Use this to envoke a parser with return properties not possible through regex:
--
-- >>> f = rXw (Rv < my (nχ ',')) "(/p,)*/p"
-- >>> gc f "abc,123,456"
-- ["cba,321,654"]
-- >>> gc f "abc,123"
-- ["cba,321"]
-- >>> gc f "xxxxxxxxxy"
-- ["yxxxxxxxxx"]
--
rXw ::
  (
  )
    => Parser String String String -> RegexString -> Parser String String String
rXw =
  rXW < rp


-- | Flip of 'rXw'.
rxw ::
  (
  )
    => RegexString -> Parser String String String -> Parser String String String
rxw =
  f' rXw


-- | Takes a regex as a string and creates an equivalent parser.
-- Takes a list of custom parsers from the user, each time @/p@ is called in the regex it uses the next parser from the user list.
-- If it runs out of parsers @/p@ stops being a special escape sequence.
--
rXW ::
  (
  )
    => List (Parser String String String) -> RegexString -> Parser String String String
rXW _ "" =
  p ""
rXW userParsers x =
  regexToParser userParsers $ stringToRegex x


-- | Flip of 'rXW'.
rxW ::
  (
  )
    => RegexString -> List (Parser String String String) -> Parser String String String
rxW =
  f' rXW


-- | Takes a regex as a string and creates a parser which returns all matches to the regex in the string.
rXx ::
  (
  )
    => RegexString -> Parser String String String
rXx s =
  h' *> rX s


-- | Apply a regex to a string giving a list of possible parses as a pair of the remaining unparsed string and the result of the parse.
--
-- ==== __Examples__
--
-- >>> uPx ".*n" "Only on mondays"
-- [("days","Only on mon"),(" mondays","Only on"),("ly on mondays","On")]
uPx ::
  (
  )
    => RegexString -> String -> List (String, String)
uPx =
  uP < rX


-- | Applies a regex to a string like 'uPx' except it begins and ends parsing at any location in the string.
--
-- ==== __Examples__
--
-- >>> uPX ".*n" "Monday"
-- [("day","n"),("day","on"),("day","Mon")]
uPX ::
  (  )
    => RegexString -> String -> List (String, String)
uPX =
  uP < rXx


-- | Determines if a regex matches the front of a string.
--
-- Variant of 'pP' for regexes.
pPx ::
  (
  )
    => RegexString -> String -> Bool
pPx =
  pP < rX


-- | Determines if a regex matches any part of a string.
pPX ::
  (  )
    => RegexString -> String -> Bool
pPX =
  pP < rXx


-- | Determines if a regex matches the entire string in exactly one way.
--
-- ===== __Examples__
--
-- The following regex matches any string, but matches strings of length 1 two ways.
--
-- >>> x1x".+|.?" ""
-- True
-- >>> x1x".+|.?" "a"
-- False
-- >>> x1x".+|.?" "aa"
-- True
-- >>> x1x".+|.?" "aaa"
-- True
--
x1x ::
  (
  )
    => RegexString -> String -> Bool
x1x =
  x1 < rX


-- | Determine if a regex matches a string in exactly one way.
-- The match must end at the end of the string but can start anywhere in the string.
x1X ::
  (
  )
    => RegexString -> String -> Bool
x1X =
  x1 < rXx


-- | Gets all parses of a string using a given regex.
-- Parses start at the beginning of the string.
gPx ::
  (
  )
    => RegexString -> String -> List String
gPx =
  gP < rX


-- | Gets all parses of a string using a given regex.
-- Parses can start anywhere in the string.
gPX ::
  (  )
    => RegexString -> String -> List String
gPX =
  gP < rXx


-- | Gets all complete parses of a string using a given regex.
-- Parses must start at the beginning of the string and end at the end of the string.
gcx ::
  (
  )
    => RegexString -> String -> List String
gcx =
  gc < rX


-- | Gets all parses of a string using a given regex.
-- Parses can start anywhere in the string but must end at the end of the string.
gcX ::
  (
  )
    => RegexString -> String -> List String
gcX =
  gc < rXx


-- | Gets the first complete parse of a string using a given regex.
-- Parses must start at the beginning of the string and end at the end of the string.
--
-- Produces an error if there are no parses.
gkx ::
  (
  )
    => RegexString -> String -> String
gkx =
  gk < rX


-- | Gets the first complete parse of a string using a given regex.
-- Parses can start anywhere string but must end at the end of the string.
--
-- Produces an error if there are no parses.
gkX ::
  (
  )
    => RegexString -> String -> String
gkX =
  gk < rXx


-- | Find and replace with a regex.
--
-- Outputs all the ways to replaces matches with their result in place.
-- Unless a regex contains a silencer its result is the same as the text matched, so 'srX' is only useful when using silencers.
--
-- === __Examples__
--
-- Get all ways to replace @a@s with @o@s:
--
-- >>> srX"a{o_" "cabal"
-- ["cobol","cobal","cabol","cabal"]
--
-- Get all ways to replace an @a@ after whitespace or @-@ with an @A@:
--
-- >>> srX"(-|/w)a{A_" "An alpabetical old-aged book"
-- ["An Alpabetical old-Aged book","An Alpabetical old-aged book","An alpabetical old-Aged book","An alpabetical old-aged book"]
srX ::
  (
  )
    => RegexString -> String -> List String
srX =
  sre < rX


-- | Like 'srX' but only outputting the first result.
--
-- === __Examples__
--
-- Replace @a@s with @o@s:
--
-- >>> skX"a{o_" "cabal"
-- "cobol"
--
-- Replace an @a@ after whitespace or @-@ with an @A@:
--
-- >>> skX"(-|/w)a{A_" "An alpabetical old-aged book"
-- "An Alpabetical old-Aged book"
--
skX ::
  (
  )
    => RegexString -> String -> String
skX =
  sk < rX
