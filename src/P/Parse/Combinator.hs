{-|
Module :
  P.Parse.Combinator
Description :
  Parser combinators
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Parser combinators.

Some useful combinators are implemented as a result of 'Parser' instances.

== Alternative

Alternative provides number of useful functions, some of the particularly geared towards parsing.

@
(++) :: Parser a b c -> Parser a b c -> Parser a b c
@

@(++)@ runs takes two parsers and attempts to run both of them.
This can be thought of as an *or* operation on parsers

==== __Examples__

>>> uP (gsO (ʃa ++ ʃb)) "aab2bb2a"
[("2bb2a","aab"),("b2bb2a","aa"),("ab2bb2a","a")]

== Weakalign

==== __Examples__

>>> gc (so $ gSo dg #| hdS) "test1234mes"
[["t","e","s","t","1234","m","e","s"]]
-}
module P.Parse.Combinator
  (
  -- * Parser combinators
    lA
  , nA
  , kB
  , bkB
  , nK
  , χ
  , x_
  , x'
  , bx
  , nχ
  , xx
  , zWx
  , xxh
  , bXx
  , ʃ
  , s_
  , s'
  , bH
  , hh
  , zWh
  , bhh
  , gy
  , bgy
  , lGk
  , gy'
  , bgY
  , bkw
  , xay
  , xys
  , nxy
  , asy
  , ivP
  , gre
  , laz
  , bʃ
  , bSh
  , isP
  , ($|$)
  , fiP
  , pew
  , peW
  , pEw
  , νa
  , enk
  , fNk
  , yS
  , yχ
  -- ** Composition
  , lp2
  , (#*>)
  , (#<*)
  , yju
  , fyj
  , (@<*)
  , yku
  , fyk
  , (@*>)
  , ymu
  , fym
  , (@^*)
  -- * Pre-made Parsers
  -- For more see 'P.Parse.Extra'.
  , hd
  , h_
  , h'
  , hdS
  , bhd
  , mhd
  , dg
  , af
  , p_
  , pc
  , pL
  , pU
  , aN
  , pR
  , wd
  , wr
  , en
  , ens
  , p16
  , lWs
  , tWs
  , ν
  , νw
  , νd
  -- * Deprecated
  , string
  , char
  , liftParser2
  )
  where


import qualified Prelude
import Prelude
  ( Read
  , Maybe (..)
  , Integral
  )


import qualified Text.Read


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Aliases
import P.Alternative
import P.Alternative.Many
import P.Alternative.Some
import P.Applicative
import P.Arithmetic
import P.Bifunctor
import P.Bool
import P.Category
import P.Char
import P.Eq
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Curry
import P.Function.Flip
import P.Functor
import P.Last
import P.Monad
import P.Monad.Plus.Filter
import P.Parse
import P.Reverse
import P.String
import P.Swap
import P.Zip
import P.Zip.WeakAlign


-- | Compose two parsers ignoring the result of the first parser.
--
-- More general version of '(*>)' for parsers.
(#*>) ::
  (
  )
    => Parser a b c -> Parser b d e -> Parser a d e
P p1 #*> P p2 =
  P $ \ v -> do
    (x, _) <- p1 v
    p2 x


-- | Compose two parsers ignoring the result of the second parser.
--
-- More general version of '(<*)' for parsers.
(#<*) ::
  (
  )
    => Parser a b c -> Parser b d e -> Parser a d c
P p1 #<* P p2 =
  P $ \ v -> do
    (x, y) <- p1 v
    (k, _) <- p2 x
    p (k, y)


-- | Adds any amount of leading whitespace to a parser.
lWs ::
  (
  )
    => Parser String a b -> Parser String a b
lWs =
  (my p_ #*>)


-- | Adds any amount of trailing whitespace to a parser.
tWs ::
  (
  )
    => Parser a String b -> Parser a String b
tWs =
  (#<* my p_)


-- | A look-ahead.
-- It takes a parser and produces a new parser which creates the same results without consuming the input.
--
-- Useful if you want to run two parsers on the same thing.
--
-- ==== __Examples__
--
-- Here's it being used to create a parser which consumes palindromes off the front of a list:
--
-- >>> f=lA h'>~ʃ<rv
-- >>> x1 f "abeba"
-- True
-- >>> x1 f "abba"
-- True
-- >>> x1 f "abeja"
-- False
--
lA ::
  (
  )
    => Parser k j a -> Parser k k a
lA parser =
  P
    ( \ input ->
      do
        (_, res) <- uP parser input
        p (input, res)
    )


-- | A negative look-ahead.
-- I takes a parser and produces a new parser which consumes no input and is successful if an only if the given parser was not.
--
nA ::
  (
  )
    => Parser k j a -> Parser k k ()
nA parser =
  P
    ( \ input ->
      case
        uP parser input
      of
        [] ->
          p (input, ())
        _ ->
          em
    )


-- | Accepts and returns single character.
hd ::
  (
  )
    => Parser (List a) (List a) a
hd =
  P run
  where
    run [] =
      em
    run (head : tail) =
      p (tail, head)


-- | Accepts a single character and returns it as a string.
hdS ::
  (
  )
    => Parser (List a) (List a) (List a)
hdS =
  mhd p


-- | Accepts and returns at least one character.
h_ ::
  (
  )
    => Parser (List a) (List a) (List a)
h_ =
  so hd


-- | Accpets and returns any number of characters including none at all.
h' ::
  (
  )
    => Parser (List a) (List a) (List a)
h' =
  my hd


-- | Accepts and returns a single character off the back of the input.
--
-- In general this is much slower than 'hd' because it needs to traverse to the end of the list.
--
-- ===== __Examples__
--
-- >>> uP bhd "Hello, world!"
-- [("Hello, world",'!')]
--
bhd ::
  (
  )
    => Parser (List a) (List a) a
bhd =
  P run
  where
    run [] =
      em
    run (last :> init) =
      p (init, last)


-- | Accepts a single character and applies a function to it.
mhd ::
  (
  )
    => (a -> b) -> Parser (List a) (List a) b
mhd =
  fm hd


-- | Parses a single character accepting if it satisfies some predicate.
kB ::
  (
  )
    => Predicate a -> Parser (List a) (List a) a
kB =
  wh hd


-- | Parses a single character off the back of a list accepting if it satisfies some predicate.
--
-- In general this is much slower than 'kB' because it needs to traverse to the end of a list.
--
-- ===== __Examples__
--
-- >>> uP (bkB α) "Toast"
-- [("Toas",'t')]
--
bkB ::
  (
  )
    => Predicate a -> Parser (List a) (List a) a
bkB =
  wh bhd


-- | Parses a single character accepting it if it doesn't satisfy the given predicate.
nK ::
  (
  )
    => Predicate a -> Parser (List a) (List a) a
nK =
  kB < m n


-- | Greedily consumes as many characters as possible matching a predicate.
gy ::
  (
  )
    => Predicate a -> Parser (List a) (List a) (List a)
gy =
  gMy < kB


-- | Greedily consibes as many characters as possible matching a predicate off the end of a list.
--
-- Significantly slower than consuming characters off the front of a list with 'gy'.
bgy ::
  (
  )
    => Predicate a -> Parser (List a) (List a) (List a)
bgy =
  rv << gMy < bkB


-- | Greedily consumes as many characters as possible matching a predicate and returns the number of characters consumed.
lGk ::
  ( Integral i
  )
    => Predicate a -> Parser (List a) (List a) i
lGk =
  l << gy


-- | Greedily consumes at least one character matching a predicate.
gy' ::
  (
  )
    => Predicate a -> Parser (List a) (List a) (List a)
gy' =
  gSo < kB


-- | Greedily consomes at least one character matching a predicate off the back of a list.
--
-- Significantly slower than consuming characters off the front of a list with 'gy''.
--
-- ===== __Examples__
--
-- >>> uP (bgy (eq '!')) "Hello, world!!!!"
-- [("Hello, world","!!!!")]
--
-- >>> uP (bgY α) "Hello, world"
-- [("Hello, ","world")]
--
bgY ::
  (
  )
    => Predicate a -> Parser (List a) (List a) (List a)
bgY =
  rv << gSo < bkB


{-# Deprecated char "Use χ instead" #-}
-- | Long version of 'χ'.
-- Parses a char accepting it if it is equal to the given char.
char ::
  ( Eq a
  )
    => a -> Parser (List a) (List a) a
char =
  χ


-- | Parses a single character accepting it if it is equal to a particular character.
--
-- When parsing a particular character it is often shorter altogether to use 'ʃ'.
-- For example @ʃ\"*\"@ is shorter than @χ \'*\'@ (the space is necessary to split the tokens).
--
-- Warning depending on your font this may look like @x@.
-- It is not, it is the greek letter chi.
χ ::
  ( Eq a
  )
    => a -> Parser (List a) (List a) a
χ char =
  kB (Prelude.== char)


-- | Parses at least one of the given character.
x_ ::
  ( Eq a
  )
    => a -> Parser (List a) (List a) (List a)
x_ =
  so < χ


-- | Parses any number of the given character.
x' ::
  ( Eq a
  )
    => a -> Parser (List a) (List a) (List a)
x' =
  my < χ


-- | Parses a single character accepting it if it is not equal to a particular character.
--
-- Warning depending on your font this may look like @nx@.
-- It is not. The @x@ is the greek letter chi.
nχ ::
  ( Eq a
  )
    => a -> Parser (List a) (List a) a
nχ =
  kB < (/=)


-- | Parses a single element off the back of a list.
--
-- In general this is slower than 'χ' since it must traverse to the end of the list.
--
-- ===== __Examples__
--
-- >>> uP (bx '!') "Hello, world!"
-- [("Hello, world",'!')]
--
bx ::
  ( Eq a
  )
    => a -> Parser (List a) (List a) a
bx =
  bkB < eq


-- | Parse a single character giving a particular result if it accepts.
--
-- ==== __Examples__
--
-- Simple example.
--
-- >>> uP (xx 's' "z") "stop"
-- [("top","z")]
--
-- Replace @s@ with @z@
--
-- >>> uP (gSo $ xx 's' 'z' #| hd) "passenger"
-- [("","pazzenger")]
--
xx ::
  ( Eq a
  )
    => a -> b -> Parser (List a) (List a) b
xx =
  fpM < χ


-- | Parse a single character off the back of a list giving a particular result if it accepts.
bXx ::
  ( Eq a
  )
    => a -> b -> Parser (List a) (List a) b
bXx =
  fpM < bx


-- | Uncons and apply 'xx' to create a parser.
--
-- If the input is empty this returns 'en'.
--
-- ==== __Examples__
--
-- Simple example.
--
-- >>> uP (xxh "sz") "stop"
-- [("top","z")]
--
-- Replace @s@ with @z@
--
-- >>> uP (gSO $ xxh "sz" #| hdS) "passenger"
-- [("","pazzenger")]
--
xxh ::
  ( Eq a
  )
    => List a -> Parser (List a) (List a) (List a)
xxh [] =
  [] <$ en
xxh (x : xs) =
  xx x xs


-- | Zips with 'xx'.
zWx ::
  ( Eq a
  )
    => List a -> List b -> List (Parser (List a) (List a) b)
zWx =
  zW xx


-- | Parses a single digit character.
dg ::
  (
  )
    => Parser String String Char
dg =
  kB iD


-- | Parses a single alphabetic character.
af ::
  (
  )
    => Parser String String Char
af =
  kB α


-- | Parses a single whitespace character.
p_ ::
  (
  )
    => Parser String String Char
p_ =
  kB iW


-- | Parses a single control character.
pc ::
  (
  )
    => Parser String String Char
pc =
  kB iC


-- | Parses a single lowercase character.
pL ::
  (
  )
    => Parser String String Char
pL =
  kB iL


-- | Parses a single uppercase character.
pU ::
  (
  )
    => Parser String String Char
pU =
  kB iU


-- | Parses a single alpha-numeric character.
aN ::
  (
  )
    => Parser String String Char
aN =
  kB iA


-- | Parses a single printable character.
pR ::
  (
  )
    => Parser String String Char
pR =
  kB iP


-- | Parses a complete complete word.
-- That is it parses all alphabetic characters off the front of a string.
-- If there are none it fails.
wd ::
  (
  )
    => Parser String String String
wd =
  gSo af


-- | Parses a string into a list of whitespace separated chunks.
wr ::
  (
  )
    => Parser String String (List String)
wr =
  gMy $ gSo (nK iW) <* gMy p_


{-# Deprecated string "Use ʃ instead" #-}
-- | Long version of 'ʃ'.
-- Parses a string accepting it if it is equal to the given string
string ::
  ( Eq a
  )
    => List a -> Parser (List a) (List a) (List a)
string =
  ʃ


-- | Parses a string accepting it if it is equal to the given string.
ʃ ::
  ( Eq a
  )
    => List a -> Parser (List a) (List a) (List a)
ʃ =
  Prelude.traverse χ


-- | Parses at least one of the given string.
s_ ::
  ( Eq a
  )
    => List a -> Parser (List a) (List a) (List a)
s_ =
  sO < ʃ


-- | Parses any number of the given string.
s' ::
  ( Eq a
  )
    => List a -> Parser (List a) (List a) (List a)
s' =
  mY < ʃ


-- | Parses a string off the back of a list.
--
-- In general this is much less efficient than 'ʃ'.
bH ::
  ( Eq a
  )
    => List a -> Parser (List a) (List a) (List a)
bH =
  Prelude.traverse bx < Rv


-- | Parses a string giving a particular result if it accepts.
--
-- ==== __Examples__
--
-- Simple example.
--
-- >>> uP (hh "st" "z") "stop"
-- [("top","z")]
--
-- Replace @s@ with @z@
--
-- >>> uP (gSo $ hh "ss" 'z' ++ hd) "passenger"
-- [("","pazenger")]
--
hh ::
  ( Eq a
  )
    => List a -> b -> Parser (List a) (List a) b
hh =
  fpM < ʃ


-- | Parses a string off the back of a list giving a particular result if it accepts.
--
-- In general this is much less efficient than 'hh'.
bhh ::
  ( Eq a
  )
    => List a -> b -> Parser (List a) (List a) b
bhh =
  fpM < bH


-- | Zips with 'hh'.
zWh ::
  ( Eq a
  )
    => List (List a) -> List b -> List (Parser (List a) (List a) b)
zWh =
  zW hh


-- | A parser which takes a list of options and matches any of them.
--
-- ==== __Examples__
--
-- A parser which parses any one vowel
--
-- >>> f=xay"aeiou"
-- >>> gP f "agf"
-- ['a']
-- >>> gP f "xs"
-- []
--
xay ::
  ( Eq a
  , Foldable f
  )
    => f a -> Parser (List a) (List a) a
xay =
  kB < fe


-- | A parser which takes a list of characters and matches any of them as a string.
--
-- Like 'xay' but @xay@ gives a character and @xys@ gives a string.
--
-- ==== __Examples__
--
-- A parser which parses any one vowel
--
-- >>> f=xys"aeiou"
-- >>> gP f "agf"
-- ["a"]
-- >>> gP f "xs"
-- []
--
xys ::
  ( Eq a
  , Foldable f
  )
    => f a -> Parser (List a) (List a) (List a)
xys =
  p << xay


-- | A parser which takes a list of options and matches one character not in the options.
--
-- ==== __Examples__
--
-- A parser which parses anything other than a vowel
--
-- >>> f=nxy"aeiou"
-- >>> gP f "agf"
-- []
-- >>> gP f "xs"
-- ['x']
--
nxy ::
  ( Eq a
  , Foldable f
  )
    => f a -> Parser (List a) (List a) a
nxy =
  kB < fE


-- | A parser which takes a list of strings and matches any one of them with 'ʃ'.
--
-- ==== __Examples__
--
-- >>> gP (asy ["axy","a","b"]) "axyl"
-- ["axy","a"]
asy ::
  ( Eq a
  , Foldable f
  , Functor f
  )
    => f (List a) -> Parser (List a) (List a) (List a)
asy =
  cM ʃ


-- | Parse a readable.
--
-- Warning depending on your font this may look like @v@.
-- It is not.
-- It is the greek letter nu.
ν ::
  ( Read a
  )
    => Parser String String a
ν =
  νa h'


-- | Parse a readable with a given witness type.
-- The witness is unused, but the output will have the same type.
--
-- Warning depending on your font this may look like @vw@.
-- It is not.
-- The first letter, @ν@, is the greek letter nu.
--
-- ==== __Examples__
--
-- >>> uP (νw ()) "()HELLO"
-- [("HELLO",())]
-- >>> uP (νw ()) "(((())))HELLO"
-- [("HELLO",())]
-- >>> uP (νw 1) "23HELLO"
-- [("HELLO",23)]
-- >>> uP (νw T) "False HELLO"
-- [(" HELLO",False)]
-- >>> uP (νw T) "True"
-- [("",True)]
-- >>> uP (νw B) "TrueHELLO"
-- []
-- >>> uP (νw T) "TrueHELLO"
-- []
νw ::
  ( Read a
  )
    => a -> Parser String String a
νw _ =
  ν


-- | Takes a parser and creates a new parser which reads the results.
-- If the read fails the parse is ignored.
--
-- Warning depending on your font this may look like @va@.
-- It is not.
-- The first letter, @ν@, is the greek letter nu.
νa ::
  ( Read b
  )
    => Parser a a String -> Parser a a b
νa parser = do
  x <- parser
  case
      Text.Read.readMaybe x
    of
      Nothing ->
        em
      Just value ->
        p value


-- | Parses a single digit and returns it as a number.
--
-- Warning depending on your font this may look like @vd@.
-- It is not. @ν@ is the greek letter nu.
νd ::
  ( Integral i
  )
    => Parser String String i
νd =
  Prelude.fromInteger < νa (p < dg)


-- | Parse a hexadecimal integer case insensitive.
-- This parser is greedy.
--
-- ==== __Examples__
--
-- Will always give at least one parse, if it consumes nothing it will result in zero.
--
-- >>> gP p16 ""
-- [0]
-- >>> gP p16 "11"
-- [17]
-- >>> gP p16 "11m"
-- [17]
-- >>> gP p16 "1bAm"
-- [442]
--
p16 ::
  (
  )
    => Parser String String Prelude.Int
p16 =
  go 0
  where
    go n =
      ( (16*n+) < digit >~ go )
      #| p n
    digit =
      (Prelude.read < p < xay "0123456789")
      #| (xay "Aa" *> p 10)
      #| (xay "Bb" *> p 11)
      #| (xay "Cc" *> p 12)
      #| (xay "Dd" *> p 13)
      #| (xay "Ee" *> p 14)
      #| (xay "Ff" *> p 15)


-- | Runs a parser on the back of a list.
--
-- ==== __Examples__
--
-- Here's it being used to create a parser which matches palindromes:
--
-- >>> f=lA(gMy hd)>~bkw<ʃ
-- >>> x1 f "abeba"
-- True
-- >>> x1 f "abba"
-- True
-- >>> x1 f "abeja"
-- False
--
-- @gMy hd@ will parse the entire input.
-- @lA@ makes it a lookahead.
-- We use @bkw<ʃ@ to parse the same thing off the end of the list.
bkw ::
  ( Reversable r
  , Monoid (r a)
  )
    => Parser (r a) (r a) b -> Parser (r a) (r a) b
bkw (P func) =
  P $ m (mst Rv) < func < Rv


-- | A parser which matches when the input has been completely consumed.
en ::
  ( Eq a
  , Monoid a
  )
    => Parser a a ()
en =
  P go
  where
    go x
      | x == i
      =
        [(i, ())]
    go _ =
      []


-- | A parser which matches when the input has been completely consumed.
-- Gives the empty list as a result.
ens ::
  ( Eq a
  , Monoid a
  )
    => Parser a a (List b)
ens =
  p [] < en


-- | Takes a value and creates a "parser" which unwrites that prepends that value to the working string.
-- The resulting parser always returns @()@.
-- For a version which returns the object given see 'bSh'.
--
-- You can think of this as doing the opposite of what 'ʃ' does.
--
-- Because @bʃ@ violates monotonicity there can be some real performance issues with solutions that use it.
--
-- ==== __Examples__
--
-- >>> uP (bʃ"Hello ") "world!"
-- [("Hello world!",())]
bʃ ::
  ( Semigroup m
  )
    => m -> Parser m m ()
bʃ xs =
  P
    { uP =
      ( \ unprocessed ->
        [(xs <> unprocessed, ())]
      )
    }


-- | Takes a value and creates a "parser" which unwrites that prepends that value to the working string.
-- The resulting parser returns the object given.
-- For a version which always returns @()@ see @bʃ@.
--
-- You can think of this as doing the opposite of what 'ʃ' does.
--
-- Because @bSh@ violates monotonicity there can be some real performance issues with solutions that use it.
--
-- ==== __Examples__
--
-- >>> uP (bSh "Hello ") "world!"
-- [("Hello world!","Hello ")]
bSh ::
  ( Monoid m
  )
    => m -> Parser m m m
bSh =
  (i <$) < bʃ


-- | Intersperses two parsers.
--
-- ==== __Examples__
--
-- >>> f=isP wd(ʃ",")
-- >>> gc f "hello,world,hi,word"
-- [["hello","world","hi","word"]]
--
isP ::
  (
  )
    => Parser a a c -> Parser a a d -> Parser a a (List c)
    -- => Parser a b c -> Parser b a d -> Parser a b (List c)
isP parser1 parser2 =
  p [] ++ l2 (:) parser1 (my (parser2 #*> parser1))


-- | Flip of 'isP'.
fiP ::
  (
  )
    => Parser a a d -> Parser a a c -> Parser a a (List c)
    -- => Parser b a d -> Parser a b c -> Parser a b (List c)
fiP =
  F isP


-- | Infix of 'isP'.
($|$) ::
  (
  )
    => Parser a a c -> Parser a a d -> Parser a a (List c)
    -- => Parser a b c -> Parser b a d -> Parser a b (List c)
($|$) =
  isP


{-# Deprecated ivP "Use Rv instead" #-}
-- | Inverts the priority of parses.
--
-- ==== __Examples__
--
-- Here we have a two parsers each which consume two characters.
-- @h_@ will parse any number of characters giving with more characters having higher priority.
--
-- >>> gP (h_) "ab"
-- ["ab","a"]
--
-- @ivP@ takes this parser and switches the priority so fewer characters have higher priority.
--
-- >>> gP (ivP $ h_) "ab"
-- ["a","ab"]
--
ivP ::
  (
  )
    => Parser a b c -> Parser a b c
ivP parser =
  P
    { uP =
      rv < uP parser
    }


-- | Removes all parses other than the one with the highest priority.
--
-- Can be used to retroactively make a parser greedy.
gre ::
  (
  )
    => Parser a b c -> Parser a b c
gre parser =
  P
    { uP =
      Prelude.take 1 < uP parser
    }


-- | Removes all parses ohter than the one with the lowest priority.
--
-- Can be used to retroactively make a parser lazy.
laz ::
  (
  )
    => Parser a b c -> Parser a b c
laz =
  gre < ivP


-- | Takes a parser @p@ and creates a parser that matches a sequence ending with a valid @p@ parse.
-- The result of @p@ is ignored and only the preceding string is returned.
--
-- For a version which returns only the result of @p@ see 'peW'.
--
-- ==== __Examples__
--
-- Parse for any string up to a @y@
--
-- >>> gP (pew $ χ 'y') "pollywoggy"
-- ["pollywogg","poll"]
pew ::
  (
  )
    => Parser (List a) (List a) b -> Parser (List a) (List a) (List a)
pew =
  aK h'


-- | Takes a parser @p@ and creates a parser that matches a sequence ending with a valid @p@ parse.
-- The result is the result of the @p@ parse and discards the leading sequence.
--
-- For a version which returns only the prefix see 'pew'.
--
-- ==== __Examples__
--
-- Parse any capital letter.
--
-- >>> gP (peW $ pUS) "Sam the Dog"
-- ["D","S"]
peW ::
  (
  )
    => Parser (List a) (List a) b -> Parser (List a) (List a) b
peW =
  aT h'


-- | Takes a parser @p@ and creates a parser that matches a sequence ending with a valid @p@ parse.
-- The result the preceding string combined with the result of the parse of @p@.
pEw ::
  (
  )
    => Parser (List a) (List a) (List a) -> Parser (List a) (List a) (List a)
pEw =
  l2p h'


-- | Sandwiches a parser between two copies of another parser.
-- Returns the result of the middle parser.
--
-- ===== __Examples__
--
-- To parse a word surrounded in quotes you can use 'enk (ʃ"\"") wd'.
--
enk ::
  (
  )
    => Parser a a b -> Parser a a c -> Parser a a c
enk parser1 parser2 =
  parser1 *> parser2 <* parser1


-- | Flip of 'enk'.
fNk ::
  (
  )
    => Parser a a b -> Parser a a c -> Parser a a b
fNk =
  f' enk


-- | Run a parser and then attempt to parse its result.
--
-- More general version of @'fb' 'ʃ'@.
--
-- ===== __Examples__
--
-- Parse a string of the form @a ++ a@:
--
-- >>> p=yS h_
-- >>> gP p "papa"
-- ["pa"]
-- >>> gP p "mapa"
-- []
yS ::
  ( Eq b
  )
    => Parser a (List b) (List b) -> Parser a (List b) (List b)
yS parser =
  P (\ j ->
      uP parser j >~ U (F $ uP < ʃ)
    )


-- | Run a parser which produces an element and then attempt to parse its result.
--
-- More general version of @'fb' 'χ'@.
--
-- Warning depending on your font this may look like @yx@.
-- It is not, the second letter is the greek letter chi.
--
-- ===== __Examples__
--
-- Parse a string of two equal characters:
--
-- >>> p=yχ hd
-- >>> gP p "aa"
-- "a"
-- >>> gP p "xx"
-- "x"
-- >>> gP p "xy"
-- ""
yχ ::
  ( Eq b
  )
    => Parser a (List b) b -> Parser a (List b) b
yχ parser =
  P (\ j ->
      uP parser j >~ U (F $ uP < χ)
    )


-- | Prefix version of '(@<*)'.
yju ::
  (
  )
    => Parser a b c -> Parser c d e -> Parser a b e
yju =
  (@<*)


-- | Flip of 'yju'.
fyj ::
  (
  )
    => Parser a b c -> Parser d e a -> Parser d e c
fyj =
  f' yju


-- | Run one parser on the result of another.
-- Gives the remainder of the first parser.
(@<*) ::
  (
  )
    => Parser a b c -> Parser c d e -> Parser a b e
p1 @<* p2 =
  Sw (Sw p1 #<* Sw p2)


-- | Prefix version of '(@*>)'.
yku ::
  (
  )
    => Parser a b c -> Parser c d e -> Parser a d e
yku =
  (@*>)


-- | Flip of 'yku'.
fyk ::
  (
  )
    => Parser a b c -> Parser d e a -> Parser d b c
fyk =
  f' yku


-- | Run one parser on the result of another.
-- Gives the remainder of the second parser.
(@*>) ::
  (
  )
    => Parser a b c -> Parser c d e -> Parser a d e
p1 @*> p2 =
  Sw (Sw p1 #*> Sw p2)


-- | Prefix version of '(@^*)'.
ymu ::
  ( Semigroup b
  )
    => Parser a b c -> Parser c b d -> Parser a b d
ymu =
  (@^*)


-- | Flip of 'ymu'.
fym ::
  ( Semigroup b
  )
    => Parser a b c -> Parser d b a -> Parser d b c
fym =
  f' ymu


-- | Run one parser on the result of another.
-- Combines the remainders with the semigroup action.
--
-- Often the first parser is used to restrict the domain the second parser acts on.
(@^*) ::
  ( Semigroup b
  )
    => Parser a b c -> Parser c b d -> Parser a b d
P p1 @^* P p2 =
  P $ \ i -> do
    (rem1, v1) <- p1 i
    (rem2, v2) <- p2 v1
    p (rem2 <> rem1, v2)
