{-# Language FlexibleContexts #-}
{-|
Module :
  P.Map.Class.Extra
Description :
  Additional functions for the P.Map.Class
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions for parsing that have been moved to this file to reduce clutter in the main P.Map.Class library.
-}
module P.Map.Class.Extra where


import P.Aliases
import P.Map.Class


-- | Shorthand for @'qx' 0@.
qx0 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx0 =
  qx (0 :: Integer)


-- | Shorthand for @'qx' 1@.
qx1 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx1 =
  qx (1 :: Integer)


-- | Shorthand for @'qx' 2@.
qx2 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx2 =
  qx (2 :: Integer)


-- | Shorthand for @'qx' 3@.
qx3 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx3 =
  qx (3 :: Integer)


-- | Shorthand for @'qx' 4@.
qx4 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx4 =
  qx (4 :: Integer)


-- | Shorthand for @'qx' 5@.
qx5 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx5 =
  qx (5 :: Integer)


-- | Shorthand for @'qx' 6@.
qx6 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx6 =
  qx (6 :: Integer)


-- | Shorthand for @'qx' 7@.
qx7 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx7 =
  qx (7 :: Integer)


-- | Shorthand for @'qx' 8@.
qx8 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx8 =
  qx (8 :: Integer)


-- | Shorthand for @'qx' 9@.
qx9 ::
  ( Indexable f Integer
  )
    => f a -> Maybe' a
qx9 =
  qx (9 :: Integer)


-- | Shorthand for @'fqd' 0@.
--
-- More general version of 'Data.Maybe.fromMaybe'.
qd0 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd0 =
  fqd (0 :: Integer)


-- | Shorthand for @'fqd' 1@.
qd1 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd1 =
  fqd (1 :: Integer)


-- | Shorthand for @'fqd' 2@.
qd2 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd2 =
  fqd (2 :: Integer)


-- | Shorthand for @'fqd' 3@.
qd3 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd3 =
  fqd (3 :: Integer)


-- | Shorthand for @'fqd' 4@.
qd4 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd4 =
  fqd (4 :: Integer)


-- | Shorthand for @'fqd' 5@.
qd5 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd5 =
  fqd (5 :: Integer)


-- | Shorthand for @'fqd' 6@.
qd6 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd6 =
  fqd (6 :: Integer)


-- | Shorthand for @'fqd' 7@.
qd7 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd7 =
  fqd (7 :: Integer)


-- | Shorthand for @'fqd' 8@.
qd8 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd8 =
  fqd (8 :: Integer)


-- | Shorthand for @'fqd' 9@.
qd9 ::
  ( Indexable f Integer
  )
    => a -> f a -> a
qd9 =
  fqd (9 :: Integer)


-- | Shorthand for @'mr' 1@.
mr1 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr1 =
  mr (1 :: Integer)


-- | Short hand for @'nmr' 1@.
nm1 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm1 =
  nmr (1 :: Integer)


-- | Shorthand for @'mr' 2@.
mr2 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr2 =
  mr (2 :: Integer)


-- | Short hand for @'nmr' 2@.
nm2 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm2 =
  nmr (2 :: Integer)


-- | Shorthand for @'mr' 3@.
mr3 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr3 =
  mr (3 :: Integer)


-- | Short hand for @'nmr' 3@.
nm3 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm3 =
  nmr (3 :: Integer)


-- | Shorthand for @'mr' 4@.
mr4 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr4 =
  mr (4 :: Integer)


-- | Short hand for @'nmr' 4@.
nm4 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm4 =
  nmr (4 :: Integer)


-- | Shorthand for @'mr' 5@.
mr5 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr5 =
  mr (5 :: Integer)


-- | Short hand for @'nmr' 5@.
nm5 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm5 =
  nmr (5 :: Integer)


-- | Shorthand for @'mr' 6@.
mr6 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr6 =
  mr (6 :: Integer)


-- | Short hand for @'nmr' 6@.
nm6 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm6 =
  nmr (6 :: Integer)


-- | Shorthand for @'mr' 7@.
mr7 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr7 =
  mr (7 :: Integer)


-- | Short hand for @'nmr' 7@.
nm7 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm7 =
  nmr (7 :: Integer)


-- | Shorthand for @'mr' 8@.
mr8 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr8 =
  mr (8 :: Integer)


-- | Short hand for @'nmr' 8@.
nm8 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm8 =
  nmr (8 :: Integer)


-- | Shorthand for @'mr' 9@.
mr9 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr9 =
  mr (9 :: Integer)


-- | Short hand for @'nmr' 9@.
nm9 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm9 =
  nmr (9 :: Integer)


-- | Shorthand for @'mr' 10@.
--
-- Use 'P.Foldable.Length.nø' to determine if a list is not empty.
mr0 ::
  ( Indexable f Integer
  )
    => f a -> Bool
mr0 =
  mr (10 :: Integer)


-- | Short hand for @'nmr' 10@.
--
-- Use 'P.Foldable.Length.ø' to determine if a list is empty.
nm0 ::
  ( Indexable f Integer
  )
    => f a -> Bool
nm0 =
  nmr (10 :: Integer)


-- | Insert a value at index @0@.
-- Shorthand for @ns 0@.
ns0 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns0 =
  ns 0


-- | Insert a value at index @1@.
-- Shorthand for @ns 1@.
ns1 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns1 =
  ns 1


-- | Insert a value at index @2@.
-- Shorthand for @ns 2@.
ns2 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns2 =
  ns 2


-- | Insert a value at index @3@.
-- Shorthand for @ns 3@.
ns3 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns3 =
  ns 3


-- | Insert a value at index @4@.
-- Shorthand for @ns 4@.
ns4 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns4 =
  ns 4


-- | Insert a value at index @5@.
-- Shorthand for @ns 5@.
ns5 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns5 =
  ns 5


-- | Insert a value at index @6@.
-- Shorthand for @ns 6@.
ns6 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns6 =
  ns 6


-- | Insert a value at index @7@.
-- Shorthand for @ns 7@.
ns7 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns7 =
  ns 7


-- | Insert a value at index @8@.
-- Shorthand for @ns 8@.
ns8 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns8 =
  ns 8


-- | Insert a value at index @9@.
-- Shorthand for @ns 9@.
ns9 ::
  ( Indexable f Integer
  )
    => a -> f a -> f a
ns9 =
  ns 9


