module P.Alternative
  ( Alternative
  , em
  , (++)
  , ao
  , fao
  , gu
  , cx
  , cM
  , (>/)
  , fcM
  , cMp
  , cs
  , (<|)
  , fcs
  , ec
  , (|>)
  , fec
  , rl
  , (#*)
  , frl
  , rm
  , rM
  , (*%)
  -- * Deprecated
  , asum
  , foldMapA
  , foldA
  )
  where


import qualified Prelude
import Prelude
  ( Bool
  , Integral
  )


import qualified Control.Applicative
import Control.Applicative
  ( Alternative
  )
import qualified Control.Monad
import qualified Data.Monoid


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Aliases
import P.Arithmetic
import P.Arithmetic.Nat
import P.Applicative
import P.Category
import P.Enum
import P.Foldable
import P.Foldable.Length
import P.Function.Compose
import P.Function.Flip
import P.Functor
import P.Ord
import P.Traversable


infixr 5 ++
infixl 1 >/


-- | 'Alternative' is a monoid on applicative functors.
-- 'em' is the identity of that monoid.
--
-- Equivalent to 'Control.Applicative.empty'.
em ::
  ( Alternative t
  )
    => t a
em =
  Control.Applicative.empty


-- | 'Alternative' is a monoid on applicative functors.
-- '(++)' is the associative action of that monoid.
--
-- Equivalent to '(Control.Applicative.<|>)'.
--
-- More general version of '(Prelude.++)'.
(++) ::
  ( Alternative t
  )
    => t a -> t a -> t a
(++) =
  (Control.Applicative.<|>)


-- | 'Alternative' is a monoid on applicative functors.
-- 'ao' is the associative action of that monoid.
--
-- Equivalent to '(Control.Applicative.<|>)'.
--
-- More general version of '(Prelude.++)'.
ao ::
  ( Alternative t
  )
    => t a -> t a -> t a
ao =
  (++)


-- | Flip of 'ao'.
fao ::
  ( Alternative t
  )
    => t a -> t a -> t a
fao =
  F ao


-- | Takes a boolean and is 'em' when false and @p ()@ when true.
--
-- Equivalent to 'Control.Monad.guard'.
gu ::
  ( Alternative t
  )
    => Bool -> t ()
gu =
  Control.Monad.guard


{-# Deprecated asum "Use cx instead" #-}
-- | Long version of 'cx'.
asum ::
  ( Alternative m
  , Foldable t
  )
    => t (m a) -> m a
asum =
  cx


-- | Takes multiple alternatives and combines them with `(++)`.
--
-- More general version of 'Prelude.concat'.
--
-- ==== __Examples__
--
-- Can be used to concat lists:
--
-- >>> cx [[1,2,3],[2,3,3],[2,2]]
-- [1,2,3,2,3,3,2,2]
--
cx ::
  ( Alternative m
  , Foldable t
  )
    => t (m a) -> m a
cx =
  rF (++) em


{-# Deprecated foldMapA "Use cM instead" #-}
-- | Long version of 'cM'.
foldMapA ::
  ( Alternative m
  , Foldable t
  , Functor t
  )
    => (a -> m b) -> t a -> m b
foldMapA =
  cM


-- | A concatmap.
-- Maps and combines with 'cx'.
--
-- ==== __Examples__
--
-- On lists this works as a regular concatmap.
--
-- >>> cM ( \ x -> [1,x]) [1, 4, 5, 9]
-- [1,1,1,4,1,5,1,9]
--
-- Also works on parsers.
--
-- >>> uP (cM ʃ ["red", "blue"]) "red ball"
-- [(" ball","red")]
cM ::
  ( Alternative m
  , Foldable t
  )
    => (a -> m b) -> t a -> m b
cM =
  Data.Monoid.getAlt << mF < m Data.Monoid.Alt


-- | Infix of 'cM'.
(>/) ::
  ( Alternative m
  , Foldable t
  )
    => (a -> m b) -> t a -> m b
(>/) =
  cM


-- | Flip of 'cM'.
fcM ::
  ( Alternative m
  , Foldable t
  )
    => t a -> (a -> m b) -> m b
fcM =
  f' cM


{-# Deprecated foldA "Use cMp instead" #-}
-- | Long version of 'cMp'.
foldA ::
  ( Alternative f
  , Foldable t
  )
    => t a -> f a
foldA =
  cMp

-- | Branch over a foldable collection of values.
cMp ::
  ( Alternative f
  , Foldable t
  )
    => t a -> f a
cMp =
  cM p


-- | Adds an element to the front of a structure; usually a list.
--
-- Generalization of @cons@.
cs ::
  ( Alternative f
  )
    => a -> f a -> f a
cs =
  (++) < p


-- | Infix of 'cs'.
--
-- For 'List's use @(:)@ instead.
(<|) ::
  ( Alternative f
  )
    => a -> f a -> f a
(<|) =
  cs


-- | Flip of 'cs'.
fcs ::
  ( Alternative f
  )
    => f a -> a -> f a
fcs =
  F cs


-- | Adds an element to the end of a structure; usually a list.
--
-- Generalization of @snoc@.
ec ::
  ( Alternative f
  )
    => f a -> a -> f a
ec =
  fm p < (++)


-- | Infix of 'ec'.
(|>) ::
  ( Alternative f
  )
    => f a -> a -> f a
(|>) =
  ec


-- | Flip of 'ec'
fec ::
  ( Alternative f
  )
    => a -> f a -> f a
fec =
  F ec


-- | Create a structure containing a specific number of elements; usually a list.
--
-- More general version of 'Prelude.replicate'.
-- More general version of 'Data.List.genericReplicate'.
rl ::
  ( Integral i
  , Ord i
  , Alternative f
  )
    => i -> a -> f a
rl count _
  | count <= 0
  =
    em
rl count x =
  p x ++ rl (Pv count) x


-- | Infix version of 'rl'.
(#*) ::
  ( Integral i
  , Ord i
  , Alternative f
  )
    => i -> a -> f a
(#*) =
  rl


-- | Flip of 'rl'.
frl ::
  ( Integral i
  , Ord i
  , Alternative f
  )
    => a -> i -> f a
frl =
  F rl


-- | Replicates an applicative a specific number of times.
--
-- Flip of 'rM'.
--
-- ==== __Examples__
--
-- ===== Parsers
--
-- Use with a 'P.Parse.Parser' to repeat that parser a specific number of times.
--
-- Here 'P.Parse.hd' parses exactly one character, so @rm 5 hd@ parses exactly 5 characters.
--
-- >>> rm 5 hd -% "Hello!"
-- ["Hello"]
--
-- Here 'P.Parse.h_' parses at least one character so @rm 3 h_@ parses non empty three way partitions.
--
-- >>> rm 3 h_ -^ "12345"
-- [["123","4","5"],["12","34","5"],["12","3","45"],["1","234","5"],["1","23","45"],["1","2","345"]]
--
-- ===== Lists
--
-- On a list @rm count list@ gives all ways to pick @count@ items from @list@.
--
-- So if @list = "01"@ then this gives all binary strings of length @count@.
--
-- >>> rm 3 "01"
-- ["000","001","010","011","100","101","110","111"]
--
rm ::
  ( Integral i
  , Ord i
  , Traversable t
  , Alternative t
  , Applicative f
  )
    => i -> f a -> f (t a)
rm =
  sQ << rl


-- | Flip of rm
rM ::
  ( Integral i
  , Ord i
  , Traversable t
  , Alternative t
  , Applicative f
  )
    => f a -> i -> f (t a)
rM =
  F rm


-- | Infix of 'rm' and 'rM'.
(*%) ::
  ( Integral i
  , Ord i
  , Traversable t
  , Alternative t
  , Applicative f
  )
    => i -> f a -> f (t a)
(*%) =
  rm
