{-# Language ViewPatterns #-}
{-# Language PatternSynonyms #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
module P.Applicative.Unpure
  ( Unpure (..)
  , pattern Pu
  , pattern Le
  )
  where


import qualified Prelude


import P.Algebra.Monoid
import P.Aliases
import P.Applicative
import P.Bifunctor.Flip
import P.Eq
import P.Functor
import P.Monad.Free


-- | Structures on which "pure" can be pattern matched.
--
-- ==== Laws
--
-- 'unp' should undo a pure.
--
-- prop> unp < pure' = p
--
-- When @f@ is an applicative 'pure'' should be its pure:
--
-- prop> pure' = p
--
class Unpure f where
  unp :: f a -> Maybe' a
  pure' :: a -> f a


{-# Deprecated pure' "Use Pu or p instead" #-}


instance Unpure [] where
  unp [ x ] =
    [ x ]
  unp _ =
    []
  pure' =
    p


instance Unpure (Free f) where
  unp (Pur x) =
    [ x ]
  unp _ =
    []
  pure' =
    Pur


instance Unpure ((->) ()) where
  unp f =
    [ f () ]
  pure' =
    p


-- instance
--   ( Eq a
--   , Monoid a
--   )
--     => Unpure ((,) a)
--   where
--     unp (x, y)
--       | x == i
--       =
--         [ y ]
--     unp _ =
--       []
--     pure' =
--       p
-- 
-- instance
--   ( Eq a
--   , Monoid a
--   , Eq b
--   , Monoid b
--   )
--     => Unpure ((,,) a b)
--   where
--     unp (x, y, z)
--       | x == i
--       , y == i
--       =
--         [ z ]
--     unp _ =
--       []
--     pure' =
--       p
--
-- instance
--   ( Eq a
--   , Monoid a
--   , Eq b
--   , Monoid b
--   , Eq c
--   , Monoid c
--   )
--     => Unpure ((,,,) a b c)
--   where
--     unp (w, x, y, z)
--       | x == i
--       , x == i
--       , y == i
--       =
--         [ z ]
--     unp _ =
--       []
--     pure' =
--       p


-- | A pattern for pure.
--
-- For the function use the shorter 'p'.
pattern Pu ::
  ( Unpure f
  )
    => a -> f a
pattern Pu x <- (unp -> [x]) where
  Pu =
    pure'


-- | A pattern for left pure.
pattern Le ::
  ( Unpure (Flip f a)
  )
    => b -> f b a
pattern Le x =
  UFl (Pu x)
