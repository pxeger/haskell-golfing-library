{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
module P.Function.Curry
  ( pattern Cu
  , fCu
  , pattern Cuf
  , fCf
  , pattern U
  , fUc
  , pattern Ucf
  , fUf
  , pattern Cu3
  , fC3
  , pattern U3
  , fU3
  , pattern Cu4
  , fC4
  , pattern U4
  , fU4
  )
  where


import qualified Prelude
import Prelude
  (
  )


import P.Function.Flip


{-# Complete Cu #-}
-- |
-- Equivalent to 'Prelude.curry'.
pattern Cu ::
  (
  )
    => ((a, b) -> c) -> (a -> b -> c)
pattern Cu x <- (Prelude.uncurry -> x) where
  Cu =
    Prelude.curry


-- | Flip of 'Cu'.
fCu ::
  (
  )
    => a -> ((a, b) -> c) -> b -> c
fCu =
  f' Cu


{-# Complete Cuf #-}
-- | Curries with 'Cu' then flips result.
pattern Cuf ::
  (
  )
    => ((a, b) -> c) -> (b -> a -> c)
pattern Cuf x =
  F (Cu x)


-- | Flip of 'Cuf'.
fCf ::
  (
  )
    => b -> ((a, b) -> c) -> a -> c
fCf =
  f' Cuf


{-# Complete U #-}
-- |
-- Equivalent to 'Prelude.uncurry'.
pattern U ::
  (
  )
    => (a -> b -> c) -> (a, b) -> c
pattern U x <- (Prelude.curry -> x) where
  U =
    Prelude.uncurry


-- | Flip of 'U'.
fUc ::
  (
  )
    => (a, b) -> (a -> b -> c) -> c
fUc =
  f' U


{-# Complete Ucf #-}
-- | Flips the input and uncurries the result with 'U'.
pattern Ucf ::
  (
  )
    => (a -> b -> c) -> ((b, a) -> c)
pattern Ucf x =
  U (F x)


-- | Flip of 'Ucf'.
fUf ::
  (
  )
    => (b, a) -> (a -> b -> c) -> c
fUf =
  f' Ucf


-- | Internal function.
_cu3 ::
  (
  )
    => ((a, b, c) -> d) -> (a -> b -> c -> d)
_cu3 func x1 x2 x3 =
  func (x1, x2, x3)


-- | Internal function
_uc3 ::
  (
  )
    => (a -> b -> c -> d) -> ((a, b, c) -> d)
_uc3 func (x1, x2, x3) =
  func x1 x2 x3


-- | Curry a 3 tuple
{-# Complete Cu3 #-}
pattern Cu3 ::
  (
  )
    => ((a, b, c) -> d) -> (a -> b -> c -> d)
pattern Cu3 x <- (_uc3 -> x) where
  Cu3 =
    _cu3


-- | Flip of 'Cu3'.
fC3 ::
  (
  )
    => a -> ((a, b, c) -> d) -> b -> c -> d
fC3 =
  f' Cu3


-- | Uncurry a function which takes 3 arguments to function that takes a 3 tuple.
{-# Complete U3 #-}
pattern U3 ::
  (
  )
    => (a -> b -> c -> d) -> ((a, b, c) -> d)
pattern U3 x <- (_cu3 -> x) where
  U3 =
    _uc3


-- | Flip of 'U3'.
fU3 ::
  (
  )
    => (a, b, c) -> (a -> b -> c -> d) -> d
fU3 =
  f' U3


-- | Internal function.
_cu4 ::
  (
  )
    => ((a, b, c, d) -> e) -> (a -> b -> c -> d -> e)
_cu4 func x1 x2 x3 x4 =
  func (x1, x2, x3, x4)


-- | Internal function
_uc4 ::
  (
  )
    => (a -> b -> c -> d -> e) -> ((a, b, c, d) -> e)
_uc4 func (x1, x2, x3, x4) =
  func x1 x2 x3 x4


-- | Curry a 4 tuple
{-# Complete Cu4 #-}
pattern Cu4 ::
  (
  )
    => ((a, b, c, d) -> e) -> (a -> b -> c -> d -> e)
pattern Cu4 x <- (_uc4 -> x) where
  Cu4 =
    _cu4


-- | Flip of 'Cu4'.
fC4 ::
  (
  )
    => a -> ((a, b, c, d) -> e) -> b -> c -> d -> e
fC4 =
  f' Cu4


-- | Uncurry a function which takes 4 arguments to function that takes a 4 tuple.
{-# Complete U4 #-}
pattern U4 ::
  (
  )
    => (a -> b -> c -> d -> e) -> ((a, b, c, d) -> e)
pattern U4 x <- (_cu4 -> x) where
  U4 =
    _uc4


-- | Flip of 'U4'.
fU4 ::
  (
  )
    => (a, b, c, d) -> (a -> b -> c -> d -> e) -> e
fU4 =
  f' U4
