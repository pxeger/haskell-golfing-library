{-# Language FlexibleInstances #-}
{-|
Module :
  P.Category.Endo
Description :
  Endomorphisms
Copyright :
  (c) E. Olive, 2024
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Category.Endo
  ( Endo (..)
  )
  where


import qualified Prelude
import Prelude
  ( Num (..)
  )


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Algebra.Semigroup.Commutative
import P.Algebra.Group
import P.Aliases
import P.Category
import P.Ord


-- | Endomorphisms in a category.
--
-- Unwrap using 'P.Algebra.Monoid.Action.aq'.
newtype Endo p a =
  E (p a a)


instance
  ( Semigroupoid p
  )
    => Semigroup (Endo p a)
  where
    (E f) <> (E g) =
      E (f <@ g)


instance
  ( Category p
  )
    => Monoid (Endo p a)
  where
    mempty =
      E id


-- | Assumes that @Endo@s are true endomorphisms on that group.
instance
  ( Group a
  , Commutative a
  )
    => Num (Endo (->) a)
  where
    E f + E g =
      E (f <> g)

    E f - E g =
      E (f ~~ g)

    E f * E g =
      E (f <@ g)

    negate (E f) =
      E (IV f)

    fromInteger 0 =
      E (\ x -> i)
    fromInteger n
      | 0 > n
      =
        negate $ fromInteger $ negate n
      | (m, n) <- Prelude.divMod n 2
      , s <- fromInteger m
      =
        s + s + E (\ x -> x)
