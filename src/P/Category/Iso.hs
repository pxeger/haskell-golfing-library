{-# Language TypeOperators #-}
{-# Language FlexibleInstances #-}
module P.Category.Iso
  ( Iso (..)
  , type (<->)
  , swc
  , (<#<)
  , fwc
  , swC
  , sWc
  , (<%<)
  , fWc
  , sWC
  )
  where


import qualified Prelude


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Category
import P.Eq
import P.Function.Compose
import P.Function.Flip


-- | Shorthand for the most common isomorphism type.
type a <-> b =
  Iso (->) a b


data Iso p a b =
  Iso
    {
    -- | The forward morphism
      fwd ::
      p a b
    -- | The backward morphism and inverse of the forward morphism.
    , bwd ::
      p b a
    }


-- | Lifts a morphism from one type to another using an 'Iso'.
--
-- This "sandwiches" the morphism between the two directions of the isomorphism.
swc ::
  ( Semigroupoid p
  )
    => Iso p a b -> p b b -> p a a
swc =
  fwd < swC


-- | Infix version of 'swc'.
(<#<) ::
  ( Semigroupoid p
  )
    => Iso p a b -> p b b -> p a a
(<#<) =
  swc


-- | Flip of 'swc'.
fwc ::
  ( Semigroupoid p
  )
    => p b b -> Iso p a b -> p a a
fwc =
  F swc


-- | 'swc' as an 'Iso'.
swC ::
  ( Semigroupoid p
  )
    => Iso p a b -> (p b b <-> p a a)
swC iso1 =
  Iso
    { fwd =
      \ g -> bwd iso1 <@ g <@ fwd iso1
    , bwd =
      \ g -> fwd iso1 <@ g <@ bwd iso1
    }


-- | Lifts an 'Iso' endomorphism to another type using another 'Iso'.
sWc ::
  ( Semigroupoid p
  )
    => Iso p a b -> Iso p b b -> Iso p a a
sWc iso1 iso2 =
  Iso
    { fwd =
      iso1 <#< fwd iso2
    , bwd =
      iso1 <#< bwd iso2
    }


-- | Infix version of 'sWc'.
(<%<) ::
  ( Semigroupoid p
  )
    => Iso p a b -> Iso p b b -> Iso p a a
(<%<) =
  sWc


-- | Flip of 'sWc'.
fWc ::
  ( Semigroupoid p
  )
    => Iso p b b -> Iso p a b -> Iso p a a
fWc =
  F sWc


-- | 'sWc' as an 'Iso'.
sWC ::
  ( Semigroupoid p
  )
    => Iso p a b -> (Iso p b b <-> Iso p a a)
sWC iso1 =
  Iso
    { fwd =
      sWc iso1
    , bwd =
      sWc $ Iso (bwd iso1) (fwd iso1)
    }


instance
  ( Semigroupoid p
  )
    => Semigroupoid (Iso p)
  where
    Iso f1 b1 <@ Iso f2 b2 =
      Iso (f1 <@ f2) (b2 <@ b1)


instance
  ( Category p
  )
    => Category (Iso p)
  where
    id =
      Iso id id


instance
  ( Eq (p a b)
  , Eq (p b a)
  )
    => Eq (Iso p a b)
  where
    Iso f1 b1 == Iso f2 b2 =
      f1 == f2 Prelude.&& b1 == b2


instance
  ( Semigroupoid p
  )
    => Semigroup (Iso p a a)
  where
    x <> y =
      x <@ y


instance
  ( Category p
  )
    => Monoid (Iso p a a)
  where
    mempty =
      id
