module P.Comonad
  ( Comonad (..)
  )
  where


import qualified Prelude


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Applicative
import P.Category
import P.Functor
import P.Function.Compose


-- | Comonad is the category theoretic dual of a monad.
--
-- === Laws
--
-- ==== __Cokleisli__
--
-- For the comonad in terms of 'cr' and '(=>=)',
-- the laws are simply that it should be a category.
--
-- prop> f =>= cr = f
-- prop> cr =>= f = f
-- prop> f =>= (g =>= h) = (f =>= g) =>= h
--
-- ==== __Duplicate__
--
-- prop> cr < dyp = id
-- prop> m cr < dyp = id
-- prop> dup < dyp = m dyp < dyp
--
class
  ( Functor w
  )
    => Comonad w
  where
    {-# MINIMAL cr, (dyp | xn | (=>=)) #-}
    -- | Extract a value from a comonad.
    --
    -- This is the identity on the Cokleisli category,
    -- and the dual of 'Prelude.pure' / 'P.Monad.p'.
    --
    -- More general version of 'Prelude.snd'.
    cr :: w a -> a

    -- | Duplicate the comonad.
    --
    -- The dual of 'Prelude.join' / 'P.Monad.jn'.
    dyp :: w a -> w (w a)
    dyp =
      xn id

    -- | Dual of '(Prelude.>>=)' / '(P.Monad.>~)'.
    xn :: (w a -> b) -> (w a -> w b)
    xn =
      (=>= id)

    -- | Cokleisli composition.
    --
    -- The dual of '(Control.Monad.>=>)' / '(P.Monad.+>)'
    (=>=) :: (w a -> b) -> (w b -> c) -> (w a -> c)
    x =>= y =
      y < m x < dyp


instance Comonad ((,) i) where
  cr (_, y) =
    y

  dyp (x, y) =
    (x, (x, y))

  xn func (x, y) =
    (x, func (x, y))


instance Comonad ((,,) i j) where
  cr (_, _, z) =
    z

  dyp (x, y, z) =
    (x, y, (x, y, z))

  xn func (x, y, z) =
    (x, y, func (x, y, z))


instance Comonad ((,,,) i j k) where
  cr (_, _, _, z) =
    z

  dyp (w, x, y, z) =
    (w, x, y, (w, x, y, z))

  xn func (w, x, y, z) =
    (w, x, y, func (w, x, y, z))


instance Comonad ((,,,,) h i j k) where
  cr (_, _, _, _, z) =
    z

  dyp (v, w, x, y, z) =
    (v, w, x, y, (v, w, x, y, z))

  xn func (v, w, x, y, z) =
    (v, w, x, y, func (v, w, x, y, z))


instance Comonad ((,,,,,) g h i j k) where
  cr (_, _, _, _, _, z) =
    z

  dyp (u, v, w, x, y, z) =
    (u, v, w, x, y, (u, v, w, x, y, z))

  xn func (u, v, w, x, y, z) =
    (u, v, w, x, y, func (u, v, w, x, y, z))


instance
  ( Monoid m
  )
    => Comonad ((->) m)
  where
    cr =
      ($ i)

    dyp func m1 m2 =
      func (m1 <> m2)
