module P.Category
  ( Semigroupoid (..)
  , og
  , fog
  , Category (..)
  , ($)
  , fid
  , (&)
  )
  where


import Prelude
  ( Functor
  )


import P.Function.Flip


infixr 0 $
infixl 1 &
infixr 1 <@


-- | A semigroupoid is a generalization of a category which is not necessarily unital.
--
-- ===== Associativity
--
-- prop> f <@ (g <@ h) = (f <@ g) <@ h
--
class Semigroupoid p where
  -- | Morphism composition.
  -- For functions use '(P.Function.Compose.<)' as it is shorter.
  --
  -- More general version of '(Prelude..)'.
  (<@) :: p b c -> p a b -> p a c


-- | Prefix version of '(<@)'.
og ::
  ( Semigroupoid p
  )
    => p a b -> p b c -> p a c
og =
  F (<@)


-- | Flip of 'og'
fog ::
  ( Semigroupoid p
  )
    => p b c -> p a b -> p a c
fog =
  (<@)


instance Semigroupoid (->) where
  (f <@ g) x =
    f (g x)


instance Semigroupoid (,) where
  (_, x) <@ (y, _) =
    (y, x)


-- | A category.
--
-- ==== Laws
--
-- In addition to associativity required of 'Semigroupoid's,
-- instances should obey the following laws:
--
-- ===== Left identity
--
-- prop> id << f = f
--
-- ===== Right identity
--
-- prop> f << id = f
--
class Semigroupoid p => Category p where
  -- | Identity morphism.
  -- On functions this is the identity function.
  --
  -- More general version of 'Prelude.id'.
  id :: p a a


instance Category (->) where
  id x =
    x


-- | Identity infix.
--
-- More general version of '(Prelude.$)' with the same precedence.
($) ::
  ( Category p
  )
    => p a a
($) =
  id


-- | Flip of identity.
fid ::
  ( Functor (p (a -> b))
  , Category p
  )
    => a -> p (a -> b) b
fid =
  f' id


-- | Elm's pipe operator and the flip of '($)'.
--
-- ==== __Examples__
--
-- >>> [1 ..] & dr 12 & tk 5
-- [13,14,15,16,17]
(&) ::
  ( Functor (p (a -> b))
  , Category p
  )
    => a -> p (a -> b) b
(&) =
  fid
