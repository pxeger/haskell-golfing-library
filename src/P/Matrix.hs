{-# Language PatternSynonyms #-}
{-# Language ViewPatterns #-}
{-# Language MultiParamTypeClasses #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language ScopedTypeVariables #-}
{-# Language GADTs #-}
{-# Language InstanceSigs #-}
module P.Matrix
  where

import qualified Prelude
import Prelude
  ( Integer
  )

import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Algebra.Ring
import P.Aliases
import P.Applicative
import P.Applicative.Ring
import P.Bifunctor
import P.Category
import P.Comonad
import P.First
import P.Foldable
import P.Foldable.Length
import P.Foldable.MinMax
import P.Functor
import P.Function.Compose
import P.Function.Flip
import P.Functor.Compose
import P.List.Intersperse
import P.Monad
import P.Show
import P.String
import P.Tensor
import P.Traversable
import P.Tuple
import P.Vector

-- | The compositions of vectors forms a matrix.
type Matrix n m =
  Comp (Vec n) (Vec m)

instance Show (Matrix Zr n a) where
  show CEv =
    "[]"

instance
  ( Show a
  , Functor (Vec (Nx m))
  , Foldable (Vec (Nx m))
  , Functor (Vec n)
  , Foldable (Vec n)
  )
    => Show (Matrix (Nx m) n a)
  where
    show (Co matrix) =
      ic "\n" $ m func matrix
      where
        paddingSize :: Integer
        paddingSize =
          mx $ m (l < sh) $ Co matrix
        func :: Vec n a -> String
        func vec =
          '[' : mF ((' ' :) < rP paddingSize < sh) vec <> " ]"

instance
  ( Applicative (Vec n)
  )
    => Tensor (Comp (Vec n) Vec0) Vec0 (Vec n) where
  (#$) ::
    ( Ring r
    )
      => Comp (Vec n) Vec0 r -> Vec0 r -> Vec n r
  Co vecs #$ Ev =
    vecs $> mId
  make ::
    ( Ring r
    )
      => (Vec0 r -> Vec n r) -> Comp (Vec n) Vec0 r
  make _ =
    Co $ p Ev

instance
  ( Foldable (Vec (Nx m))
  , RingApplicative (Vec n)
  , RingApplicative (Vec (Nx m))
  , Tensor (Comp (Vec n) (Vec m)) (Vec m) (Vec n)
  )
    => Tensor (Comp (Vec n) (Vec (Nx m))) (Vec (Nx m)) (Vec n)
  where
    (#$) ::
      ( Ring r
      )
        => Comp (Vec n) (Vec (Nx m)) r -> Vec (Nx m) r -> Vec n r
    Co mVecs #$ mVec' =
      m (mF id < F (ml2 (^*)) mVec') mVecs
    make ::
      ( Ring r
      )
        => (Vec (Nx m) r -> Vec n r) -> Comp (Vec n) (Vec (Nx m)) r
    make func =
      func (mId :+ p i) +:| make (func < (:+) i)

-- | Matches all 0 by n matrices.
--
-- For pattern matching a @_@ will usually suffice but @CEv@ may sometimes be needed to aid the type checker.
pattern CEv ::
  forall n g a.
    (
    )
      =>
    ( n ~ Zr
    )
      => Comp (Vec n) g a
pattern CEv =
  Co Ev

-- | Cons a row to the top of a matrix.
{-# Complete (:+-) #-}
pattern (:+-) ::
  forall n f a.
  forall m.
    ( n ~ Nx m
    )
      => f a -> Comp (Vec m) f a -> Comp (Vec n) f a
pattern x :+- xs =
  Co (x :++ UC xs)

-- | Matches all n by 0 matrices.
--
-- For pattern matching a @_@ will usually suffice but @PEv@ may sometimes be needed to aid the type checker.
pattern PEv ::
  ( Applicative f
  )
    => Comp f Vec0 a
pattern PEv <- _ where
  PEv =
    Co $ p Ev

-- | Internal function.
_colCons ::
  ( Traversable f
  , Firstable g h
  )
    => Comp f g a -> Maybe' (f a, Comp f h a)
_colCons =
  m (jn $ bAp (m st, Co < m cr)) < sQ < m _uC < UC

-- | Cons a col onto the left of a matrix.
--
-- A more specific version of this '(:+|)' can be used as a pattern.
(+:|) ::
  ( Applicative f
  , Firstable g h
  )
    => f a -> Comp f h a -> Comp f g a
(+:|) =
  LC < l2 K

-- | Cons a col onto the left of a matrix.
--
-- A more general version of this '(+:|)' exists but can only be used as a function.
{-# Complete (:+|) #-}
pattern (:+|) ::
  ( Applicative f
  , Traversable f
  , Firstable g h
  )
    => f a -> Comp f h a -> Comp f g a
pattern x :+| xs <- (_colCons -> [(x, xs)]) where
  (:+|) =
    (+:|)

-- | Transposes a matrix.
pattern TP ::
  ( Applicative f
  , Traversable f
  , Applicative g
  , Traversable g
  )
    => Comp f g r -> Comp g f r
pattern TP x <- (LC sQ -> x) where
  TP =
    LC sQ

-- | Structures which are in a sense functors over their columns.
class ColFunctor f where
  mC ::
    ( Applicative g
    , Traversable g
    , Applicative h
    )
      => (g a -> h b) -> Comp g f a -> Comp h f b

instance ColFunctor Vec0 where
  mC _ _ =
    PEv

instance
  ( ColFunctor (Vec n)
  )
    => ColFunctor (Vec (Nx n))
  where
    mC func (x :+| xs) =
      func x +:| mC func xs
