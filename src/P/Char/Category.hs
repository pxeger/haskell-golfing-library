{-# Language PatternSynonyms #-}
{-|
Module :
  P.Char.Category
Description :
  Getting and handling the General Category of characters from the Unicode standard
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental
-}
module P.Char.Category
  ( LetterCategory (..)
  , MarkCategory (..)
  , NumberCategory (..)
  , PunctuationCategory (..)
  , SymbolCategory (..)
  , SeparatorCategory (..)
  , OtherCategory (..)
  , GeneralCategory (..)
  , pattern ULu
  , pattern ULl
  , pattern ULt
  , pattern ULm
  , pattern ULo
  , pattern UMn
  , pattern UMc
  , pattern UMe
  , pattern UNd
  , pattern UNl
  , pattern UNo
  , pattern UPc
  , pattern UPd
  , pattern UPs
  , pattern UPe
  , pattern UPi
  , pattern UPf
  , pattern UPo
  , pattern USm
  , pattern USc
  , pattern USk
  , pattern USo
  , pattern UZs
  , pattern UZl
  , pattern UZp
  , pattern UCc
  , pattern UCf
  , pattern UCs
  , pattern UCo
  , pattern UCn
  , gCt
  , iCt
  , iCT
  , nCt
  , nCT
  -- * Deprecated
  , generalCategory
  , pattern UppercaseLetter
  , pattern LowercaseLetter
  , pattern TitlecaseLetter
  , pattern ModifierLetter
  , pattern OtherLetter
  , pattern NonSpacingMark
  , pattern SpacingCombiningMark
  , pattern DecimalNumber
  , pattern LetterNumber
  , pattern OtherNumber
  , pattern ConnectorPunctuation
  , pattern DashPunctuation
  , pattern OpenPunctuation
  , pattern ClosePunctuation
  , pattern InitialQuote
  , pattern FinalQuote
  , pattern OtherPunctuation
  , pattern MathSymbol
  , pattern CurrencySymbol
  , pattern ModifierSymbol
  , pattern OtherSymbol
  , pattern Space
  , pattern LineSeparator
  , pattern ParagraphSeparator
  , pattern Control
  , pattern Format
  , pattern Surrogate
  , pattern PrivateUse
  , pattern NotAssigned
  )
  where


import qualified Prelude
import Prelude
  ( Eq
  )


import qualified Data.Char


import P.Bool
import P.Char
import P.Function.Compose
import P.Function.Flip
import P.Eq
import P.Ord
import P.Show


-- | Unicode General Categories in the "Letter" category.
data LetterCategory
  -- | @Lu@: Uppercase letter
  = LUp
  -- | @Ll@: Lowercase letter
  | LLo
  -- | @Lt@: Titlecase letter
  | LTc
  -- | @Lm@: Modifier letter
  | LMo
  -- | @Lo@: Other letter
  | LOt
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


instance
  (
  )
    => Ord LetterCategory
  where
    cp =
      mm ofp Prelude.compare


-- | Unicode General Categories in the "Mark" category.
data MarkCategory
  -- | @Mn@: Non-spacing mark
  = MNs
  -- | @Mc@: Spacing combining mark
  | MSc
  -- | @Me@: Enclosing mark
  | Men
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


instance
  (
  )
    => Ord MarkCategory
  where
    cp =
      mm ofp Prelude.compare


-- | Unicode General Categories in the "Number" category.
data NumberCategory
  -- | @Nd@: Decimal number
  = NDc
  -- | @Nl@: Letter number
  | NLe
  -- | @No@: Other number
  | NOt
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


instance
  (
  )
    => Ord NumberCategory
  where
    cp =
      mm ofp Prelude.compare


-- | Unicode General Categories in the "Punctuation" category.
data PunctuationCategory
  -- | @Pc@: Connector punctuation
  = PCn
  -- | @Pd@: Dash punctuation
  | PDa
  -- | @Ps@: Open punctuation
  | POp
  -- | @Pe@: Close punctuation
  | PCl
  -- | @Pi@: Inial quote
  | PIQ
  -- | @Pf@: Final quote
  | PFQ
  -- | @Po@: Other punctuation
  | POt
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


instance
  (
  )
    => Ord PunctuationCategory
  where
    cp =
      mm ofp Prelude.compare


-- | Unicode General Categories in the "Symbol" category.
data SymbolCategory
  -- | @Sm@: Math symbol
  = SyM
  -- | @Sc@: Currency symbol
  | SyC
  -- | @Sk@: Modifier symbol
  | SyK
  -- | @So@: Other symbol
  | Syo
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


instance
  (
  )
    => Ord SymbolCategory
  where
    cp =
      mm ofp Prelude.compare


-- | Unicode General Categories in the "Separator" category.
data SeparatorCategory
  -- | @Zs@: Space separator
  = ZSp
  -- | @Zl@: Line separator
  | ZLn
  -- | @Zp@: Paragraph separator
  | ZPa
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


instance
  (
  )
    => Ord SeparatorCategory
  where
    cp =
      mm ofp Prelude.compare


-- | Unicode General Categories in the "Other" category.
data OtherCategory
  -- | @Cc@: Control
  = OCn
  -- | @Cf@: Format
  | OFo
  -- | @Cs@: Surrogate
  | OSu
  -- | @Co@: Private use
  | OPi
  -- | @Cn@: Not assigned
  | ONA
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


instance
  (
  )
    => Ord OtherCategory
  where
    cp =
      mm ofp Prelude.compare


-- | Unicode General Categories (column 2 of the UnicodeData table) in the order they are listed in the Unicode standard (the Unicode Character Database, in particular).
data GeneralCategory
  -- | Letter
  = ULe LetterCategory
  -- | Mark
  | UMk MarkCategory
  -- | Number
  | UNm NumberCategory
  -- | Punctuation
  | UPu PunctuationCategory
  -- | Symbol
  | USy SymbolCategory
  -- | Separator
  | UZr SeparatorCategory
  -- | Other
  | UOt OtherCategory
  deriving
    ( Eq
    , Prelude.Ord
    , Show
    )


instance
  (
  )
    => Ord GeneralCategory
  where
    cp =
      mm ofp Prelude.compare


-- | Uppercase letters. (@Lu@)
pattern ULu :: GeneralCategory
pattern ULu =
  ULe LUp


-- | Lowercase letters. (@Ll@)
pattern ULl :: GeneralCategory
pattern ULl =
  ULe LLo


-- | Titlecase letters. (@Lt@)
pattern ULt :: GeneralCategory
pattern ULt =
  ULe LTc


-- | Modifier letters. (@Lm@)
pattern ULm :: GeneralCategory
pattern ULm =
  ULe LMo


-- | Other letters. (@Lo@)
pattern ULo :: GeneralCategory
pattern ULo =
  ULe LOt


-- | Non-spacing mark. (@Mn@)
pattern UMn :: GeneralCategory
pattern UMn =
  UMk MNs


-- | Spacing combining mark. (@Mc@)
pattern UMc :: GeneralCategory
pattern UMc =
  UMk MSc


-- | Enclosing mark. (@Me@)
pattern UMe :: GeneralCategory
pattern UMe =
  UMk Men


-- | Decimal number. (@Nd@)
pattern UNd :: GeneralCategory
pattern UNd =
  UNm NDc


-- | Letter number. (@Nl@)
pattern UNl :: GeneralCategory
pattern UNl =
  UNm NLe


-- | Other number. (@No@)
pattern UNo :: GeneralCategory
pattern UNo =
  UNm NOt


-- | Connector punctuation. (@Pc@)
pattern UPc :: GeneralCategory
pattern UPc =
  UPu PCn


-- | Dash punctutation. (@Pd@)
pattern UPd :: GeneralCategory
pattern UPd =
  UPu PDa


-- | Open punctutation. (@Ps@)
pattern UPs :: GeneralCategory
pattern UPs =
  UPu POp


-- | Close punctutation. (@Pe@)
pattern UPe :: GeneralCategory
pattern UPe =
  UPu PCl


-- | Initial quote. (@Pi@)
pattern UPi :: GeneralCategory
pattern UPi =
  UPu PIQ


-- | Final quote. (@Pf@)
pattern UPf :: GeneralCategory
pattern UPf =
  UPu PFQ


-- | Other punctutation. (@Po@)
pattern UPo :: GeneralCategory
pattern UPo =
  UPu POt


-- | Math symbol. (@Sm@)
pattern USm :: GeneralCategory
pattern USm =
  USy SyM


-- | Currency symbol. (@Sc@)
pattern USc :: GeneralCategory
pattern USc =
  USy SyC


-- | Modifier symbol. (@Sk@)
pattern USk :: GeneralCategory
pattern USk =
  USy SyK


-- | Other symbol. (@So@)
pattern USo :: GeneralCategory
pattern USo =
  USy Syo


-- | Space separator. (@Zs@)
pattern UZs :: GeneralCategory
pattern UZs =
  UZr ZSp


-- | Line separator. (@Zl@)
pattern UZl :: GeneralCategory
pattern UZl =
  UZr ZLn


-- | Paragraph separator. (@Zp@)
pattern UZp :: GeneralCategory
pattern UZp =
  UZr ZPa


-- | Control. (@Cc@)
pattern UCc :: GeneralCategory
pattern UCc =
  UOt OCn


-- | Format. (@Cf@)
pattern UCf :: GeneralCategory
pattern UCf =
  UOt OFo


-- | Surrogate. (@Cs@)
pattern UCs :: GeneralCategory
pattern UCs =
  UOt OSu


-- | Private use. (@Co@)
pattern UCo :: GeneralCategory
pattern UCo =
  UOt OPi


-- | Not assigned. (@Cn@)
pattern UCn :: GeneralCategory
pattern UCn =
  UOt ONA


-- | Get the unicode general category of character.
gCt ::
  (
  )
    => Char -> GeneralCategory
gCt x =
  case
    Data.Char.generalCategory x
  of
    Data.Char.UppercaseLetter ->
      ULu
    Data.Char.LowercaseLetter ->
      ULl
    Data.Char.TitlecaseLetter ->
      ULt
    Data.Char.ModifierLetter ->
      ULm
    Data.Char.OtherLetter ->
      ULo
    Data.Char.NonSpacingMark ->
      UMn
    Data.Char.SpacingCombiningMark ->
      UMc
    Data.Char.DecimalNumber ->
      UNd
    Data.Char.LetterNumber ->
      UNl
    Data.Char.OtherNumber ->
      UNo
    Data.Char.ConnectorPunctuation ->
      UPc
    Data.Char.DashPunctuation ->
      UPd
    Data.Char.OpenPunctuation ->
      UPs
    Data.Char.ClosePunctuation ->
      UPe
    Data.Char.InitialQuote ->
      UPi
    Data.Char.FinalQuote ->
      UPf
    Data.Char.OtherPunctuation ->
      UPo
    Data.Char.MathSymbol ->
      USm
    Data.Char.CurrencySymbol ->
      USc
    Data.Char.ModifierSymbol ->
      USk
    Data.Char.OtherSymbol ->
      USo
    Data.Char.Space ->
      UZs
    Data.Char.LineSeparator ->
      UZl
    Data.Char.ParagraphSeparator ->
      UZp
    Data.Char.Control ->
      UCc
    Data.Char.Format ->
      UCf
    Data.Char.Surrogate ->
      UCs
    Data.Char.PrivateUse ->
      UCo
    Data.Char.NotAssigned ->
      UCn


-- | Takes a 'Char' and a 'GeneralCategory' and determines if that character is in that general category.
iCt ::
  (
  )
    => GeneralCategory -> Char -> Bool
iCt =
  eq ^. gCt


-- | Flip of 'iCt'.
iCT ::
  (
  )
    => Char -> GeneralCategory -> Bool
iCT =
  F iCt

-- | Takes a 'Char' and a 'GeneralCategory' and determines if that character is not in that general category.
nCt ::
  (
  )
    => GeneralCategory -> Char -> Bool
nCt =
  n << iCt


-- | Flip of 'nCt'.
nCT ::
  (
  )
    => Char -> GeneralCategory -> Bool
nCT =
  F nCt


{-# Deprecated generalCategory "Use gCt instead" #-}
-- | Long version of 'gCt'.
generalCategory ::
  (
  )
    => Char -> GeneralCategory
generalCategory =
  gCt


{-# Deprecated UppercaseLetter "Use ULu instead" #-}
-- | Long version of 'ULu'.
pattern UppercaseLetter ::
  (
  )
    => GeneralCategory
pattern UppercaseLetter =
  ULu


{-# Deprecated LowercaseLetter "Use ULl instead" #-}
-- | Long version of 'ULl'.
pattern LowercaseLetter ::
  (
  )
    => GeneralCategory
pattern LowercaseLetter =
  ULl


{-# Deprecated TitlecaseLetter "Use ULt instead" #-}
-- | Long version of 'ULt'.
pattern TitlecaseLetter ::
  (
  )
    => GeneralCategory
pattern TitlecaseLetter =
  ULt


{-# Deprecated ModifierLetter "Use ULm instead" #-}
-- | Long version of 'ULm'.
pattern ModifierLetter ::
  (
  )
    => GeneralCategory
pattern ModifierLetter =
  ULm


{-# Deprecated OtherLetter "Use ULo instead" #-}
-- | Long version of 'ULo'.
pattern OtherLetter ::
  (
  )
    => GeneralCategory
pattern OtherLetter =
  ULo


{-# Deprecated NonSpacingMark "Use UMn instead" #-}
-- | Long version of 'UMn'.
pattern NonSpacingMark ::
  (
  )
    => GeneralCategory
pattern NonSpacingMark =
  UMn


{-# Deprecated SpacingCombiningMark "Use UMc instead" #-}
-- | Long version of 'UMc'.
pattern SpacingCombiningMark ::
  (
  )
    => GeneralCategory
pattern SpacingCombiningMark =
  UMc


{-# Deprecated DecimalNumber "Use UNd instead" #-}
-- | Long version of 'UNd'.
pattern DecimalNumber ::
  (
  )
    => GeneralCategory
pattern DecimalNumber =
  UNd


{-# Deprecated LetterNumber "Use UNl instead" #-}
-- | Long version of 'UNl'.
pattern LetterNumber ::
  (
  )
    => GeneralCategory
pattern LetterNumber =
  UNl


{-# Deprecated OtherNumber "Use UNo instead" #-}
-- | Long version of 'UNo'.
pattern OtherNumber ::
  (
  )
    => GeneralCategory
pattern OtherNumber =
  UNo


{-# Deprecated ConnectorPunctuation "Use UPc instead" #-}
-- | Long version of 'UPc'.
pattern ConnectorPunctuation ::
  (
  )
    => GeneralCategory
pattern ConnectorPunctuation =
  UPc


{-# Deprecated DashPunctuation "Use UPd instead" #-}
-- | Long version of 'UPd'.
pattern DashPunctuation ::
  (
  )
    => GeneralCategory
pattern DashPunctuation =
  UPd


{-# Deprecated OpenPunctuation "Use UPs instead" #-}
-- | Long version of 'UPs'.
pattern OpenPunctuation ::
  (
  )
    => GeneralCategory
pattern OpenPunctuation =
  UPs


{-# Deprecated ClosePunctuation "Use UPe instead" #-}
-- | Long version of 'UPe'.
pattern ClosePunctuation ::
  (
  )
    => GeneralCategory
pattern ClosePunctuation =
  UPe


{-# Deprecated InitialQuote "Use UPi instead" #-}
-- | Long version of 'UPi'.
pattern InitialQuote ::
  (
  )
    => GeneralCategory
pattern InitialQuote =
  UPi


{-# Deprecated FinalQuote "Use UPf instead" #-}
-- | Long version of 'UPf'.
pattern FinalQuote ::
  (
  )
    => GeneralCategory
pattern FinalQuote =
  UPf


{-# Deprecated OtherPunctuation "Use UPo instead" #-}
-- | Long version of 'UPo'.
pattern OtherPunctuation ::
  (
  )
    => GeneralCategory
pattern OtherPunctuation =
  UPo


{-# Deprecated MathSymbol "Use USm instead" #-}
-- | Long version of 'USm'.
pattern MathSymbol ::
  (
  )
    => GeneralCategory
pattern MathSymbol =
  USm


{-# Deprecated CurrencySymbol "Use USc instead" #-}
-- | Long version of 'USc'.
pattern CurrencySymbol ::
  (
  )
    => GeneralCategory
pattern CurrencySymbol =
  USc


{-# Deprecated ModifierSymbol "Use USk instead" #-}
-- | Long version of 'USk'.
pattern ModifierSymbol ::
  (
  )
    => GeneralCategory
pattern ModifierSymbol =
  USk


{-# Deprecated OtherSymbol "Use USo instead" #-}
-- | Long version of 'USo'.
pattern OtherSymbol ::
  (
  )
    => GeneralCategory
pattern OtherSymbol =
  USo


{-# Deprecated Space "Use UZs instead" #-}
-- | Long version of 'UZs'.
pattern Space ::
  (
  )
    => GeneralCategory
pattern Space =
  UZs


{-# Deprecated LineSeparator "Use UZl instead" #-}
-- | Long version of 'UZl'.
pattern LineSeparator ::
  (
  )
    => GeneralCategory
pattern LineSeparator =
  UZl


{-# Deprecated ParagraphSeparator "Use UZp instead" #-}
-- | Long version of 'UZp'.
pattern ParagraphSeparator ::
  (
  )
    => GeneralCategory
pattern ParagraphSeparator =
  UZp


{-# Deprecated Control "Use UCc instead" #-}
-- | Long version of 'UCc'.
pattern Control ::
  (
  )
    => GeneralCategory
pattern Control =
  UCc


{-# Deprecated Format "Use UCf instead" #-}
-- | Long version of 'UCf'.
pattern Format ::
  (
  )
    => GeneralCategory
pattern Format =
  UCf


{-# Deprecated Surrogate "Use UCs instead" #-}
-- | Long version of 'UCs'.
pattern Surrogate ::
  (
  )
    => GeneralCategory
pattern Surrogate =
  UCs


{-# Deprecated PrivateUse "Use UCo instead" #-}
-- | Long version of 'UCo'.
pattern PrivateUse ::
  (
  )
    => GeneralCategory
pattern PrivateUse =
  UCo


{-# Deprecated NotAssigned "Use UCn instead" #-}
-- | Long version of 'UCn'.
pattern NotAssigned ::
  (
  )
    => GeneralCategory
pattern NotAssigned =
  UCn
