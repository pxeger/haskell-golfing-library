{-|
Module :
  P.Arithmetic.Base.Extra
Description :
  Additional base conversion functions
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A large number of less important functions for base conversion that have been moved to this file to reduce clutter in the main P.Arithmetic.Base library.
-}
module P.Arithmetic.Base.Extra
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import P.Aliases
import P.Arithmetic.Base
import P.Ord


-- | Convert a natural number to little endian base 2.
bs2 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs2 =
  bs 2


-- | Convert a natural number to little endian base 3.
bs3 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs3 =
  bs 3


-- | Convert a natural number to little endian base 4.
bs4 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs4 =
  bs 4


-- | Convert a natural number to little endian base 5.
bs5 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs5 =
  bs 5


-- | Convert a natural number to little endian base 6.
bs6 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs6 =
  bs 6


-- | Convert a natural number to little endian base 7.
bs7 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs7 =
  bs 7


-- | Convert a natural number to little endian base 8.
bs8 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs8 =
  bs 8


-- | Convert a natural number to little endian base 9.
bs9 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs9 =
  bs 9


-- | Convert a natural number to little endian base 10.
bs0 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs0 =
  bs 10


-- | Convert a natural number to little endian base 11.
bs1 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bs1 =
  bs 11



-- | Convert a natural number to big endian base 2.
bS2 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS2 =
  bS 2


-- | Convert a natural number to big endian base 3.
bS3 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS3 =
  bS 3


-- | Convert a natural number to big endian base 4.
bS4 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS4 =
  bS 4


-- | Convert a natural number to big endian base 5.
bS5 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS5 =
  bS 5


-- | Convert a natural number to big endian base 6.
bS6 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS6 =
  bS 6


-- | Convert a natural number to big endian base 7.
bS7 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS7 =
  bS 7


-- | Convert a natural number to big endian base 8.
bS8 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS8 =
  bS 8


-- | Convert a natural number to big endian base 9.
bS9 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS9 =
  bS 9


-- | Convert a natural number to big endian base 10.
bS0 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS0 =
  bS 10


-- | Convert a natural number to big endian base 11.
bS1 ::
  ( Integral i
  , Ord i
  )
    => i -> List i
bS1 =
  bS 11
