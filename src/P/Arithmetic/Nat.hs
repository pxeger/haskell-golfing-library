{-|
Module :
  P.Arithmetic.Nat
Description :
  Lazy natural numbers
Copyright :
  (c) E. Olive, 2022
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

-}
module P.Arithmetic.Nat
  ( Nat (..)
  , nMi
  )
  where


import Prelude
  ( (==)
  , Show (..)
  , Num (..)
  , Enum (..)
  , Real (..)
  , Integral (..)
  )
import qualified Prelude
import qualified Data.Ix


import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Algebra.Semigroup.Commutative
import P.Aliases
import P.Bifunctor
import P.Bool
import P.Category
import P.Eq
import P.Function.Compose
import P.Ix
import P.Ord


{-# Deprecated Zero "Use i or 0 instead" #-}
-- | NonNegative integers or "Natural numbers".
--
-- Useful because comparisons can be done without fully evaluating the number.
--
-- == Negatives
--
-- Certain classes may not be well behaved with this type.
-- Check the instances to see which ones are.
-- Non-negative numbers are possible to create but are deemed illegal, because they break assumptions.
--
-- The 'Num' instance is given to let you use literals to represent nonnegative numbers, but it can result in errors when you attempt to make a negative non-negative.
--
-- >>> (1) :: Nat
-- 1
-- >>> (-1) :: Nat
-- *** Exception: Cannot negate a positive natural number
--
-- == Infinity
--
-- An infinite value can also be represented, and will occasionally be produced by functions such as 'P.Foldable.Length.lg'.
-- It can be created directly with:
--
-- @
-- infinity = Succ infinity
-- @
--
-- And it can successfully be compared with finite @Nat@ in finite time.
--
-- However comparisons such as:
--
-- @
-- eq infinity infinity
-- @
--
-- @
-- cp infinity infinity
-- @
--
-- will never halt.
data Nat
  -- | @Zero@ represents zero.
  -- 'i' or @0@ is equivalent and shorter.
  = Zero
  -- | @Succ@ gives the successor to a natural number.
  | Succ Nat
  deriving
    ( Prelude.Eq
    , Prelude.Ord
    )


-- | Attempt to subtract one natural from another.
--
-- Unlike '(-)' which fails when the result would be negative @nMi@ yields a 'Maybe'' which is empty when the result would be negative.
nMi ::
  (
  )
    => Nat -> Nat -> Maybe' Nat
nMi (Succ x) (Succ y) =
  nMi x y
nMi x Zero =
  [x]
nMi Zero (Succ _) =
  []


instance
  (
  )
    => Ord Nat
  where
    cp Zero Zero =
      EQ
    cp (Succ x) (Succ y) =
      cp x y
    cp (Succ _) Zero =
      GT
    cp Zero (Succ _) =
      LT

    Zero <= _ =
      T
    Succ _ <= Zero =
      B
    Succ x <= Succ y =
      x <= y

    ma Zero y =
      y
    ma x Zero =
      x
    ma (Succ x) (Succ y) =
      Succ (ma x y)

    mN Zero _ =
      Zero
    mN _ Zero =
      Zero
    mN (Succ x) (Succ y) =
      Succ (mN x y)


instance
  (
  )
    => Semigroup Nat
  where
    Zero <> y =
      y
    Succ x <> y =
      Succ (x <> y)


instance
  (
  )
    => Monoid Nat
  where
    mempty =
      Zero


instance
  (
  )
    => Commutative Nat


-- | Not a well behaved instance of @Num@.
-- Avoid unqualified use of 'negate', 'fromInteger'.
instance
  (
  )
    => Num Nat
  where
    Zero * _ =
      Zero
    _ * Zero =
      Zero
    Succ x * Succ y =
      Succ $ x + y + (x * y)

    Zero + y =
      y
    x + Zero =
      x
    Succ x + Succ y =
      Succ $ Succ $ x + y

    x - Zero =
      x
    Zero - (Succ _) =
      Prelude.error "Cannot subtract a larger natural number from a smaller one"
    (Succ x) - (Succ y) =
      x - y

    abs =
      id

    signum Zero =
      Zero
    signum (Succ _) =
      Succ Zero

    negate Zero =
      Zero
    negate (Succ _) =
      Prelude.error "Cannot negate a positive natural number"

    fromInteger x =
      case
        cp x 0
      of
        LT ->
          Prelude.error "Cannot convert negative number to the non-negative type"
        EQ ->
          Zero
        GT ->
          Succ $ fromInteger $ x - 1


instance
  (
  )
    => Enum Nat
  where
    succ =
      Succ

    toEnum x =
      case
        cp x 0
      of
        LT ->
          Prelude.error "Cannot convert negative to Nat"
        EQ ->
          0
        GT ->
          Succ $ toEnum (x - 1)

    fromEnum Zero =
      0
    fromEnum (Succ n) =
      1 + fromEnum n

    enumFrom x =
      x : enumFrom (Succ x)

    enumFromTo (Succ _) 0 =
      []
    enumFromTo 0 m =
      0 : enumFromTo 1 m
    enumFromTo (Succ x) (Succ y) =
      Succ < enumFromTo x y

    enumFromThen x =
      (x :) < go Zero x
      where
        go n (Succ x) (Succ y) =
          go (Succ n) x y
        go n Zero y =
          (y +) < (n : go n Zero y)
        go n y Zero =
          n : do
            m <- nMi n y
            go m y Zero

    enumFromThenTo =
      go Zero
      where
        go n (Succ x) (Succ y) i =
          go (Succ n) x y i
        go n Zero y i = do
          j <- nMi i y
          n : (y +) < go n Zero y j
        go n y Zero i = do
          v <- (n + y) : go' n y
          (+ i) < (nMi v i)

        go' n y =
          n : do
            m <- nMi n y
            go' m y


instance
  (
  )
    => Real Nat
  where
    toRational =
      toRational < toInteger


instance
  (
  )
    => Integral Nat
  where
    toInteger Zero =
      0
    toInteger (Succ n) =
      1 + toInteger n

    quotRem _ Zero =
      Prelude.error "divide by zero"
    quotRem x y =
      case
        nMi x y
      of
        [] ->
          (0, x)
        [z] ->
          mst Succ $ quotRem z y

    -- | Doesn't matter without negatives
    divMod =
      quotRem


instance
  (
  )
    => Show Nat
  where
    show =
      show < toInteger


instance
  (
  )
    => Ix Nat
  where
    uDx (a, b) x
      | x > b
      =
        Prelude.error $ Prelude.concat
          [ "Index ("
          , show x
          , ") out or range ("
          , show (a,b)
          , ")"
          ]

    uIr (a, b) x =
      (x >= a) <> (b >= x)

    uRg (a, b) =
      case
        cp a b
      of
        LT ->
          a : uRg (Succ a, b)
        EQ ->
          [a]
        GT ->
          []
