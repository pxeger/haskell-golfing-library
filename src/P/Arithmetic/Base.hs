{-|
Module :
  P.Arithmetic.Base
Description :
  Base conversion functions
Copyright :
  (c) E. Olive, 2023
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

Functions for base conversion.
-}
module P.Arithmetic.Base
  ( hx
  , bs
  , fbs
  , bS
  , fbS
  , bi
  , ubs
  , fub
  , ubS
  , fuB
  )
  where


import qualified Prelude
import Prelude
  ( Integral
  )


import qualified Data.List


import P.Aliases
import P.Arithmetic
import P.Bool
import P.Category
import P.Foldable
import P.Foldable.Unfold
import P.Function.Compose
import P.Function.Flip
import P.Ord
import P.String
import P.Zip


-- | Converts a positive integer to a string of its hexidecimal representation.
hx ::
  ( Integral i
  , Ord i
  )
    => i -> String
hx x = do
  val <- bs 16 x & m (f' Data.List.genericDrop "0123456789ABCDEF")
  val & Prelude.take 1 & Prelude.reverse


-- | Converts a positive integer to a particular base.
--
-- The outputted sequence is little endian, with the least significant place being the first in the list.
bs ::
  ( Integral i
  , Ord i
  )
    => i -> i -> List i
bs base x
  | x Prelude.== 0
  =
    []
  | (quot, rem) <- qR x base
  =
    if
      0 > rem
    then
      (rem - base) : bs base (quot + 1)
    else
      rem : bs base quot


-- | Flip of 'bs'.
fbs ::
  ( Integral i
  , Ord i
  )
    => i -> i -> List i
fbs =
  f' bs


-- | Converts a positive integer to a particular base.
--
-- The outputted sequence is big endian, with the most significant place being the first in the list.
bS ::
  ( Integral i
  , Ord i
  )
    => i -> i -> List i
bS =
  Prelude.reverse << bs


-- | Flip of 'bS'.
fbS ::
  ( Integral i
  , Ord i
  )
    => i -> i -> List i
fbS =
  f' bS


-- | Converts an integer to binary in terms of boolean values.
--
-- The outputted sequence is little endian, with the least significant place being the first in the list.
bi ::
  ( Integral i
  , Ord i
  )
    => i -> List Bool
bi =
  m (Prelude.> 0) < bs 2


-- | Converts a little endian base representation to an integer.
ubs ::
  ( Integral i
  , Foldable t
  )
    => i -> t i -> i
ubs base x =
  sm $ zW (*) (tL x) $ ix (* base) 1


-- | Flip of 'ubs'.
fub ::
  ( Integral i
  , Foldable t
  )
    => t i -> i -> i
fub =
  f' ubs


-- | Converts a big endian base representation to an integer.
ubS ::
  ( Integral i
  , Foldable t
  )
    => i -> t i -> i
ubS base =
  lF' ((+) < (* base)) 0


-- | Flip of 'ubS'.
fuB ::
  ( Integral i
  , Foldable t
  )
    => t i -> i -> i
fuB =
  f' ubS
