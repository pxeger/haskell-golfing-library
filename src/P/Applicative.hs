module P.Applicative
  ( Applicative
  , p
  , fp
  , pp
  , fpp
  , p3
  , fp3
  , p4
  , fp4
  , μ
  , fμ
  , ap
  , (*^)
  , fA
  , aT
  , (*>)
  , faT
  , aK
  , (<*)
  , faK
  , l2
  , fl2
  , (**)
  , l3
  , fl3
  , (**#)
  , l4
  , fl4
  , (**$)
  , jl2
  , xly
  , fXy
  , xlm
  , fXm
  , onp
  , l2p
  , (*^*)
  , l2x
  , (*|*)
  , l2n
  , (*/*)
  , l2m
  , (**<)
  , l2M
  , (*×)
  , l2q
  , flq
  , (×*)
  , u2q
  , j2q
  , l3q
  , u3q
  , j3q
  , l4q
  , u4q
  , j4q
  , eqo
  , (*=*)
  -- * Deprecated
  , fxly
  , fxlm
  , fpOp
  , liftA
  , liftA2
  , liftA3
  , liftM3
  , pure
  )
  where


import Prelude
  ( Integral
  , Num
  )
import qualified Prelude


import qualified Control.Applicative
import Control.Applicative
  ( Applicative
  )


import P.Algebra.Semigroup
import P.Algebra.Monoid
import P.Aliases
import P.Arithmetic
import P.Bool
import P.Category
import P.Eq
import P.Foldable
import P.Function
import P.Function.Compose
import P.Function.Curry
import P.Function.Flip
import P.Ord


infixl 4 <*, *>, *^
infixr 8 *=*, *^*, *|*, */*, *×, ×*
infixr 9 **<
infixl 9 **, **#


-- |
-- Equivalent to 'Control.Applicative.pure'.
p ::
  ( Applicative f
  )
    => a -> f a
p =
  Control.Applicative.pure


-- | Flip of 'p'.
-- Takes two arguments and returns the second.
fp ::
  (
  )
    => a -> b -> b
fp =
  F p


-- | Double 'p'.
-- Wraps a value in two 'Applicative's.
pp ::
  ( Applicative f
  , Applicative g
  )
    => a -> f (g a)
pp =
  p < p


-- | Flip of 'p'.
fpp ::
  ( Applicative f
  )
    => a -> b -> f b
fpp =
  F pp


-- | Triple 'p'.
-- Wraps a value in three 'Applicative's.
p3 ::
  ( Applicative f
  , Applicative g
  , Applicative h
  )
    => a -> f (g (h a))
p3 =
  pp < p


-- | Flip of 'p3'.
fp3 ::
  ( Applicative f
  , Applicative g
  )
    => a -> b -> f (g b)
fp3 =
  f' p3


-- | Quadruple 'p'.
-- Wraps a value in four 'Applicative's.
p4 ::
  ( Applicative f
  , Applicative g
  , Applicative h
  , Applicative k
  )
    => a -> f (g (h (k a)))
p4 =
  pp < pp


-- | Flip of 'p4'.
fp4 ::
  ( Applicative f
  , Applicative g
  , Applicative h
  )
    => a -> b -> f (g (h b))
fp4 =
  f' p4


-- | Map 'p' over a functor.
μ ::
  ( Applicative p
  , Functor f
  )
    => f a -> f (p a)
μ =
  m p


-- | Flip of 'μ'.
fμ ::
  ( Applicative p
  )
    => a -> (a -> b) -> p b
fμ =
  f' μ


-- |
-- Equivalent to '(Control.Applicative.<*>)'.
ap ::
  ( Applicative f
  )
    => f (a -> b) -> f a -> f b
ap =
  (Control.Applicative.<*>)


-- | Flip of 'ap'.
fA ::
  ( Applicative f
  )
    => f a -> f (a -> b) -> f b
fA =
  F ap


-- | Infix of 'ap'.
(*^) ::
  ( Applicative f
  )
    => f (a -> b) -> f a -> f b
(*^) =
  ap


-- | Perform two applicatives discarding the result of the first.
--
-- Prefix version of '(*>)'.
-- Equivalent to '(Control.Applicative.*>)'
aT ::
  ( Applicative f
  )
    => f a -> f b -> f b
aT =
  (Control.Applicative.*>)


-- | Perform two applicatives discarding the result of the first.
--
-- Not the flip of '(*>)'.
--
-- Equivalent to '(Control.Applicative.*>)'.
-- More general version of '(Prelude.>>)'.
(*>) ::
  ( Applicative f
  )
    => f a -> f b -> f b
(*>) =
  (Control.Applicative.*>)


-- | Flip of 'aT'.
--
-- Not equivalent to '(<*)'
faT ::
  ( Applicative f
  )
    => f b -> f a -> f b
faT =
  F aT


-- | Perform two applicatives discarding the result of the second.
--
-- Not the flip of '(*>)'.
--
-- Equivalent to '(Control.Applicative.*>)'.
-- More general version of '(Prelude.>>)'.
(<*) ::
  ( Applicative f
  )
    => f a -> f b -> f a
(<*) =
  (Control.Applicative.<*)


-- | Perform two applicatives discarding the result of the second.
--
-- Prefix of '(<*)'.
aK ::
  ( Applicative f
  )
    => f a -> f b -> f a
aK =
  (<*)


-- | Flip of 'aK'.
--
-- Prefix of '(<*)'.
faK ::
  ( Applicative f
  )
    => f b -> f a -> f a
faK =
  F aK


-- | Lift a binary function to a function on applicative functors.
--
-- Equivalent to 'Control.Applicative.liftA2'.
-- More general version of 'Prelude.liftM2'.
l2 ::
  ( Applicative f
  )
    => (a -> b -> c) -> f a -> f b -> f c
l2 =
  Control.Applicative.liftA2


-- | Flip of 'l2'.
fl2 ::
  ( Applicative f
  )
    => f a -> (a -> b -> c) -> f b -> f c
fl2 =
  F l2


-- | Infix of 'l2'.
--
-- Equivalent to 'Control.Applicative.liftA2'.
-- More general version of 'Prelude.liftM2'.
(**) ::
  ( Applicative f
  )
    => (a -> b -> c) -> f a -> f b -> f c
(**) =
  l2


-- | Lifted semigroup action.
--
-- More general version of a lifted logical and.
--
-- The equivalent of @l2(<>)@.
l2p ::
  ( Applicative f
  , Semigroup m
  )
    => f m -> f m -> f m
l2p =
  l2 (<>)


-- | Infix version of 'l2p'.
(*^*) ::
  ( Applicative f
  , Semigroup m
  )
    => f m -> f m -> f m
(*^*) =
  l2p


-- | Lifted maximum.
--
-- More general version of a lifted logical or.
--
-- The equivalent of @l2(^^)@.
l2x ::
  ( Applicative f
  , Ord a
  )
    => f a -> f a -> f a
l2x =
  l2 ma


-- | Infix version of 'l2x'.
(*|*) ::
  ( Applicative f
  )
    => f Bool -> f Bool -> f Bool
(*|*) =
  l2x


-- | Lifted minimum.
--
-- More general version of a lifted logical and.
--
-- The equivalent of @l2(!^)@
l2n ::
  ( Applicative f
  , Ord a
  )
    => f a -> f a -> f a
l2n =
  l2 mN


-- | Infix version of 'l2n'.
(*/*) ::
  ( Applicative f
  , Ord a
  )
    => f a -> f a -> f a
(*/*) =
  l2n


-- | Fold with 'l2p'.
lmk ::
  ( Applicative f
  , Monoid m
  , Foldable t
  )
    => t (f m) -> f m
lmk =
  lmk


-- | Joined 'l2'.
jl2 ::
  ( Applicative f
  )
    => (a -> a -> b) -> f a -> f b
jl2 f x =
  l2 f x x


-- | Lifted map.
--
-- Useful when @f@ and @g@ are functions.
--
-- @
-- l2m :: (a -> b -> c) -> (a -> d -> b) -> (a -> d -> c)
-- @
l2m ::
  ( Applicative f
  , Functor g
  )
    => f (a -> b) -> f (g a) -> f (g b)
l2m =
  l2 m


-- | Infix version of 'l2m'.
(**<) ::
  ( Applicative f
  , Functor g
  )
    => f (a -> b) -> f (g a) -> f (g b)
(**<) =
  l2m


-- | Lifted multiplication.
l2M ::
  ( Applicative f
  , Num n
  )
    => f n -> f n -> f n
l2M =
  l2 (Prelude.*)


-- | Infix version of 'l2M'.
(*×) ::
  ( Applicative f
  , Num n
  )
    => f n -> f n -> f n
(*×) =
  l2M


-- | Lifted @(,)@.
--
-- Can be thought of as a Cartesian product.
l2q ::
  ( Applicative f
  )
    => f a -> f b -> f (a, b)
l2q =
  l2 (,)


-- | Flip of 'l2q'.
flq ::
  ( Applicative f
  )
    => f a -> f b -> f (b, a)
flq =
  f' l2q


-- | Infix of 'l2q'.
(×*) ::
  ( Applicative f
  )
    => f a -> f b -> f (a, b)
(×*) =
  l2q


-- | Uncurried version of 'l2q'.
u2q ::
  ( Applicative f
  )
    => (f a, f b) -> f (a, b)
u2q =
  U l2q


-- | Joined 'l2q'.
--
-- Gets the Cartesian product of a list with itself.
j2q ::
  ( Applicative f
  )
    => f a -> f (a, a)
j2q x =
  l2q x x


-- | Lifted @(,,)@.
l3q ::
  ( Applicative f
  )
    => f a -> f b -> f c -> f (a, b, c)
l3q =
  l3 (,,)


-- | Uncurried version of 'l3q'.
u3q ::
  ( Applicative f
  )
    => (f a, f b, f c) -> f (a, b, c)
u3q =
  U3 l3q


-- | Joined 'l3q'.
--
-- Gets the Cartesian product of three copies of a list.
j3q ::
  ( Applicative f
  )
    => f a -> f (a, a, a)
j3q x =
  l3q x x x


-- | Lifted @(,,,)@.
l4q ::
  ( Applicative f
  )
    => f a -> f b -> f c -> f d -> f (a, b, c, d)
l4q =
  l4 (,,,)


-- | Uncurried version of 'l4q'.
u4q ::
  ( Applicative f
  )
    => (f a, f b, f c, f d) -> f (a, b, c, d)
u4q =
  U4 l4q


-- | Joined 'l4q'.
--
-- Gets the Cartesian product of four copies of a list.
j4q ::
  ( Applicative f
  )
    => f a -> f (a, a, a, a)
j4q x =
  l4q x x x x


-- | Lifted equality.
-- Determine if two functions give the same result when applied to a value.
eqo ::
  ( Applicative f
  , Eq a
  )
    => f a -> f a -> f Bool
eqo =
  l2 eq


-- | Infix version of 'eqo'.
(*=*) ::
  ( Applicative f
  , Eq a
  )
    => f a -> f a -> f Bool
(*=*) =
  eqo


{-# Deprecated liftA3, liftM3 "Use l3 instead" #-}
-- | Long version of 'l3'.
liftA3 ::
  ( Applicative f
  )
    => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
liftA3 =
  l3


-- | Long version of 'l3'.
liftM3 ::
  ( Applicative f
  )
    => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
liftM3 =
  l3


-- | Lift a function across 3 applicatives.
--
-- Equivalent to 'Control.Applicative.liftA3'.
-- More general version of 'Control.Monad.liftM3'.
l3 ::
  ( Applicative f
  )
    => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
l3 =
  ap <<< l2


-- | Flip of 'l3'.
fl3 ::
  ( Applicative f
  )
    => f a -> (a -> b -> c -> d) -> f b -> f c -> f d
fl3 =
  f' l3


-- | Infix of 'l3'.
(**#) ::
  ( Applicative f
  )
    => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
(**#) =
  l3


-- | Lift a function across 4 applicatives.
l4 ::
  ( Applicative f
  )
    => (a -> b -> c -> d -> e) -> f a -> f b -> f c -> f d -> f e
l4 =
  ap <<& l3


-- | Flip of 'l3'.
fl4 ::
  ( Applicative f
  )
    => f a -> (a -> b -> c -> d -> e) -> f b -> f c -> f d -> f e
fl4 =
  f' l4


-- | Infix of 'l4'.
(**$) ::
  ( Applicative f
  )
    => (a -> b -> c -> d -> e) -> f a -> f b -> f c -> f d -> f e
(**$) =
  l4


-- | Combines an applicative exactly @n@ times collecting the results into a list.
--
-- ==== __Examples__
--
-- On a list this gives all the ways to choose @n@ elements from a list.
--
-- >>> xly 3 [1,2]
-- [[1,1,1],[1,1,2],[1,2,1],[1,2,2],[2,1,1],[2,1,2],[2,2,1],[2,2,2]]
--
-- On a parser this will repeat the parser @n@ times.
--
-- >>> xly 5 (ʃ "ab") -^ "ababababab"
-- [["ab","ab","ab","ab","ab"]]
--
-- >>> xly 3 (lSo $ ʃ "a") -^ "aaaaa"
-- [[3,1,1],[2,2,1],[2,1,2],[1,3,1],[1,2,2],[1,1,3]]
--
xly ::
  ( Integral n
  , Ord n
  , Applicative t
  )
    => n -> t a -> t (List a)
xly n _
  | 1 > n
  =
    p []
xly n alt =
  l2 (:) alt $ xly (n-1) alt


{-# Deprecated fxly "Use fXy instead" #-}
-- | Long version of 'fXy'.
-- Flip of 'xly'.
fxly ::
  ( Integral n
  , Ord n
  , Applicative t
  )
    => t a -> n -> t (List a)
fxly =
  F xly


-- | Flip of 'xly'.
--
-- ==== __Examples__
--
-- Used on a parser:
--
-- >>> (lSo ʃa >~ fXy ʃb) -# "aaaaabbbbb"
-- True
--
-- This matches strings starting with @n@ @a@s and ending with @n@ @b@s.
fXy ::
  ( Integral n
  , Ord n
  , Applicative t
  )
    => t a -> n -> t (List a)
fXy =
  F xly


-- | Combines an applicative exactly @n@ times combining the results with '(<>)'.
xlm ::
  ( Monoid a
  , Integral n
  , Ord n
  , Applicative t
  )
    => n -> t a -> t a
xlm =
  fo <<< xly


{-# Deprecated fxlm "Use fXm instead" #-}
-- | Long version of 'fXm'.
-- Flip of 'xlm'.
fxlm ::
  ( Monoid a
  , Integral n
  , Ord n
  , Applicative t
  )
    => t a -> n -> t a
fxlm =
  fXm


-- | Flip of 'xlm'.
fXm ::
  ( Monoid a
  , Integral n
  , Ord n
  , Applicative t
  )
    => t a -> n -> t a
fXm =
  F xlm


{-# Deprecated fpOp "Use onp instead" #-}
-- | Long version of 'onp'.
fpOp ::
  ( Applicative f
  )
    => (f a -> f b -> c) -> a -> b -> c
fpOp =
  onp


-- | Takes a function on applicative functors and composes 'p' on both arguments.
onp ::
  ( Applicative f
  )
    => (f a -> f b -> c) -> a -> b -> c
onp =
  fpO p


{-# Deprecated liftA2 "Use l2 or (**) instead" #-}
-- | Lift a binary function to a function on applicative functors.
-- Long version of 'l2' and '(**)'
liftA2 ::
  ( Applicative f
  )
    => (a -> b -> c) -> f a -> f b -> f c
liftA2 =
  l2


{-# Deprecated liftA "Use m or (<) instead" #-}
-- | Map a function over an arbitrary applicative functor.
-- Long version of 'P.Function.Compose.m' and '(P.Function.Compose.<)'
liftA ::
  ( Applicative f
  )
    => (a -> b) -> f a -> f b
liftA =
  Prelude.fmap


{-# Deprecated pure "Use p or Pu instead" #-}
-- | Long version of 'p' or 'P.Functor.Identity.Pu'.
pure ::
  ( Applicative f
  )
    => a -> f a
pure =
  p
