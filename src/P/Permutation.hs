{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language UndecidableInstances #-}
{-# Language GADTs #-}
module P.Permutation
  ( Permutation (..)
  )
  where

import Prelude
  ( Integral
  , show
  , (==)
  )
import qualified Prelude

import P.Algebra.Group
import P.Algebra.Monoid
import P.Algebra.Semigroup
import P.Arithmetic
import P.Bool
import P.Category
import P.Eq
import P.Foldable
import P.Function.Compose
import P.List.Index
import P.Ord
import P.Reverse
import P.Show
import P.Sort.Class
import P.Vector

{-# Deprecated UsP "Unsafe"#-}
newtype Permutation n a
  = UsP
    { uPr ::
      Vec n a
    }

instance
  ( Show a
  )
    => Show (Permutation n a)
  where
    show =
      sh < tL

instance Foldable (Permutation n)
  where
    foldr func accum =
      rF func accum < uPr
    foldMap func =
      Prelude.foldMap func < uPr

instance
  (
  )
    => Eq1 (Permutation n)
  where
    q1 _ (UsP Ev) (UsP Ev) =
      T
    q1 userEq (UsP (x :++ xs)) (UsP (y :++ ys)) =
      userEq x y <> q1 userEq (UsP xs) (UsP ys)

instance
  ( Eq a
  )
    => Eq (Permutation n a)
  where
    (==) =
      q1 eq

instance
  ( Integral i
  )
    => Semigroup (Permutation n i)
  where
    (UsP v1) <> (UsP v2) =
      UsP $ m (v1 !) v2

instance
  ( Integral i
  )
    => Monoid (Permutation Zr i)
  where
    mempty =
      UsP Ev

instance
  ( Integral i
  , Monoid (Permutation n i)
  )
    => Monoid (Permutation (Nx n) i)
  where
    mempty =
      UsP $ 0 :++ m (+ 1) (uPr i)

instance
  (
  )
    => Reversable (Permutation n)
  where
    _rv (UsP xs) =
      UsP (_rv xs)

instance
  ( Integral i
  , Monoid (Permutation n i)
  , Ord i
  )
    => Group (Permutation n i)
  where
    _iv (UsP vec) =
      UsP $ xsr vec
