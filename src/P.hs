{-# Language IncoherentInstances #-}
{-# Language PatternSynonyms #-}
{-# Language ExplicitNamespaces #-}
{-|
Module :
  P
Description :
  A substitution for Prelude aimed at making programs as small as possible for code golf.
Copyright :
  (c) E. Olive, 2021
License :
  GPL-3
Maintainer :
  ejolive97@gmail.com
Stability :
  Experimental

A substitution for Haskell's Prelude aimed at making programs as small as possible for code golf.

Please do not use this in real code.
-}
module P
  ( pattern (:=)
  , sh
  -- * Semigroupoids
  , Semigroupoid (..)
  , og
  , fog
  -- * Categories
  , Category (..)
  , ($)
  , fid
  , (&)
  , Endo (..)
  , Iso (..)
  , type (<->)
  , swc
  , (<#<)
  , fwc
  , swC
  , sWc
  , (<%<)
  , fWc
  , sWC
  -- * Functions
  , yy
  , utl
  , fut
  , whl
  , fwh
  , fyy
  , yyc
  , fyc
  , yyC
  , fyC
  , myy
  , m2y
  -- ** On
  , on
  , fon
  , (.*)
  , pOn
  , fpO
  , qOn
  , fqO
  , kOn
  , fkO
  , onp
  , xOn
  , fxO
  , yOn
  , fyO
  -- ** Flips
  , pattern F
  , f'
  , ff
  , (/$)
  , pattern F2
  , fF2
  -- * Booleans
  , Bool
  , pattern T
  , pattern B
  , n
  , (||)
  , nO
  , (~|)
  -- ** Conditional branching
  , iF
  , (?.)
  , fiF
  , liF
  , fli
  , lii
  , fIi
  , fiI
  , ffI
  , li2
  , fI2
  , li3
  , fI3
  -- * Swap
  , Swap (..)
  , pattern Sw
  -- * Assoc
  , Assoc (..)
  , pattern Asx
  , pattern Usx
  -- * Either
  , Either (..)
  -- ** Extract
  , fI
  , fR
  , ffR
  , fL
  , ffL
  , ei
  -- * These
  , These (..)
  -- * Maybe
  , Maybe
  -- * Tuples
  -- | Functions having to do with tuples.
  -- For an equivalent to 'Prelude.snd' use 'cr'.
  -- Among other things 'cr' returns the final element of tuples of varying sizes.
  , st
  , pattern Bp
  , pattern (:@)
  , pattern Pb
  , pattern Jbp
  -- * Currying
  , pattern Cu
  , fCu
  , pattern Cuf
  , fCf
  , pattern U
  , fUc
  , pattern Ucf
  , fUf
  , pattern Cu3
  , fC3
  , pattern U3
  , fU3
  , pattern Cu4
  , fC4
  , pattern U4
  , fU4
  -- * Lists
  , List
  , dr
  , (#>)
  , fd
  , sA
  , (#=)
  , fsA
  , dW
  , fdW
  , sp
  , bk
  , sy
  , sl
  , sY
  , (|/)
  , sL
  , (|\)
  , sYm
  , sYM
  , sLm
  , sLM
  , sps
  , spn
  , spe
  , spN
  , spM
  , snM
  , seM
  , sNM
  , pm
  , pmN
  , ƪ
  , ƪN
  , pa
  , (%%)
  , fpa
  , paf
  , paF
  , pA
  , pac
  , pax
  , fpx
  , δ
  , δ'
  , paq
  , pnq
  , xQ
  , xX
  , r1
  , fr1
  , ak
  , fak
  , pattern (:%)
  , uak
  , ak'
  , rn
  , frn
  , tx
  , txi
  , txx
  , xts
  , rp
  , rL1
  , rL2
  , rr1
  , rr2
  , ryl
  , ryr
  , module P.List.Extra
  -- ** Interspersing
  , is
  , fis
  , pattern Is
  , is1
  , fi1
  , is2
  , fi2
  , is3
  , fi3
  , ic
  , fic
  , module P.List.Intersperse.Extra
  -- ** Padding
  , lpW
  , rpW
  , lLW
  , rLW
  , lpp
  , rpp
  -- ** Grouping
  , gr
  , gW
  , fgW
  , gB
  , fgB
  , gF
  , fgF
  , gFn
  , gfn
  , gfq
  , fgq
  , gnq
  , gNq
  -- ** Chopping
  , wx
  , fwx
  , wX
  , fwX
  , module P.List.Chop.Extra
  -- ** Internal comparisons
  , lq
  , nlq
  , lqw
  , lqW
  , lqb
  , lqB
  , cδ
  , cΔ
  , uq
  , nuq
  , uqb
  , uqB
  , uqw
  , uqW
  , sMI
  , miw
  , miW
  , mnI
  , mIw
  , mIW
  , sMD
  , mdw
  , mdW
  , mnD
  , mDw
  , mDW
  , mno
  , mNo
  -- ** Indexing
  , (!)
  , (!!)
  , im
  , (>!)
  , fim
  , module P.Foldable.Index.Extra
  -- ** Translation
  , tr
  , ftr
  , trM
  , frM
  , tI
  , ftI
  , tIM
  , fIM
  , trB
  -- ** Intersections and Differences
  -- | See also 'cX' and 'fcX' in MonadPlus.
  , nx
  , fnx
  , nxw
  , nxW
  , nxb
  , nxB
  , bI
  , fbI
  , bIw
  , bIW
  , bIb
  , bIB
  , df
  , fdf
  , dfw
  , dfW
  , dfb
  , dfB
  , bD
  , fbD
  , bDw
  , bDW
  , bDb
  , bDB
  , nxe
  , xew
  , xeb
  -- ** Prefixes
  , pxs
  , pXs
  , pxn
  , pXn
  , pxS
  , pXS
  -- *** Prefix maps
  , ƥ
  , fƥ
  , mP
  , fmP
  , mpn
  , mPn
  -- *** Miscellaneous
  , lp
  , lpw
  , lpb
  , lPw
  , lPb
  , sw
  , (%<)
  , fsw
  , nsw
  , swW
  , fSW
  , swB
  , fSB
  , tk
  , (#<)
  , ft
  , tW
  , ftW
  -- ** Unfolding
  , ufo
  , fuf
  , uf1
  , ff1
  , ufl
  , ffl
  , ul1
  , fl1
  , ufu
  , fFu
  , ufU
  , fFU
  , uuf
  , uUf
  , uue
  , fue
  , uUe
  , fUe
  , uef
  , uEf
  , uuz
  , fuz
  , uUz
  , fUz
  , uzf
  , uZf
  -- *** Iterate
  , ix
  , fX
  , ixu
  , ixe
  , fxe
  , ixz
  , fxz
  -- ** Suffixes
  , ms
  , fms
  , mS
  , fmS
  , sxn
  , sXn
  , msn
  , mSn
  , sxs
  , sXs
  , mss
  , mSs
  , sxS
  , sXS
  , msS
  , mSS
  , ew
  , (>%)
  , few
  , new
  , ewW
  , fEW
  , ewB
  , fEB
  , lx
  , lSw
  , lSb
  , yv
  , fY
  , xh
  , fxh
  -- ** Infixes
  , cg
  , cG
  , iw
  , (%=)
  , fiw
  , iwW
  , fIW
  , iWB
  , fIB
  , ih
  , fih
  -- ** Find
  , FindAutomaton
  , mAt
  , mAW
  , afw
  , alf
  , arw
  , avw
  , aRw
  , asw
  , azw
  , fa
  , lfa
  , ra
  , rA
  , va
  , sa
  , za
  , faw
  , raw
  , rAw
  , vaw
  , saw
  , zaw
  -- ** Infinite Lists
  , IList
  , pattern (:..)
  -- * Maps
  , StrictMap
  -- ** Class
  , Indexable (..)
  , qx
  , fqx
  , qxd
  , fqd
  , nmr
  , fns
  , nR
  , fnR
  , iM
  , fiM
  , (!<)
  , fjt
  , ajs
  , aJs
  , ReverseIndexable (..)
  , Map (..)
  , nZ
  , fnZ
  , nzs
  , fzs
  , dks
  , fdk
  , module P.Map.Class.Extra
  -- * Reverse
  , Reversable (..)
  , rv
  , pattern Rv
  , pattern Rrv
  , ReversableFunctor (..)
  -- * First
  , Firstable (..)
  , pattern K
  , pattern Fk
  , (*:*)
  , module P.First.Extra
  -- * Last
  , pattern (:>)
  , gj
  , nt
  -- * Zips
  , Zip (..)
  , (=#=)
  , zwp
  , uz
  , z3
  , fz3
  , ź
  , fZ3
  , fzW
  , fzp
  , jzW
  , (=#?)
  -- ** WeakAlign
  , WeakAlign (..)
  , gSo
  , gMy
  , gPy
  , gSO
  , gMY
  , gPY
  , lGo
  , lGy
  -- ** SemiAlign
  , SemiAlign (..)
  , zD
  , zd
  , zd'
  , fzd
  , zdm
  , fZm
  , fZd
  -- ** UnitalZip
  , UnitalZip (..)
  , cnF
  , cFM
  -- ** Align
  , rZd
  , lZd
  , rzd
  , lzd
  , rzm
  , lzm
  -- * Foldable
  , lF
  , flF
  , lF'
  , fLF
  , fo
  , mF
  , fmF
  , (^<)
  , mf
  , fmf
  , rF
  , frF
  , rHM
  , frm
  , lHM
  , flm
  , lhM
  , fLm
  , rH
  , frH
  , lH
  , flH
  , e
  , (?>)
  , fe
  , ne
  , (?<)
  , fE
  , cn
  , fcn
  , ce
  , fce
  , cne
  , fcN
  , cnT
  , cnn
  , tL
  , uqs
  , uQW
  , uQB
  , dpc
  , dPW
  , dpB
  , sm
  , pd
  , module P.Foldable.Extra
  -- ** Nubs
  , nb
  , nW
  , fnW
  , nB
  , fnB
  , lnb
  , lnB
  , lnW
  , xuq
  , xqW
  , xqB
  , nbT
  , nTf
  , nBT
  , nbr
  , nRw
  , nRb
  , unb
  , uNw
  , uNb
  -- ** Length
  , l
  , lnt
  , lg
  , ø
  , nø
  , eL
  , lL
  , leL
  , mL
  , meL
  , lEq
  , (|=|)
  , nlE
  , (|~|)
  , lLX
  , lGX
  , (|<|)
  , lLE
  , lGE
  , (|>|)
  , lCp
  , (|?|)
  , pattern Ø
  , pattern NO
  , module P.Foldable.Length.Extra
  -- ** Bags
  , bg
  , bgW
  , bgB
  , rbg
  , rbW
  , rbB
  , bG
  , bGW
  , bGB
  -- ** Scans
  , lS
  , flS
  , rS
  , frS
  , ls
  , fls
  , rs
  , frs
  , lz
  , flz
  , rz
  , frz
  -- ** Minimums and Maximums
  , mx
  , mn
  , x_W
  , xW
  , fxW
  , mW
  , fmW
  , xB
  , fxB
  , mB
  , fmB
  , xM
  , fxM
  , nM
  , fnM
  , xb
  , fxb
  , xbW
  , xbB
  , xbM
  , mb
  , fmb
  , mbW
  , mbB
  , mbM
  , xd
  , fxd
  , xdW
  , xdB
  , xdM
  , md
  , fmd
  , ndW
  , mdB
  , mdM
  , xBl
  , xBL
  , mBl
  , xMl
  , mMl
  -- *** Extract extrema
  , xxo
  , xoW
  , xoB
  , xol
  , mmo
  , moW
  , moB
  , mol
  -- ** Finds
  , g_N
  , gN
  , fgN
  , gNs
  , fGN
  , g1
  , fg1
  , g1s
  , fG1
  , module P.Foldable.Find.Extra
  -- * Scan
  , Scan (..)
  , fmA
  , fsc
  , ixm
  , ixM
  , ixo
  , fio
  , (!//)
  , scP
  , dph
  , dpH
  , eu
  , cz
  , zcz
  , czm
  , scp
  , scs
  , fSs
  , module P.Scan.Extra
  -- ** Paths
  , pxx
  , mpX
  , pxX
  , mPX
  -- * Traversable
  , Traversable
  , sQ
  , tv
  , ftv
  , tv2
  , tvA
  , fvA
  , vA2
  -- * Eq
  , Eq
  , (==)
  , eq
  , (/=)
  , nq
  , qb
  , nqb
  , Eq1 (..)
  , fq1
  , Eq2 (..)
  , fq2
  , q1'
  , q1x
  , rw
  , frw
  , module P.Eq.Extra
  -- * Ord
  , Ord
  , (>)
  , gt
  , (>=)
  , ge
  , (<.)
  , lt
  , (<=)
  , le
  , Ordering (..)
  , cp
  , fcp
  , tcp
  , cb
  , gb
  , geb
  , ltb
  , lb
  , pattern RO
  , pattern RC
  , Ord1 (..)
  , Ord2 (..)
  , c1'
  , module P.Ord.Extra
  -- ** Bounded
  , MinBounded (..)
  , MaxBounded (..)
  , pattern NB
  , pattern XB
  , mJ
  , or
  , ay
  , fay
  , all
  , fll
  , nr
  , no
  , fno
  -- ** Mins
  , mN
  , (!^)
  , mNW
  , mNw
  , mNB
  , mNb
  , mNM
  , mNL
  , mNl
  -- ** Maxes
  , ma
  , (^^)
  , maW
  , maw
  , maB
  , mab
  , maM
  , maL
  , mal
  -- ** Minmax
  , xN
  , xNW
  , xNB
  , xNl
  , nX
  , nXW
  , nXB
  , nXl
  -- ** Sorting
  , Sortable (..)
  , rsr
  , fsW
  , rsW
  , fRW
  , sB
  , fsB
  , rsB
  , fRB
  , sj
  , sJ
  , xsr
  , xsW
  , xsB
  -- ** Compare 3
  , Ordering3 (..)
  , cp3
  , eI
  -- * Ix
  , Ix
  , Range
  , uRg
  , rg
  , frg
  , uDx
  , fux
  , dx
  , fdx
  , uIr
  , fur
  , ir
  , fir
  , uGz
  , gz
  , fgz
  -- * Permutations
  , Permutation (..)
  -- * Linear algebra
  -- ** Vectors
  , Zr
  , Nx
  , Vec (..)
  , pattern (:+)
  , Vec0
  , pattern V0
  , Vec1
  , pattern V1
  , Vec2
  , pattern V2
  , Vec3
  , pattern V3
  , Vec4
  , pattern V4
  , Vec5
  , pattern V5
  , Vec6
  , pattern V6
  , Vec7
  , pattern V7
  , pattern VUC
  , pattern VUS
  -- ** Tensors
  , Tensor (..)
  , ta
  , pattern Mk
  , ti
  -- ** Matrices
  , Matrix
  , pattern CEv
  , pattern (:+-)
  , pattern PEv
  , pattern (:+|)
  , (+:|)
  , pattern TP
  , ColFunctor (..)
  -- * Arithmetic
  , Int
  , Prelude.Integer
  , Num
  , Prelude.Integral
  , (+)
  , pl
  , (-)
  , sb
  , fsb
  , pattern Ng
  , (*)
  , ml
  , db
  , tp
  , (^)
  , pw
  , fpw
  , sq
  , (%)
  , mu
  , fmu
  , (//)
  , dv
  , fdv
  , vD
  , fvD
  , qR
  , fqR
  , av
  , aD
  , (-|)
  , tNu
  , xrt
  , fxr
  , xlg
  , fxl
  , pwt
  , fpt
  , pwo
  , fpo
  , module P.Arithmetic.Extra
  -- ** Parity
  , ev
  , od
  -- ** Patterns
  , pattern X
  , pattern N
  , pattern NP
  , pattern NN
  , pattern NZ
  , pattern EV
  , pattern OD
  -- ** Natural numbers
  , Nat (..)
  -- ** Divisibility and Primes
  , by
  , fby
  , (|?)
  , nby
  , (/|?)
  , fnb
  , a'
  , i'
  , pattern I'
  -- ** Base Conversion
  , hx
  , bs
  , fbs
  , bS
  , fbS
  , bi
  , ubs
  , fub
  , ubS
  , fuB
  , module P.Arithmetic.Base.Extra
  -- ** Integer partitions
  -- *** Ordered integer partitions
  , ipt
  , ipS
  , ipW
  , ipe
  -- *** Descending integer partitions
  , dip
  , dpS
  , dpW
  -- * Constants
  , nn
  , nN
  , β
  , pattern Β
  , module P.Constants.Language
  -- * Enum
  , Enum
  , pattern Sc
  , pattern Pv
  , eF
  , ef
  , fef
  , (#>#)
  , tef
  , lef
  , xef
  , ref
  , fRf
  , (#<#)
  , eR
  , (##)
  , eR'
  , eRx
  , xR'
  , fln
  , flN
  , module P.Enum.Extra
  -- * Algebra
  -- ** Semigroup
  , Semigroup
  , (<>)
  , fmp
  , mp
  , cy
  , Reversed (..)
  -- ** Monoid
  , Monoid
  , i
  , rt
  , frt
  , rt2
  , rt3
  , rt4
  , rt5
  , rt6
  , rt7
  , rt8
  , rt9
  , rt0
  , rt1
  , Commutative (..)
  -- *** Monoid actions
  , Action (..)
  , ($$)
  , faq
  , aqE
  , Regular (..)
  , Conjugate (..)
  -- *** Free
  , FreeMonoid (..)
  , Partition (..)
  -- *** Partitions
  , pt
  , pST
  , pWK
  , iRf
  , fiR
  , rfn
  -- *** Splits
  , sPs
  , sPn
  , sPe
  , sPN
  , mps
  , mqn
  , mpe
  , mpN
  , cxS
  , fxS
  -- *** Sublists
  , iS
  , fiS
  , nS
  , fnS
  , iWq
  , nWq
  , iBq
  , nBq
  , iSW
  , nSW
  , iSB
  , nSB
  , lsS
  , lWq
  , lBq
  , lSW
  , lSB
  , ss
  , sS
  , sI
  , sD
  , ssn
  , fsn
  , scn
  , fSn
  , sSt
  , lss
  , sss
  , sSm
  , sSn
  , module P.Algebra.Monoid.Free.Substring.Extra
  -- *** Min and Max
  , Min (..)
  , Max (..)
  -- *** First and Last
  , First (..)
  , Last (..)
  -- *** Ap
  , Ap (..)
  -- *** Aligned
  , Aligned (..)
  -- ** Group
  , Group (..)
  , pattern IV
  -- ** Ring
  , Ring (..)
  , rim
  , Polynomial (..)
  -- * Char
  , Char
  , iC
  , niC
  , pattern IC
  , iW
  , niW
  , pattern IW
  , iL
  , niL
  , pattern IL
  , iU
  , niU
  , pattern IU
  , α
  , nα
  , pattern Α
  , iA
  , niA
  , pattern IA
  , iP
  , niP
  , pattern IP
  , iD
  , niD
  , pattern IS
  , iSy
  , nSy
  , pattern ID
  , pattern TU
  , mtU
  , pattern TL
  , mtL
  , pattern Or
  , pattern Ch
  , pattern Nl
  , pattern NL
  , pattern SC
  , pattern Sm
  , scI
  -- ** Character Categories
  , LetterCategory (..)
  , MarkCategory (..)
  , NumberCategory (..)
  , PunctuationCategory (..)
  , SymbolCategory (..)
  , SeparatorCategory (..)
  , OtherCategory (..)
  , GeneralCategory (..)
  , pattern ULu
  , pattern ULl
  , pattern ULt
  , pattern ULm
  , pattern ULo
  , pattern UMn
  , pattern UMc
  , pattern UMe
  , pattern UNd
  , pattern UNl
  , pattern UNo
  , pattern UPc
  , pattern UPd
  , pattern UPs
  , pattern UPe
  , pattern UPi
  , pattern UPf
  , pattern UPo
  , pattern USm
  , pattern USc
  , pattern USk
  , pattern USo
  , pattern UZs
  , pattern UZl
  , pattern UZp
  , pattern UCc
  , pattern UCs
  , pattern UCo
  , pattern UCn
  , gCt
  , iCt
  , iCT
  , nCt
  , nCT
  -- * Strings
  , String
  , pattern Ln
  , pattern Ul
  , wR
  , pattern Wr
  , pattern Uw
  , pattern Pn
  -- ** Padding
  , rP
  , lP
  , rPL
  , lPL
  , rPp
  , lPp
  -- * Functor
  , Functor
  , m
  , fm
  , (<)
  , pM
  , (<$)
  , fpM
  , ($>)
  , jpM
  , mw
  , fmw
  , mwn
  , fwn
  , mwc
  , fWC
  , mm
  , (<<)
  , fM
  , mX
  , (^.)
  , fmX
  , m'
  , fm'
  , mof
  , fMF
  , (><)
  , m3
  , (<<<)
  , fm3
  , zz
  , (<.<)
  , fzz
  , z2
  , (<.^)
  , fz2
  , xc
  , fxc
  , pxc
  , qxc
  , ly
  , fly
  , (<|<)
  , mi
  , mI
  , mcm
  , fMM
  , mc2
  , fM2
  , dcp
  , fdC
  -- ** Curry maps
  , cqp
  , (<%)
  , fqp
  , ccq
  , (<<%)
  , fcq
  , cq3
  , (<%%)
  , fq3
  -- ** Fix
  , Fix (..)
  , hFx
  , fFx
  , uFx
  -- ** Functor composition
  , Comp (Co)
  , pattern UC
  , pattern LC
  , mR
  , pattern LAC
  , pattern RAC
  -- ** Bifunctor
  , bm
  , fbm
  , (***)
  , jB
  , fjB
  , (&@)
  , mjB
  , (&@<)
  , mJB
  , (&<<)
  , jBm
  , (<&@)
  , pjB
  , fpJ
  , qjB
  , fqJ
  , kjB
  , fkJ
  , mst
  , fMt
  , m2t
  , f2t
  , bAp
  , fBp
  -- *** Blackbird
  , Blackbird (..)
  -- *** Flip
  , Flip (..)
  , pattern UFl
  , pattern LF2
  -- ** Identity
  , Ident (..)
  , Identity
  , pattern UI
  , pattern Li2
  , pattern UL2
  , pattern La2
  , pattern L2'
  , pattern Ul2
  -- * Applicative
  , Applicative
  , p
  , fp
  , pp
  , fpp
  , p3
  , fp3
  , p4
  , fp4
  , μ
  , fμ
  , ap
  , (*^)
  , fA
  , aT
  , (*>)
  , faT
  , aK
  , (<*)
  , faK
  , l2
  , fl2
  , (**)
  , l3
  , fl3
  , (**#)
  , l4
  , fl4
  , (**$)
  , jl2
  , xly
  , fXy
  , xlm
  , fXm
  , l2p
  , (*^*)
  , l2x
  , (*|*)
  , l2n
  , (*/*)
  , l2m
  , (**<)
  , l2M
  , (*×)
  , l2q
  , flq
  , (×*)
  , u2q
  , j2q
  , l3q
  , u3q
  , j3q
  , l4q
  , u4q
  , j4q
  , eqo
  , (*=*)
  , knt
  , fKt
  , (+^$)
  -- * Unpure
  , Unpure (..)
  , pattern Pu
  -- * Ring Applicative
  , RingApplicative (..)
  -- * Alternative
  , module P.Alternative.Extra
  , Alternative
  , em
  , (++)
  , ao
  , fao
  , gu
  , cx
  , cM
  , (>/)
  , fcM
  , cs
  , (<|)
  , fcs
  , ec
  , (|>)
  , fec
  , rl
  , (#*)
  , frl
  , rm
  , rM
  , (*%)
  -- ** Multiple application
  -- To apply an alternative an exact number of times see 'xly'.
  -- *** Some
  , so
  , so'
  , sO
  , sO'
  , (+<>)
  , msO
  , (<>+)
  , sOm
  , (+=>)
  , mSO
  , (<=+)
  , sOM
  , (+<*)
  , soa
  , (<*+)
  , aso
  , (+*>)
  , soA
  , (*>+)
  , aSo
  , lSo
  , lSO
  , sot
  , sOt
  , sow
  , soW
  -- *** Many
  , my
  , my'
  , mY
  , mY'
  , lMy
  , lMY
  , myt
  , mYt
  , myw
  , myW
  -- *** Possible
  , py
  , py'
  , pY
  , pY'
  , (?<>)
  , mpY
  , (<>?)
  , pYm
  , (?=>)
  , mPY
  , (<=?)
  , pYM
  , (<*?)
  , apy
  , (?*>)
  , pya
  , lPy
  , lPY
  -- *** At least
  , al
  , fal
  , al'
  , fAl
  , aL
  , faL
  , aL'
  , fAL
  , lAl
  , lAL
  -- *** At most
  , am
  , fam
  , am'
  , fAm
  , aM
  , faM
  , aM'
  , fAM
  , lAm
  , lAM
  -- *** Between
  , bw
  , bw'
  , bW
  , bW'
  , lBw
  , lBW
  -- *** Interspersed
  , isa
  , (>|-)
  , isA
  , (>|+)
  -- * Monad
  , Prelude.Monad
  , bn
  , (>~)
  , fb
  , (~<)
  , mbn
  , (~~<)
  , (~<<)
  , bn_
  , (>~.)
  , fb_
  , (+>)
  , (<+)
  , jn
  , fjn
  , jj
  , fjj
  , mjn
  , mJn
  , mmj
  , mmJ
  , m3j
  , m3J
  , mM
  , fmM
  , m_
  , fm_
  , vd
  , vr
  , vR
  , vl
  , vL
  -- ** Free
  , Free (..)
  , lFe
  , dFP
  , dPF
  , (*~|)
  , dPr
  , dFe
  , rtc
  , itr
  , foF
  , hoF
  , shp
  , fSp
  , shq
  , fSq
  , maF
  , feu
  , fdm
  , ffm
  , dsy
  , lyr
  -- ** MonadPlus
  -- | No MonadPlus type class has been implemented currently, since it is mostly redundant.
  -- Here we have functions that require both 'Control.Applicative.Alternative' and 'Monad'.
  -- Currently just variants of 'Control.Monad.mfilter'.
  , wh
  , fl
  , (@~)
  , wn
  , fn
  , (@!)
  , er
  , fer
  , (=-)
  , cX
  , fcX
  , qX
  , fqX
  , fLs
  , fFL
  -- * Profunctor
  , Profunctor (..)
  , arr
  , jbp
  , rmp
  -- * Arrow
  , LooseArrow (..)
  , syr
  , (==:)
  , sy3
  , Arrow (..)
  , ot
  , fot
  , oti
  , iot
  , ot2
  -- ** Kleisli
  , Kleisli (..)
  -- ** Cokleisli
  , Cokleisli (..)
  -- * Comonad
  , Comonad (..)
  -- ** Cofree
  , Cofree (..)
  , hCF
  , hCf
  -- * IO
  , IO
  , io
  , iop
  -- ** Input
  , gC
  , gO
  , gl
  , gL
  , ga
  -- ** Output
  , ps
  , pS
  , pC
  , pO
  , pr
  , pW
  , fpW
  -- * Parsing
  , Parser (..)
  -- ** Perform a parse
  , pP
  , (-@)
  , cP
  , (-#)
  , x1
  , (-$)
  , gP
  , (-%)
  , gc
  , (-^)
  , gk
  , (-!)
  , glk
  , (-!$)
  , gpW
  , gpB
  , gpg
  , (->#)
  , gpl
  , (-<#)
  , ggL
  , (->|)
  , glL
  , (-<|)
  , gcy
  , (-^*)
  , gcY
  , gky
  , (-!*)
  , gkY
  , cPy
  , x1y
  , sre
  , sk
  , (-!%)
  -- ** Parser combinators
  , lA
  , nA
  , kB
  , bkB
  , nK
  , χ
  , x_
  , x'
  , bx
  , nχ
  , xx
  , zWx
  , xxh
  , bXx
  , ʃ
  , s_
  , s'
  , bH
  , hh
  , zWh
  , bhh
  , νa
  , gy
  , bgy
  , lGk
  , gy'
  , bgY
  , bkw
  , xay
  , xys
  , nxy
  , asy
  , ivP
  , gre
  , laz
  , bʃ
  , bSh
  , isP
  , fiP
  , ($|$)
  , pew
  , peW
  , pEw
  , enk
  , fNk
  , yS
  , yχ
  -- *** Composition
  , lp2
  , (#*>)
  , (#<*)
  , yju
  , fyj
  , (@<*)
  , yku
  , fyk
  , (@*>)
  , ymu
  , fym
  , (@^*)
  -- ** Regex
  , rX
  , rXw
  , rxw
  , rXW
  , rxW
  , rXx
  , uPx
  , uPX
  , pPx
  , pPX
  , x1x
  , x1X
  , gPx
  , gPX
  , gcx
  , gcX
  , gkx
  , gkX
  , srX
  , skX
  -- ** Pre-made Parsers
  , hd
  , h_
  , h'
  , hdS
  , bhd
  , mhd
  , dg
  , νd
  , af
  , p_
  , pc
  , pL
  , pU
  , aN
  , pR
  , wd
  , wr
  , en
  , ens
  , p16
  , lWs
  , tWs
  , ν
  -- | Exports a ton of small parsers for specific tasks.
  , module P.Parse.Extra
  -- * Deprecated
  -- | Exports long form versions of functions.
  -- Using these will produce a warning with the name of the short function to use.
  , module P.Deprecated
  )
  where


import qualified Prelude
import Prelude
  ( Num
  , Integral
  , Enum
  , Int
  )


import qualified Data.List


import P.Algebra.Group
import P.Algebra.Monoid
import P.Algebra.Monoid.Action
import P.Algebra.Monoid.Action.Conjugate
import P.Algebra.Monoid.Action.Regular
import P.Algebra.Monoid.Aligned
import P.Algebra.Monoid.Ap
import P.Algebra.Monoid.FirstLast
import P.Algebra.Monoid.Free
import P.Algebra.Monoid.Free.Prefix
import P.Algebra.Monoid.Free.Split
import P.Algebra.Monoid.Free.Substring
import P.Algebra.Monoid.Free.Substring.Extra
import P.Algebra.Monoid.Free.Suffix
import P.Algebra.Monoid.MinMax
import P.Algebra.Polynomial
import P.Algebra.Ring
import P.Algebra.Semigroup
import P.Algebra.Semigroup.Commutative
import P.Algebra.Semigroup.Extra
import P.Algebra.Semigroup.Reversed
import P.Aliases
import P.Alternative
import P.Alternative.AtLeast
import P.Alternative.AtMost
import P.Alternative.Between
import P.Alternative.Intersperse
import P.Alternative.Many
import P.Alternative.Possible
import P.Alternative.Some
import P.Alternative.Extra
import P.Applicative
import P.Applicative.Ring
import P.Applicative.Unpure
import P.Arithmetic
import P.Arithmetic.Base
import P.Arithmetic.Base.Extra
import P.Arithmetic.Combinatorics.Partition
import P.Arithmetic.Extra
import P.Arithmetic.Nat
import P.Arithmetic.Pattern
import P.Arithmetic.Primes
import P.Arrow
import P.Arrow.Cokleisli
import P.Arrow.Kleisli
import P.Assoc
import P.Bifunctor
import P.Bifunctor.Blackbird
import P.Bifunctor.Either
import P.Bifunctor.Flip
import P.Bifunctor.Profunctor
import P.Bifunctor.These
import P.Bool
import P.Category
import P.Category.Endo
import P.Category.Iso
import P.Char
import P.Char.Category
import P.Comonad
import P.Comonad.Cofree
import P.Constants.Language
import P.Deprecated
import P.Enum
import P.Enum.Extra
import P.Eq
import P.Eq.Extra
import P.First
import P.First.Extra
import P.Fix
import P.Foldable
import P.Foldable.Bag
import P.Foldable.Extra
import P.Foldable.Find
import P.Foldable.Find.Extra
import P.Foldable.Index.Extra
import P.Foldable.Length
import P.Foldable.Length.Extra
import P.Foldable.Length.Pattern
import P.Foldable.MinMax
import P.Foldable.Unfold
import P.Foldable.Uniques
import P.Function
import P.Function.Compose
import P.Function.Curry
import P.Function.Flip
import P.Functor
import P.Functor.Compose
import P.Functor.Fix
import P.Functor.Identity
import P.Functor.Identity.Class
import P.IO
import P.Intersection
import P.Ix
import P.Last
import P.List
import P.List.Chop
import P.List.Chop.Extra
import P.List.Comparison
import P.List.Extra
import P.List.Find
import P.List.Group
import P.List.Index
import P.List.Infinite
import P.List.Infix
import P.List.Intersperse
import P.List.Intersperse.Extra
import P.List.Padding
import P.List.Pairs
-- import P.List.Partition
import P.List.Permutation
import P.List.Prefix
import P.List.Split
import P.List.Suffix
import P.List.Translate
import P.Map
import P.Map.Class
import P.Map.Class.Extra
import P.Matrix
import P.Maybe
import P.Monad
import P.Monad.Free
import P.Monad.Plus.Filter
import P.Ord
import P.Ord.Bounded
import P.Ord.Compare3
import P.Ord.Extra
import P.Ord.MinMax
import P.Parse
import P.Parse.Combinator
import P.Parse.Extra
import P.Parse.Regex
import P.Pattern
import P.Permutation
import P.Reverse
import P.Scan
import P.Scan.Extra
import P.Scan.Foldable
import P.Show
import P.Sort.Class
import P.String
import P.Swap
import P.Tensor
import P.Traversable
import P.Tuple
import P.Vector
import P.Zip
import P.Zip.Align
import P.Zip.SemiAlign
import P.Zip.UnitalZip
import P.Zip.WeakAlign
